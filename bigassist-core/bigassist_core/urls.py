#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
# from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView

from django.core.urlresolvers import reverse_lazy
from django.conf.urls import patterns, url, include
from bigassist_core import views

from django.conf import settings

urlpatterns = patterns('',

    url(r'^generate-visit-payments$', views.GenerateVisitPayment.as_view()),

    url(r'^$', views.home_view_dispatcher, name='home'),
    url(r'^$', views.home_view_dispatcher, name="registration_register"),

    url(r'^login/', views.LoginView.as_view(), name="auth_login"),
    # this view is created to support common Django registration_register view, eg. for PyBB templates.
    url(r'^logout/', views.logout_view, name="logout"),

    url(r'^news/(?P<slug>[-\w]+)$', views.NewsItemView.as_view(), name='newsitem'),
    url(r'^profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^profile/edit/$', views.ProfileEditView.as_view(), name='profile_edit'),

    url(r'^check_company/$', views.CheckCompany.as_view(), name="check_company"),
    url(r'^contact_company/(?P<org_id>[\d]+)/$', views.ContactCompanyOwnerView.as_view(), name="contact_company_owner"),
    # approve user person_id as a member of organisation_id and send email to user (person)
    url(r'^approve_user/(?P<organisation_id>[\d]+)/(?P<person_id>[\d]+)$', views.ApproveUserView.as_view(), name="approve_user"),

    url(r'^lookup/company/$',
        views.LookupCompany.as_view(), name="lookup_company_template"),
    url(r'^lookup/company/(?P<id>[\d]+)/$',
        views.LookupCompany.as_view(), name="lookup_company"),
    url(r'^lookup/charity/$',
        views.LookupCharity.as_view(), name="lookup_charity_template"),
    url(r'^lookup/charity/(?P<id>[\d]+)/$',
        views.LookupCharity.as_view(), name="lookup_charity"),

    url(r'^organisation/users/$',
        views.OrganisationUsersWrapperView.as_view(),
        name='org_users'),

    url(r'^join/(?P<pk>[\d]+)/$',
        views.JoinView.as_view(), name="join_bigassist"),

    url(r'^organisation/(?P<pk>[\d]+)/request_membership$',
        views.OrganisationRequestMembershipView.as_view(),
        name="request_membership"),

    url(r'^choose_company/$',
        views.ChooseCompany.as_view(), name="choose_company"),

    url(r'^roles/$',
        views.ChooseRoleView.as_view(), name="choose_role"),

    url(r'^roles/add_customer$',
        views.OrganisationAddRoleView.as_view(org_role='customer'),
        name='add_customer_role'
        ),

    url(r'^roles/add_supplier$',
        views.OrganisationAddRoleView.as_view(org_role='supplier'),
        name='add_supplier_role'
        ),

    url(r'^signin/$',
        views.SigninView.as_view(
            next_url=reverse_lazy('generic_register')
            ),
        name='generic_signin'),

    url(r'^register/$',
        views.RegistrationView.as_view(
            user_type=None,
            signin_url=reverse_lazy('generic_signin')
            ),
        name='generic_register'),

    url(r'^register/new/$',
        views.RegistrationView.as_view(
            user_type=None,
            new_org=True,
            signin_url=reverse_lazy('generic_signin')
            ),
        name='generic_register_new'),

    url(r'^register/(?P<organisation_id>[\d]+)/$',
        views.KnownUserRegistrationView.as_view(user_type=None,
                                       signin_url=reverse_lazy('generic_signin')),
        name='known_generic_register'),

    url(r'^dashboard/$',
        views.GenericDashboardView.as_view(),
        name='generic_dashboard'),

    url(r'^organisation_profile/$',
        views.OrganisationEditView.as_view(
            user_type=None,
            register_url=reverse_lazy('generic_register')),
        name='generic_org_profile'),

    url(r'^register/match_email$', 
        views.RegistrationMatchEmailDomainView.as_view(),
        name='generic_match_email'),
)


# CUSTOMER
urlpatterns += patterns('',
    url(r'^customer/prequals/$', views.CustomerPrequalsView.as_view(),
        name="customer_prequals"),

    url(r'^customer/signin/$',
        views.SigninView.as_view(next_url=reverse_lazy('customer_register')),
        name='customer_signin'),

    url(r'^customer/register/$',
        views.RegistrationView.as_view(user_type='customer',
                                       signin_url=reverse_lazy('customer_signin')),
        name='customer_register'),

    url(r'^customer/register/(?P<organisation_id>[\d]+)/$',
        views.KnownUserRegistrationView.as_view(user_type='customer',
                                       signin_url=reverse_lazy('customer_signin')),
        name='known_customer_register'),

    url(r'^customer/register/match_orgs$', 
        views.RegistrationOrganisationSearchView.as_view(user_type='customer'),
        name='customer_match_orgs'),

    url(r'^customer/organisation/(?P<pk>[\d]+)/become_manager$',
        views.OrganisationBecomeManagerView.as_view(user_type='customer'),
        name="customer_become_manager"),

    url(r'^customer/organisation_profile/$',
        views.OrganisationEditView.as_view(user_type='customer',
                                       register_url=reverse_lazy('customer_register')),
        name='customer_org_profile'),

    url(r'^customer/qualify-failed$',
        # direct_to_template, {'template': 'customer/customer_qualify_failed.html'},
        TemplateView.as_view(template_name='customer/customer_qualify_failed.html'),
        name="customer_qualify_failed"),

    url(r'^customer/qualify-held$',
        # direct_to_template, {'template': 'customer/customer_qualify_held.html'},
        TemplateView.as_view(template_name='customer/customer_qualify_held.html'),
        name="customer_qualify_held"),

    url(r'^customer/qualification/$',
        views.CustomerQualificationView.as_view(), name="customer_qualification"),

    url(r'^customer/qualification/model.xml$',
        never_cache(views.CustomerQualificationModelView.as_view()), name="customer_qualification_xml_model"),

    url(r'^customer/qualification/save$',
        csrf_exempt(views.CustomerQualificationSaveView.as_view()), name="customer_qualification_save"),

# Customer self assessment
    url(r'^customer/self-assessment/contact_edit$',
        views.CustomerDiagnosticContactView.as_view(),
        name="customer_diagnostic_contact_edit"),

    url(r'^customer/self-assessment/$',
        views.CustomerDiagnosticView.as_view(), 
        name="customer_diagnostic"),

    url(r'^customer/self-assessment/model.xml$',
        never_cache(views.CustomerDiagnosticModelView.as_view()), 
        name="customer_diagnostic_xml_model"),

    url(r'^customer/self-assessment/save$',
        csrf_exempt(views.CustomerDiagnosticSaveView.as_view()), 
        name="customer_diagnostic_save"),

    url(r'^customer/self-assessment/submit$',
        csrf_exempt(views.CustomerDiagnosticSaveView.as_view()), 
        {'submit': True}, 
        name="customer_diagnostic_submit"),

# Customer self assessment resumission
    url(r'^customer/self-assessment/prevmodel.xml$',
        never_cache(views.CustomerDiagnosticPrevModelView.as_view()), 
        name="customer_diagnostic_xml_prevmodel"),

    url(r'^customer/resubmit-self-assessment/$',
        views.CustomerDiagnosticResubmitView.as_view(),
        name='customer_diagnostic_resubmit'),


    url(r'^customer/get-in-touch/$',
        views.CustomerGetInTouchView.as_view(), name="customer_get_in_touch"),

    url(r'^customer/prequal-reasons/$',
        # direct_to_template, {'template': 'customer/prequal_reasons.html'}, 
        TemplateView.as_view(template_name='customer/prequal_reasons.html'),
        name="customer_prequal_reasons"),

    url(r'^customer/supplier/profile/(?P<id>[\d]+)/$',
        views.CustomerSupplierProfileView.as_view(), name="supplier_full_profile"),

    url(r'^customer/customer/contact/(?P<pk>[\d]+)/$',
        views.CustomerCustomerContactView.as_view(), name="customer_customer_contact"),

    url(r'^customer/supplier/contact/(?P<pk>[\d]+)/$',
        views.CustomerSupplierContactView.as_view(), name="customer_supplier_contact"),

    url(r'^customer/supplier/contact/dismiss/(?P<pk>[\d]+)/$',
        views.CustomerSupplierContactDismissView.as_view(), name="customer_supplier_contact_dismiss"),

    url(r'^customer/supplier/contact/accept/(?P<pk>[\d]+)/$',
        views.CustomerSupplierContactAcceptView.as_view(), name="customer_supplier_contact_accept"),


    url(r'^customer/recommendation$',
        views.CustomerRecommendationView.as_view(),
        name='customer_recommendation'),

    url(r'^customer/recommendation/approve$',
        views.CustomerRecommendationApprove.as_view(),
        name='customer_recommendation_approve'),

    url(r'^customer/recommendation/appeal$',
        views.CustomerRecommendationAppeal.as_view(),
        name='customer_recommendation_appeal'),

    url(r'^customer/recommendation/reject$',
        views.CustomerRecommendationReject.as_view(),
        name='customer_recommendation_reject'),

    url(r'^customer/log/(?P<pk>[\d]+)$',
        views.CustomerDashboardCommLogDetailView.as_view(),
        name='customer_commlog_detail'),

    url(r'^customer/log/$',
        views.CustomerDashboardCommLogList.as_view(),
        name='customer_commlog_list'),

    url(r'^customer/dashboard/$',
        views.CustomerDashboardDiagnosticView.as_view(),
        name='customer_dashboard_diagnostic'),

    url(r'^customer/dashboard/(?P<tab>.+)$',
        views.CustomerDashboardView.as_view(),
        name='customer_dashboard'),

    url(r'^customer/get-in-touch/(?P<supplier_pk>\d+)$',
        views.CustomerGetInTouchSupplierView.as_view(),
        name='customer_contacts_supplier'),

    url(r'^customer/project/(?P<pk>[\d]+)$',
        views.CustomerProjectDetailView.as_view(),
        name='customer_project_details'),

    url(r'^customer/project/(?P<pk>[\d]+)/feedback$',
        views.CustomerProjectFeedbackView.as_view(),
        name='customer_project_feedback'),

    url(r'^admin/customer/chase_feedback/$',
        views.CutomerFeedbackChaseEmailView.as_view(),
        name='customer_chase_feedback'),

    url(r'^admin/customer/voucher_expiry_reminder$',
        views.CustomerVoucherExpiryReminderEmailView.as_view(),
        name='customer_voucher_expiry_reminder'),

    url(r'^admin/customer/project_reminders$',
        views.CustomerProjectRemindersEmailView.as_view(),
        name='customer_project_reminders'),

    url(r'^customer/project/(?P<pk>[\d]+)/not-complete$',
        views.CustomerProjectNotCompleteView.as_view(),
        name='customer_project_not_complete'),

    url(r'^customer/project/(?P<pk>[\d]+)/problems$',
        views.CustomerProjectProblemsView.as_view(),
        name='customer_project_problems'),

)


## SUPPLIER
urlpatterns += patterns('',

    url(r'^supplier/prequals/$', views.SupplierPrequalsView.as_view(),
        name="supplier_prequals"),

    url(r'^supplier/signin/$',
        views.SigninView.as_view(next_url=reverse_lazy('supplier_register')),
        name='supplier_signin'),

    url(r'^supplier/register/$',
        views.RegistrationView.as_view(user_type='supplier',
                                       signin_url=reverse_lazy('supplier_signin')),
        name='supplier_register'),

    url(r'^supplier/register/(?P<organisation_id>[\d]+)/$',
        views.KnownUserRegistrationView.as_view(user_type='supplier',
                                       signin_url=reverse_lazy('customer_signin')),
        name='known_supplier_register'),

    url(r'^supplier/register/match_orgs$', 
        views.RegistrationOrganisationSearchView.as_view(user_type='supplier'),
        name='supplier_match_orgs'),

    url(r'^supplier/organisation/(?P<pk>[\d]+)/become_manager$',
        views.OrganisationBecomeManagerView.as_view(user_type='supplier'),
        name="supplier_become_manager"),


    url(r'^supplier/organisation_profile/$',
        views.OrganisationEditView.as_view(user_type='supplier',
                                       register_url=reverse_lazy('supplier_register')),
        name='supplier_org_profile'),

    url(r'^supplier/meeting-our-requirements/$',
        # direct_to_template, {'template': 'supplier/prequals_failed.html'},
        TemplateView.as_view(template_name='supplier/prequals_failed.html'),
        name='supplier_dont_meet'),

    url(r'^supplier/assessment/contact_edit$',
        views.SupplierAssessmentContactView.as_view(),
        name='supplier_assessment_contact_edit'),

    url(r'^supplier/assessment/$',
        views.SupplierAssessmentView.as_view(),
        name='supplier_assessment'),

    url(r'^supplier/assessment/model.xml$',
        never_cache(views.SupplierAssessmentModelView.as_view()), name="supplier_assessment_xml_model"),

    url(r'^supplier/assessment/prevmodel.xml$',
        never_cache(views.SupplierAssessmentPrevModelView.as_view()), name="supplier_assessment_xml_prevmodel"),

    url(r'^supplier/assessment/save$',
        csrf_exempt(views.SupplierAssessmentSaveView.as_view()), name="supplier_assessment_save"),

    url(r'^supplier/assessment/submit$',
        csrf_exempt(views.SupplierAssessmentSaveView.as_view()), {'submit': True}, name="supplier_assessment_submit"),

    url(r'^supplier/resubmit-assessment/$',
        views.SupplierAssessmentResubmitView.as_view(),
        name='supplier_resubmit_assessment'),

    url(r'^supplier/assessment/appeal/$',
        views.SupplierAssessmentAppealView.as_view(),
        name='supplier_appeal_assessment'),

    url(r'^supplier/dashboard/$',
        views.SupplierDashboardAssessmentView.as_view(),
        name='supplier_dashboard_assessment'),

    url(r'^supplier/dashboard/(?P<tab>.+)$',
        views.SupplierDashboardView.as_view(),
        name='supplier_dashboard'),

    url(r'^supplier/project/(?P<pk>[\d]+)/eoi$',
        views.SupplierPreProjectDetailView.as_view(),
        name='supplier_preproject_detail_view'),

    url(r'^supplier/project/(?P<pk>[\d]+)/make$',
        views.SupplierProjectMakeView.as_view(),
        name='supplier_project_make_view'),

    url(r'^supplier/project/(?P<pk>[\d]+)/edit$',
        views.SupplierProjectEditView.as_view(),
        name='supplier_project_edit_view'),

    url(r'^supplier/project/(?P<pk>[\d]+)/dismiss$',
        views.SupplierProjectDismissView.as_view(),
        name="supplier_project_dismiss_view"),

    url(r'^supplier/project/(?P<pk>[\d]+)/contacts$',
        views.SupplierProjectContactsView.as_view(),
        name="supplier_project_contacts_view"),

    url(r'^supplier/project/(?P<pk>[\d]+)/detail$',
        views.SupplierProjectDetailView.as_view(),
        name='supplier_project_detail_view'),

    url(r'^supplier/project/(?P<pk>[\d]+)/complete$',
        views.SupplierProjectCompleteView.as_view(),
        name='supplier_project_complete_view'),

    url(r'^supplier/project/(?P<pk>[\d]+)/capability-report$',
        views.SupplierCustomerCapabilityReportView.as_view(),
        name='supplier_customer_capability_report'),


)


## XFORMS
urlpatterns += patterns('',
    # This view requires a parameter formtype which must contain a value from models.XFORM_TYPES
    url(r'^xforms/model/(?P<formtype>.+)/(?P<id>.+)/$',
        views.XFormsModelView.as_view(),
        name='xforms_model'),

    url(r'^xforms/upload/(?P<formtype>.)/(?P<fileilk>.+)/(?P<diag_id>[\d]+)/$',
        views.XFormsFileUpload.as_view(),
        name='xforms_fileupload'),

    url(r'^xforms/download/(?P<formtype>.)/(?P<fileilk>.+)/(?P<diag_id>[\d]+)/$',
        views.XFormsFileDownload.as_view(),
        name='xforms_filedownload'),

    url(r'^xforms/download/(?P<formtype>.)/(?P<fileilk>.+)/(?P<diag_id>[\d]+)/(?P<filename>.+)$',
        views.XFormsFileDownload.as_view(download=True),
        name='xforms_filedownload_process'),

)

## ADMIN
urlpatterns += patterns('',

    # This method is no longer safe. Especially, it can remove unwanted relationships.
    # We can't call .delete() to Relationship because there is wrong primary key
    # See AdminOrganisationUsersView.post method (DELETE FROM)
    # url(r'^admin/remove_user/$',
    #     views.AdminRemoveUserView.as_view(),
    #     name='admin_remove_user'),

    url(r'^admin/dashboard/$',
        views.AdminDashboardDatabaseView.as_view(),
        name='admin_dashboard_database'),

    url(r'^admin/dashboard/customer$',
        views.AdminDashboardCustomerView.as_view(),
        name='admin_dashboard_customer'),

    url(r'^admin/dashboard/supplier$',
        views.AdminDashboardSupplierView.as_view(),
        name='admin_dashboard_supplier'),

    url(r'^admin/dashboard/invoices$',
        views.AdminDashboardInvoicesView.as_view(),
        name='admin_dashboard_invoices'),

    url(r'^admin/dashboard/connectspace$',
        views.AdminDashboardMeetspaceView.as_view(),
        name='admin_dashboard_meetspace'),

    url(r'^admin/dashboard/sponsorship$',
        views.AdminDashboardBursaryView.as_view(),
        name='admin_dashboard_bursary'),

    url(r'^admin/dashboard/sponsorship/history$',
        views.AdminDashboardBursaryHistoryView.as_view(),
        name='admin_dashboard_bursary_history'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation$',
        views.AdminCustomerRecommendationView.as_view(),
        name='admin_customer_recommendation'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/edit$',
        views.AdminCustomerRecommendationEdit.as_view(),
        name='admin_customer_recommendation_edit'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/revise$',
        views.AdminCustomerRecommendationRevise.as_view(),
        name='admin_customer_recommendation_revise'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/approve$',
        views.AdminCustomerRecommendationApprove.as_view(),
        name='admin_customer_recommendation_approve'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/reject$',
        views.AdminCustomerRecommendationReject.as_view(),
        name='admin_customer_recommendation_reject'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/send$',
        views.AdminCustomerRecommendationSend.as_view(force=False,
                                                      description="You are about to submit the recommendation to the customer?"),
        name='admin_customer_recommendation_send'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/send_revised$',
        views.AdminCustomerRecommendationRevisedSend.as_view(force=False,),
        name='admin_customer_recommendation_send_revised'),

    url(r'^admin/customer/(?P<pk>[\d]+)/recommendation/send_no_consent$',
        views.AdminCustomerRecommendationSend.as_view(force=True,
                                                      description="Are you sure you wish to force complete this diagnostic? This option should only be used in special circumstances."),
        name='admin_customer_recommendation_send_no_consent'),

    url(r'^admin/customer/(?P<pk>[\d]+)/make_ineligible$',
        views.AdminCustomerMakeIneligibleView.as_view(title="Customer ineligibility",
                                                      description="You are about make the customer ineligible, continue?"),
        name='admin_customer_make_ineligible'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment$',
        views.AdminCustomerDiagnosticView.as_view(),
        name='admin_customer_diagnostic'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/edit$',
        views.AdminCustomerDiagnosticView.as_view(editable=True),
        name='admin_customer_diagnostic_edit'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/model.xml$',
        never_cache(views.AdminCustomerDiagnosticModelView.as_view()),
        name='admin_customer_diagnostic_xml_model'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/(?P<diag_id>[\d]+)/model.xml$',
        never_cache(views.AdminCustomerDiagnosticModelView.as_view()),
        name='admin_customer_diagnostic_xml_model_2'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/prevmodel.xml$',
        never_cache(views.AdminCustomerDiagnosticPrevModelView.as_view()),
        name='admin_customer_diagnostic_xml_prev_model'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/comments.xml$',
        never_cache(views.AdminCustomerDiagnosticCommentsModelView.as_view()),
        name='admin_customer_diagnostic_comments_model'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/submit$',
        csrf_exempt(views.AdminCustomerDiagnosticSubmitView.as_view()),
        name='admin_customer_diagnostic_submit'),

    url(r'^admin/customer/(?P<pk>[\d]+)/self-assessment/submit-comments$',
        csrf_exempt(views.AdminCustomerDiagnosticSubmitView.as_view(comments=True)),
        name='admin_customer_diagnostic_submit_comments'),

    url(r'^admin/customer/(?P<diag_id>[\d]+)/communication$',
        views.AdminCustomerCommunicationView.as_view(),
        name='admin_customer_communication'),

    url(r'^admin/customer/(?P<pk>[\d]+)/capability-report$',
        views.AdminCustomerCapabilityReportView.as_view(),
        name='admin_customer_capability_report'),

    url(r'^admin/customer/(?P<organisation_id>[\d]+)/log$',
        views.AdminCustomerCommLogListView.as_view(),
        name='admin_customer_commlog_list'),


    url(r'^admin/supplier/(?P<pk>[\d]+)/review$',
        views.AdminSupplierReviewView.as_view(),
        name='admin_supplier_approval_review'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/$',
        views.AdminSupplierApprovalView.as_view(),
        name='admin_supplier_approval'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/edit$',
        views.AdminSupplierApprovalView.as_view(editable=True),
        name='admin_supplier_approval_edit'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/model.xml$',
        never_cache(views.AdminSupplierApprovalModelView.as_view()),
        name='admin_supplier_approval_xml_model'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/prevmodel.xml$',
        never_cache(views.AdminSupplierApprovalPrevModelView.as_view()),
        name='admin_supplier_approval_xml_prevmodel'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/comments.xml$',
        never_cache(views.AdminSupplierApprovalCommentsModelView.as_view()),
        name='admin_supplier_approval_comments_model'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/submit$',
        csrf_exempt(views.AdminSupplierApprovalSubmitView.as_view()),
        name='admin_supplier_approval_submit'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/submit-comments$',
        csrf_exempt(views.AdminSupplierApprovalSubmitView.as_view(comments=True)),
        name='admin_supplier_approval_submit_comments'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/unlock/$',
        views.AdminSupplierApprovalUnlockView.as_view(),
        name='admin_supplier_approval_unlock'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/comments/$',
        views.AdminSupplierApprovalCommentsView.as_view(),
        name='admin_supplier_approval_comments'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/set/reviewed/$',
        views.AdminSupplierApprovalReviewView.as_view(),
        name='admin_supplier_approval_set_reviewed'),

    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment/set/(?P<state>\w+)/$',
        views.AdminSupplierApprovalView.as_view(),
        name='admin_supplier_approval_setstate'),

    # This is intended to be used as source page for generating PDF
    # version of the assessment
    url(r'^admin/supplier/(?P<pk>[\d]+)/assessment.html$',
        views.AdminSupplierAssessmentHTMLView.as_view(),
        name='admin_supplier_assessment_html'),

    url(r'^admin/download_all_self_assessments$',
        views.AdminSelfAssessmentCSVDownloadView.as_view(),
        name='admin_selfassessments_csv'),

    url(r'^admin/assessments.pdf$',
        views.AdminSupplierAssessmentsPDFView.as_view(),
        name='admin_assessments_pdf'),

    url(r'^admin/deadline/(?P<pk>[\d]+)/assessments.pdf$',
        views.AdminSupplierAssessmentsPDFDownloadView.as_view(),
        name='admin_assessments_pdf_download'),

    url(r'^admin/organisation/(?P<org_pk>[\d]+)/supplierapproval/(?P<pk>[\d]+)/edit$',
        views.AdminSupplierApprovalContactEditView.as_view(),
        name='admin_supplier_approval_contact_edit'),

    url(r'^admin/organisation/(?P<org_pk>[\d]+)/customerdiagnostic/(?P<pk>[\d]+)/edit$',
        views.AdminCustomerDiagnosticContactEditView.as_view(
            diag_type='customer'
            ),
        name='admin_customer_diagnostic_contact_edit'),

    url(r'^admin/organisation/(?P<pk>[\d]+)/edit$',
        views.AdminOrganisationEditView.as_view(),
        name='admin_organisation_edit'),

    url(r'^admin/organisation/(?P<pk>[\d]+)/$',
        views.AdminOrganisationProfileView.as_view(),
        name='admin_organisation_profile'),

    url(r'^admin/organisation/(?P<pk>[\d]+)/users$',
        views.AdminOrganisationUsersView.as_view(),
        name='admin_org_users'),

    url(r'^admin/payment/(?P<pk>[\d]+)/add_pop/$',
        views.AdminAddPOPView.as_view(),
        name='admin_add_pop_view'),

    url(r'^admin/payment/(?P<pk>[\d]+)/send_to_finance/$',
        views.AdminSendToFinanceView.as_view(),
        name='admin_send_to_finance'),

    url(r'^panel/dashboard',
        views.PanelDashboardView.as_view(),
        name='panel_dashboard'),

    url(r'^panel/approval/(?P<pk>[\d]+)/review/$',
        views.PanelApprovalReviewView.as_view(),
        name='panel_approval_review'),

    url(r'^admin/visit/approve/(?P<pk>[\d]+)$',
        views.AdminApproveRejectVisitView.as_view(approve=True,
                                                  action='Approve',
                                                  description='Do you really want to approve this visit?'),
        name='admin_visit_approve'),

    url(r'^admin/visit/reject/(?P<pk>[\d]+)$',
        views.AdminApproveRejectVisitView.as_view(approve=False,
                                                  action='Reject',
                                                  description='Do you really want to reject this visit?'),
        name='admin_visit_reject'),

    url(r'^admin/mentor/approve/(?P<pk>[\d]+)$',
        views.AdminApproveRejectMentorView.as_view(approve=True,
                                                  action='Approve',
                                                  description='Do you really want to approve this mentor?'),
        name='admin_mentor_approve'),

    url(r'^admin/mentor/reject/(?P<pk>[\d]+)$',
        views.AdminApproveRejectMentorView.as_view(approve=False,
                                                  action='Reject',
                                                  description='Do you really want to reject this mentor?'),
        name='admin_mentor_reject'),

    url(r'^admin/voucher/change/',
        csrf_exempt(views.VoucherChangeDate.as_view()),
        name='voucher_change_date'),

    url(r'^admin/sponsorship/approve/(?P<typ>[a-z]+)/(?P<pk>[\d]+)$',
        views.AdminApproveRejectSponsorshipView.as_view(approve=True,
                                                        action='Grant'),
        name='admin_sponsorship_grant'),

    url(r'^admin/sponsorship/reject/(?P<typ>[a-z]+)/(?P<pk>[\d]+)$',
        views.AdminApproveRejectSponsorshipView.as_view(approve=False,
                                                        action='Reject'),
        name='admin_sponsorship_reject'),

)

## MARKETPLACE
urlpatterns += patterns('',
    url(r'^marketplace/$', views.MarketplaceView.as_view(), name='marketplace'),
    url(r'^marketplace/library$', views.LibraryGroupView.as_view(), name='librarygroup'),
    url(r'^marketplace/library/article$', views.LibraryArticleView.as_view(), name='library_article'),
    url(r'^marketplace/experts$', views.ExpertsView.as_view(), name='expertsgroup'),
    url(r'^marketplace/connects$', views.ConnectsView.as_view(), name='connectsgroup'),
)


## VISITS
urlpatterns += patterns('',
    url(r'^customer/visit/host/check$',
        views.CustomerHostVisitPreView.as_view(),
        name='customer_host_a_visit'),

    url(r'^customer/visit/create$',
        views.CustomerHostVisitView.as_view(),
        name='customer_host_a_visit2'),

    url(r'visit/(?P<pk>[\d]+)/$',
        views.VisitDetailView.as_view(),
        name='visit_detail'),

    url(r'visit/(?P<pk>[\d]+)/remove$',
        views.CustomerVisitRemoveView.as_view(),
        name='visit_remove'),

    url(r'visit/(?P<pk>[\d]+)/edit$',
        views.CustomerVisitEditView.as_view(),
        name='visit_edit'),

    url(r'visit/(?P<visit>[\d]+)/get-in-touch$',
        views.VisitGetInTouchView.as_view(),
        name='visit_get_in_touch'),

    # pending visitor discards own visit proposal
    url(r'visitor/(?P<visitor>[\d]+)/discard$',
        views.CustomerVisitorDiscardView.as_view(),
        name='visitor_discard'),

    # host accepts a visitor
    url(r'visitor/(?P<visitor>[\d]+)/accept$',
        views.CustomerVisitorAcceptView.as_view(),
        name='visitor_accept'),

    # host rejects a visitor
    url(r'visitor/(?P<visitor>[\d]+)/reject$',
        views.CustomerVisitorRejectView.as_view(),
        name='visitor_reject'),

    # visitor requests for bursary
    url(r'visitor/(?P<visitor>[\d]+)/sponsorship/$',
        views.VisitorRequestsBursaryView.as_view(),
        name='visitor_bursary'),

    # visitor leaves feedback
    url(r'visitor/(?P<pk>[\d]+)/feedback/$',
        views.VisitorVisitFeedbackView.as_view(),
        name='visitor_feedback'),

)


## MENTORING
urlpatterns += patterns('',
    url(r'^customer/mentor/check$',
        views.CustomerBecomeMentorPreView.as_view(),
        name='customer_become_a_mentor'),

    url(r'^customer/mentor/create$',
        views.CustomerBecomeMentorView.as_view(),
        name='customer_become_a_mentor2'),

    url(r'^mentor/(?P<pk>[\d]+)/$',
        views.MentorDetailView.as_view(),
        name='mentor_detail'),

    url(r'^mentor/(?P<pk>[\d]+)/edit$',
        views.CustomerMentorEditView.as_view(),
        name='mentor_edit'),

    url(r'^mentor/(?P<pk>[\d]+)/get-in-touch$',
        views.MentorGetInTouchView.as_view(),
        name='mentor_get_in_touch'),

    url(r'^mentee/arrange/$',
        csrf_exempt(views.MentorMenteeArrangeDateView.as_view()),
        name='mentee_arrange_date'),

    # host accepts a mentee
    url(r'^mentor/(?P<pk>[\d]+)/accept$',
        views.CustomerMenteeRejectAcceptView.as_view(accept=True,
                                               action='Accept',
                                               description='Do you really want to accept this mentee?'),
        name='mentee_accept'),

    # host rejects a mentee
    url(r'^mentor/(?P<pk>[\d]+)/reject$',
        views.CustomerMenteeRejectAcceptView.as_view(accept=False,
                                               action='Reject',
                                               description='Do you really want to reject this mentee?'),
        name='mentee_reject'),

    # pending mentee discards own mentor proposal
    url(r'mentee/(?P<pk>[\d]+)/discard$',
        views.CustomerMenteeDiscardView.as_view(),
        name='mentee_discard'),

    url(r'mentee/(?P<pk>[\d]+)/complete$',  # pk = Mentee
        views.CustomerMenteeCompleteView.as_view(complete_early=False),
        name='mentee_complete'),

    url(r'mentee/(?P<pk>[\d]+)/complete_early$',  # pk = Mentee
        views.CustomerMenteeCompleteView.as_view(complete_early=True),
        name='mentee_complete_early'),

    # mentee requests for bursary
    url(r'mentee/(?P<mentee>[\d]+)/sponsorship/$',
        views.MenteeRequestsBursaryView.as_view(),
        name='mentee_bursary'),

    # mentee leaves feedback
    url(r'mentee/(?P<pk>[\d]+)/feedback/$',
        views.MenteeFeedbackView.as_view(),
        name='mentee_feedback'),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
    )
