(function($) {
    'use strict';

    $(function() {
        // this click bind is added because the current version of
        // django-browserid does not support redirect to href attribute of
        // logout link.
        $('.assist-browserid-logout').bind('click', function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            navigator.id.logout(); // Clears User Agent BrowserID state.
            window.location = href;
        });

        // Setup of all links that should open a modal window
        $('.openmodal').click(setup_modal);

        // Setup all datepicers
        $('.datepicker').datepicker({"dateFormat": 'dd M yy'});
        if ( $(".datepicker").attr('data-minDate') ) {
            $(".datepicker").datepicker("option", "minDate", $('.datepicker').attr('data-minDate'));
        }

        var $company_form = $('form#choose-company');
        if ( $company_form.length ) {
            $company_form.find("select[name='company']").change(function() {
                $company_form.submit();
            });
            $company_form.find("input[type='submit']").hide();
        }

        var $role_form = $('form#choose-role');
        if ( $role_form.length ) {
            $role_form.find("select[name='role']").change(function() {
                $role_form.submit();
            });
            $role_form.find("input[type='submit']").hide();
        }

        $('.batch-size-form').submit(function(evt) {
          evt.preventDefault();
          var url = $(this).attr('action');
          if (url.indexOf('?') == -1)
            url += "?";
          else
            url += "&";
          url += $(this).serialize();
          window.location = url;
        });


    });
})(jQuery);

function getURLParameter(name) {
    var result = decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
    if ( result == 'null') {
        return null;
    } else {
        return result;
    }
}

function setup_modal(evt) {
  evt.preventDefault();
  var url = $(this).attr('href');
  $('#common-modal-box').load(url + ' #main div.centre > *',
    function(responseText, textStatus, req) {
    if (textStatus == "error") {
      alert("There was an error loading the popup.");
      return
      }
    setup_submit.call(this);
    $('#common-modal-box').modal();
  });
};

function setup_submit() {
  $('#common-modal-box form input[type="submit"]').click(function(e) {
    e.preventDefault();
    var form = $('#common-modal-box form');
    var data = $('#common-modal-box form').serialize();

    this.value = "Processing...";
    this.disabled = true;

    $.post(form.attr('action'), data, function(data, text_status, jqxhr) {
      if (typeof(data) == "object") {
        //$('#common-modal-box').modal('hide');
        if (data['url'] == undefined) location.reload();
        else location.href = data['url'];
      } else {
        var filtered = $(data).find('#main div.centre').html();
        $('#common-modal-box').html(filtered);
        $('.simplemodal-close').bind('click.simplemodal', function (e) {
          e.preventDefault();
          $.modal.close();
        });
        setup_submit();
      }
    }).fail(function(error){
      var errorMessage = 'An error occured';
      var modalContent = $('#common-modal-box');
      if(error.status == 500) {
        errorMessage = 'Internal server error'
      } 
      // console.log(errorMessage);
      modalContent.html('<h2>' + errorMessage + '</h2> <p>Please close this window and try again later. If the problem persists, please contact the BIG Assist team.</p>');
    });
  });
}
