#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from lxml import etree
from lxml.html import fromstring
from bigassist_core.models import OrgRegion, SupportTopic
from bigassist_core.models import SupplierApproval, CustomerDiagnostic
from django.contrib.staticfiles.finders import find

AC_READ_FILE = etree.XSLTAccessControl(read_file=True, write_file=False, create_dir=False, read_network=False, write_network=False)


# class FileResolver(etree.Resolver):
#     """ Fixes "Cannot resolve URI" problem """
#     def resolve(self, url, pubid, context):
#         return self.resolve_filename(url, context)


class XMLManipulator(object):
    stylesheet = None

    def __init__(self, xml=None, tree=None, remove_blank_text=False):
        assert(tree or xml)
        if not tree:
            # etree parser or tostring removes the xml-stylesheet line from XML
            # I'm trying to remeber this line and put it back to XML when tostring is called
            if isinstance(xml, unicode):
                xml = xml.encode('utf-8')
            parser = etree.XMLParser(remove_pis=False, remove_comments=False, remove_blank_text=remove_blank_text)
            tree = etree.XML(xml, parser=parser)
        self.tree = tree

        # xpath expression doesn't like None in nsmap
        # http://lxml.de/FAQ.html#how-can-i-specify-a-default-namespace-for-xpath-expressions
        nsmap = tree.nsmap
        if None in nsmap:
            nsmap['xhtml'] = nsmap[None]
            nsmap.pop(None)
        # be sure there is a tm namespace. It should not happen though. I saw only one situation:
        # which was a <dummy /> XML (for testing), but the app failed on that.
        if 'tm' not in nsmap:
            nsmap['tm'] = "http://textmatters.com"
        self.nsmap = nsmap

    def whitespace_cleanup(self):
        for element in self.tree.iter("*"):
            if element.text is not None and not element.text.strip():
                element.text = None

    def get_text(self, xpath):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            return element.text or ''

    def set_text(self, xpath, value):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            element.text = value

    def set_attribute(self, xpath, attribute, value):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            element.set(attribute, str(value))

    def insert_organisation_info(self, company, diag=None):
        """ Fill organisation info to tm:organisation tags """
        def update_text(tag, value):
            node = element.find(tag, namespaces=self.nsmap)
            if node is not None:
                if value is None:
                    value = ''
                if not isinstance(value, basestring):
                    value = str(value)
                node.text = value
        elements = self.tree.xpath('.//tm:organisation', namespaces=self.nsmap)
        if elements:
            element = elements[0]
        else:
            # Invalid XML, without tm:organisation
            return
        person = company.org
        update_text('tm:org_name', person.displayname)
        if company.org_type:
            update_text('tm:org_type_id', company.org_type.pk)
        if 'supplier' in company.get_roles():
            update_text('tm:detail', company.get_detail())
        else:
            update_text('tm:org_type_other', person.charityno)
            update_text('tm:org_flavour_other', person.companyno)
            update_text('tm:detail', company.detail)
        address = person.get_address(2)
        if address:
            update_text('tm:address', address.address)
            update_text('tm:postcode', address.postcode)
        address = person.get_address(5)
        if address:
            update_text('tm:reg_address', address.address)
            update_text('tm:reg_postcode', address.postcode)
        update_text('tm:website', person.get_contact('website'))
        update_text('tm:tel_general', person.get_contact('tel_general'))
        update_text('tm:email_general', person.get_contact('email_general'))
        if diag:
            update_text('tm:contact_name', diag.contact_name)
            update_text('tm:short_desc', diag.contact_title)
            update_text('tm:contact_email', diag.contact_email)
            update_text('tm:contact_tel', diag.contact_phone)
        else:
            update_text('tm:contact_name', person.maincontactname)
            update_text('tm:short_desc', person.maincontacttitle)
            update_text('tm:contact_email', company.org.get_contact('email_direct'))
            update_text('tm:contact_tel', company.org.get_contact('tel_direct'))

    def set_xfinstance(self, id_attr, url):
        for element in self.tree.xpath('.//xf:instance', namespaces=self.nsmap):
            if element.get('id') == id_attr:
                talcontent = "{%(tal)s}content" % self.nsmap
                if element.get(talcontent):
                    del element.attrib[talcontent]
                element.set('src', url)
                if not element.xpath('./tm:dummy', namespaces=self.nsmap):
                    dummy = etree.Element('{%(tm)s}dummy' % self.nsmap, nsmap=self.nsmap)
                    element.append(dummy)

    def set_iframe(self, id_attr, url):
        for element in self.tree.xpath('.//xhtml:iframe', namespaces=self.nsmap):
            if element.get('id') == id_attr:
                if url:
                    element.set('src', url)
                else:
                    parent = element.getparent()
                    parent.remove(element)

    def set_xfsubmission(self, id_attr, url):
        for element in self.tree.xpath('.//xf:submission', namespaces=self.nsmap):
            if element.get('id') == id_attr:
                element.set('action', url)

    def fix_links_paths(self):
        static = settings.STATIC_URL
        for element in self.tree.xpath('.//xhtml:link', namespaces=self.nsmap):
            name = element.get('href').split('/')[-1]
            path = static + 'css/' + name
            element.set('href', path)

        for element in self.tree.xpath('.//xhtml:script', namespaces=self.nsmap):
            srcattr = element.get('src')
            if srcattr and not srcattr.startswith(static):
                srcattr = srcattr.replace('++resource++assist.theme.xsltforms2', 'xsltforms')
                path = static + srcattr
                element.set('src', path)

    def fix_images_paths(self):
        path = settings.STATIC_URL + 'images/'
        for element in self.tree.xpath('.//xf:output', namespaces=self.nsmap):
            if element.get('mediatype') == "image/*":
                value = element.get('value')
                value = value.replace("concat('progress'", "concat('%sprogress'" % path)
                element.set('value', value)

    def fix_stylesheet_path(self):
        stylesheet = self.tree.getprevious()
        if isinstance(stylesheet, etree._XSLTProcessingInstruction):
            name = stylesheet.get('href').split('/')[-1]
            path = settings.STATIC_URL + 'xsltforms/' + name
            stylesheet.set('href', path)

    def update_company(self, diag):
        if isinstance(diag, SupplierApproval):
            company = diag.suppl
        elif isinstance(diag, CustomerDiagnostic):
            company = diag.cust

        if 'supplier' in company.get_roles() and isinstance(diag, SupplierApproval):
            # <tm:services><tm:delivery_regions comment="">NE NW Yorks EM WM</tm:delivery_regions>
            nr_text = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_regions') or ''
            new_regions = set(nr_text.split())
            old_regions = set([x[0] for x in OrgRegion.objects.filter(org=company).values_list('region')])
            OrgRegion.objects.filter(org=company, region__in=old_regions - new_regions).delete()
            for to_add in new_regions - old_regions:
                OrgRegion.objects.create(org=company, region=to_add)

            # https://it.fry-it.com/text-matters/dev/061/#i3
            new_topics = set()
            for category in self.tree.xpath('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category', namespaces=self.nsmap):
                if category.text:
                    for topic_id in category.text.split():
                        if topic_id:
                            try:
                                topic = SupportTopic.objects.get(topic_id=int(topic_id))
                            except:
                                continue
                            new_topics.add(topic)

            #old_topics = set([x[0] for x in SupplierTopics.objects.filter(suppl=company).values_list('topic')])
            #SupplierTopics.objects.filter(suppl=company, topic__in=old_topics - new_topics).delete()
            #for to_add in new_topics - old_topics:
            #    SupplierTopics.objects.create(suppl=company, topic=to_add)

            who_delivers = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_people') or ''
            diag.delivery_people = who_delivers[:200]  # max length
            diag.short_desc = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:short_description') or ''
            # diag.long_desc = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:long_description') or ''
            diag.expertise = self.get_text('/tm:assist_supplier/tm:organisation/tm:long_desc') or None
            diag.blogs_link = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:spares/tm:spare3') or None

            diag.employee_no = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:policies/tm:employee_no') or None

            io_types_list = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:spares/tm:spare1')
            io_other = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:spares/tm:spare2')
            if io_other:
                diag.io_types_worked_with = io_types_list + ' ' + io_other.replace(' ', '_')
            else:
                diag.io_types_worked_with = io_types_list

            def sanitise_pc_dict(value):
                value = ''.join([n for n in value if n in '0123456789.,'])
                return value

            pc_turnover_from_dict = {
                1: (('Voluntary sector infrastructure organisations'), (sanitise_pc_dict(self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:customer_types/tm:customer_type[1]/tm:customer_type_percent')))),
                2: (('Other voluntary sector organisations'), (sanitise_pc_dict(self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:customer_types/tm:customer_type[2]/tm:customer_type_percent')))),
                3: (('Central and local government'), (sanitise_pc_dict(self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:customer_types/tm:customer_type[3]/tm:customer_type_percent')))),
                4: (('Commercial organisations'), (sanitise_pc_dict(self.get_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:customer_types/tm:customer_type[4]/tm:customer_type_percent')))),
            }

            diag.pc_turnover_from = str(pc_turnover_from_dict)
            # DO NOT SAVE diag HERE. This method is called from SupplierApproval.save() method and saves the diagnostic itself.

            diag.approvaltopic_set.exclude(topic__in=new_topics).delete()
            for to_add in new_topics:
                diag.approvaltopic_set.get_or_create(topic=to_add)

            vat_no = self.get_text('/tm:assist_supplier/tm:supplier_application/tm:finance/tm:vat_number') or ''
            company.vat_no = vat_no[:20]
            company.save()
        return diag

    def tostring(self, **kwargs):
        if 'xml_declaration' not in kwargs:
            kwargs['xml_declaration'] = True
        if 'encoding' not in kwargs:
            kwargs['encoding'] = 'utf-8'
        # flushing self.tree.getroottree() to preserve stylesheet processing instructions
        return etree.tostring(self.tree.getroottree(), **kwargs)

    def make_transformation(self, parse_parts=False):
        parser = etree.XMLParser(ns_clean=False)
        # parser.resolvers.add(FileResolver())

        transform_xml = etree.parse(find('xsltforms/xsltforms.xsl'), parser)
        transform = etree.XSLT(transform_xml, access_control=AC_READ_FILE)

        for element in self.tree.xpath('.//xhtml:link', namespaces=self.nsmap):
            element.attrib['href'] = str('file://' + find('css/form.css'))

        result = transform(self.tree, baseuri="'/static/xsltforms/'")

        html = str(result)
        extra = dict(
            full_result=html,
        )

        if parse_parts:
            root = fromstring(html)
            head = ''

            for element in root.findall('./head/link')\
                           + root.findall('./head/script')\
                           + root.findall('./head/span'):
                head += etree.tostring(element, method='html') + '\n'
            body_element = root.find('./body')
            body = ''
            for element in body_element.getchildren():
                body += etree.tostring(element, method='html') + '\n'
            body_attrs = 'onload="' + body_element.get('onload') + '"'

            extra.update(dict(
                head=head,
                body_attrs=body_attrs,
                content=body,
            ))
        return extra
