from django.template import Context
from django.template.loader import get_template
from django import template

register = template.Library()


@register.filter
def form(form):
    template = get_template("form.html")
    context = Context({'form': form})
    return template.render(context)


@register.filter
def field(field):
    template = get_template("field.html")
    context = Context({'field': field})
    return template.render(context)

