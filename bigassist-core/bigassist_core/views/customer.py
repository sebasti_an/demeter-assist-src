#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from dateutil.relativedelta import relativedelta
import json
from django.db.models import Avg, Count
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, \
    HttpResponseBadRequest, HttpResponseForbidden
from django.views.generic import View
from django.views.generic import TemplateView, FormView, DetailView, UpdateView, ListView
from django.views.generic import CreateView
from bigassist_core.forms import CustomerGetInTouchForm
from bigassist_core.forms import CustomerGetInTouchFormAnon
from bigassist_core.forms import CustomerGetInTouchSupplierForm
from bigassist_core.forms import CustomerProjectFeedbackForm
from bigassist_core.forms import CustomerProjectNotCompleteForm
from bigassist_core.forms import CustomerHostVisitPreForm
from bigassist_core.forms import CustomerHostVisitForm
from bigassist_core.forms import CustomerBecomeMentorPreForm
from bigassist_core.forms import CustomerBecomeMentorForm
from bigassist_core.forms import CustomerProjectProblemsForm
from bigassist_core.forms import VisitGetInTouchForm
from bigassist_core.forms import VisitorArrangeMeetingForm
from bigassist_core.forms import VisitorBursaryForm
from bigassist_core.forms import VisitorFeedbackForm
from bigassist_core.forms import MentorGetInTouchForm
from bigassist_core.forms import UpdateCustomerDiagnosticContactForm
from bigassist_core.views.abstract import InviteUsersBaseView
from bigassist_core import models
from bigassist_cms.models import Section
from bigassist_core.user import KHNPUser
from bigassist_core.utils import CustomerRequiredDecorator, ConfirmationView
from bigassist_core.utils import LoginRequiredDecorator
from bigassist_core.utils import CustomerOrStaffRequiredDecorator
from bigassist_core.utils import ConfirmationDetailView
from bigassist_core.utils import get_xform
from bigassist_core.xml import XMLManipulator
from bigassist_core.security import CustomerDiagnosticSecurity
from bigassist_core.security import CustomerProjectSecurity
from bigassist_core.security import UserSecurity
from bigassist_core.security import VisitSecurity, VisitorSecurity
from bigassist_core.security import MentorSecurity, MenteeSecurity
from bigassist_core.emails import send_ba_email
from django.db import transaction
from lxml import etree
from urlparse import urlparse

import logging
logger = logging.getLogger(__name__)


@LoginRequiredDecorator
class CustomerPrequalsView(TemplateView):
    template_name = "customer/prequals.html"


class CustomerGetInTouchView(FormView):
    template_name = "customer/get_in_touch.html"
    success_url = reverse_lazy('home')

    def get_form_class(self):
        if self.request.user.is_anonymous():
            return CustomerGetInTouchFormAnon
        else:
            return CustomerGetInTouchForm

    def get_form_kwargs(self):
        kwargs = super(CustomerGetInTouchView, self).get_form_kwargs()
        # copy data from initial to 'data' because the form requires these
        if 'data' in kwargs:
            kwargs['data'] = kwargs['data'].copy()
            kwargs['data']['recipient'] = kwargs['initial']['recipient']
        return kwargs

    def get_initial(self):
        initial = dict()
        initial['recipient'] = u'BIG Assist team'
        return initial

    def form_valid(self, form):
        if self.request.user.is_anonymous():
            sender = form.cleaned_data['email']
        else:
            sender = KHNPUser(self.request.user).get_contact()
        form.send_email(sender)
        return super(CustomerGetInTouchView, self).form_valid(form)


@CustomerRequiredDecorator
class CustomerQualificationView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        # Qualification can be displayed only if it does not exist in the datbaase yet.
        user = KHNPUser(request.user)
        company = user.get_company()
        if models.CustomerPrequal.objects.filter(cust=company).exists():
            messages.warning(request, "You've started your pre qualification form already.")
            return HttpResponseRedirect(reverse('home'))

        xml = get_xform('P_customer_prequal_source.html')
        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        return HttpResponse(xml, content_type='text/xml')


@CustomerRequiredDecorator
class CustomerQualificationModelView(View):

    def get(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()
        try:
            model = models.CustomerPrequal.objects.get(cust=company).prequal_questions
        except models.CustomerPrequal.DoesNotExist:
            model = get_xform('P_customer_prequal_model.xml')
        if isinstance(model, unicode):
            model = model.encode('utf-8')
        return HttpResponse(model, content_type='text/xml')


@CustomerRequiredDecorator
class CustomerQualificationSaveView(View):

    def post(self, request, *args, **kwargs):
        """ This view should get XML data from prequal XForms page and do these steps:
            - validate XML data. If invalid, return BadRequest
            - save them to db
            - analyze customer's eligibility
            - if eligible, redirect to customer diagnostics
            - if not eligible, redirect to customer_qualify_failed or to customer_qualify_held
        """
        xml = request.body
        try:
            xmlroot = etree.XML(xml)
        except etree.XMLSyntaxError:
            return HttpResponse('ERROR: Invalid XML content')
            # return HttpResponseBadRequest('Invalid XML content')

        user = KHNPUser(request.user)
        company = user.get_company()
        if models.CustomerPrequal.objects.filter(cust=company).exists():
            return HttpResponse('ERROR: Duplicate submission')
            # return HttpResponseBadRequest('Duplicate submission')
        if models.CustomerDiagnostic.objects.filter(cust=company).exists():
            return HttpResponse('Duplicate submission (diagnostic already exists)')
            # return HttpResponseBadRequest('Duplicate submission (diagnostic already exists)')

        with transaction.commit_manually():
            try:
                models.CustomerPrequal.objects.create(cust=company, prequal_questions=xml)

                # https://fry-it.basecamphq.com/projects/9419045-text-matters/posts/68220755/comments#comment_202749381
                eligible = None  # invalid input
                elements = xmlroot.xpath('/tm:assist_cust_pre/tm:decision', namespaces=xmlroot.nsmap)
                if elements:
                    decision = elements[0].text
                    if decision == 'PASSED':
                        eligible = 1
                        initialize_customer_diagnostic(company, state='prequalified')
                    elif decision == 'HUMAN CHECK':
                        eligible = 2
                    elif decision == 'DISQUALIFIED':
                        eligible = 3
                transaction.commit()
            except Exception, e:
                transaction.rollback()
                return HttpResponse("ERROR: %s" % str(e))

        if eligible == 1:
            return HttpResponseRedirect(reverse('home'))
        elif eligible == 2:  # hold your horses
            return HttpResponseRedirect(reverse('customer_qualify_held'))
        else:  # not eligible
            return HttpResponseRedirect(reverse('customer_qualify_failed'))


@CustomerRequiredDecorator
class CustomerDiagnosticContactView(UpdateView):
    model = models.CustomerDiagnostic
    form_class = UpdateCustomerDiagnosticContactForm

    success_message = 'Customer diagnostic contact details updated'

    template_name = 'customer/diagnostic_contact_edit.html'

    diag = None

    modal = False

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        if company:
            # make sure we have model already, otherwise we can't generate correct URL for file upload
            self.diag = company.get_customer_diagnostic()
        if 'modal' in self.request.GET or 'modal' in self.request.POST:
            self.modal = True
        return super(CustomerDiagnosticContactView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        return self.model.objects.get(pk=self.diag.pk)

    def get_success_url(self):
        if self.modal:
            return reverse_lazy('customer_dashboard', args=['assessment'])
        return reverse('customer_diagnostic')

    def get_context_data(self, **kwargs):
        context = super(CustomerDiagnosticContactView, self).get_context_data(**kwargs)
        context['is_modal'] = self.modal
        return context

    def get_initial(self):
        if self.diag.prev_diag and not self.diag.contact_name:
            prev_diag = models.CustomerDiagnostic.objects.get(pk=self.diag.prev_diag.pk)
            initial = {
                'contact_name': prev_diag.contact_name,
                'contact_title': prev_diag.contact_title,
                'contact_phone': prev_diag.contact_phone,
                'contact_email': prev_diag.contact_email,
            }
            return initial

    def form_valid(self, form):
        form.save()

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")
        return HttpResponseRedirect(url)


@CustomerRequiredDecorator
class CustomerDiagnosticView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        # It seems they don't need prequal before diagnostic, so if user wants the diagnostic,
        # we should allow it.
        user = KHNPUser(request.user)
        company = user.get_company()
        diag = company.get_customer_diagnostic()

        if not diag:
            initialize_customer_diagnostic(company)
            return CustomerDiagnosticContactView.as_view()(request, **kwargs)

        if diag is not None:

            # We now need to make sure there is a relationship between
            # the programme and the diagnostic.
            prog_role = models.ProgrammeRole.objects.get(prog_id=1, role='customer')
            diag_prog_role_rel = models.CustomerDiagnosticProgrammeRole.objects.get_or_create(prog_role=prog_role, diag=diag)
            dpr_obj, dpr_created = diag_prog_role_rel
            if not dpr_created:
                dpr_obj.save()
            # print diag_prog_role_rel

            if not diag.has_contact_info():
                return CustomerDiagnosticContactView.as_view()(request, **kwargs)

            security = CustomerDiagnosticSecurity(diag)
            if not security.check('cust_can_edit_diagnostic'):
                xml = get_xform('C_customer_self_assessment_readonly.html')
            else:
                if diag.resubmission():
                    # If resubmission, use XFORM stored in custom_2 field.
                    # See https://it.fry-it.com/text-matters/dev/135#i3
                    xml = get_xform('C_customer_self_assessment_resubmission.html')
                else:
                    xml = get_xform('C_customer_self_assessment_source.html')
        else:
            xml = get_xform('C_customer_self_assessment_source.html')

        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        return HttpResponse(xml, content_type='text/xml')


@CustomerRequiredDecorator
class CustomerDiagnosticResubmitView(ConfirmationView):
    template_name = 'confirmation.html'
    success_url = reverse_lazy('customer_dashboard', args=['assessment'])
    description = "You will be able to edit and re-submit your self-assessment. Continue?"
    layout = 'bottom'
    action = 'Yes'

    def dispatch(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()
        self.diag = company.get_customer_diagnostic()

        security = CustomerDiagnosticSecurity(self.diag)
        # if not security.check('can_resubmit'):
        if security.check('can_resubmit'):
            return HttpResponseForbidden("You can't resubmit self-assessment now.")
        return super(CustomerDiagnosticResubmitView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        """
        This view needs to take the current self-assessment/diagnostic model object,
        make a new one, with the current ID as prev_diag *new, and copy the current
        XForm questions in to diag_questions, change the state to 'in_progress'

        after that, we need to use this newly created object, get it's new id
        and add its new id to the current diagnostic's field next_diag so there's
        a direct relationship between the two.
        """
        #create new diagnostic record

        #empty_diag_questions = get_xform('C_customer_self_assessment_model.xml')

        new_diag = models.CustomerDiagnostic.objects.create(cust_id=self.diag.cust_id,
            diag_questions=self.diag.diag_questions, # use existing answers
            prev_diag=self.diag,
            state='in_progress')
        self.diag.next_diag = new_diag
        self.diag.save()

        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerDiagnosticModelView(View):

    @transaction.commit_manually
    def get(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()

        diagnostics = models.CustomerDiagnostic.objects.filter(cust=company).order_by('-created').all()
        if diagnostics:
            diagnostic = diagnostics[0]
            model = diagnostic.diag_questions
        else:
            model = initialize_customer_diagnostic(company).diag_questions

        transaction.commit()
        if isinstance(model, unicode):
            model = model.encode('utf-8')
        # cleanup XML whitespace. Refs. https://it.fry-it.com/text-matters/dev/029
        with transaction.commit_manually():
            xml = XMLManipulator(model, remove_blank_text=True)
            xml.insert_organisation_info(company, diagnostic)
            # xml.insert_organisation_info(company)
            xml.whitespace_cleanup()
            model = xml.tostring()
            del xml
            transaction.commit()
        return HttpResponse(model, content_type='text/xml')


@CustomerRequiredDecorator
class CustomerDiagnosticPrevModelView(View):
    """
    This view should get the previous 'model' from the db
    much like the SupplierAssessmentPrevModelView in supplier.py
    we need to add a self reference field in the customer diagnostic
    model/table
    """
    def get(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()

        model = None
        diagnostic = company.get_customer_diagnostic()
        if diagnostic is not None:
            prev = diagnostic.prev_diag
            if prev:
                model = prev.diag_questions

        if model is None:
            # This should never happen
            return HttpResponse('Invalid data', content_type='text/plain')

        if isinstance(model, unicode):
            model = model.encode('utf-8')
        # cleanup XML whitespace. Refs. https://it.fry-it.com/text-matters/dev/029
        xml = XMLManipulator(model, remove_blank_text=True)
        xml.whitespace_cleanup()
        return HttpResponse(xml.tostring(), content_type='text/xml')

@CustomerRequiredDecorator
class CustomerDiagnosticSaveView(View):

    def post(self, request, *args, **kwargs):
        """ This view should get XML data from prequal XForms page and do these steps:
            - validate XML data. If invalid, return BadRequest
            - save them to db
            - analyze data???
            - how to handle quicksave and final submission?
        """
        xml = request.body
        try:
            etree.XML(xml)
        except etree.XMLSyntaxError:
            return HttpResponseBadRequest('Invalid XML content')
        user = KHNPUser(request.user)
        company = user.get_company()

        try:
            objs = models.CustomerDiagnostic.objects.filter(cust=company).order_by('-created').all()
            obj = objs[0]
            obj.diag_questions = xml
            obj.state = 'in_progress'
            obj.save()
        except models.CustomerDiagnostic.DoesNotExist:
            models.CustomerDiagnostic.objects.create(cust=company,
                                                     diag_questions=xml)

        if kwargs.get('submit'):
            security = CustomerDiagnosticSecurity(obj)
            if not security.check("can_submit"):
                return HttpResponseForbidden()
            obj.set_state('submitted')
            send_ba_email('06_diagnostic_submitted', {}, sender=None, recipients=user.get_contact())
            return HttpResponseRedirect(reverse('customer_dashboard', kwargs={'tab':'diagnostic'}))
        else:
            # nothing important, just return something
            return HttpResponse('ok')


@CustomerRequiredDecorator
class CustomerDashboardView(View):

    def dispatch(self, request, *args, **kwargs):
        super(CustomerDashboardView, self).dispatch(request, *args, **kwargs)
        tab = self.kwargs.get('tab')
        if tab == 'diagnostic':
            return CustomerDashboardDiagnosticView.as_view()(request, **kwargs)
        elif tab == 'vouchers':
            return CustomerDashboardVouchersView.as_view()(request, **kwargs)
        elif tab == 'projects':
            return CustomerDashboardProjectsView.as_view()(request, **kwargs)
        elif tab in ('connectspace', 'meetspace'):
            return CustomerDashboardMeetspaceView.as_view()(request, **kwargs)
        elif tab == 'users':
            return CustomerDashboardUsersView.as_view()(request, **kwargs)
        elif tab == 'visitors':
            return CustomerDashboardVisitorsView.as_view()(request, **kwargs)
        elif tab == 'mentoring':
            return CustomerDashboardMentoringView.as_view()(request, **kwargs)
        else:
            return HttpResponseRedirect(reverse('home'))


class CustomerDashboardBaseView(TemplateView):
    tab_name = 'diagnostic'

    def dispatch(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        self.person = user.get_person()
        return super(CustomerDashboardBaseView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardBaseView, self).get_context_data(**kwargs)
        now = datetime.datetime.now()

        today = now.date()
        tomorrow = today + datetime.timedelta(days=1)

        context['today'] = today

        context['tab'] = self.tab_name
        context['khnp_user'] = KHNPUser(self.request.user)
        person = context['khnp_user'].get_person()
        context['object'] = company = context['khnp_user'].get_company()

        context['prequal'] = models.CustomerPrequal.objects.filter(cust=context['object'])
        context['diagnostic'] = context['object'].get_customer_diagnostic()
        context['total_vouchers'] = models.CustomerVoucher.objects.filter(cust=context['object'],
                                                                          expires__gte=tomorrow).count()
        context['spent_vouchers'] = models.CustomerVoucher.objects.filter(cust=context['object'],
                                                                          spent__gte=1,
                                                                          expires__gte=tomorrow).count()
        context['total_projects'] = models.CustomerProject.objects.filter(cust=context['object']).count()
        # total_meetspace - how many times my organisation is Vistor or Mentee
        context['total_meetspace'] = context['object'].visitor_set.count() \
                                   + context['object'].mentee_set.active().count()
        # total_visitors - how many visitors are going to visit me
        context['is_visit_host'] = context['object'].visit_set.exists()
        context['total_visitors'] = models.Visitor.objects.filter(visit__host=context['object']).count()
        # total_mentoring - how many mentees are going to visit me
        context['is_mentor_in_company'] = person.mentor_set.filter(mentor_org=company).exists()
        context['total_mentoring'] = models.Mentee.objects.filter(state__in=('pending', 'agreed'),
                                                                  mentor__mentor=person).count()
        context['section'] = get_object_or_404(Section, slug='news')

        return context


@CustomerRequiredDecorator
class CustomerDashboardDiagnosticView(CustomerDashboardBaseView):
    tab_name = 'diagnostic'
    template_name = "customer/dashboard_diagnostic.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardDiagnosticView, self).get_context_data(**kwargs)
        context['vouchers'] = models.CustomerVoucher.objects.filter(cust=context['object'])
        return context


@CustomerRequiredDecorator
class CustomerDashboardVouchersView(CustomerDashboardBaseView):
    tab_name = 'vouchers'
    template_name = "customer/dashboard_vouchers.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardVouchersView, self).get_context_data(**kwargs)
        context['vouchers'] = models.CustomerVoucher.objects.filter(cust=context['object'])
        context['not_current_states'] = ('expired', 'all')
        now = datetime.datetime.now()
        # non javascript handling - probably not needed at all
        vf = self.request.POST.get('voucher_filter')
        if vf == 'all':
            context['voucher_filter'] = 'all'
        elif vf == 'expired':
            context['voucher_filter'] = 'expired'
            context['vouchers'].filter(expires__lt=now)
        else:
            context['voucher_filter'] = 'current'
            context['vouchers'].filter(assigned__lte=now, expires__gte=now)

        return context


@CustomerRequiredDecorator
class CustomerDashboardProjectsView(CustomerDashboardBaseView):
    tab_name = 'projects'
    template_name = "customer/dashboard_projects.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardProjectsView, self).get_context_data(**kwargs)

        cust = context['object']
        context['potential'] = cust.customer_projects.potential()
        context['current'] = cust.customer_projects.current()
        context['dismissed'] = cust.customer_projects.dismissed()
        context['invoiced'] = cust.customer_projects.invoiced()

        context['total_potential'] = context['potential'].count()
        context['total_current'] = context['current'].count()

        return context


@CustomerRequiredDecorator
class CustomerDashboardMeetspaceView(CustomerDashboardBaseView):
    """
    Meetspace tab lists all visits and mentoring where Organisation
    or people in the Organisation are a Visitors or Mentees
    """
    tab_name = 'connectspace'
    template_name = "customer/dashboard_meetspace.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardMeetspaceView, self).get_context_data(**kwargs)
        cust = context['object']

        context['visits'] = cust.visitor_set.all()
        context['mentoring'] = cust.mentee_set.active().all()
        context['total_potentialv'] = cust.visitor_set.potential().count()
        context['total_completedv'] = cust.visitor_set.completed().count()
        context['total_potentialm'] = cust.mentee_set.potential().count()
        context['total_confirmedm'] = cust.mentee_set.confirmed().count()

        return context


@CustomerRequiredDecorator
class CustomerDashboardVisitorsView(CustomerDashboardBaseView):
    """
    Visitors tab lists all visits hosted by this Organisation and allows to
    Edit, Remove visit, Accept Reject visitors etc.
    """
    tab_name = 'visitors'
    template_name = "customer/dashboard_visitors.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardVisitorsView, self).get_context_data(**kwargs)
        cust = context['object']

        context['visits'] = cust.visit_set.exclude(available=False)

        context['total_potential'] = models.Visitor.objects.potential().filter(visit__host=cust).count()
        context['total_confirmed'] = models.Visitor.objects.confirmed().filter(visit__host=cust).count()

        return context


@CustomerRequiredDecorator
class CustomerDashboardMentoringView(CustomerDashboardBaseView):
    """
    Mentoring tab lists all mentoring hosted by people in this Organisation
    """
    tab_name = 'mentoring'
    template_name = "customer/dashboard_mentoring.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardMentoringView, self).get_context_data(**kwargs)
        context['mentor'] = None
        mentor = self.person.mentor_set.filter(mentor_org=context['object']).exclude(available=False)
        if mentor:
            context['mentor'] = mentor[0]
        context['potential'] = models.Mentee.objects.potential().filter(mentor__mentor=self.person)
        context['confirmed'] = models.Mentee.objects.confirmed().filter(mentor__mentor=self.person)
        return context


@LoginRequiredDecorator
class CustomerDashboardUsersView(InviteUsersBaseView, CustomerDashboardBaseView):
    tab_name = 'users'
    template_name = 'org_users.html'

    def get_context_data(self, **kwargs):
        context = super(CustomerDashboardUsersView, self).get_context_data(**kwargs)
        context['base_template'] = 'customer/dashboard.html'
        return context


@CustomerRequiredDecorator
class CustomerSupplierContactDismissView(ConfirmationDetailView):
    template_name = 'confirmation.html'
    model = models.CustomerProject
    success_url = reverse_lazy('customer_dashboard', args=['projects'])
    description = "You are about to dismiss this contact. Continue?"
    action = 'Yes'

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.set_state('dismissed', by_customer=True)

        context = {'project': obj,
                   'customer': obj.cust,
                   'supplier': obj.suppl}
        send_ba_email('31_customer_dismisses_project', context, sender=None, recipients=obj.suppl, recipient_role='supplier')

        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerSupplierContactAcceptView(ConfirmationDetailView):
    template_name = 'customer/accept_project.html'
    model = models.CustomerProject
    success_url = reverse_lazy('customer_dashboard', args=['projects'])
    description = "You are about to accept this project. Continue?"
    action = 'Yes'
    layout = 'bottom'

    def get_voucher(self):
        if self.request.method == "POST":
            pk = self.request.POST.get('voucher')
        else:
            pk = self.request.GET.get('voucher')

        try:
            voucher = self.get_object().cust.customervoucher_set.available().get(pk=pk)
        except:
            voucher = None
        return voucher


    def get_context_data(self, **kwargs):
        context = super(CustomerSupplierContactAcceptView, self).get_context_data(**kwargs)
        project = self.get_object()
        voucher = self.get_voucher()

        if project.applicable_vouchers().exists():
            context['warn_vouchers'] = True

        context['project'] = project
        context['voucher'] = voucher

        total_number = int(self.request.GET.get('project_total'))
        pos_total = int(total_number*-1)

        context['project_total'] = total_number
        context['pos_total'] = pos_total

        return context

    def post(self, request, *args, **kwargs):
        obj = self.get_object()

        voucher = self.get_voucher()
        if voucher:
            obj.apply_voucher(voucher)

        obj.set_state('accepted')
        context = dict(project=obj)

        send_ba_email('111_customer_accepts_project_supplier', context, sender=None, recipients=obj.suppl, recipient_role='supplier')
        send_ba_email('112_customer_accepts_project_customer', context, sender=None, recipients=obj.cust, recipient_role='customer')
        return self.render_success(request)


@CustomerOrStaffRequiredDecorator
class CustomerDashboardCommLogDetailView(ConfirmationDetailView):
    model = models.CustomerCommLog
    template_name = "customer/dashboard_commlog.html"


@CustomerRequiredDecorator
class CustomerDashboardCommLogList(ListView):
    model = models.CustomerCommLog
    template_name = "customer/commlog_list.html"
    paginate_by = 10

    def get_queryset(self):
        company = KHNPUser(self.request.user).get_company()
        if company is None:
            return []
        diag = company.get_customer_diagnostic()
        if diag is None:
            return []
        return models.CustomerCommLog.objects.filter(diag=diag)


# Anonymous can view this template - #049
class CustomerSupplierProfileView(TemplateView):
    template_name = "customer/supplier_profile.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerSupplierProfileView, self).get_context_data(**kwargs)
        pk = int(kwargs.get('id'))
        context['supplier'] = get_object_or_404(models.Organisation, pk=pk)
        context['diag'] = context['supplier'].get_supplier_diagnostic()
        # context['topics'] = models.SupplierTopics.objects.filter(suppl=context['supplier'])
        context['topics'] = models.SupplierTopics.objects.filter(suppl=context['supplier']).distinct('topic__area')
        context['regions'] = models.OrgRegion.objects.filter(org=context['supplier'])
        context['total_regions'] = len(context['regions'])
        approval = context['supplier'].get_supplier_diagnostic()
        context['delivery_people'] = approval.delivery_people
        # context['supplier_phone'] = context['supplier'].org.get_contact('tel_direct')
        context['supplier_phone'] = context['supplier'].org.get_contact('tel_general')
        context['supplier_email'] = context['supplier'].org.get_contact('email_general')
        url = context['supplier'].org.get_contact('website')
        if url:
            context['supplier_website'] = url if "://" in url else "http://" + url
            context['supplier_website_display'] = urlparse(context['supplier_website']).netloc
        address = context['supplier'].org.get_address()
        context['supplier_address'] = address and address.formatted_address()


        feedback = models.CustomerProject.objects.filter(suppl=context['supplier'],
                                                         feedback__isnull=False,
                                                         state='completed')\
                .aggregate(feedback=Avg('fb_score'), count=Count('fb_score'))
        complete = models.CustomerProject.objects.filter(suppl=context['supplier'],
                                                         state='completed')\
                .aggregate(count=Count('fb_score'))
        
        context['feedback_object'] = models.CustomerProject.objects.filter(
                                                    suppl=context['supplier'],
                                                    state='completed')

        if feedback['count']:
            context['feedback'] = feedback['feedback']
            context['feedback_count'] = feedback['count']
            context['project_count'] = complete['count']
        
        return context


@CustomerRequiredDecorator
class CustomerSupplierContactView(ConfirmationDetailView):
    model = models.Organisation
    template_name = "customer/supplier_contact.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerSupplierContactView, self).get_context_data(**kwargs)
        obj = self.get_object()
        address = obj.org.get_address()
        if address:
            context['address'] = address.formatted_address()
        # context['email'] = obj.org.get_contact_email()
        context['email'] = obj.get_supplier_diagnostic().contact_email
        context['phone'] = obj.org.get_contact('tel_general')
        return context


@CustomerRequiredDecorator
class CustomerCustomerContactView(ConfirmationDetailView):
    """This class is too similar to above. Consider refactoring this
    contact system in future version."""
    model = models.Organisation
    template_name = "customer/supplier_contact.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerCustomerContactView, self).get_context_data(**kwargs)
        obj = self.get_object()
        address = obj.org.get_address()
        if address:
            context['address'] = address.formatted_address()
        # Line below is the only difference to the class CustomerSupplierContactView
        context['email'] = obj.get_customer_diagnostic().contact_email
        context['phone'] = obj.org.get_contact('tel_general')
        return context


@CustomerRequiredDecorator
class CustomerRecommendationView(TemplateView):

    model = models.CustomerDiagnostic
    template_name = "customer/recommendation.html"
    context_object_name = "diag"

    def dispatch(self, request, *args, **kwargs):
        self.user = KHNPUser(request.user)
        self.diag = self.user.get_company().get_customer_diagnostic()
        security = CustomerDiagnosticSecurity(self.diag)
        if not security.check("cust_view_recommendation"):
            return HttpResponseForbidden()

        return super(CustomerRecommendationView, self).dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CustomerRecommendationView, self).get_context_data(**kwargs)
        context['diag'] = self.diag
        context['dev_pris'] = self.diag.customerdevpri_set.all()

        has_vouchers = False
        for dev_pri in context['dev_pris']:
            if dev_pri.value:
                has_vouchers = True
                break
        context['show_disclaimer'] = (self.diag.state != 'approved') and has_vouchers
        return context


@CustomerRequiredDecorator
class CustomerRecommendationApprove(ConfirmationView):

    description = "You are about to approve your recommendations which means your voucher(s) will be available to spend in the marketplace."
    action = "Approve"
    layout = "bottom"
    success_url = reverse_lazy("home")

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        # diag = user.get_company().get_diagnostic('diagnostic')
        diag = user.get_company().get_customer_diagnostic()

        security = CustomerDiagnosticSecurity(diag)
        error = security.error("cust_approve_recommendation")
        if error:
            msg = "Cannot approve recommendations: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        # When Recommendation is approved, we also have to create vouchers from customer_dev_pri table
        # this is done in the set_state method.
        diag.set_state("approved")
        voucher_expires = datetime.datetime.now() + relativedelta(months=4)
        send_ba_email('081_recommendation_accepted', {'voucher_expires': voucher_expires}, sender=None, recipients=user.get_contact())
        send_ba_email('082_recommendation_accepted', {'company': user.get_company()}, sender=None, recipients=None)
        models.CustomerCommLog.objects.create(diag=diag,
                                       comm_type=models.CustomerCommType.objects.get(
                                                     pk=settings.KHNP_COMMLOG_RECOMMENDATION_ACCEPTED_ID),
                                       details='')
        msg = "Recommendation has been approved"
        messages.success(request, msg)

        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerRecommendationAppeal(ConfirmationView):

    description = """You are about to appeal your recommendations.
                  Appealing the recommendations will require action
                  on your part."""
    action = "Appeal"
    layout = "bottom"
    success_url = reverse_lazy("home")

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        diag = user.get_company().get_customer_diagnostic()

        security = CustomerDiagnosticSecurity(diag)
        error = security.error("cust_appeal_recommendation")
        if error:
            msg = "Cannot reject recommendations: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        diag.set_state("recommendation_appealed")
        msg = "Recommendation has been marked for appeal"

        send_ba_email('50_customer_appeals_recommendation', {'none': None}, sender=None, recipients=user.get_contact())
        send_ba_email('51_customer_appeals_recommendation_admin', {'customer': diag.cust}, sender=None, recipients=None)
        messages.success(request, msg)
        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerRecommendationReject(ConfirmationView):

    description = "You are about to reject your recommendations"
    action = "Reject"
    layout = "bottom"
    success_url = reverse_lazy("home")

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        diag = user.get_company().get_customer_diagnostic()

        security = CustomerDiagnosticSecurity(diag)
        error = security.error("cust_reject_recommendation")
        if error:
            msg = "Cannot reject recommendations: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        msg = "Recommendation has been rejected"

        diag.set_state("recommendation_rejected")

        models.CustomerCommLog.objects.create(
            diag=diag,
            comm_type=models.CustomerCommType.objects.get(
                pk=settings.KHNP_COMMLOG_RECOMMENDATION_REJECTED_ID
                ),
            details=''
            )

        send_ba_email('55_customer_rejects_recommendation', data=None, sender=None, recipients=user.get_contact())
        send_ba_email('56_customer_rejects_recommendation_admin', {'customer': diag.cust}, sender=None, recipients=None)

        messages.success(request, msg)
        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerGetInTouchSupplierView(FormView):
    form_class = CustomerGetInTouchSupplierForm
    template_name = "customer/get_in_touch_supplier.html"
    success_url = reverse_lazy('home')

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.supplier = get_object_or_404(models.Organisation, pk=kwargs.get('supplier_pk'))
        khnp_user = KHNPUser(request.user)
        self.customer = khnp_user.get_company()
        if self.customer:
            security = UserSecurity(request.user)
            err = security.error('contact_supplier')
            if err:
                return HttpResponseForbidden(err)
        else:
            return HttpResponseForbidden('Company is missing.')
        return super(CustomerGetInTouchSupplierView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = dict()
        initial['provider'] = self.get_context_data()['supplier']
        initial['report'] = u'Note that suppliers will be able to view your ' \
                            u'recommendations to better assess your needs ' \
                            u'and that they can offer useful help.'
        return initial

    def get_form_kwargs(self):
        kwargs = super(CustomerGetInTouchSupplierView, self).get_form_kwargs()
        # copy data from initial to 'data' because the form requires these
        if 'data' in kwargs:
            kwargs['data'] = kwargs['data'].copy()
            kwargs['data']['provider'] = kwargs['initial']['provider']
            kwargs['data']['report'] = kwargs['initial']['report']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CustomerGetInTouchSupplierView, self).get_context_data(**kwargs)
        context['supplier'] = self.supplier
        return context

    @transaction.atomic
    def form_valid(self, form):
        # context for email
        context = dict()
        context['supplier'] = self.supplier
        context['customer'] = self.customer

        # Add record to CustomerProject table
        project = models.CustomerProject.objects.create(
            cust=context['customer'],
            suppl=context['supplier'],
            cover_note=form.cleaned_data['message'],
            comm_method=form.cleaned_data['typ'],
        )
        context['pk'] = project.pk

        context['voucher'] = models.CustomerVoucher.objects.available().filter(cust=self.customer).order_by('-expires')[:1].get()

        messages.info(self.request, 'Thank you for contacting the BIG Assist supplier %s' % context['supplier'])

        # Send mail to all members of supplier's org.
        send_ba_email('092_customer_contacts_supplier', context, sender=None, recipients=context['supplier'], recipient_role='supplier')
        send_ba_email('091_customer_contacts_supplier', context, sender=None, recipients=context['customer'], recipient_role='customer')

        return super(CustomerGetInTouchSupplierView, self).form_valid(form)


class CustomerProjectMixin(object):

    permission = None

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        company = khnp_user.get_company()
        self.project = get_object_or_404(models.CustomerProject, pk=kwargs['pk'])

        if self.project.cust != company:
            return HttpResponseForbidden()

        if self.permission:
            security = CustomerProjectSecurity(self.project)
            if not security.check(self.permission):
                return HttpResponseForbidden()

        return super(CustomerProjectMixin, self).dispatch(request, *args, **kwargs)


@CustomerRequiredDecorator
class CustomerProjectDetailView(CustomerProjectMixin, DetailView):

    model = models.CustomerProject
    context_object_name = "project"
    template_name = "customer/project_detail.html"

    permission = "cust_can_view_project"


@CustomerRequiredDecorator
class CustomerProjectFeedbackView(CustomerProjectMixin, UpdateView):
    model = models.CustomerProject
    context_object_name = "project"
    form_class = CustomerProjectFeedbackForm
    template_name = "customer/project_feedback.html"
    success_url = reverse_lazy("customer_dashboard", args=("projects", ))

    permission = "cust_can_provide_feedback"

    @transaction.atomic
    def form_valid(self, form):
        project = form.save(commit=False)
        project.set_state("completed", commit=False)
        project.save()
        context = {'project': self.project}
        send_ba_email('114_customer_chase_feedback_thank_you', context, recipients=self.project.cust, recipient_role='customer')

        return HttpResponseRedirect(self.get_success_url())


class CutomerFeedbackChaseEmailView(View):
    model = models.CustomerProject

    def get(self, request, *args, **kwargs):
        completed_projects = self.model.objects.filter(state='mark_completed', feedback__isnull=True)

        for project in completed_projects:
            context = {'project': project}
            send_ba_email('113_customer_chase_feedback', context, recipients=project.cust, recipient_role='customer')

        return HttpResponse(completed_projects)


class CustomerVoucherExpiryReminderEmailView(View):
    model = models.CustomerVoucher
    days = 60

    def get(self, request, *args, **kwargs):
        if request.GET.get('days'):
            days = int(request.GET.get('days'))
        else:
            days = self.days

        vouchers = self.model.objects.available()
        now = datetime.datetime.now()

        timedifference = now+datetime.timedelta(days=days)

        expires = vouchers.filter(expires__exact=timedifference)

        for voucher in expires:
            context = {'voucher': voucher}
            if days == 0:
                send_ba_email('115b_customer_voucher_expired', context, recipients=voucher.cust, cc=settings.BIGASSIST_ADMIN_EMAIL)
            else:
                send_ba_email('115_customer_voucher_expiry_prompt', context, recipients=voucher.cust, recipient_role='customer')

        return HttpResponse(expires)


class CustomerProjectRemindersEmailView(View):
    voucher_model = models.CustomerVoucher
    project_model = models.CustomerProject
    days = 30

    def get(self, request, *args, **kwargs):
        if request.GET.get('days'):
            days = int(request.GET.get('days'))
        else:
            days = self.days

        vouchers = self.voucher_model.objects.available()
        
        now = datetime.datetime.now()

        timedifference = now+datetime.timedelta(days=days)

        expires = vouchers.filter(expires__exact=timedifference)

        supplier_contacted_range = (90, 60, 30, 20)
        project_proposed_range = (90, 60, 30)
        project_accepted_range = (30, 20, 10, 3)

        for voucher in expires:
            if days in project_accepted_range:
                projects = self.project_model.objects.accepted().filter(cust=voucher.cust)

                for project in projects:
                    context = {
                        'voucher': voucher, 
                        'project': project,
                        'num_days': days
                        }
                    if days in (30, 20):
                        send_ba_email('118_customer_accepts_project_supplier_reminder', context, recipients=project.suppl, recipient_role='supplier')
                    if days in (10, 3):
                        send_ba_email('119_customer_accepts_project_supplier_reminder', context, recipients=project.suppl, recipient_role='supplier')

            if days in project_proposed_range:
                projects = self.project_model.objects.proposed().filter(cust=voucher.cust)

                for project in projects:
                    context = {
                        'voucher': voucher, 
                        'project': project,
                        'num_days': days
                        }
                    send_ba_email('103_customer_supplier_proposed_project_reminder', context, recipients=voucher.cust, recipient_role='customer')

            if days in supplier_contacted_range:
                projects = self.project_model.objects.created().filter(cust=voucher.cust)
                for index, project in enumerate(projects):
                    context = {
                        'voucher': voucher, 
                        'project': project,
                        'num_days': days
                        }
                    #only send once (to customer)
                    if index == 0:
                        send_ba_email('116_customer_supplier_contacted', context, recipients=voucher.cust, recipient_role='customer')

                    # send email to every supplier
                    send_ba_email('117_supplier_contacted_by_customer', context, recipients=project.suppl, recipient_role='supplier')

        return HttpResponse("OK")


# class CustomerProjectProposedReminderEmailView(View):
#     model = models.CustomerProject
#     days = 60

#     def get(self, request, *args, **kwargs):
#         if request.GET.get('days'):
#             days = int(request.GET.get('days'))
#         else:
#             days = self.days

#         vouchers = self.model.objects.available()
#         # now = datetime.datetime.now()
#         now = datetime.date(2014, 9, 25) 
#         timedifference = now+datetime.timedelta(days=days)

#         expires = vouchers.filter(expires__exact=timedifference)

#         for voucher in expires:
#             context = {'voucher': voucher}
#             send_ba_email('115_customer_voucher_expiry_prompt', context, recipients=voucher.cust, recipient_role='customer')

#         return HttpResponse(expires)



@CustomerRequiredDecorator
class CustomerProjectNotCompleteView(CustomerProjectMixin, FormView):
    form_class = CustomerProjectNotCompleteForm
    template_name = "customer/project_not_complete.html"
    success_url = reverse_lazy("customer_dashboard", args=("projects", ))

    permission = "cust_can_provide_feedback"

    def get_context_data(self, *args, **kwargs):
        context = super(CustomerProjectNotCompleteView, self).get_context_data(*args, **kwargs)
        context['project'] = self.project
        return context

    @transaction.atomic
    def form_valid(self, form):
        customer_proj_comm = form.save(commit=False)
        customer_proj_comm.project = self.project
        customer_proj_comm.save()

        try:
            payment = models.Payment.objects.get(proj=self.project)
            payment.has_complaint = True
            payment.save()
        except models.Payment.DoesNotExist:
            logger.warning("The project %s has no invoice even though it is marked completed" % self.project)

        # Send confirmation email to Customer and to Bigassist
        context = {'project': self.project,
                   'proj_comm': customer_proj_comm}

        send_ba_email('131_customer_says_project_not_complete', context, recipients=self.project.suppl, recipient_role='supplier')
        send_ba_email('132_customer_says_project_not_complete', context, recipients=self.project.cust, recipient_role='customer')
        send_ba_email('133_customer_says_project_not_complete', context)
        messages.info(self.request, 'Your complaint has been recorded.')
        return HttpResponseRedirect(self.get_success_url())


@CustomerRequiredDecorator
class CustomerProjectProblemsView(CustomerProjectMixin, FormView):
    template_name = "customer/project_report_problem.html"
    form_class = CustomerProjectProblemsForm
    success_url = reverse_lazy("customer_dashboard", args=("projects", ))

    permission = "cust_can_complain_about_project"

    def get_initial(self):
        initial = dict()
        initial['recipient'] = u'BIG Assist team'
        return initial

    def get_form_kwargs(self):
        kwargs = super(CustomerProjectProblemsView, self).get_form_kwargs()
        # copy data from initial to 'data' because the form requires these
        if 'data' in kwargs:
            kwargs['data'] = kwargs['data'].copy()
            kwargs['data']['recipient'] = kwargs['initial']['recipient']
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(CustomerProjectProblemsView, self).get_context_data(*args, **kwargs)
        context['project'] = self.project
        return context

    @transaction.atomic
    def form_valid(self, form):
        customer_proj_comm = form.save(commit=False)
        customer_proj_comm.project = self.project
        customer_proj_comm.save()

        context = {'project': self.project,
                   'proj_comm': customer_proj_comm}

        send_ba_email('141_customer_complains_about_project', context, recipients=self.project.suppl, recipient_role='supplier')
        send_ba_email('142_customer_complains_about_project', context, recipients=self.project.cust, recipient_role='customer')
        send_ba_email('143_customer_complains_about_project', context)

        messages.info(self.request, 'Your complaint has been recorded.')
        return HttpResponseRedirect(self.get_success_url())


@CustomerRequiredDecorator
class CustomerHostVisitPreView(FormView):
    template_name = 'customer/host_a_visit_pre.html'
    form_class = CustomerHostVisitPreForm
    success_url = reverse_lazy('customer_host_a_visit2')

    def get_context_data(self, *args, **kwargs):
        context = super(CustomerHostVisitPreView, self).get_context_data(*args, **kwargs)
        context['amount'] = models.BursaryTypes.objects.get(name='host').value
        return context

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        company = khnp_user.get_company()

        if company.visit_set.exists():
            # there is at least one existing visit - it means company
            # has accepted the conditions already and we can skip this pre-form.
            return HttpResponseRedirect(self.get_success_url())
        return super(CustomerHostVisitPreView, self).dispatch(request, *args, **kwargs)


@CustomerRequiredDecorator
class CustomerHostVisitView(CreateView):
    template_name = 'customer/host_a_visit.html'
    form_class = CustomerHostVisitForm
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()

        visit = form.save(commit=False)
        if form.cleaned_data['date_type'] == 'anytime':
            visit.date = None
        visit.host = company
        visit.save()
        form.save_m2m()
        context = dict(visit=visit)
        send_ba_email('261_customer_applies_as_host', context, recipients=company, recipient_role='customer')
        send_ba_email('262_customer_applies_as_host', context)
        return HttpResponseRedirect(self.success_url)


@CustomerRequiredDecorator
class CustomerVisitEditView(UpdateView):
    template_name = 'customer/host_a_visit_edit.html'
    model = models.Visit
    form_class = CustomerHostVisitForm
    success_url = reverse_lazy('customer_dashboard', args=['visitors'])

    def get_form_kwargs(self):
        kwargs = super(CustomerVisitEditView, self).get_form_kwargs()
        kwargs['initial']['date_type'] = 'fixed' if self.get_object().date else 'anytime'
        return kwargs

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()

        visit = form.save(commit=False)
        if form.cleaned_data['date_type'] == 'anytime':
            visit.date = None
        visit.host = company
        visit.save()
        form.save_m2m()

        return HttpResponseRedirect(self.success_url)


@CustomerRequiredDecorator
class CustomerVisitRemoveView(ConfirmationDetailView):
    model = models.Visit
    success_url = reverse_lazy('customer_dashboard', args=['visitors'])
    description = "You are about to completely remove this visit. Continue?"
    action = 'Yes'

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        obj = self.get_object()
        security = VisitSecurity(obj)
        if not security.can('remove_visit'):
            return HttpResponseRedirect(reverse('customer_dashboard', args=['visitors']))
        return super(CustomerVisitRemoveView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        # remove all visitors and visit instances
        obj.visitinstance_set.all().delete()
        obj.visitor_set.all().delete()
        obj.delete()
        return self.render_success(request)


@CustomerRequiredDecorator
class VisitGetInTouchView(CreateView):
    template_name = 'customer/visit_get_in_touch.html'
    form_class = VisitGetInTouchForm

    def dispatch(self, request, *args, **kwargs):
        self.visit = get_object_or_404(models.Visit, pk=kwargs['visit'])
        security = VisitSecurity(self.visit)
        err = security.error('get_in_touch')
        if err:
            return HttpResponseForbidden(err)
        return super(VisitGetInTouchView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('marketplace') + '?tab=connectspace'

    def get_context_data(self, **kwargs):
        context = super(VisitGetInTouchView, self).get_context_data(**kwargs)
        context['visit_pk'] = self.kwargs['visit']
        context['visit'] = self.visit
        return context

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()

        visitor = form.save(commit=False)
        visitor.visit = self.visit
        visitor.visitor_org = company
        visitor.save()
        form.save_m2m()

        context = {'visitor': visitor, 'host': self.visit.host}
        send_ba_email('151_visit_host_contacted_by_visitor', context, recipients=visitor.visitor_org, recipient_role='customer')
        send_ba_email('152_visit_host_contacted_by_visitor', context, recipients=self.visit.host, recipient_role='customer')
        messages.info(self.request, 'The contact message has been sent to %s' % self.visit.host)
        return HttpResponseRedirect(self.get_success_url())


@CustomerRequiredDecorator
class CustomerVisitorDiscardView(ConfirmationView):
    description = "You are about to discard your visit"
    action = "Discard"
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        visitor = get_object_or_404(models.Visitor, pk=self.kwargs['visitor'])
        security = VisitorSecurity(visitor)
        if not security.can('discard_visit'):
            messages.error(request, "This visit can't be discarded.")
        else:
            visitor.delete()
            messages.success(request, "The visit has been discarded")
        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerVisitorAcceptView(FormView):
    template_name = 'customer/visitor_accepted.html'
    form_class = VisitorArrangeMeetingForm
    success_url = reverse_lazy('customer_dashboard', args=['visitors'])

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.
        """
        kwargs = super(CustomerVisitorAcceptView, self).get_form_kwargs()
        kwargs['visit'] = self.visitor.visit
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CustomerVisitorAcceptView, self).get_context_data(**kwargs)
        context['visitor'] = self.visitor
        return context

    def dispatch(self, request, *args, **kwargs):
        self.visitor = get_object_or_404(models.Visitor, pk=kwargs['visitor'])
        security = VisitorSecurity(self.visitor)
        if not security.can('accept_visitor'):
            messages.error(request, "You are not allowed to accept this visitor")
            return HttpResponseRedirect(self.get_success_url())
        return super(CustomerVisitorAcceptView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        sdate = form.cleaned_data.get('date')
        visit = self.visitor.visit
        sponsorship_for_host = models.BursaryTypes.objects.get(name='host').value
        if sdate:
            date = datetime.datetime.strptime(sdate, settings.DATE_FORMAT_P)
            instance = models.VisitInstance.objects.create(visit=visit,
                                                   date=date,
                                                   amount=sponsorship_for_host)
        else:
            existing = form.cleaned_data.get('existing')
            if existing == '0':
                # fixed meeting date. There must be just single visitinstance
                instance, created = models.VisitInstance.objects.get_or_create(visit=visit,
                                                                      defaults={'date': visit.date,
                                                                                'amount': sponsorship_for_host})
            else:
                instance = get_object_or_404(models.VisitInstance, pk=existing)
        self.visitor.visit_instance = instance
        self.visitor.save()

        # if visitor has got approved sponsorship already, we must approve particular
        # visit instance as well. (Host is paid if at least one visitor is sponsored)
        # Refs. #188
        # Please note, very similar code is in AdminApproveRejectSponsorshipView.post
        if instance and instance.amount and not instance.bursary and self.visitor.bursary:
            instance.bursary = True
            instance.granted = datetime.datetime.now()
            instance.save()

        context = dict(visitor=self.visitor)
        send_ba_email('181_visit_host_decision_accept', context, recipients=self.visitor.visitor_org, recipient_role='customer')  # visitor
        send_ba_email('183_visit_host_decision_accept', context, recipients=self.visitor.visit.host, recipient_role='customer')  # host
        messages.success(self.request, "The visitor has been accepted for visit.")
        return super(CustomerVisitorAcceptView, self).form_valid(form)


@CustomerRequiredDecorator
class CustomerVisitorRejectView(ConfirmationView):
    description = "Do you really want to decline this visit request?"
    action = "Decline"
    layout = "bottom"

    def get_success_url(self):
        return reverse('customer_dashboard', args=['visitors'])

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.visitor = get_object_or_404(models.Visitor, pk=self.kwargs['visitor'])
        security = VisitorSecurity(self.visitor)
        if not security.can('reject_visitor'):
            messages.error(request, "You are not allowed to decline this visit request")
        else:
            self.visitor.delete()
            context = dict(visitor=self.visitor)
            send_ba_email('182_visit_host_decision_reject', context, recipients=self.visitor.visitor_org, recipient_role='customer')  # visitor
            send_ba_email('184_visit_host_decision_reject', context, recipients=self.visitor.visit.host, recipient_role='customer')  # host
            messages.success(request, "The visit request has been declined.")
        return self.render_success(request)


@LoginRequiredDecorator
class VisitorRequestsBursaryView(FormView):
    form_class = VisitorBursaryForm
    template_name = 'customer/visitor_requests_bursary.html'
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    def get_context_data(self, **kwargs):
        context = super(VisitorRequestsBursaryView, self).get_context_data(**kwargs)
        context['visitor'] = self.visitor
        context['amount'] = self.amount
        return context

    def dispatch(self, request, *args, **kwargs):
        self.visitor = get_object_or_404(models.Visitor, pk=kwargs['visitor'])
        security = VisitorSecurity(self.visitor)
        self.amount = models.BursaryTypes.objects.get(name='visitor').value
        if not security.can('apply_for_bursary'):
            messages.error(request, "You are not allowed to request a bursary")
            return HttpResponseRedirect(self.get_success_url())
        return super(VisitorRequestsBursaryView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        self.visitor.purpose = form.cleaned_data.get('purpose')
        self.visitor.benefit = form.cleaned_data.get('benefit')
        self.visitor.amount = self.amount
        self.visitor.save()
        data = dict(visitor=self.visitor)
        send_ba_email('161_visitor_applies_for_bursary', data, recipients=self.visitor.visitor_org, recipient_role='customer')
        send_ba_email('162_visitor_applies_for_bursary', data)
        messages.success(self.request, "The sponsorship request has been sent.")
        return super(VisitorRequestsBursaryView, self).form_valid(form)


@CustomerOrStaffRequiredDecorator
class VisitDetailView(DetailView):
    template_name = 'customer/visit_detail.html'
    context_object_name = 'visit'
    model = models.Visit

    def get_context_data(self, **kwargs):
        context = super(VisitDetailView, self).get_context_data(**kwargs)
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        visitor = self.get_object().visitor_set.filter(visitor_org=company)
        if visitor:
            context['visitor'] = visitor[0]
        return context


@CustomerRequiredDecorator
class VisitorVisitFeedbackView(UpdateView):
    model = models.Visitor
    context_object_name = "visitor"
    form_class = VisitorFeedbackForm
    template_name = "customer/visitor_feedback.html"
    success_url = reverse_lazy("customer_dashboard", args=['connectspace'])

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        visitor = self.get_object()
        security = VisitorSecurity(visitor)
        if not security.can('leave_feedback'):
            messages.error(request, "You are not allowed to provide feedback right now.")
            return HttpResponseRedirect(self.get_success_url())
        return super(VisitorVisitFeedbackView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        visitor = form.save(commit=False)
        visitor.feedback = datetime.datetime.now()
        visitor.save()
        return HttpResponseRedirect(self.get_success_url())


@CustomerRequiredDecorator
class CustomerBecomeMentorPreView(FormView):
    template_name = 'customer/become_a_mentor_pre.html'
    form_class = CustomerBecomeMentorPreForm
    success_url = reverse_lazy('customer_become_a_mentor2')

    def get_context_data(self, *args, **kwargs):
        context = super(CustomerBecomeMentorPreView, self).get_context_data(*args, **kwargs)
        context['amount'] = models.BursaryTypes.objects.get(name='mentor').value
        return context

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        company = khnp_user.get_company()

        if company.mentor_set.exists():
            # there is at least one existing Mentor - it means company
            # has accepted the conditions already and we can skip this pre-form.
            return HttpResponseRedirect(self.get_success_url())
        return super(CustomerBecomeMentorPreView, self).dispatch(request, *args, **kwargs)


@CustomerRequiredDecorator
class CustomerBecomeMentorView(CreateView):
    template_name = 'customer/become_a_mentor.html'
    form_class = CustomerBecomeMentorForm
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)

        if khnp_user.get_person().mentor_set.exists():
            # user is a mentor already. Disallow another mentoring instance. Refs. #181
            messages.warning(request, 'You are a Mentor already.')
            return HttpResponseRedirect(self.get_success_url())
        return super(CustomerBecomeMentorView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()

        mentor = form.save(commit=False)
        mentor.mentor = khnp_user.get_person()
        mentor.mentor_org = company
        mentor.created = datetime.datetime.now()
        mentor.save()
        form.save_m2m()
        context = dict(mentor=mentor)
        send_ba_email('281_customer_applies_as_mentor', context, recipients=company, recipient_role='customer')
        send_ba_email('282_customer_applies_as_mentor', context)
        return HttpResponseRedirect(self.success_url)


@CustomerOrStaffRequiredDecorator
class MentorDetailView(DetailView):
    template_name = 'customer/mentor_detail.html'
    context_object_name = 'mentor'
    model = models.Mentor

    def get_context_data(self, **kwargs):
        context = super(MentorDetailView, self).get_context_data(**kwargs)
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        mentee = self.get_object().mentee_set.filter(mentee_org=company)
        if mentee:
            context['mentee'] = mentee[0]
        return context


@CustomerRequiredDecorator
class CustomerMentorEditView(UpdateView):
    template_name = 'customer/mentor_edit.html'
    model = models.Mentor
    form_class = CustomerBecomeMentorForm
    success_url = reverse_lazy('customer_dashboard', args=['mentoring'])

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        person = khnp_user.get_person()

        visit = form.save(commit=False)
        visit.mentor = person
        visit.mentor_org = company
        visit.save()
        form.save_m2m()

        return HttpResponseRedirect(self.success_url)


@CustomerRequiredDecorator
class MentorGetInTouchView(CreateView):
    template_name = 'customer/mentor_get_in_touch.html'
    form_class = MentorGetInTouchForm

    def dispatch(self, request, *args, **kwargs):
        self.mentor = get_object_or_404(models.Mentor, pk=kwargs['pk'])
        security = MentorSecurity(self.mentor)
        err = security.error('get_in_touch')
        if err:
            return HttpResponseForbidden(err)
        return super(MentorGetInTouchView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('marketplace') + '?tab=connectspace'

    def get_context_data(self, **kwargs):
        context = super(MentorGetInTouchView, self).get_context_data(**kwargs)
        context['mentor_pk'] = self.kwargs['pk']
        context['mentor'] = self.mentor
        return context

    @transaction.atomic
    def form_valid(self, form):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()

        mentee = form.save(commit=False)
        mentee.mentor = self.mentor
        mentee.mentee_org = company
        mentee.save()
        form.save_m2m()

        context = {'customer': mentee.mentee_org, 'mentor': self.mentor, 'message': mentee.contact_msg}
        send_ba_email('201_customer_contacts_mentor_cust', context, recipients=mentee.mentee_org, recipient_role='customer')
        send_ba_email('202_customer_contacts_mentor_mentor', context, recipients=self.mentor.mentor_org, recipient_role='customer')
        messages.info(self.request, u'The contact message has been sent to %s' % self.mentor.mentor)
        return HttpResponseRedirect(self.get_success_url())


@CustomerRequiredDecorator
class CustomerMenteeRejectAcceptView(ConfirmationDetailView):
    model = models.Mentee
    accept = None
    success_url = reverse_lazy('customer_dashboard', args=['mentoring'])

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        mentee = self.get_object()
        security = MenteeSecurity(mentee)
        if not security.can('acceptreject_mentee'):
            messages.error(request, "You are not allowed to accept or reject this mentee")
            return HttpResponseRedirect(self.get_success_url())
        return super(CustomerMenteeRejectAcceptView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        if self.accept:
            obj.set_state('agreed')
        else:
            obj.set_state('discarded')

        if self.accept:
            context = dict(mentee=obj.mentee_org,
                           mentor=obj.mentor.mentor)
            send_ba_email('231_mentor_accepts_customer_mentee', context, recipients=obj.mentee_org, recipient_role='customer')
            send_ba_email('232_mentor_accepts_customer_mentor', context, recipients=obj.mentor.mentor_org, recipient_role='customer')
            messages.success(self.request, "The mentee has been accepted.")
        else:
            messages.success(self.request, "The mentee has been rejected.")
        return self.render_success(request)


# This is AJAX POST request from dashboard_mentoring
class MentorMenteeArrangeDateView(View):

    def dispatch(self, request, *args, **kwargs):
        mpk = request.POST.get('mentee')
        date_id = request.POST.get('did')
        date = request.POST.get('value')
        try:
            self.value = datetime.datetime.strptime(date, settings.DATE_FORMAT_P)
        except:
            self.value = None
        self.mentee = get_object_or_404(models.Mentee, pk=mpk)
        if date_id.isdigit() and (1 <= int(date_id) <= 4):
            self.date_id = date_id
            return super(MentorMenteeArrangeDateView, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden('Invalid date')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        if self.value:
            now = datetime.datetime.now()
            if self.value.date() < now.date():
                return HttpResponse('PAST')
            else:
                field_name = 'date' + self.date_id
                setattr(self.mentee, field_name, self.value)
                self.mentee.save()
                date = getattr(self.mentee, field_name)
                str_date = date.strftime(settings.DATE_FORMAT_P)
                context = dict(date=str_date,
                               mentor=self.mentee.mentor.mentor)
                send_ba_email('241_mentor_adds_date_mentee', context, recipients=self.mentee.mentee_org, recipient_role='customer')
                send_ba_email('242_mentor_adds_date_mentor', context, recipients=self.mentee.mentor.mentor_org, recipient_role='customer')

                return HttpResponse(str_date)
        return HttpResponse('')


@CustomerRequiredDecorator
class CustomerMenteeDiscardView(ConfirmationView):
    description = "You are about to discard your mentoring"
    action = "Discard"
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        mentee = get_object_or_404(models.Mentee, pk=self.kwargs['pk'])
        security = MenteeSecurity(mentee)
        if not security.can('discard_mentee'):
            messages.error(request, "This mentoring can't be discarded.")
        else:
            mentee.delete()
            messages.success(request, "The mentoring has been discarded")
        return self.render_success(request)


@CustomerRequiredDecorator
class CustomerMenteeCompleteView(ConfirmationView):
    description = "You are about to complete this mentoring"
    action = "Complete"
    success_url = reverse_lazy('customer_dashboard', args=['mentoring'])
    complete_early = None

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        mentee = get_object_or_404(models.Mentee, pk=self.kwargs['pk'])
        security = MenteeSecurity(mentee)
        ok = True
        if self.complete_early and not security.can('complete_early'):
            messages.error(request, "This mentoring can't be completed yet.")
            ok = False
        elif (not self.complete_early) and not security.can('complete'):
            messages.error(request, "This mentoring can't be completed yet.")
            ok = False

        if ok:
            if self.complete_early:
                mentee.completed_early = True
            mentee.set_state('completed', commit=False)
            mentee.save()
            # Raise a invoice
            payment = mentee.invoice()
            if payment:
                context = dict(
                    mentee=mentee,
                    payment=payment,
                    )
                send_ba_email('251_mentoring_complete_mentor', context, recipients=mentee.mentor.mentor_org, recipient_role='customer')
                send_ba_email('252_mentoring_complete_mentee', context, recipients=mentee.mentee_org, recipient_role='customer')

            messages.success(request, "The mentoring has been completed")
        return self.render_success(request)


@LoginRequiredDecorator
class MenteeRequestsBursaryView(FormView):
    form_class = VisitorBursaryForm  # using the same form
    template_name = 'customer/mentee_requests_bursary.html'
    success_url = reverse_lazy('customer_dashboard', args=['connectspace'])

    def get_context_data(self, **kwargs):
        context = super(MenteeRequestsBursaryView, self).get_context_data(**kwargs)
        context['mentee'] = self.mentee
        context['amount'] = self.amount
        return context

    def dispatch(self, request, *args, **kwargs):
        self.mentee = get_object_or_404(models.Mentee, pk=kwargs['mentee'])
        security = MenteeSecurity(self.mentee)
        self.amount = models.BursaryTypes.objects.get(name='mentor').value
        if not security.can('apply_for_bursary'):
            messages.error(request, "You are not allowed to request a bursary")
            return HttpResponseRedirect(self.get_success_url())
        return super(MenteeRequestsBursaryView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        self.mentee.purpose = form.cleaned_data.get('purpose')
        self.mentee.benefit = form.cleaned_data.get('benefit')
        self.mentee.amount = self.amount
        self.mentee.save()
        data = dict(mentee=self.mentee)
        send_ba_email('211_mentee_applies_for_bursary', data, recipients=self.mentee.mentee_org, recipient_role='customer')
        send_ba_email('212_mentee_applies_for_bursary', data)
        messages.success(self.request, "The sponsorship request has been received.")
        return super(MenteeRequestsBursaryView, self).form_valid(form)


@CustomerRequiredDecorator
class MenteeFeedbackView(UpdateView):
    model = models.Mentee
    context_object_name = "mentee"
    # Reusing VisitorFeedbackForm because it is the same for mentee
    form_class = VisitorFeedbackForm
    template_name = "customer/mentee_feedback.html"
    success_url = reverse_lazy("customer_dashboard", args=['connectspace'])

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        mentee = self.get_object()
        security = MenteeSecurity(mentee)
        if not security.can('leave_feedback'):
            messages.error(request, "You are not allowed to provide feedback right now.")
            return HttpResponseRedirect(self.get_success_url())
        return super(MenteeFeedbackView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        mentee = form.save(commit=False)
        mentee.feedback = datetime.datetime.now()
        mentee.save()
        return HttpResponseRedirect(self.get_success_url())


def initialize_customer_diagnostic(company, state=None):

    model = get_xform('C_customer_self_assessment_model.xml')

    if state is None:
        diag = models.CustomerDiagnostic.objects.create(cust=company, diag_questions=model)
    else:
        diag = models.CustomerDiagnostic.objects.create(cust=company, diag_questions=model, state=state)
    diag_id = diag.pk
    manip = XMLManipulator(model)
    manip.set_attribute('./tm:assist_customer', 'appl_no', diag_id)
    diag.diag_questions = manip.tostring()
    diag.save()

    return diag


class GenerateVisitPayment(View):

    def get(self, request, *args, **kwargs):
        result = []
        now = datetime.datetime.now().date()
        for row in models.VisitInstance.objects.filter(date__lt=now, payment__isnull=True, bursary=True):
            with transaction.commit_on_success():
                payment = row.invoice()
                if payment:
                    result.append('Generated payment (host) %d %s' % (payment.pk, payment))
                    context = dict(
                        visit=row.visit,
                        visit_instance=row,
                        payment=payment,
                        )
                    send_ba_email('191_visit_complete_host', context, recipients=row.visit.host, recipient_role='customer')
                    result.append('Email to host %s has been sent' % (row.visit.host))
                    result.append('')

        for row in models.Visitor.objects.filter(visit_instance__date__lt=now, payment__isnull=True, bursary=True):
            with transaction.commit_on_success():
                payment = row.invoice()
                if payment:
                    result.append('Generated payment (visitor) %d %s' % (payment.pk, payment))
                    context = dict(
                        visit=row.visit,
                        visit_instance=row.visit_instance,
                        payment=payment,
                        )
                    send_ba_email('192_visit_complete_visitor', context, recipients=row.visitor_org, recipient_role='customer')
                    result.append('Email to host %s has been sent' % (row.visitor_org))
                    result.append('')

        if not result:
            result = ['No payment has been generated.']
        return HttpResponse('\n'.join(result), content_type='text/plain')