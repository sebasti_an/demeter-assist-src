#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from bigassist_core import models
from bigassist_core import admin_forms
from django.contrib.admin import SimpleListFilter


class IsSubmittedFilter(SimpleListFilter):
    title = 'Approval submitted'
    parameter_name = 'is_submitted'

    def lookups(self, request, model_admin):
        return (
            ('Yes', "Yes"),
            ('No', "No"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'Yes':
            return queryset.filter(submitted__isnull=False)
        if self.value() == 'No':
            return queryset.filter(submitted__isnull=True)


class XFormsAdmin(admin.ModelAdmin):

    form = admin_forms.XFormsAdminForm


class OrganisationAdmin(admin.ModelAdmin):
    raw_id_fields = ['org']
    search_fields = ['org__displayname', 'detail', 'org__contact__value']
    # list_filter = ('is_supplier', 'org_type') 
    # Change is_supplier to prog__role as is_supplier is now a method 
    # thus cannot be filtered on
    list_filter = ('prog__role', 'org_type', )


class ContactAdmin(admin.ModelAdmin):
    raw_id_fields = ['person']
    search_fields = ['value']
    list_filter = ('type', 'use')


class RelationshipAdmin(admin.ModelAdmin):
    raw_id_fields = ['subject', 'object']
    search_fields = ['subject__displayname', 'object__displayname']
    list_filter = ['type']


class SupplierTopicsAdmin(admin.ModelAdmin):
    raw_id_fields = ['suppl']


class PersonAdmin(admin.ModelAdmin):
    search_fields = ['displayname']
    list_filter = ('type',)


class IdentityAdmin(admin.ModelAdmin):
    search_fields = ['persona']
    raw_id_fields = ['person']


class SupplierApprovalAdmin(admin.ModelAdmin):
    raw_id_fields = ['suppl', 'prev_appr', 'assessor']
    search_fields = ['suppl__org__displayname', 'panel_text']
    list_filter = (IsSubmittedFilter, 'is_approved')
    search_fields = ['suppl__org__pk', 'suppl__org__displayname']


class CustomerProjectAdmin(admin.ModelAdmin):
    raw_id_fields = ['cust', 'suppl']
    search_fields = ['cust__org__displayname', 'suppl__org__displayname']


class VisitAdmin(admin.ModelAdmin):
    raw_id_fields = ['host']


class VisitorAdmin(admin.ModelAdmin):
    raw_id_fields = ['visitor_org']


class MentorAdmin(admin.ModelAdmin):
    raw_id_fields = ['mentor', 'mentor_org']


class MenteeAdmin(admin.ModelAdmin):
    raw_id_fields = ['mentee_org']


class CustomerVoucherAdmin(admin.ModelAdmin):
    raw_id_fields = ['cust']
    search_fields = ['cust__org__displayname']


class InvitedUserAdmin(admin.ModelAdmin):
    raw_id_fields = ['org', 'user', 'person']
    search_fields = ['org__org__displayname', 'email']
    list_filter = ['registered']


admin.site.register(models.XForms, XFormsAdmin)
admin.site.register(models.ContactType)
admin.site.register(models.PersonType)
admin.site.register(models.Person, PersonAdmin)
admin.site.register(models.Identity, IdentityAdmin)
admin.site.register(models.InvitedUser, InvitedUserAdmin)
admin.site.register(models.Use)
admin.site.register(models.Contact, ContactAdmin)
admin.site.register(models.LocalAuthority)
admin.site.register(models.Address)
admin.site.register(models.RelationshipType)
admin.site.register(models.Relationship, RelationshipAdmin)
admin.site.register(models.OrgType)
admin.site.register(models.Organisation, OrganisationAdmin)
admin.site.register(models.CustomerPrequal)
admin.site.register(models.CustomerDiagnostic)
admin.site.register(models.CustomerDevPri)
admin.site.register(models.CommMethod)
admin.site.register(models.CustomerCommType)
admin.site.register(models.CustomerCommLog)
admin.site.register(models.CustomerVoucher, CustomerVoucherAdmin)
admin.site.register(models.CustomerProject, CustomerProjectAdmin)
admin.site.register(models.DevDuration)
admin.site.register(models.DevPriLev)
admin.site.register(models.DevSupportType)
admin.site.register(models.SupportTopic)
admin.site.register(models.SupplierApproval, SupplierApprovalAdmin)
admin.site.register(models.SupportArea)
admin.site.register(models.SupplierTopics, SupplierTopicsAdmin)
admin.site.register(models.Region)
admin.site.register(models.Visit, VisitAdmin)
admin.site.register(models.Visitor, VisitorAdmin)
admin.site.register(models.VisitInstance)
admin.site.register(models.Mentor, MentorAdmin)
admin.site.register(models.Mentee, MenteeAdmin)
admin.site.register(models.SupplierDeadline)
admin.site.register(models.MailTemplate)
admin.site.register(models.Report)
admin.site.register(models.BursaryTypes)
admin.site.register(models.Payment)


class NewsItemAdmin(admin.ModelAdmin):

    prepopulated_fields = {"slug": ("title",)}

admin.site.register(models.NewsItem, NewsItemAdmin)
