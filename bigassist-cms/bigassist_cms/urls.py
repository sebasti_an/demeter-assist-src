#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from bigassist_cms.views import *

urlpatterns = patterns('',
	url(r'^photos/upload/$', 'bigassist_cms.views.upload_photos', name='cms_upload_photos'),
	url(r'^photos/recent/$', 'bigassist_cms.views.recent_photos', name='cms_recent_photos'),

    url(r'^page/(?P<section_slug>.*)/(?P<slug>.*)/', PageDetailView.as_view(), name='cms_page'),
    url(r'^section/(?P<section_slug>.*)/', PageListView.as_view(), name='cms_section'),

    url(r'^events/', EventListView.as_view(), name='cms_events'),
)