from django.core.urlresolvers import reverse
from django.db import connection
from bigassist_core.user import KHNPUser
from bigassist_core.models import Organisation
from bigassist_core.models import Person
from django.contrib.auth.models import User
from django.conf import settings
from .base import TestCase
from django.core import mail
from bigassist_core import version
from bigassist_core import helpers
from bigassist_core.emails import send_ba_email


class BigAssistTest(TestCase):

    fixtures = ['types', 'mails']

    def test_version(self):
        self.failUnless(isinstance(version.VERSION, tuple))

    def test_ba_email(self):
        data = dict(company_owner='John Doe',
                    person='Paul Doe',
                    organisation='Doe & co',
                    person_email='paul@doe.com',
                    message='Hello!',
                    organisation_pk=1,
                    person_pk=1)
        send_ba_email('30_person_contacts_company_owner', data, sender=None, recipients=None, bcc=None, cc=None)
        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, settings.SERVER_EMAIL)
        self.assertEqual(msg.to, list(settings.BIGASSIST_ADMIN_EMAIL))
        self.assertEqual(msg.cc, [])
        self.assertEqual(msg.bcc, [])
        self.assertEqual(msg.subject, 'BIG Assist: please approve new user')
        self.failUnless('Dear John Doe' in msg.body)
        self.failUnless("Paul Doe has asked to be added as a user of Doe &amp; co's account on http://www.bigassist.org.uk.<br />" in msg.body)

        send_ba_email('30_person_contacts_company_owner', data, sender='test@khnp.org', recipients=None, bcc=None, cc=None)
        self.assertEqual(len(mail.outbox), 2)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, 'test@khnp.org')
        self.assertEqual(msg.to, list(settings.BIGASSIST_ADMIN_EMAIL))
        self.assertEqual(msg.cc, [])
        self.assertEqual(msg.bcc, [])

        send_ba_email('30_person_contacts_company_owner', data, sender='test@khnp.org', recipients='test2@khnp.org', bcc=None, cc=None)
        self.assertEqual(len(mail.outbox), 3)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, 'test@khnp.org')
        self.assertEqual(msg.to, ['test2@khnp.org'])
        self.assertEqual(msg.cc, [])
        self.assertEqual(msg.bcc, [])

        send_ba_email('30_person_contacts_company_owner', data, sender='test@khnp.org', recipients=['test2@khnp.org'], bcc='info@fry-it.com', cc=['radim@fry-it.com'])
        self.assertEqual(len(mail.outbox), 4)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, 'test@khnp.org')
        self.assertEqual(msg.to, ['test2@khnp.org'])
        self.assertEqual(msg.cc, ['radim@fry-it.com'])
        self.assertEqual(msg.bcc, ['info@fry-it.com'])

    def test_ba_email_organisation(self):
        self.user = User.objects.create_user(username='testuser', email='test3@khnp.org', password='secret')
        user = KHNPUser(self.user)
        person = user.get_person()
        company = Organisation.objects.register(person, defaults={'is_supplier': False})
        company.org.set_contact('email_direct', 'test2@khnp.org')
        company.org.displayname = 'John Doe'
        company.org.save()

        data = dict(company_owner='John Doe',
                    person='Paul Doe',
                    organisation='Doe & co',
                    person_email='paul@doe.com',
                    message='Hello!',
                    organisation_pk=company.pk,
                    person_pk=person.pk)
        send_ba_email('30_person_contacts_company_owner', data, sender=None, recipients=company)
        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, settings.SERVER_EMAIL)
        self.assertEqual(msg.to, ['test2@khnp.org'])  # email_direct
        self.assertEqual(msg.cc, ['test3@khnp.org'])  # person (manager)

        # let's add another user to the org. to be able to check autometic Cc
        person2 = Person.objects.create(firstname='George', surname='Doe',
                                        displayname='George Doe',
                                        type_id=settings.KHNP_PERSON_TYPE)
        cursor = connection.cursor()
        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [person2.pk, company.org.pk,
                               settings.KHNP_MEMBER_TYPE_ID])

        person2.set_contact('email', 'george@khnp.org')

        send_ba_email('30_person_contacts_company_owner', data, sender=None, recipients=company)
        self.assertEqual(len(mail.outbox), 2)
        msg = mail.outbox[-1]
        self.assertEqual(msg.from_email, settings.SERVER_EMAIL)
        self.assertEqual(msg.to, ['test2@khnp.org'])  # email_direct
        self.assertEqual(msg.cc, ['test3@khnp.org', 'george@khnp.org'])

    def test_urls(self):
        """ we use some URLs in javascript so test they did not change """
        self.assertEqual(reverse('lookup_charity_template'), '/lookup/charity/', msg='URL changed. Please check bigassist-org.js')
        self.assertEqual(reverse('lookup_company_template'), '/lookup/company/', msg='URL changed. Please check bigassist-org.js')
        self.assertEqual(reverse('check_company'), '/check_company/', msg='URL changed. Please check bigassist-org.js')

    def test_date_passed(self):
        from datetime import datetime
        now = datetime(2013, 1, 1, 15, 0, 0)
        self.assertTrue(helpers.date_passed(datetime(2012, 12, 31), now))
        self.assertTrue(helpers.date_passed(datetime(2012, 12, 31).date(), now))
        # checking 0:00 agains 15:00 so it has passed already
        self.assertTrue(helpers.date_passed(datetime(2013, 1, 1), now))
        # but from the date point of view, it has not passed yet
        self.assertFalse(helpers.date_passed(datetime(2013, 1, 1).date(), now))
        self.assertFalse(helpers.date_passed(datetime(2013, 1, 2), now))
        self.assertFalse(helpers.date_passed(datetime(2013, 1, 2).date(), now))

        # force date check
        self.assertFalse(helpers.date_passed(datetime(2013, 1, 1), now, force_date_check=True))
        self.assertFalse(helpers.date_passed(datetime(2013, 1, 1).date(), now, force_date_check=True))
