import datetime
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from base import TestCase

from bigassist_core.models import SupplierDeadline, Organisation, SupportTopic, \
    SupplierApproval
from bigassist_core.views.supplier import initialize_supplier_approval
from bigassist_core.user import KHNPUser


class PanelTest(TestCase):

    fixtures = ['types', 'xforms']

    def setUp(self):
        super(PanelTest, self).setUp()

        # create a Django user
        self.user = User.objects.create_superuser(username='testuser', email='test@khnp.org', password='secret')

    def create_approval(self, username, email):
        user = User.objects.create_user(username=username, email=email, password='secret')
        khnpuser = KHNPUser(user).get_person()
        company = Organisation.objects.register(khnpuser, {'is_supplier': True})
        initialize_supplier_approval(company)

        approval = company.get_diagnostic()
        approval.set_state('submitted')
        approval.set_state('reviewed')

        return approval

    def test_no_deadline(self):
        self.client.login(username='testuser', password='secret')
        url = reverse('panel_dashboard')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        deadline = SupplierDeadline.objects.create(deadline=datetime.date.today())
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, deadline.deadline.strftime(settings.DATE_FORMAT_P))
        self.assertContains(response, "There are currently no submissions to review")

    def test_dashboard(self):
        url = reverse('panel_dashboard')
        self.client.login(username='testuser', password='secret')
        deadline = SupplierDeadline.objects.create(deadline=datetime.date.today())
        topic = SupportTopic.objects.all()[0]

        approval = self.create_approval(username='testuser2', email='test2#khnp.org')
        approval.approvaltopic_set.create(topic=topic)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        self.assertNotContains(response, "There are currently no submissions to review")
        self.assertContains(response, "1 new submission")
        self.assertContains(response, "1 review uncompleted")
        self.assertContains(response, "1 %s" % topic.area.area)

        self.assertNotContains(response, "go_to_submission")
        self.assertContains(response, "start_panel")
        self.assertNotContains(response, "complete_panel")

        # Start panel
        review_url = reverse('panel_approval_review', args=(approval.pk, ))
        response = self.client.post(url, {'start_panel': 'clicked'})
        self.assertRedirects(response, review_url)

        response = self.client.get(review_url)
        self.assertEqual(response.status_code, 200)

        data = {
            'approved': 0,
            'topics': [approval.approvaltopic_set.get().pk, ],
            'panel_text': "Rejected",
        }

        response = self.client.post(review_url, data)

        self.assertRedirects(response, url)

        # Check the progress on dashboard
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        self.assertNotContains(response, "There are currently no submissions to review")
        self.assertContains(response, "1 new submission")
        self.assertContains(response, "1 review completed")
        self.assertContains(response, "1 %s" % topic.area.area)

        self.assertContains(response, "go_to_submission")
        self.assertContains(response, "complete_panel")

        # complete panel
        response = self.client.post(url, {'complete_panel': 'clicked'})
        self.assertRedirects(response, url)

        # check as a supplier
        self.client.login(username="testuser2", password='secret')
        supplier_url = reverse('supplier_dashboard', args=('assessment', ))
        response = self.client.get(supplier_url)
        self.assertContains(response, "Edit and reapply")

        resubmit_url = reverse('supplier_resubmit_assessment')
        response = self.client.post(resubmit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, supplier_url)

        self.client.login(username='testuser', password='secret')

        # Hack to restart panel
        deadline.set_state('not_started')

        # Get the new approval
        approval = SupplierApproval.objects.get(pk=approval.pk)
        self.assertEqual(approval.state, 'rejected')
        approval = approval.suppl.get_diagnostic()
        self.assertEqual(approval.state, 'in_progress')
        approval.set_state('submitted')
        approval.set_state('reviewed')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        self.assertNotContains(response, "There are currently no submissions to review")
        self.assertContains(response, "1 resubmission")
        self.assertContains(response, "1 review uncompleted")
        self.assertContains(response, "1 %s" % topic.area.area)

        self.assertNotContains(response, "go_to_submission")
        self.assertNotContains(response, "complete_panel")

        # Start the panel
        review_url = reverse('panel_approval_review', args=(approval.pk, ))
        response = self.client.post(url, {'start_panel': 'clicked'})
        self.assertRedirects(response, review_url)

        response = self.client.get(review_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Status: Resubmission")

        data = {
            'approved': 1,
            'topics': [approval.approvaltopic_set.get().pk, ],
            'panel_text': "Accepted",
        }

        response = self.client.post(review_url, data)

        self.assertRedirects(response, url)

        response = self.client.get(url)
        self.assertContains(response, "1 resubmission")
        self.assertContains(response, "1 review completed")

        # Complete
        response = self.client.post(url, {'complete_panel': 'clicked'})
        self.assertRedirects(response, url)

        # Check supplier
        self.client.login(username="testuser2", password='secret')
        supplier_url = reverse('supplier_dashboard', args=('assessment', ))
        response = self.client.get(supplier_url)

        # FIXME - how to check its accepted?

        # Not reject and remove
        # using a heck to reject approval
        approval.set_state('rejected')
        self.client.login(username="testuser2", password='secret')
        supplier_url = reverse('supplier_dashboard', args=('assessment', ))
        response = self.client.get(supplier_url)
        self.assertContains(response, "Edit and reapply")

        resubmit_url = reverse('supplier_resubmit_assessment')
        response = self.client.post(resubmit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, supplier_url)

        self.client.login(username='testuser', password='secret')

        # Hack to restart panel
        deadline.set_state('not_started')

        # Get the new approval
        approval = SupplierApproval.objects.get(pk=approval.pk)
        self.assertEqual(approval.state, 'rejected')
        approval = approval.suppl.get_diagnostic()
        self.assertEqual(approval.state, 'in_progress')
        approval.set_state('submitted')
        approval.set_state('reviewed')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        # Start the panel
        review_url = reverse('panel_approval_review', args=(approval.pk, ))
        response = self.client.post(url, {'start_panel': 'clicked'})
        self.assertRedirects(response, review_url)

        response = self.client.get(review_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Status: Resubmission")

        data = {
            'approved': -1,
            'topics': [approval.approvaltopic_set.get().pk, ],
            'panel_text': "Removed",
        }

        response = self.client.post(review_url, data)

        self.assertRedirects(response, url)

        response = self.client.get(url)
        self.assertContains(response, "1 resubmission")
        self.assertContains(response, "1 review completed")

        # Complete so the organisation is removed
        response = self.client.post(url, {'complete_panel': 'clicked'})
        self.assertRedirects(response, url)

        with self.assertRaises(Organisation.DoesNotExist):
            Organisation.objects.get(pk=approval.suppl.pk)
