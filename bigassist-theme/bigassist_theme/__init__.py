from bigassist_theme import version

VERSION = version.VERSION
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))
