import tempfile
import urlparse
import os
import shutil
from subprocess import Popen, PIPE

from celery import task
from celery.result import AsyncResult
from celery import current_task

from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.files import File
from bigassist_core.pdf import pdf
from bigassist_core import models


def get_celery_result(task_id):
    return AsyncResult(task_id)


@task()
def celery_make_pdf(deadline_id, approval_ids):
    # This import would cause a circular imports.
    from bigassist_core.utils import create_token
    temp_dir = tempfile.mkdtemp()
    stderrdata = None
    domain = settings.XFORMS_TO_PDF_BASE_URL
    deadline = models.SupplierDeadline.objects.get(pk=deadline_id)
    total_approvals = len(approval_ids)
    try:
        for idx, approval_id in enumerate(approval_ids):
            try:
                approval = models.SupplierApproval.objects.get(pk=approval_id)
            except models.SupplierApproval.DoesNotExist:
                continue
            org = approval.suppl
            token = create_token(org)
            path = reverse('admin_supplier_assessment_html', args=[org.pk])
            url = urlparse.urljoin(domain, path) + '?t=%s' % token
            fp, filename = tempfile.mkstemp(prefix="%.3d-" % idx, suffix='.pdf', dir=temp_dir)
            current_task.update_state(state='PROGRESS',
                                      meta={'current': idx + 1, 'total': total_approvals})
            pdf(url, filename, temp_dir)
        # generate single PDFs from all the PDF files in order
        fp, result_fname = tempfile.mkstemp(prefix='combined', suffix='.pdf', dir=temp_dir)
        # we must remove the file first
        os.unlink(result_fname)
        # command = 'pdftk %s/*.pdf cat output %s' % (temp_dir, result_fname)
        command = 'gs -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=%s %s/*.pdf' % (result_fname, temp_dir)
        proc = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        stdoutdata, stderrdata = proc.communicate()
        if not stderrdata:
            # save pdf to deadline object
            deadline.pdf_file.save('assessment-%s.pdf' % deadline.pk,
                                   File(open(result_fname, 'rb')))
            return None
        else:
            return 'ERROR: %s' % stderrdata
    finally:
        shutil.rmtree(temp_dir)
