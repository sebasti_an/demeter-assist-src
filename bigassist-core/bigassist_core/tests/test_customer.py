from django.conf import settings
import datetime
from django.core.urlresolvers import reverse
from django.core import mail
from django.contrib.auth.models import User
from bigassist_core.user import KHNPUser
from bigassist_core.models import Organisation, CustomerPrequal
from bigassist_core.models import CustomerDiagnostic, CommMethod
from bigassist_core.models import CustomerProject, ProjCommType
from bigassist_core.models import SupportTopic, DevPriLev, DevDuration, \
    DevSupportType, Visit, Region, Visitor, Mentor, Mentee, SupportArea, CustomerVoucher, \
    Payment
from bigassist_core.models import CustomerProjectComm, InvitedUser
from bigassist_core.models import CustomerCommType
from bigassist_core.models import CustomerCommLog
from bigassist_core.views.customer import initialize_customer_diagnostic
from lxml import etree

from .base import TestCase, XMLTestHelper

EMAIL = 'test@khnp.org'
CUSTOMER_DIAG_COMMENTS = '<tm:assist_customer_admin xmlns:tm="http://textmatters.com"><tm:strategy/>\n    <tm:finance/>\n    <tm:innovation/>\n    <tm:relationships/>\n    <tm:people/>\n    <tm:summary>Test</tm:summary>\n</tm:assist_customer_admin>'


class CustomerTest(TestCase):

    fixtures = ['types', 'xforms', 'mails']

    def setUp(self):
        super(CustomerTest, self).setUp()
        # create a Django user
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.adminuser = User.objects.create_superuser(username='admin', email='test2@khnp.org', password='secret')

    def test_eligibility_form_submit(self):
        url = reverse('customer_qualification_save')
        res = self.client.post(url, data='INVALID XML', content_type='text/plain')
        self.failUnless('browserid-login' in res.content)
        self.client.login(username='testuser', password='secret')
        user = KHNPUser(self.user)
        person = user.get_person()
        # we need to create organization before user can fill eligibility form
        company = Organisation.objects.register(person,
                                                defaults={'is_supplier': False})

        res = self.client.post(url, data='INVALID XML', content_type='text/xml')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content, 'ERROR: Invalid XML content')

        res = self.client.post(url, data='', content_type='text/plain')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content, 'ERROR: Invalid XML content')

        # create Diagnostic record but without Prequal record. This should really never happen though
        CustomerDiagnostic.objects.create(cust=company, diag_questions='<root xmlns:tm="http://textmatters.com" />')
        res = self.client.post(url, data='<data />', content_type='text/xml')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content, 'Duplicate submission (diagnostic already exists)')

        # remove the diagnostic record
        CustomerDiagnostic.objects.filter(cust=company).delete()

        # this is ideal situation, but customer has failed :(
        xml = """<tm:assist_cust_pre xmlns:tm="http://textmatters.com">
        <tm:decision>DISQUALIFIED</tm:decision>
        </tm:assist_cust_pre>"""
        res = self.client.post(url, data=xml, content_type='text/xml')
        self.assertRedirects(res, reverse('customer_qualify_failed'))

        # ok, remove the prequal record and try again - we can't submit more tha one prequal.
        CustomerPrequal.objects.filter(cust=company).delete()

        # since we expect correct result and redirect to DiagnosticView, we need to fill
        # a Xforms table
        xml = """<tm:assist_cust_pre xmlns:tm="http://textmatters.com">
        <tm:decision>PASSED</tm:decision>
        </tm:assist_cust_pre>"""
        res = self.client.post(url, data=xml, content_type='text/xml')
        self.assertRedirects(res, reverse('home'))

        # that's all in real life. CustomerPrequal record should be created and contain the XML data
        # Let's call the POST again
        res = self.client.post(url, data='<data xmlns:tm="http://textmatters.com" />', content_type='text/xml')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content, 'ERROR: Duplicate submission')

    def test_diagnostic_form_model(self):
        self.client.login(username='testuser', password='secret')
        user = KHNPUser(self.user)
        person = user.get_person()
        # we need to create organization before user can fill eligibility form
        Organisation.objects.register(person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 2})

        url = reverse('customer_diagnostic_xml_model')
        res = self.client.get(url)

        root = etree.XML(res.content)
        elements = root.xpath('/tm:assist_customer/tm:organisation/tm:org_type_id', namespaces=root.nsmap)
        self.failUnless(len(elements) == 1)
        element = elements[0]
        self.assertEqual(element.text, '2')  # org_type_id

    def test_customer_process(self):
        self.client.login(username='testuser', password='secret')
        user = KHNPUser(self.user)
        person = user.get_person()
        # we need to create organization before user can fill eligibility form
        Organisation.objects.register(person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 2})

        # Display the form
        res = self.client.get(reverse('customer_qualification'))
        self.assertEqual(res.status_code, 200)
        # Submit (there is no quick save) the eligibility form
        data = """<tm:assist_cust_pre xmlns:tm="http://textmatters.com">
  <tm:decision>PASSED</tm:decision>
</tm:assist_cust_pre>"""
        res = self.client.post(reverse('customer_qualification_save'),
                               data=data, content_type='text/xml')
        self.assertRedirects(res, reverse('home'))

        # What can we see it on dashboard ?
        res = self.client.get(reverse('home'))
        company = user.get_company()
        diagnostic = company.get_diagnostic()
        self.failUnlessEqual(diagnostic.state, 'in_progress')
        self.failUnless(diagnostic.get_state_display() in res.content.decode('utf8'))
        self.failUnless('Continue self-assessment' in res.content)

        # Ok, let's continue diagnostic
        res = self.client.get(reverse('customer_diagnostic_xml_model'))
        # check if org_type_id is set
        self.failUnless('<tm:org_type_id>2</tm:org_type_id>' in res.content)

        res = self.client.get(reverse('customer_diagnostic'))
        self.assertContains(res, '<div class="xforms-row">')

        data = """<tm:assist_customer xmlns:tm="http://textmatters.com">
  <tm:organisation>
      <tm:org_type_id>2</tm:org_type_id>
  </tm:organisation>
  <tm:whatSuccessLooksLike>1</tm:whatSuccessLooksLike>
</tm:assist_customer>"""
        # finally save the diagnostic
        res = self.client.post(reverse('customer_diagnostic_submit'),
                               data=data, content_type='text/xml')
        self.assertRedirects(res, reverse('home'))
        # check diagnostic state
        res = self.client.get(reverse('home'))
        diagnostic = company.get_diagnostic()
        self.failUnlessEqual(diagnostic.state, 'submitted')
        self.failUnless(diagnostic.get_state_display() in res.content.decode('utf8'))
        self.failIf('Continue self-assessment' in res.content)

    def test_communication_log_view(self):
        user = KHNPUser(self.user)
        person = user.get_person()
        # we need to create organization before user can fill eligibility form
        org = Organisation.objects.register(person,
                                            defaults={'is_supplier': False,
                                                      'org_type_id': 2})

        initialize_customer_diagnostic(org)
        diag = org.get_diagnostic()
        diag.set_state('submitted')
        draft_sent = CustomerCommType.objects.get(comm_type='Draft recommendation sent')
        draft_accepted = CustomerCommType.objects.get(comm_type='Draft recommendation accepted')
        CustomerCommLog.objects.create(diag=diag, comm_type=draft_sent, details='Not important')
        CustomerCommLog.objects.create(diag=diag, comm_type=draft_accepted, details='Not important')

        # show comm log page on customer's dashboard
        self.client.login(username='testuser', password='secret')
        response = self.client.get(reverse('home'))
        self.assertContains(response, '<h2>Status: Self-assessment submitted</h2>')
        self.assertContains(response, 'Draft recommendation sent')
        self.assertContains(response, 'Draft recommendation accepted')

    def test_customer_project_workflow(self):
        comm_method = CommMethod.objects.create(comm_method='Email')
        self.client.login(username='testuser', password='secret')
        self.assertEqual(CustomerProject.objects.count(), 0)

        user = KHNPUser(self.user)
        person = user.get_person()
        company = Organisation.objects.register(person,
                                                defaults={'is_supplier': False})
        company.org.set_contact('email_direct', self.user.email)
        initialize_customer_diagnostic(company).set_state('approved')

        area1 = SupportArea.objects.get(area_key='strategy')
        area2 = SupportArea.objects.get(area_key='finance')
        area3 = SupportArea.objects.get(area_key='innovation')
        voucher1 = company.customervoucher_set.create(area=area1,
                                           expires=datetime.date.today() + datetime.timedelta(days=10),
                                           value=100)
        voucher2 = company.customervoucher_set.create(area=area2,
                                           expires=datetime.date.today() + datetime.timedelta(days=10),
                                           value=300)

        suppl_user = User.objects.create_user(username='testsuppl', email='test2@khnp.org', password='secret')
        suser = KHNPUser(suppl_user, require_authenticated=False)
        scompany = Organisation.objects.register(suser.get_person(),
                                                 defaults={'is_supplier': True})
        scompany.org.set_contact('email_direct', suppl_user.email)

        topic1 = SupportTopic.objects.filter(area=area1)[0]
        topic2 = SupportTopic.objects.filter(area=area3)[0]

        scompany.suppliertopics_set.create(topic=topic1)
        scompany.suppliertopics_set.create(topic=topic2)

        url = reverse('customer_contacts_supplier', args=[scompany.pk])
        res = self.client.post(url, data=dict(message='Hello world',
                                              typ=str(comm_method.pk)))
        self.assertEqual(res.status_code, 302)
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(CustomerProject.objects.count(), 1)
        project = CustomerProject.objects.all()[0]
        self.assertEqual(project.cust, company)
        self.assertEqual(project.suppl, scompany)
        self.assertEqual(project.cover_note, 'Hello world')
        self.assertEqual(project.comm_method, comm_method)

        res = self.client.get(reverse('customer_dashboard', args=['projects']))
        self.failUnless('You have 0 current and 1 potential project' in res.content)
        self.failUnless('| Contact made' in res.content)

        # login as a supplier
        self.client.login(username='testsuppl', password='secret')
        url = reverse('supplier_project_make_view', args=[project.pk])
        data = dict(
            price=1000,
            description='Description of the project',
            startdate='1 Jan 2050',
            enddate='31 Jan 2050',
        )
        res = self.client.post(url, data)

        self.assertEqual(res.status_code, 302)
        self.assertEqual(len(mail.outbox), 4)  # mails to customer and supplier

        self.client.login(username='testuser', password='secret')
        res = self.client.get(reverse('customer_dashboard', args=['projects']))
        self.failUnless('You have 0 current and 1 potential project' in res.content)
        self.failUnless('| Offer received' in res.content)
        self.failUnless(">Accept this project</a>" in res.content)
        self.assertEqual(len(project.applicable_vouchers()), 1)
        self.assertContains(res, 'voucher-%d' % voucher1.pk)
        self.assertNotContains(res, 'voucher-%d' % voucher2.pk)

        url = reverse('customer_supplier_contact_accept', args=[project.pk])
        data = {'voucher': voucher1.pk}
        res = self.client.post(url, data)
        self.assertEqual(len(mail.outbox), 6)  # mails to customer and supplier

        project = CustomerProject.objects.all()[0]
        voucher1 = CustomerVoucher.objects.get(pk=voucher1.pk)
        self.assertEqual(voucher1.allocated, 100)
        self.assertEqual(project.voucher, voucher1)
        self.assertEqual(project.voucher_value, voucher1.value)
        self.assertEqual(voucher1.get_residual_value(), 0)
        self.assertEqual(len(project.applicable_vouchers()), 0)

        res = self.client.get(reverse('customer_project_details', args=[project.pk]))
        self.assertEqual(res.status_code, 200)
        self.failIf('>provide feedback<' in res.content)
        email = suppl_user.email
        self.assertContains(res, '<label class="form-label">Contact provider</label>', html=True)
        self.assertContains(res, '<a href="mailto:%s">%s</a>' % (email, email), html=True)

        # login as a supplier
        self.client.login(username='testsuppl', password='secret')
        # complete project
        url = reverse('supplier_project_complete_view', args=[project.pk])
        res = self.client.post(url)
        self.assertEqual(res.status_code, 302)
        project = CustomerProject.objects.all()[0]
        self.assertEqual(project.state, 'mark_completed')
        self.assertEqual(len(mail.outbox), 8)  # mails to customer and supplier

        voucher1 = CustomerVoucher.objects.get(pk=voucher1.pk)
        self.assertEqual(voucher1.spent, 100)
        self.assertEqual(project.voucher, voucher1)
        self.assertEqual(project.voucher_value, voucher1.value)
        self.assertEqual(voucher1.get_residual_value(), 0)

        self.assertEqual(len(project.applicable_vouchers()), 0)

        # View project detail as a Supplier
        res = self.client.get(reverse('supplier_project_detail_view', args=[project.pk]))
        email = self.user.email
        self.assertContains(res, '<label class="form-label">Contact customer</label>', html=True)
        self.assertContains(res, '<a href="mailto:%s">%s</a>' % (email, email), html=True)

        self.client.login(username='testuser', password='secret')
        res = self.client.get(reverse('customer_project_details', args=[project.pk]))
        self.assertEqual(res.status_code, 200)
        self.failUnless('>provide feedback<' in res.content)

        url = reverse('customer_project_feedback', args=[project.pk])
        res = self.client.post(url, data={'fb_score': 4, 'fb_comment': 'Everything OK', 'fb_work_again': True})
        self.assertEqual(res.status_code, 302)
        project = CustomerProject.objects.all()[0]
        self.assertEqual(project.state, 'completed')
        payment = Payment.objects.get(proj=project)
        self.assertEqual(payment.amount, voucher1.value)

    def test_customer_project_dismiss(self):
        comm_method = CommMethod.objects.create(comm_method='Email')
        self.assertEqual(CustomerProject.objects.count(), 0)

        user = KHNPUser(self.user)
        person = user.get_person()
        company = Organisation.objects.register(person,
                                      defaults={'is_supplier': False})
        company.org.displayname = 'Customer1'
        company.org.save()

        # We also test if only projects for particular customer are shown on the
        # customer dashboard. Refs. #081
        duser2 = User.objects.create_user(username='testuser2', email='test3@khnp.org', password='secret')
        user2 = KHNPUser(duser2)
        person2 = user2.get_person()
        company2 = Organisation.objects.register(person2,
                                      defaults={'is_supplier': False})
        company2.org.displayname = 'Customer2'
        company2.org.save()

        suppl_user = User.objects.create_user(username='testsuppl', email='test2@khnp.org', password='secret')
        suser = KHNPUser(suppl_user, require_authenticated=False)
        scompany = Organisation.objects.register(suser.get_person(),
                                                 defaults={'is_supplier': True})
        scompany.org.set_contact('email_direct', 'test2@khnp.org')
        scompany.org.displayname = 'Supplier1'
        scompany.org.save()

        # contact supplier as user 2
        self.client.login(username='testuser2', password='secret')
        url = reverse('customer_contacts_supplier', args=[scompany.pk])
        res = self.client.post(url, data=dict(message='Hello world 2',
                                              typ=str(comm_method.pk)))
        # 403 raised, because user does not have approved diagnostic yet
        self.assertEqual(res.status_code, 403)
        self.assertEqual(CustomerProject.objects.count(), 0)
        self.assertTrue(company2.get_diagnostic() is None)

        initialize_customer_diagnostic(company2)
        diag = company2.get_diagnostic()
        diag.set_state('submitted')
        res = self.client.post(url, data=dict(message='Hello world 2',
                                              typ=str(comm_method.pk)))
        # The diagnostic has to be approved!
        self.assertEqual(res.status_code, 403)

        diag.set_state('approved')
        res = self.client.post(url, data=dict(message='Hello world 2',
                                              typ=str(comm_method.pk)))
        # finally, it works
        self.assertEqual(res.status_code, 302)
        self.assertEqual(CustomerProject.objects.count(), 1)

        # contact supplier as user 2
        self.client.login(username='testuser', password='secret')
        initialize_customer_diagnostic(company).set_state('approved')
        url = reverse('customer_contacts_supplier', args=[scompany.pk])
        res = self.client.post(url, data=dict(message='Hello world 1',
                                              typ=str(comm_method.pk)))
        self.assertEqual(res.status_code, 302)
        self.assertEqual(CustomerProject.objects.count(), 2)
        project = CustomerProject.objects.filter(cust=company)[0]

        res = self.client.get(reverse('customer_dashboard', args=['projects']))
        self.assertContains(res, 'You have 0 current and 1 potential project')

        url = reverse('customer_supplier_contact_dismiss', args=[project.pk])
        res = self.client.post(url, follow=True)
        self.assertEqual(res.status_code, 200)
        self.assertContains(res, "You don't have any projects")
        project = CustomerProject.objects.filter(cust=company).all()[0]
        self.assertEqual(project.state, 'dismissed')
        self.assertEqual(project.dismissed_by_cust, True)
        self.failUnless(len(mail.outbox), 1)
        # check the mail content
        msg = mail.outbox[-1]
        # email to Supplier only
        self.assertEqual(msg.from_email, settings.SERVER_EMAIL)
        self.assertEqual(msg.to, [suppl_user.email])
        self.assertEqual(msg.cc, [])
        self.assertEqual(msg.subject, u'Your project with Customer1 has been dismissed')
        self.failUnless('You have dismissed Project with Supplier1' in msg.body)

    def test_customer_project_not_complete(self):
        comm_method = CommMethod.objects.create(comm_method='Post')
        self.assertEqual(CustomerProject.objects.count(), 0)

        user = KHNPUser(self.user)
        person = user.get_person()
        customer = Organisation.objects.register(person,
                                      defaults={'is_supplier': False})
        suppl_user = User.objects.create_user(username='testsuppl', email='test2@khnp.org', password='secret')
        suser = KHNPUser(suppl_user, require_authenticated=False)
        scompany = Organisation.objects.register(suser.get_person(),
                                                 defaults={'is_supplier': True})

        project = CustomerProject.objects.create(**{
            'comm_method': comm_method,
            'cover_note': u'Hello world',
            'created': datetime.datetime(2012, 11, 20).date(),
            'proposed': datetime.datetime(2012, 11, 20).date(),
            'cust': customer,
            'description': u'Description of the project',
            'startdate': datetime.datetime(2013, 1, 1).date(),
            'enddate': datetime.datetime(2013, 1, 31).date(),
            'price': 1000,
            'voucher_value': 1000,
            'suppl': scompany,
            'state': 'mark_completed',
        })
        project.invoice()

        # raise complaints
        self.client.login(username='testuser', password='secret')
        url = reverse('customer_project_not_complete', args=[project.pk])
        res = self.client.post(url, {'details': 'The project has not been successfully completed yet.'})
        self.assertEqual(res.status_code, 302)
        self.failUnless(project.state, 'mark_completed')  # no change
        self.assertEqual(len(mail.outbox), 3)  # three mails (suppl, cust and admin)
        self.assertEqual(CustomerProjectComm.objects.count(), 1)
        res = self.client.get(res['Location'])
        self.assertContains(res, 'Your complaint has been recorded.')
        payment = project.payment_set.get()
        self.failUnless(payment.has_complaint)

    def test_recommendation(self):
        user = KHNPUser(self.user)
        person = user.get_person()
        # we need to create organization before user can fill eligibility form
        company = Organisation.objects.register(person,
                                                defaults={'is_supplier': False})

        diag = CustomerDiagnostic.objects.create(cust=company, diag_questions='<root xmlns:tm="http://textmatters.com" />')
        diag.set_state('submitted')

        edit_url = reverse("admin_customer_recommendation_edit",
                           args=(diag.pk,))
        send_url = reverse("admin_customer_recommendation_send",
                           args=(diag.pk,))

        topic_1 = SupportTopic.objects.get(pk=1)
        topic_2 = SupportTopic.objects.get(pk=2)

        pri_lev = DevPriLev.objects.get(pk=1)
        duration = DevDuration.objects.get(pk=1)
        support_type = DevSupportType.objects.get(pk=1)
        data = {
            'save': 'the button save',
            'orgnl_overview': "Overview",
            'financial_health': "Financials",
            'snr_lev_commit': "Commitment",
            'support_topics': []
        }

        data['support_topics'] = [topic_1.pk, topic_2.pk]

        data['pri_1-topic'] = topic_1.pk
        data['pri_1-pri_lev'] = pri_lev.pk
        data['pri_1-duration'] = duration.pk
        data['pri_1-support_type'] = support_type.pk
        data['pri_1-value'] = "200"

        data['pri_2-topic'] = topic_2.pk
        data['pri_2-pri_lev'] = pri_lev.pk
        data['pri_2-duration'] = duration.pk
        data['pri_2-support_type'] = support_type.pk
        data['pri_2-value'] = ""

        self.client.login(username="admin", password="secret")
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(diag.customerdevpri_set.count(), 2)

        response = self.client.post(send_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Please enter a review summary to show it has been properly reviewed")

        diag = CustomerDiagnostic.objects.get(pk=diag.pk)
        diag.assessor_comments = CUSTOMER_DIAG_COMMENTS
        diag.save()

        response = self.client.post(send_url)
        self.assertEqual(response.status_code, 302)


        # Customer shoeld receive the recommendations now
        self.client.login(username="testuser", password="secret")
        cview_url = reverse('customer_recommendation')
        capprove_url = reverse('customer_recommendation_approve')
        creject_url = reverse('customer_recommendation_reject')

        response = self.client.get(cview_url)
        self.assertContains(response, topic_1.topic)
        self.assertContains(response, topic_2.topic)
        self.assertContains(response, '<p class="discreet">By accepting these recommendations you are also accepting terms of use for vouchers.</p>', html=True)
        # reject as a customer
        response = self.client.post(creject_url)

        # update the recommendation
        # Since there is no voucher value, customer don't have to accept any terms and conditions
        self.client.login(username="admin", password="secret")
        data['pri_1-value'] = ""
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)
        response = self.client.post(send_url)
        self.assertEqual(response.status_code, 302)

        self.client.login(username="testuser", password="secret")
        response = self.client.get(cview_url)
        self.assertContains(response, topic_1.topic)
        self.assertContains(response, topic_2.topic)
        self.assertNotContains(response, '<p class="discreet">By accepting these recommendations you are also accepting terms of use for vouchers.</p>', html=True)
        # reject again
        response = self.client.post(creject_url)

        # Put back the voucher value and check there is no disclaimer after the recommendation is accepted
        self.client.login(username="admin", password="secret")
        data['pri_1-value'] = "500"
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)
        response = self.client.post(send_url)
        self.assertEqual(response.status_code, 302)

        self.client.login(username="testuser", password="secret")
        response = self.client.get(cview_url)
        self.assertContains(response, '<p class="discreet">By accepting these recommendations you are also accepting terms of use for vouchers.</p>', html=True)
        # approve
        response = self.client.post(capprove_url)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(cview_url)
        self.assertNotContains(response, '<p class="discreet">By accepting these recommendations you are also accepting terms of use for vouchers.</p>', html=True)


class InviteUserTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(InviteUserTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        # we need to create organization before user can fill eligibility form
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 2})

    def test_send_invite(self):
        self.client.login(username='testuser', password='secret')

        url = reverse('org_users')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless(('&lt;%s&gt; (you)' % EMAIL) in response.content)
        self.failUnless('Welcome' in response.content)

        url = reverse('org_users')
        response = self.client.post(url, dict(users=' test2@khnp.org \r\ninvalid\r\ntest@khnp.org'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InvitedUser.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        self.client.logout()
        invite = InvitedUser.objects.all()[0]
        url = reverse('join_bigassist', args=[invite.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Signing in with Persona')

        # create another user (simulate persona registration) but with different email address than expected
        User.objects.create_user('testuser3', 'test3@khnp.org', password='secret')
        self.client.login(username='testuser3', password='secret')
        url = reverse('join_bigassist', args=[invite.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Your email is not valid for this invite.')

        # create correct user
        user2 = User.objects.create_user('testuser2', 'test2@khnp.org', password='secret')
        self.client.login(username='testuser2', password='secret')
        url = reverse('join_bigassist', args=[invite.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Do you really want to join')

        response = self.client.post(url, dict(submit='Yes', surname='Doe'))
        self.assertFormError(response, 'form', 'firstname', 'This field is required.')

        response = self.client.post(url, dict(submit='Yes', firstname='John', surname='Doe'))
        self.assertEqual(response.status_code, 302)

        invite = InvitedUser.objects.all()[0]
        self.failUnless(invite.registered)

        person = KHNPUser(user2).get_person()
        company = KHNPUser(user2).get_company()
        self.assertEqual(person.displayname, 'John Doe')
        self.assertEqual(company.pk, self.company.pk)
        members = self.company.get_members()
        found = False
        for member in members:
            if member.pk == person.pk:
                found = True
                break
        self.failUnless(found, msg='User has not been found in the company member list.')

        # finally check user can't invite other users, only managers can do it
        # but users can view members
        url = reverse('org_users')
        response = self.client.get(url)
        self.assertContains(response, '<h2>Users in this organisation</h2>', html=True)
        self.assertNotContains(response, 'id="add-users-button"')

    def test_send_invite_manager(self):
        self.client.login(username='testuser', password='secret')

        url = reverse('org_users')
        response = self.client.post(url, dict(users='test3@khnp.org', type='manager'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InvitedUser.objects.count(), 1)

        self.client.logout()
        invite = InvitedUser.objects.all()[0]
        url = reverse('join_bigassist', args=[invite.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Signing in with Persona')

        user3 = User.objects.create_user('testuser3', 'test3@khnp.org', password='secret')
        self.client.login(username='testuser3', password='secret')
        url = reverse('join_bigassist', args=[invite.pk])
        response = self.client.post(url, dict(submit='Yes', firstname='John3', surname='Doe3'))
        self.assertEqual(response.status_code, 302)

        invite = InvitedUser.objects.all()[0]
        self.failUnless(invite.registered)

        person = KHNPUser(user3).get_person()
        company = KHNPUser(user3).get_company()
        self.assertEqual(person.displayname, 'John3 Doe3')
        self.assertEqual(company.pk, self.company.pk)

        members = self.company.get_members(only_managers=True)
        found = False
        for member in members:
            if member.pk == person.pk:
                found = True
                break
        self.failUnless(found, msg='User (manager) has not been found in the company member list.')

        # User is manager so he can add new users (invites)
        url = reverse('org_users')
        response = self.client.get(url)
        self.assertContains(response, '<h2>Users in this organisation</h2>', html=True)
        self.assertContains(response, 'id="add-users-button"')


class CustomerXFormsTest(TestCase):

    fixtures = ['types', 'xforms', 'mails']

    def setUp(self):
        super(CustomerXFormsTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        # we need to create organization before user can fill eligibility form
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 1,
                                                'detail': '008'})
        self.company.org.charityno = '007'
        self.company.org.save()

    def test_model(self):
        # check if tm:organisation contains correct data in model.xml
        # This test is same as supplier test, just tm:assist_customer root element is different
        # and also company org_type
        org = self.company.org
        org.displayname = 'John Doe Company'
        org.maincontactname = 'John Doe'
        org.save()
        org.set_contact('website', 'http://john.com')
        org.set_contact('tel_general', '123456798')
        org.set_contact('tel_direct', '003456798')
        org.set_contact('email_general', 'corp@doe.com')
        org.set_contact('email_direct', 'john@doe.com')
        org.set_address(2, **{'address': 'John Avenue', 'postcode': 'SE1 111'})
        org.set_address(5, **{'address': 'John Street', 'postcode': 'SE1 112'})

        self.client.login(username='testuser', password='secret')
        url = reverse('customer_diagnostic_xml_model')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'].split(';')[0], 'text/xml')
        xml = XMLTestHelper(response.content)
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:org_name'), 'John Doe Company')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:org_type_id'), '1')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:detail'), '007')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:address'), 'John Avenue')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:postcode'), 'SE1 111')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:reg_address'), 'John Street')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:reg_postcode'), 'SE1 112')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:website'), 'http://john.com')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:tel_general'), '123456798')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:contact_tel'), '003456798')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:email_general'), 'corp@doe.com')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:contact_email'), 'john@doe.com')
        self.assertEqual(xml.get_text('/tm:assist_customer/tm:organisation/tm:contact_name'), 'John Doe')


class CustomerVisitsTest(TestCase):
    fixtures = ['types', 'xforms']

    def setUp(self):
        super(CustomerVisitsTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 1,
                                                'detail': '007'})

    def test_meetspace_dashboard(self):
        url = reverse('customer_dashboard', args=['connectspace'])
        response = self.client.get(url)
        self.assertContains(response, 'Please register or log in')
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Host a visit yourself')
        self.assertContains(response, 'You have no visits')
        visit = Visit()
        visit.host = self.company
        visit.description = 'Test visit'
        visit.save()
        visit.region.add(Region.objects.all()[0])
        Visitor.objects.create(visit=visit, visitor_org=self.company)

        response = self.client.get(url)
        self.assertContains(response, '1 potential visit')
        self.assertContains(response, '0 completed visits')
        self.assertContains(response, '0 potential mentors')
        self.assertContains(response, 'and 0 confirmed mentors')


class CustomerMentorTest(TestCase):
    fixtures = ['types', 'xforms']

    def setUp(self):
        super(CustomerMentorTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 1,
                                                'detail': '007'})

    def test_meetspace_dashboard(self):
        url = reverse('customer_dashboard', args=['connectspace'])
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Become a mentor')
        mentor = Mentor()
        mentor.mentor = self.person
        mentor.mentor_org = self.company
        mentor.description = 'Test visit'
        mentor.save()
        mentor.region.add(Region.objects.all()[0])

        Mentee.objects.create(mentor=mentor, mentee_org=self.company, contact_msg='Hello')
        response = self.client.get(url)
        self.assertContains(response, '1 potential mentor')
        self.assertNotContains(response, 'Become a mentor')
        self.assertNotContains(response, 'Edit my mentoring details')

    def test_mentor_discard(self):
        url = reverse('customer_dashboard', args=['connectspace'])
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Become a mentor')
        mentor = Mentor()
        mentor.mentor = self.person
        mentor.mentor_org = self.company
        mentor.description = 'Test visit'
        mentor.save()
        mentor.region.add(Region.objects.all()[0])
        mentee = Mentee.objects.create(mentor=mentor, mentee_org=self.company, contact_msg='Hello')

        response = self.client.post(reverse('mentee_discard', args=[mentee.pk]))
        response = self.client.get(url)
        self.assertContains(response, 'You have no visits')

    def test_mentee_accept(self):
        url = reverse('customer_dashboard', args=['connectspace'])
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Become a mentor')
        mentor = Mentor()
        mentor.mentor = self.person
        mentor.mentor_org = self.company
        mentor.description = 'Test visit'
        mentor.save()
        mentor.region.add(Region.objects.all()[0])
        mentee = Mentee.objects.create(mentor=mentor, mentee_org=self.company, contact_msg='Hello')

        response = self.client.post(reverse('mentee_accept', args=[mentee.pk]))
        response = self.client.get(url)
        self.assertContains(response, '<td>Agreed</td>')
        self.assertContains(response, '1 confirmed mentor')

    def test_mentee_reject(self):
        url = reverse('customer_dashboard', args=['connectspace'])
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Become a mentor')
        mentor = Mentor()
        mentor.mentor = self.person
        mentor.mentor_org = self.company
        mentor.description = 'Test visit'
        mentor.save()
        mentor.region.add(Region.objects.all()[0])
        mentee = Mentee.objects.create(mentor=mentor, mentee_org=self.company, contact_msg='Hello')

        response = self.client.post(reverse('mentee_reject', args=[mentee.pk]))
        response = self.client.get(url)
        self.assertContains(response, '<h2>You have no visits</h2>', html=True)
