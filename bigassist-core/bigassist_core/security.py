#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from bigassist_core.user import KHNPUser
from bigassist_core.middlewares import get_current_request
from bigassist_core.helpers import date_passed
from bigassist_core.xml import XMLManipulator
from bigassist_core.models import SupplierApproval


class BaseSecurity(object):

    def __init__(self, obj):
        self.obj = obj
        self.request = get_current_request()

    def permissions(self):
        return {}

    def can(self, permission):
        return self.check(permission)

    def check(self, permission):
        error = self.error(permission)
        return not error

    def error(self, permission):
        method = getattr(self, permission, None)
        if method:
            error = method()
        else:
            perms = self.permissions()
            error = perms.get(permission, "Not specified")
        return error


class CustomerDiagnosticSecurity(BaseSecurity):

    def make_ineligible(self):
        if self.obj.state in ['ineligible']:
            return "Customer self-assessment not in the right state"

    def show_ineligible(self):
        if self.obj.state in ['recommendation_submitted', 'recommendation_force_submitted',]:
            return "Customer self-assessment not in the right state"

    def add_recommendation(self):
        if self.obj.state not in ['submitted', 'review_started']:
            return "Recommendation not in the right state"

    def edit_recommendation(self):
        if self.obj.state not in ['recommendation_started',
                                  'recommendation_rejected']:
            return "Recommendation not in the right state"

    def revise_recommendation(self):
        if self.obj.state not in ['recommendation_appealed']:
            return "Recommendation not in the right state"

    def _check_comments(self):
        if not self.obj.has_comments():
            return "Please enter a review summary to show it has been properly reviewed"

    def send_recommendation(self):
        if self.obj.state not in ['recommendation_started',
                                  'recommendation_rejected',
                                  'recommendation_appealed']:
            return "Recommendation not in the right state"

        return self._check_comments()

    def force_send_recommendation(self):
        if self.obj.state != 'recommendation_rejected':
            return "Recommendation has not been rejected yet"
        return self._check_comments()

    def view_recommendation(self):
        if self.obj.state in ['in_progress',
                              'submitted',
                              'review_started']:
            return "Recomendation is not started yet"

    def cust_view_recommendation(self):
        if self.obj.state not in ['recommendation_submitted',
                                  'recommendation_revised',
                                  'recommendation_force_submitted',
                                  'approved']:
            return "Recomendation is not in the right state"

    def cust_approve_recommendation(self):
        if self.obj.state not in ["recommendation_submitted",
                                  "recommendation_revised",
                                  "recommendation_force_submitted"]:
            return "Recomendation is not in the right state"

    def cust_appeal_recommendation(self):
        if self.obj.state != "recommendation_submitted":
            return "Recomendation is not in the right state"

    def cust_reject_recommendation(self):
        if self.obj.state not in ["recommendation_appealed",
                                  "recommendation_revised"]:
            return "Recomendation is not in the right state"

        #FIXME - no allowed when no consent

    def cust_can_edit_diagnostic(self):
        if self.obj.state not in ['in_progress', 'prequalified']:
            return "Diagnostic can't be edited"

    def can_submit(self):
        if self.obj.state != 'in_progress':
            return "Diagnostic cannot be submitted"

    def can_resubmit(self):
        if self.obj.state not in (
            'in_progress', 
            'prequalified', 
            'ineligible', 
            'review_started', 
            'submitted', 
            'recommendation_started', 
            'recommendation_submitted', 
            'recommendation_appealed', 
            'recommendation_revised'):
            return "Diagnostic can't be re-submitted"

    def change_by_admin(self):
        # See #158
        if self.obj.state in ('in_progress',):
            return "Diagnostic is not in the right state"

    def edit_comments(self):
        if self.obj.state in ('in_progress', 'approved'):
            return "Diagnostic is not in the right state"


class SupplierApprovalSecurity(BaseSecurity):
    """ Security related to a SupplierApproval model instance """

    def __init__(self, obj):
        self.obj = obj
        if not isinstance(obj, SupplierApproval):
            raise ValueError("This is not supplier approval")

        self.request = get_current_request()

    def can_change_approval(self):
        if self.obj.state not in ('submitted', 'in_progress'):
            return "Approval is read only"

    def can_submit_approval(self):
        if self.obj.state != 'in_progress':
            return "Approval can't be submitted"

    def can_resubmit_approval(self):
        if self.obj.state not in ('rejected', 'approved'):
            return "Approval can't be re-submitted"

    def can_review_approval(self):
        if self.obj.state not in ('submitted'):
            return "Approval is not submitted"

        if not self.obj.assessor_comments:
            return "Please review the assessment first"

        xmlm = XMLManipulator(self.obj.assessor_comments)
        has_comments = bool(xmlm.get_text('.//tm:sign-off'))
        if not has_comments:
            return "Please enter a review summary to show it has been properly reviewed"

    def can_appeal_approval(self):
        if self.obj.state != 'rejected':
            return "Approval is not rejected"

    def can_panel_review(self):
        deadline = self.obj.get_deadline()
        if not deadline:
            return "There is no panel assigned"

        if deadline.panel_state != "in_progress":
            return "The panel is not in progress"

    def change_by_admin(self):
        # See #158
        if self.obj.state in ('in_progress',):
            return "Approval is not in the right state"


class SupplierProjectSecurity(BaseSecurity):
    """ Security related to a CustomerProject from Supplier's point of view """

    def can_view_eoi(self):
        if self.obj.state not in ["created", "proposed"]:
            return "Cannot view express of interest for existing project"

    def can_make_project(self):
        if self.obj.state != "created":
            return "Project is already created"

    def can_edit_project(self):
        if self.obj.state not in ["proposed"]:
            return "Cannot edit existing project"

    def can_dismiss_project(self):
        if self.obj.state not in ["created", "proposed"]:
            return "Cannot dismiss existing project"

    def can_view_contacts(self):
        return

    def can_view_project(self):
        if self.obj.state not in ["accepted", "completed", "mark_completed"]:
            return "Project is not ready for viewing"

    def can_complete_project(self):
        if self.obj.state != "accepted":
            return "Project is not current"

    def can_view_cust_capability_report(self):
        return self.can_view_eoi()


class CustomerProjectSecurity(BaseSecurity):
    """ Security related to a CustomerProject from Customer's point of view """

    def cust_can_view_project(self):
        if self.obj.state not in ["accepted", "completed", "mark_completed"]:
            return "Project is not ready for viewing"

    def cust_can_complain_about_project(self):
        if self.obj.state not in ["accepted"]:
            return "Project is not ready for accepting complaints"

    def cust_can_provide_feedback(self):
        if self.obj.state not in ["mark_completed"]:
            return "Project is not ready for feedback"


class ProjectSecurity(SupplierProjectSecurity, CustomerProjectSecurity):
    pass


class UserSecurity(BaseSecurity):
    """ obj is request.user """

    def contact_supplier(self):
        if self.obj.is_anonymous():
            return "Anonymous user can't contact suppliers"
        company = KHNPUser(self.obj).get_company()
        if not company:
            return "Users without a company can't contact suppliers"
        if 'supplier' in company.get_roles() and 'customer' not in company.get_roles():
            return "Only Customer type accounts can contact suppliers"
        diag = company.get_customer_diagnostic()
        if not diag or (diag and diag.state != 'approved'):
            return "Only approved customers can contact suppliers"

    def edit_organisation_profile(self):
        if self.obj.is_anonymous():
            return "Anonymous user can't edit anything"
        khnp_user = KHNPUser(self.obj)
        person = khnp_user.get_person()
        company = khnp_user.get_company()
        if not company or not person:
            return "Users without a company/person can't edit"
        is_manager = person.rel_subject_set.filter(object=company.pk, type__id=settings.KHNP_MANAGER_TYPE_ID).exists()
        if not is_manager:
            return "Only Organisation Managers can edit the profile"

    def manage_users(self):
        if self.obj.is_anonymous():
            return "Anonymous user can't manage users"
        khnp_user = KHNPUser(self.obj)
        company = khnp_user.get_company()
        if not company:
            return "Users without a company can't manage users"
        person = khnp_user.get_person()
        person_pk = person.pk
        for member in company.get_members(only_managers=True):
            if member.pk == person_pk:
                return None
        return "Only company Managers can manage users"

    def become_a_mentor(self):
        if self.obj.is_anonymous():
            return "Anonymous user can't be a mentor"
        khnp_user = KHNPUser(self.obj)
        company = khnp_user.get_company()
        if not company:
            return "Users without a company can't become a mentor"
        if 'supplier' in company.get_roles():
            return "Only customer-type companies can have mentors"
        person = khnp_user.get_person()
        # User can be mentor only in one organisation currently
        if person.mentor_set.exists():
            return 'Already a mentor'



class PaymentSecurity(BaseSecurity):

    def send_to_finance(self):
        if not self.obj.pop:
            return "The invoice POP number is not set"

        if self.obj.sent_to_finance:
            return "It has been already sent"


class PanelSecurity(BaseSecurity):

    def begin_review(self):
        if self.obj.panel_state == "in_progress":
            return "The panel is already started"

        if self.obj.panel_state == "completed":
            return "The panel is already completed"

    def complete_review(self):
        if self.obj.panel_state == "not_started":
            return "The panel has not been started yet"

        if self.obj.panel_state == "completed":
            return "The panel is already completed"

        approvals = self.obj.get_approvals().filter(decision__isnull=True)
        if approvals.count() > 0:
            return "There are still unreviewed approvals"


class VisitSecurity(BaseSecurity):
    """ obj is Visit instance """

    def remove_visit(self):
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if company.pk != self.obj.host.pk:
                return 'Unable to remove somebody else\'s visit'
        if self.obj.visitinstance_set.count():
            return 'Unable to remove visit with visit instance'

    def get_in_touch(self):
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if self.obj.visitor_set.filter(visitor_org=company).exists():
                return 'Already a visitor of this visit'
            if not company or (company and company.get_customer_diagnostic()) is None:
                return 'Not qualified customer'
        else:
            return "Unable to get in touch because there is no company"


class VisitorSecurity(BaseSecurity):
    """ obj is Visitor instance """

    def discard_visit(self):
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if company != self.obj.visitor_org:
                return 'Unable to discard someone else\'s visit'

        if self.obj.state != 'pending':
            return 'Unable to discard confirmed or completed visit'

    def accept_visitor(self):
        # Host is about to accept visitor
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if company != self.obj.visit.host:
                return 'Unable to accept someone else\'s visitor'

        if self.obj.state != 'pending':
            return 'Unable to accept accepted/rejected visitor'

    def reject_visitor(self):
        # Host is about to accept visitor
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if company != self.obj.visit.host:
                return 'Unable to reject someone else\'s visitor'

        if self.obj.state != 'pending':
            return 'Unable to reject accepted/rejected visitor'

    def leave_feedback(self):
        if not (self.obj.state == 'completed' and self.obj.feedback is None):
            return 'Not yet completed or feedback already given.'

    def apply_for_bursary(self):
        if self.obj.amount:
            return 'Sponsorship has been requested already'
        if self.obj.bursary is not None:
            return 'Sponsorship has been decided already'
        if self.obj.state == 'completed':
            return 'Visit has been completed already.'


class MentorSecurity(BaseSecurity):
    """ obj is Mentor instance """

    def get_in_touch(self):
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if self.obj.mentee_set.active().filter(mentee_org=company).exists():
                return 'Already a mentee of this visit'
            if not company or (company and company.get_customer_diagnostic() is None):
                return 'Not qualified customer'
        else:
            return "Unable to get in touch because there is no company"


class MenteeSecurity(BaseSecurity):
    """ obj is Mentee instance """

    def acceptreject_mentee(self):
        company = KHNPUser(self.request.user).get_company()
        if self.obj.mentor.mentor_org != company:
            return 'You are not mentor of this mentee'

    def discard_mentee(self):
        if self.request:
            company = KHNPUser(self.request.user).get_company()
            if company != self.obj.mentee_org:
                return 'Unable to discard someone else\'s mentoring'

        if self.obj.state != 'pending':
            return 'Unable to discard confirmed or completed mentoring'

    def complete(self):
        if self.obj.state not in ('completed', 'discarded'):
            # can be completed if at least three dates passed
            p1 = 1 if date_passed(self.obj.date1, force_date_check=True) else 0
            p2 = 1 if date_passed(self.obj.date2, force_date_check=True) else 0
            p3 = 1 if date_passed(self.obj.date3, force_date_check=True) else 0
            p4 = 1 if date_passed(self.obj.date4, force_date_check=True) else 0
            if (p1 + p2 + p3 + p4) == 4:
                return None  # can be completed
        return "Can't complete this mentoring"

    def complete_early(self):
        if self.obj.state not in ('completed', 'discarded'):
            # can be completed if at least three dates passed
            p1 = 1 if date_passed(self.obj.date1, force_date_check=True) else 0
            p2 = 1 if date_passed(self.obj.date2, force_date_check=True) else 0
            p3 = 1 if date_passed(self.obj.date3, force_date_check=True) else 0
            p4 = 1 if date_passed(self.obj.date4, force_date_check=True) else 0
            if (p1 + p2 + p3 + p4) == 3:
                return None  # can be completed
        return "Can't complete early this mentoring"

    def leave_feedback(self):
        if not (self.obj.state == 'completed' and self.obj.feedback is None):
            return 'Not yet completed or feedback already given.'

    def apply_for_bursary(self):
        if self.obj.amount:
            return 'Sponsorship has been requested already'
        if self.obj.bursary is not None:
            return 'Sponsorship has been decided already'
        if self.obj.state == 'completed':
            return 'Mentoring has been completed already.'
