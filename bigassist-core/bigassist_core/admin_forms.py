#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django import forms
from bigassist_core.models import XForms
from bigassist_core.utils import safe_utf8
from bigassist_core.xml import XMLManipulator
from lxml import etree


class XFormsAdminForm(forms.ModelForm):

    class Meta:
        model = XForms
        """The following fields attribute is added as not explicity setting
        the fields attribute on a ModelForm's Meta class is considered a major
        security vulnerability. Not setting this will break the
        code in Django > 1.8"""
        fields = ['formtype', 'xform', 'model', 'xform_ro', 'custom_1', 'custom_2']

    def fix_xform_data(self, xml):
        formtype = self.cleaned_data["formtype"]

        manip = XMLManipulator(xml)
        manip.fix_links_paths()
        manip.fix_images_paths()
        manip.fix_stylesheet_path()

        if formtype == 'P':
            manip.set_xfinstance('data-set', reverse('customer_qualification_xml_model'))
            manip.set_xfsubmission('submit', reverse('customer_qualification_save'))
        elif formtype == 'C':
            # Customer diagnostic
            # if changed, don't forget to update AdminCustomerDiagnosticView as well
            manip.set_xfinstance('data-set', reverse('customer_diagnostic_xml_model'))
            manip.set_xfinstance('old-data-set', reverse('customer_diagnostic_xml_prevmodel'))
            manip.set_xfinstance('question-set', reverse('xforms_model', args=['C', '1']))  # custom_1 field of Diagnostic form
            manip.set_xfsubmission('submit', reverse('customer_diagnostic_save'))
            manip.set_xfsubmission('finalsubmit', reverse('customer_diagnostic_submit'))
        elif formtype == 'S':
            # Supllier assessment
            manip.set_xfinstance('data-set', reverse('supplier_assessment_xml_model'))
            manip.set_xfinstance('old-data-set', reverse('supplier_assessment_xml_prevmodel'))
            manip.set_xfsubmission('submit', reverse('supplier_assessment_save'))
            manip.set_xfsubmission('finalsubmit', reverse('supplier_assessment_submit'))
        elif formtype == 'SA':
            # Supllier assessment - admin
            manip.set_xfinstance('data-set', reverse('supplier_assessment_xml_model'))
            manip.set_xfsubmission('submit', reverse('supplier_assessment_save'))
            manip.set_xfsubmission('finalsubmit', reverse('supplier_assessment_submit'))
        elif formtype == 'SRA':
            # Supllier reassessment
            manip.set_xfinstance('data-set', reverse('supplier_assessment_xml_model'))
            manip.set_xfinstance('old-data-set', reverse('supplier_assessment_xml_prevmodel'))
            manip.set_xfsubmission('submit', reverse('supplier_assessment_save'))
            manip.set_xfsubmission('finalsubmit', reverse('supplier_assessment_submit'))

        xml = manip.tostring()
        return xml

    def clean_xform_ro(self):
        xform = safe_utf8(self.cleaned_data["xform_ro"].strip())
        if xform:
            try:
                etree.XML(xform)
            except Exception, e:
                raise forms.ValidationError('XML parse error: %s' % str(e))
            return self.fix_xform_data(xform)
        else:
            return xform

    def clean_xform(self):
        """ Mangle the form data """
        xform = safe_utf8(self.cleaned_data["xform"].strip())
        if xform:
            try:
                etree.XML(xform)
            except Exception, e:
                raise forms.ValidationError('XML parse error: %s' % str(e))
            return self.fix_xform_data(xform)
        else:
            return xform

    def clean_model(self):
        """ Mangle the model data - add a namespace """
        model = safe_utf8(self.cleaned_data["model"].strip())
        model = model.replace('<tm:assist_cust_pre>', '<tm:assist_cust_pre xmlns:tm="http://textmatters.com">')
        model = model.replace('<tm:assist_supplier>', '<tm:assist_supplier xmlns:tm="http://textmatters.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')
        try:
            etree.XML(model)
        except Exception, e:
            raise forms.ValidationError('XML parse error: %s' % str(e))
        return model

    def clean_custom_1(self):
        # This is a bit strange. We store question set for some forms but full XFORM for S form.
        formtype = self.cleaned_data["formtype"]
        if formtype == 'S':
            custom_1 = safe_utf8(self.cleaned_data["custom_1"].strip())
            if custom_1:
                try:
                    etree.XML(custom_1)
                except Exception, e:
                    raise forms.ValidationError('XML parse error: %s' % str(e))
                return self.fix_xform_data(custom_1)
            else:
                return custom_1
        else:
            return self.cleaned_data["custom_1"]

    def clean_custom_2(self):
        formtype = self.cleaned_data["formtype"]
        if formtype == 'C':
            custom_2 = safe_utf8(self.cleaned_data["custom_2"].strip())
            if custom_2:
                try:
                    etree.XML(custom_2)
                except Exception, e:
                    raise forms.ValidationError('XML parse error: %s' % str(e))
                return self.fix_xform_data(custom_2)
            else:
                return custom_2
        else:
            return self.cleaned_data["custom_2"]
