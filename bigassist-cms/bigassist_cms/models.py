#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from django.utils.translation import gettext as _

from geoposition.fields import GeopositionField

from datetime import datetime
import os.path

class SectionManager(models.Manager):

    def available_in_nav(self):
        qs = self.filter(public=True)
        return qs


class Section(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField()
    created_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(auto_now=True, blank=True)
    public = models.BooleanField(default=True)
    in_navigation = models.BooleanField(default=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    objects = SectionManager()

    class Meta:
        ordering = ('order',)
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')

    def __unicode__(self):
        return self.title

    class Admin:
        pass


class PageManager(models.Manager):

    def public(self):
        qs = self.filter(
            public=True,
            parent__isnull=True,
            published_on__lt=datetime.now(),
            )
        return qs

    def news(self):
        qs = self.public().filter(section__slug='news').order_by('-published_on')
        return qs

    def guides(self):
        qs = self.public().filter(section__slug='guides')
        return qs


class Page(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField()
    introduction = models.TextField(blank=True)
    body = models.TextField(blank=True)
    created_on = models.DateTimeField(default=datetime.now, blank=True)
    published_on = models.DateTimeField(default=datetime.now, blank=True, help_text='Date to publish page')
    modified_on = models.DateTimeField(auto_now=True, blank=True)
    public = models.BooleanField(default=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    section = models.ForeignKey('Section', blank=False)

    parent = models.ForeignKey('self', help_text='Choose the parent page', blank=True, null=True)

    objects = PageManager()

    class Meta:
        ordering = ('-order', '-published_on')
        # db_table = "bussinesses"

    def __unicode__(self):
        return self.title

    class Admin:
        pass


class File(models.Model):
    upload = models.FileField(upload_to="cms_uploads/%Y/%m/%d/")  # Appended to MEDIA_ROOT automatically
    created_on = models.DateTimeField(default=datetime.now)
    is_image = models.BooleanField(default=True)
    mime_type = models.CharField(max_length=255)


class AgendaItem(models.Model):
    order = models.IntegerField(default=0)
    time = models.TimeField(help_text='Add time in 24-hour format, like 10:45')
    text = models.TextField(blank=False)

    event = models.ForeignKey('Event')

    class Meta:
        verbose_name = "Agenda item"
        verbose_name_plural = "Agenda items"
        ordering = ('time', )

    def __unicode__(self):
        return u"{event} - {time}".format(event=self.event, time=self.time)


class Venue(models.Model):
    name = models.CharField(blank=False, max_length=255)
    slug = models.SlugField()

    address = models.TextField(blank=True, help_text="Full address, like: Society Building, 8 All Saints Road, London N1 9RL")
    city = models.CharField(blank=True, max_length=255)
    location = GeopositionField(blank=True)

    public = models.BooleanField(default=True)
    created_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        verbose_name = "Venue"
        verbose_name_plural = "Venues"
        ordering = ('name', )

    def __unicode__(self):
        return u"{name} – {address}".format(name=self.name, address=self.address)


class Event(models.Model):
    title = models.CharField(blank=False, max_length=255)
    slug = models.SlugField()
    organiser = models.TextField(blank=True)
    date = models.DateField(blank=True, null=True)
    times = models.TextField(blank=True)
    audience = models.TextField(blank=True)

    text = models.TextField(blank=False)

    venue = models.ForeignKey(Venue, null=True, blank=True)

    eventbrite_link = models.URLField(blank=True)

    public = models.BooleanField(default=True)
    created_on = models.DateTimeField(default=datetime.now, blank=True)
    published_on = models.DateTimeField(default=datetime.now, blank=True, help_text='Date to publish event')
    modified_on = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"
        ordering = ('date', )

    def __unicode__(self):
        return u"{date} – {title}".format(date=self.date, title=self.title)
