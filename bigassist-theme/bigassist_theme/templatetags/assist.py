#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
from django import template
from django.template.defaultfilters import stringfilter
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from bigassist_core.user import KHNPUser
from bigassist_core.models import CustomerDiagnostic
from bigassist_core.models import CustomerProject
from bigassist_core.models import Payment
from bigassist_core.models import Organisation
from bigassist_core.models import Visit
from bigassist_core.models import Mentor
from bigassist_core.security import CustomerDiagnosticSecurity
from bigassist_core.security import ProjectSecurity
from bigassist_core.security import UserSecurity
from bigassist_core.security import PaymentSecurity
from bigassist_core.models import SupplierDeadline
from bigassist_core.models import SupplierApproval
from bigassist_core.security import PanelSecurity
from bigassist_core.security import SupplierApprovalSecurity
from bigassist_core.models import Visit
from bigassist_core.models import Visitor
from bigassist_core.security import VisitorSecurity
from bigassist_core.security import VisitSecurity
from bigassist_core.models import Mentor
from bigassist_core.models import Mentee
from bigassist_core.security import MentorSecurity
from bigassist_core.security import MenteeSecurity

register = template.Library()


@register.filter
def contact(person, field):
    return person.get_contact(field)


@register.filter
def can(obj, permission):
    if isinstance(obj, CustomerDiagnostic):
        return CustomerDiagnosticSecurity(obj).check(permission)
    elif isinstance(obj, CustomerProject):
        return ProjectSecurity(obj).check(permission)
    elif isinstance(obj, User):
        return UserSecurity(obj).check(permission)
    elif isinstance(obj, Payment):
        return PaymentSecurity(obj).check(permission)
    elif isinstance(obj, SupplierDeadline):
        return PanelSecurity(obj).check(permission)
    elif isinstance(obj, SupplierApproval):
        return SupplierApprovalSecurity(obj).check(permission)
    elif isinstance(obj, Visitor):
        return VisitorSecurity(obj).check(permission)
    elif isinstance(obj, Visit):
        return VisitSecurity(obj).check(permission)
    elif isinstance(obj, Mentor):
        return MentorSecurity(obj).check(permission)
    elif isinstance(obj, Mentee):
        return MenteeSecurity(obj).check(permission)


@register.assignment_tag
def get_khnp_user(userobject):
    return KHNPUser(userobject)


@register.filter(is_safe=True)
@stringfilter
def rjust_nbsp(value, arg):
    """
    Right-aligns the value in a field of a given width.

    Argument: field size.
    """
    return mark_safe(value.rjust(int(arg)).replace(' ', '&nbsp;'))


@register.filter(is_safe=True)
@stringfilter
def currencize(value):
    """
    amount|currencize => GBP500
    """
    return mark_safe("&#163;%s" % value)


@register.filter
def is_none(val):
    return val is None


@register.filter
def get_project_state(supplier, customer):
    result = ''
    if isinstance(customer, Organisation):
        projects = supplier.supplier_projects.filter(cust=customer)
        if projects.filter(state__in=('accepted', 'mark_completed')).exists():
            result = '<span class="flag accepted">Accepted</span>'
        elif projects.filter(state__in=('created', 'proposed')).exists():
            result = '<span class="flag contacted">Contacted</span>'
    return mark_safe(result)


@register.filter
def get_visit_state(host, visitor_mentee):
    result = ''
    state = ''
    if isinstance(visitor_mentee, Organisation):
        # 'host' is a Visit or Mentor instance
        if isinstance(host, Visit):
            for visitor in host.visitor_set.filter(visitor_org=visitor_mentee).order_by('-created'):
                if visitor.state == 'pending':
                    state = 'contacted'
                elif visitor.state == 'confirmed':
                    state = 'accepted'
                # process only the most recent visit
                break
        elif isinstance(host, Mentor):
            for mentee in host.mentee_set.filter(mentee_org=visitor_mentee).order_by('-created'):
                if mentee.state == 'pending':
                    state = 'contacted'
                elif mentee.state == 'agreed':
                    state = 'accepted'
                # process only the most recent mentoring
                break

        if state == 'accepted':
            result = '<span class="flag accepted">Accepted</span>'
        elif state == 'contacted':
            result = '<span class="flag contacted">Contacted</span>'
    return mark_safe(result)
