#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import urllib
import datetime
import urllib2
from lxml import etree
from operator import attrgetter
from django.conf import settings
from django.http import Http404
from django.views.generic import TemplateView, ListView
from bigassist_core import models
from bigassist_core.forms import MarketplaceFilterForm, TailoredMarketplaceFilterForm
from bigassist_core.models import SupportTopic, SupportArea
from bigassist_core.user import KHNPUser
from bigassist_core.utils import PaginationMixin
from pybb.models import Forum
from django.db.models import Avg, sql, Count, aggregates, Sum, Q

from itertools import chain, islice

import logging
logger = logging.getLogger(__name__)


MP_REGIONS = dict(MarketplaceFilterForm.REGIONS)


def get_marketplace_filter(request, full=False, titled=False):
    mp_filter = dict(request.session.get('marketplace_filter', {}))
    if full:
        for key, val in mp_filter.items():
            if key == "topics":
                mp_filter[key] = SupportArea.objects.filter(area_id__in=val)
            elif key == "sections":
                mp_filter[key] = models.OrgType.objects.filter(org_type_id__in=val)
            elif key == "regions":
                if titled:
                    mp_filter[key] = [MP_REGIONS.get(x, x) for x in val]
    return mp_filter


class MarketplaceView(TemplateView):
    template_name = "marketplace/marketplace.html"

    def dispatch(self, request, *args, **kwargs):
        self.show_tailored = False
        self.company = None
        if request.user.is_authenticated():
            khnp_user = KHNPUser(request.user)
            self.company = khnp_user.get_company()
            if self.company and 'customer' in self.company.get_roles():
                self.diag = self.company.get_customer_diagnostic()
                if self.diag and self.diag.state == "approved":
                    self.show_tailored = True

        return super(MarketplaceView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        return dict(self.request.session.get('marketplace_filter', {}))

    def get_general_form(self, **kwargs):
        return MarketplaceFilterForm(**kwargs)

    def get_tailored_form(self, **kwargs):
        """ We expect the user is a member of a customer company """
        # assert not self.company.is_supplier, "Tailor form can be only for customers"

        form = TailoredMarketplaceFilterForm(**kwargs)

        topics = SupportTopic.objects.filter(customerdevpri__diag=self.diag)
        areas = SupportArea.objects.filter(supporttopic__in=topics).distinct()
        form['topic'].field.queryset = areas
        return form

    def get_context_data(self, **kwargs):
        context = super(MarketplaceView, self).get_context_data(**kwargs)

        context['temp_session'] = self.request.session.get('marketplace_filter')

        context['experts_count'] = models.Organisation.objects.approved_suppliers().count()
        context['forums'] = Forum.objects
        context['show_tailored'] = self.show_tailored

        if self.show_tailored and 'tailored_form' not in context:
            initial = self.get_initial()
            topics = initial.get('topics', [])
            if len(topics) == 1:
                initial['topic'] = topics[0]

            context['tailored_form'] = self.get_tailored_form(initial=initial)

        if 'general_form' not in context:
            initial = self.get_initial()

            if self.show_tailored:
                diag_topics = []

                for topic in SupportTopic.objects.filter(customerdevpri__diag=self.diag):
                    diag_topics.append(topic.area.pk) 

                if not 'marketplace_filter' in self.request.session:
                    initial['topics'] = diag_topics
                    self.request.session['marketplace_filter'] = { 'topics': diag_topics }
                elif not 'topics' in self.request.session.get('marketplace_filter'):
                    initial['topics'] = diag_topics
                    self.request.session['marketplace_filter']['topics'] = diag_topics

                voucher_topic = self.request.GET.get('topic')
                if voucher_topic:
                    voucher_topic = int(voucher_topic)
                    initial['topics'] = [voucher_topic]
                    self.request.session['marketplace_filter']['topics'] = [voucher_topic]

            if 'marketplace_filter' in self.request.session:
                context['current_search'] = self.request.session.get('marketplace_filter').get('search') or ""

            context['general_form'] = self.get_general_form(initial=initial)

        # The search display string
        filters = get_marketplace_filter(self.request, full=True, titled=True)
        terms = []
        for field, row in filters.items():
            for value in row:
                terms.append(unicode(value))

        context['terms'] = ', '.join(terms)

        return context

    def post(self, request, *args, **kwargs):
        context = {}
        if 'tailored' in self.request.POST:
            form = self.get_tailored_form(data=self.request.POST)
            if not form.is_valid():
                return self.render_to_response(self.get_context_data(tailored_form=form))
            context['tailored_form'] = form

        else:
            form = self.get_general_form(data=self.request.POST)
            if not form.is_valid():
                return self.render_to_response(self.get_context_data(general_form=form))
            context['general_form'] = form

        data = form.cleaned_data
        # Is there a more clever way to get only ids?
        filter_dict = {}
        for field, value in data.items():
            if field == 'topic':
                filter_dict['topics'] = [getattr(value, 'pk', value), ]
            elif field == 'search':
                filter_dict['search'] = str(value)
            else:
                filter_dict[field] = map(lambda x: getattr(x, 'pk', x), value)

        self.request.session['marketplace_filter'] = filter_dict

        #if request.is_ajax():
        #    return HttpResponse("Success")

        #return HttpResponseRedirect(reverse('marketplace'))
        return self.render_to_response(self.get_context_data(**context))


class LibraryGroupView(PaginationMixin, ListView):
    template_name = "marketplace/library_group.html"
    paginate_by = 8

    def get_queryset(self):
        topic = self.request.GET.get('topic', ['strategy', ])
        filters = {'topic': topic}

        mp_filter = self.request.session.get('marketplace_filter', {})
        area_ids = mp_filter.get('topics', [])
        areas = SupportArea.objects.filter(area_id__in=area_ids).values_list('area_key', flat=True)
        filters = {'topic': areas}

        self.no_filter = not len(areas)

        try:
            url = "%s/%s" % (settings.KHNP_BASE_URL, settings.KHNP_SEARCH_VIEW)
            params = urllib.urlencode(filters, True)

            logger.debug("Searching library at %s using: %s" % (url, params))

            response = urllib2.urlopen(url, params, timeout=10)
            data = response.read()
            response.close()

            result = []
            xml = etree.fromstring(data)
            for page in xml.findall('page'):
                row = {}
                for el in page.iterchildren():
                    if el.tag == "url":
                        url = el.text.replace('http://www2.knowhownonprofit.org', '')
                        row[el.tag] = urllib.quote(url)
                    else:
                        row[el.tag] = el.text

                result.append(row)
        except Exception, e:
            result = []
            logger.error('Unable to retrieve library contents. URL: %s?%s Error: %s', url, params, str(e))

        return result

    def get_context_data(self, **kwargs):
        context = super(LibraryGroupView, self).get_context_data(**kwargs)
        context['no_filter'] = self.no_filter
        return context


# Experimental see http://stackoverflow.com/questions/431628/how-to-combine-2-or-more-querysets-in-a-django-view
class QuerySetChain(object):
    """
    Chains multiple subquerysets (possibly of different models) and behaves as
    one queryset.  Supports minimal methods needed for use with
    django.core.paginator.
    """

    def __init__(self, *subquerysets):
        self.querysets = subquerysets

    def count(self):
        """
        Performs a .count() for all subquerysets and returns the number of
        records as an integer.
        """
        return sum(qs.count() for qs in self.querysets)

    def _clone(self):
        "Returns a clone of this queryset chain"
        return self.__class__(*self.querysets)

    def _all(self):
        "Iterates records in all subquerysets"
        return chain(*self.querysets)

    def __getitem__(self, ndx):
        """
        Retrieves an item or slice from the chained set of results from all
        subquerysets.
        """
        if type(ndx) is slice:
            return list(islice(self._all(), ndx.start, ndx.stop, ndx.step or 1))
        else:
            return islice(self._all(), ndx, ndx+1).next()


class ExpertsView(PaginationMixin, ListView):
    template_name = "marketplace/experts.html"
    paginate_by = 5

    def get_queryset(self):
        # suppliers = models.Organisation.objects.approved_suppliers()
        '''
        Because we have to add project score and completed project count to the queryset
        We are fetching two querysets and joining them with the QuerySetChain class above
        This is experimental but supports pagination
        '''

#        suppliers_before = models.Organisation.objects.approved_suppliers_with_project_state_completed() \
#                                            .annotate(project_count = Count('supplier_projects', distinct=True))
        suppliers_before = models.Organisation.objects.approved_suppliers_with_project_feedback_given() \
            .annotate(feedback_count=Count('supplier_projects', distinct=True))

        # weird hack - see http://stackoverflow.com/questions/1396264/how-to-sort-by-annotated-count-in-a-related-model-in-django
        class MyAVG(sql.aggregates.Aggregate):
            is_computed = True
            sql_function = "AVG"
            sql_template = "COALESCE(AVG(%(field)s), 0)"

        sql.aggregates.MyAVG = MyAVG

        avgs = Avg('supplier_projects__fb_score')
        avgs.name = "MyAVG"
        suppliers_before = suppliers_before.annotate(avg=avgs) \
                   .order_by('-avg', 'org__displayname')


#        suppliers_after = models.Organisation.objects.approved_suppliers_with_project_state_not_completed().order_by('org__displayname')
        suppliers_after = models.Organisation.objects.approved_suppliers_with_project_feedback_not_given().order_by('org__displayname')


        mp_filter = self.request.session.get('marketplace_filter', {})
        filters = {}
        topics = mp_filter.get('topics')
        if topics:
            filters['suppliertopics__topic__area__area_id__in'] = topics

        sections = mp_filter.get('sections')
        if sections:
            filters['org_type__org_type_id__in'] = sections

        regions = mp_filter.get('regions')
        if regions:
            filters['orgregion__region__in'] = regions


        # suppliers = suppliers.filter(**filters)
        suppliers_before = suppliers_before.filter(**filters)
        suppliers_after = suppliers_after.filter(**filters)

        # text_search = self.request.session.get('marketplace_filter').get('search')
        text_search = mp_filter.get('search')
        if text_search:
            suppliers_before = suppliers_before.filter(
                Q(org__displayname__icontains=text_search) | 
                Q(supplierapproval__short_desc__icontains=text_search) | 
                Q(supplierapproval__long_desc__icontains=text_search)
                )
            suppliers_after = suppliers_after.filter(
                Q(org__displayname__icontains=text_search) | 
                Q(supplierapproval__short_desc__icontains=text_search) | 
                Q(supplierapproval__long_desc__icontains=text_search)
                )

        suppliers = QuerySetChain(suppliers_before, suppliers_after)

        return suppliers

    def get_context_data(self, **kwargs):
        context = super(ExpertsView, self).get_context_data(**kwargs)
        return context


class ConnectsView(PaginationMixin, ListView):
    template_name = "marketplace/connects.html"

    def get_queryset(self):
        # PLEASE NOTE, if you remove the anonymous check,
        # security check in template would fail because
        # VisitSecurity.get_in_touch will fail on assertion in KHNPUser (anonymous)
        if self.request.user.is_anonymous():
            return []

        now = datetime.date.today()
        
        visits = models.Visit.objects.filter(available=True).exclude(date__lt=now)
        mentors = models.Mentor.objects.filter(available=True)

        mp_filter = self.request.session.get('marketplace_filter', {})
        v_filters = {}
        m_filters = {}
        topics = mp_filter.get('topics')
        if topics:
            v_filters['topics__area__area_id__in'] = topics
            #m_filters['topics__area__area_id__in'] = topics

        sections = mp_filter.get('sections')
        if sections:
            v_filters['host__org_type_id__in'] = sections
            #m_filters['mentor_org__org_type_id__in'] = sections

        regions = mp_filter.get('regions')
        if regions:
            v_filters['region__code__in'] = regions
            #m_filters['region__code__in'] = regions

        visits = visits.filter(**v_filters).distinct()
        #mentors = mentors.filter(**m_filters).distinct()

        #qs = sorted(list(visits) + list(mentors), key=attrgetter('created'))
        qs = sorted(list(visits), key=attrgetter('created'))
        return qs

    def get_context_data(self, **kwargs):
        context = super(ConnectsView, self).get_context_data(**kwargs)
        return context


class LibraryArticleView(TemplateView):
    template_name = "marketplace/library_article.html"

    def get_context_data(self, **kwargs):
        context = super(LibraryArticleView, self).get_context_data(**kwargs)

        path = self.request.GET.get('path')

        if not path:
            raise Http404

        base_url = "%s%s" % (settings.KHNP_BASE_URL, urllib.unquote(path))
        url = "%s/info.xml" % (base_url)
        try:
            response = urllib2.urlopen(url)
            data = response.read()
            response.close()

            xml = etree.fromstring(data)
            page = {}
            for el in xml.iterchildren():
                page[el.tag] = el.text
        except:
            raise Http404

        page['body'], cnt = re.subn(r'href="(?!http)(.*?)"', 'href="%s/\g<1>"' % base_url, page['body'])

        context['page'] = page
        return context
