#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os.path

import csv
import json
import hashlib
from django.conf import settings
from functools import wraps
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import TemplateView, DetailView
from django.core.exceptions import ImproperlyConfigured
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.utils.decorators import available_attrs
from bigassist_core.user import KHNPUser
from bigassist_core.forms import BatchSizeForm


def create_token(org):
    approval = org.get_supplier_diagnostic()
    key1 = frozenset([approval.pk, org.pk, org.org.get_contact_email()])
    key = "%s-%s" % (str(key1), settings.SECRET_KEY)
    return hashlib.sha1(key).hexdigest()


def check_token(token, org):
    if not token:
        return False
    if not token.strip():
        return False
    approval = org.get_supplier_diagnostic()
    key1 = frozenset([approval.pk, org.pk, org.org.get_contact_email()])
    key = "%s-%s" % (str(key1), settings.SECRET_KEY)
    return hashlib.sha1(key).hexdigest() == token


def encode(value, encoding="utf-8"):
    if isinstance(value, unicode):
        value = value.encode('utf-8')

    return value


def ViewDecorator(custom_decorator):
    """
    Apply ``custom_decorator`` to all the handlers in a class-based
    view that delegate to the ``dispatch`` method.

    Usage:
      from django.contrib.auth.decorators import login_required

      @ViewDecorator(login_required)
      class MyListView (ListView):
        ...
    """
    def inner_decorator(cls):
        if not hasattr(cls, 'dispatch'):
            raise TypeError(('View class is not valid: %r.  Class-based views '
                             'must have a dispatch method.') % cls)

        original = cls.dispatch
        modified = method_decorator(custom_decorator)(original)
        cls.dispatch = modified

        return cls

    return inner_decorator


def assist_login_required(test_func):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            template_name = "registration/notallowed.html"
            context = dict()
            return TemplateResponse(request=request,
                                    template=template_name,
                                    context=context,
                                    )

        return _wrapped_view
    return decorator


def CustomerRequiredDecorator(cls=None):
    """  """

    def check(u):
        if u.is_anonymous():
            return False
        khnp_user = KHNPUser(u)
        company = khnp_user.get_company()
        if company and 'customer' in company.get_roles():
            return True
        return False

    custom_decorator = assist_login_required(check)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def MemberRequiredDecorator(cls=None):
    """  User is member of currently visited company """

    def check(u):
        if u.is_anonymous():
            return False
        khnp_user = KHNPUser(u)
        company = khnp_user.get_company()
        if company and 'customer' in company.get_roles():
            return True
        return False

    custom_decorator = assist_login_required(check)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def CustomerOrStaffRequiredDecorator(cls=None):
    """  """

    def check(u):
        if u.is_anonymous():
            return False
        if u.has_perm('bigassist_core.can_view_admin'):
            return True
        khnp_user = KHNPUser(u)
        company = khnp_user.get_company()
        if company and 'customer' in company.get_roles():
            return True
        return False

    custom_decorator = assist_login_required(check)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def SupplierRequiredDecorator(cls=None):
    """  """

    def check(u):
        if u.is_anonymous():
            return False
        khnp_user = KHNPUser(u)
        company = khnp_user.get_company()
        if company and 'supplier' in company.get_roles():
            return True
        return False

    custom_decorator = assist_login_required(check)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def StaffRequiredDecorator(cls=None):
    """ shortcut for assist_login_required(lambda u: u.is_staff) """

    custom_decorator = assist_login_required(lambda u: u.is_staff)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def AdminRequiredDecorator(cls=None):

    custom_decorator = assist_login_required(lambda u: u.has_perm('bigassist_core.can_modify_admin'))

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)

def ROAdminRequiredDecorator(cls=None):

    custom_decorator = assist_login_required(lambda u: u.has_perm('bigassist_core.can_view_admin'))

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def LoginRequiredDecorator(cls=None):
    """ shortcut for ViewDecorator(assist_login_required """

    custom_decorator = assist_login_required(lambda u: u.is_authenticated())

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def CompanyManagerRequiredDecorator(cls=None):
    def check(u):
        if u.is_anonymous():
            return False
        khnp_user = KHNPUser(u)
        company = khnp_user.get_company()
        if company:
            person = khnp_user.get_person()
            is_manager = person.rel_subject_set.filter(object=company.pk, type__id=settings.KHNP_MANAGER_TYPE_ID).exists()
            if is_manager:
                return True
        return False

    custom_decorator = assist_login_required(check)

    if cls is not None:
        return ViewDecorator(custom_decorator)(cls)
    return ViewDecorator(custom_decorator)


def get_detail_fieldname(otype):
    if otype == "Voluntary or community organisation":
        det_col = 'detail_1'
    elif otype in ("Private company", "Company"):
        det_col = 'detail_2'
    elif otype == "Other":
        det_col = 'detail_3'
    else:
        det_col = ''
    return det_col


def safe_utf8(text):
    if isinstance(text, unicode):
        text = text.encode('utf-8')
    return text


def get_xform(filename):
    xform_file = os.path.join(os.path.dirname(__file__), 'views', 'xforms', filename)
    open_file = open(xform_file, 'rb')
    return open_file.read()


def email_domain(email_address):
    """Returns everything after @ in email_address"""
    return email_address.split('@')[1]


class ConfirmationMixin(object):

    title = "Confirmation"
    description = None
    url = None
    org_role = None
    success_url = None
    action = None
    cancel = None
    layout = None

    def load_texts(self):
        context = {}
        context['title'] = self.title
        context['org_role'] = self.org_role
        context['description'] = self.description
        context['url'] = self.get_url()
        context['action'] = self.action
        context['cancel'] = self.cancel
        context['layout'] = self.layout
        return context

    def get_url(self):
        if self.url is not None:
            return self.url
        return self.request.get_full_path()

    def get_success_url(self):
        return self.success_url

    def render_success(self, request):
        url = str(self.get_success_url())
        if request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


class ConfirmationView(TemplateView, ConfirmationMixin):

    template_name = "confirmation.html"

    def get_context_data(self, **kwargs):
        context = super(ConfirmationView, self).get_context_data(**kwargs)
        context.update(self.load_texts())
        return context


class ConfirmationDetailView(DetailView, ConfirmationMixin):

    template_name = "confirmation.html"

    def get_context_data(self, **kwargs):
        context = super(ConfirmationDetailView, self).get_context_data(**kwargs)
        context.update(self.load_texts())
        return context


class CSVRespondMixin(object):
    filename = None
    csv_object_name = "csv_list"

    def get_filename(self):
        return self.filename

    def get_csv_object_name(self):
        return self.csv_object_name

    def header(self, context):
        return None

    def rowify(self, obj):
        return obj

    def render_to_csv(self, context, **response_kwargs):
        filename = self.get_filename()
        if filename is None:
            raise ImproperlyConfigured(u"You must specify filename")

        csv_object_name = self.get_csv_object_name()
        if csv_object_name not in context:
            raise ImproperlyConfigured(u"Context is missing csv object name %s"
                                       % csv_object_name)

        csv_list = context[csv_object_name]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % \
                   filename

        writer = csv.writer(response)

        header_row = self.header(context)
        if header_row:
            writer.writerow(header_row)

        for obj in csv_list:
            row = self.rowify(obj)
            row = [encode(x) for x in row]
            writer.writerow(row)

        return response


class PaginationMixin(object):

    def get_paginate_by(self, queryset):
        if 'batch_size' in self.request.GET:
            try:
                self.paginate_by = int(self.request.GET['batch_size'])
            except (ValueError, TypeError):
                self.paginate_by = 0
            self.paginate_by = self.paginate_by or None
            self.request.session['batch_size'] = self.paginate_by
        elif 'batch_size' in self.request.session:
            self.paginate_by = self.request.session['batch_size']
        else:
            self.paginate_by = settings.BATCH_SIZE

        return self.paginate_by

    def get_context_data(self, **kwargs):
        context = super(PaginationMixin, self).get_context_data(**kwargs)
        initial = {'batch_size': self.get_paginate_by(None) or 0}
        context['batch_size_form'] = BatchSizeForm(initial=initial)
        return context
