(function($) {
    'use strict';

    $(function() {
        $('.group-checkbox-select-multiple dt input').click(function(e) {
            var klass = '.' + $(this).data('itemsclass');
            $(klass).prop('checked', this.checked);
        });

        $('.group-checkbox-select-multiple dd input').click(function(e) {
            var klass = '.' + $(this).prop('class');
            var l1 = $(klass).length;
            var l2 = $(klass + ':checked').length;
            // find all dt's, pick the first one found (closest one) and find input in it
            var $group_input = $(this).parents('dd').prevAll('dt').eq(0).find('input');
            if ( l1 == l2 ) {
                $group_input.prop('checked', 'checked');
                $group_input.prop("indeterminate", false);
            } else if ( l2 === 0 ) {
                $group_input.prop('checked', false);
                $group_input.prop("indeterminate", false);
            } else {
                $group_input.prop('checked', false);
                $group_input.prop("indeterminate", true);
            }

        });

    });
})(jQuery);

