# Create invoices for VisitInstance
from datetime import datetime
from django.db import transaction
# from django.core.management.base import CommandError
from django.core.management.base import NoArgsCommand
from bigassist_core.models import VisitInstance, Visitor
from bigassist_core.emails import send_ba_email


class Command(NoArgsCommand):
    # There is no trigger on the site so we must use cron.
    help = "Create invoices for all completed and not-yet-invoiced VisitInstances."
    # args = '<basedir>'

    def handle(self, *args, **options):
        now = datetime.now().date()
        for row in VisitInstance.objects.filter(date__lt=now, payment__isnull=True, bursary=True):
            with transaction.commit_on_success():
                payment = row.invoice()
                if payment:
                    context = dict(
                        visit=row.visit,
                        visit_instance=row,
                        payment=payment,
                        )
                    send_ba_email('191_visit_complete_host', context, recipients=row.visit.host, recipient_role='customer')

        for row in Visitor.objects.filter(visit_instance__date__lt=now, payment__isnull=True, bursary=True):
            with transaction.commit_on_success():
                payment = row.invoice()
                if payment:
                    context = dict(
                        visit=row.visit,
                        visit_instance=row.visit_instance,
                        payment=payment,
                        )
                    send_ba_email('192_visit_complete_visitor', context, recipients=row.visitor_org, recipient_role='customer')
