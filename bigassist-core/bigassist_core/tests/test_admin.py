from datetime import datetime
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core import mail

from base import TestCase

from bigassist_core.models import Organisation
from bigassist_core.models import CustomerDiagnostic
from bigassist_core.models import SupportTopic, DevPriLev, DevDuration, \
    DevSupportType, CustomerCommLog, SupplierApproval, SupplierTopics, \
    CustomerCommType, CommMethod, CustomerProject, Payment, Visit, Mentor, Report, \
    Identity, InvitedUser, RelationshipType, Person
from bigassist_core.views import AdminDashboardCustomerView, AdminDashboardSupplierView
from bigassist_core.user import KHNPUser


CUSTOMER_DIAG_COMMENTS = '<tm:assist_customer_admin xmlns:tm="http://textmatters.com"><tm:strategy/>\n    <tm:finance/>\n    <tm:innovation/>\n    <tm:relationships/>\n    <tm:people/>\n    <tm:summary>Test</tm:summary>\n</tm:assist_customer_admin>'

class AdminTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(AdminTest, self).setUp()

        # create a Django user
        self.user = User.objects.create_user(username='testuser', email='test@khnp.org', password='secret')

    def test_dashboard(self):
        url = reverse('home')
        response = self.client.get(url)
        self.failIf('Admin' in response.content)

        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.failIf('Admin' in response.content)

        self.user.is_staff = True
        self.user.user_permissions.add(self.can_view_admin)
        self.user.save()
        response = self.client.get(url)
        self.failUnless('Admin' in response.content)

    def test_permissions(self):
        self.client.login(username='testuser', password='secret')

        url = reverse('admin_dashboard_database')
        response = self.client.get(url)

        self.failUnless('Your current user role does not allow you to view this page.' in response.content)

        self.user.user_permissions.add(self.can_view_admin)
        self.user.save()

        response = self.client.get(url)
        self.failIf('Your current user role does not allow you to view this page.' in response.content)

        user1 = User.objects.create_user(username='testuser1', email='test1@khnp.org', password='secret')
        self.user1 = KHNPUser(user1).get_person()
        c1 = Organisation.objects.register(self.user1, {'is_supplier': False})

        url = reverse('admin_organisation_edit', args=(c1.pk, ))
        response = self.client.get(url)

        self.failUnless('Your current user role does not allow you to view this page.' in response.content)

        self.user.user_permissions.add(self.can_modify_admin)
        self.user.save()

        response = self.client.get(url)
        self.failIf('Your current user role does not allow you to view this page.' in response.content)


class AdminDashboardCustomerViewTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(AdminDashboardCustomerViewTest, self).setUp()
        # create a Django user
        user1 = User.objects.create_user(username='testuser1', email='test1@khnp.org', password='secret')
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        user3 = User.objects.create_user(username='testuser3', email='test3@khnp.org', password='secret')

        self.user1 = KHNPUser(user1).get_person()
        self.user2 = KHNPUser(user2).get_person()
        self.user3 = KHNPUser(user3).get_person()

    def tearDown(self):
        User.objects.all().delete()

    def test_pure_view(self):
        view = AdminDashboardCustomerView()

        c1 = Organisation.objects.register(self.user1, {'is_supplier': False})
        c2 = Organisation.objects.register(self.user2, {'is_supplier': False})
        c3 = Organisation.objects.register(self.user3, {'is_supplier': False})

        # currenlty, the view returns all companies because there is no CustomerDiagnostic record so it is not
        # possible to follow customerdiagnostic__is_approved relationship.
        # context = view.get_context_data()
        # self.assertEqual(context['total'], 0)
        # self.failIf(context['customers'])

        now = datetime.now()
        d1 = CustomerDiagnostic.objects.create(cust=c1, diag_questions='<root />', submitted=now)
        d2 = CustomerDiagnostic.objects.create(cust=c2, diag_questions='<root />', submitted=now)
        d3 = CustomerDiagnostic.objects.create(cust=c3, diag_questions='<root />', submitted=now)

        # no customer has is_approved set yet
        context = view.get_context_data()
        self.assertEqual(context['total_customer'], 0)

        d1.set_state('submitted')
        d2.set_state('approved')

        context = view.get_context_data()
        self.assertEqual(context['total_customer'], 1)
        self.failUnless(c1 in context['customers'])
        self.failIf(c2 in context['customers'])
        self.failIf(c3 in context['customers'])

        d1.set_state('approved')
        d3.set_state('approved')
        context = view.get_context_data()
        self.assertEqual(context['total_customer'], 0)
        self.failIf(context['customers'])


class AdminCustomerRecommendationTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(AdminCustomerRecommendationTest, self).setUp()

        self.user = User.objects.create_superuser(username='testuser',
                                        email='test@khnp.org',
                                        password='secret')

        self.person = KHNPUser(self.user).get_person()

        self.org = Organisation.objects.register(self.person, {'is_supplier': False})
        self.diag = self.org.customerdiagnostic_set.create()

    def test_permissions(self):
        self.client.login(username="testuser", password="secret")

        view_url = reverse("admin_customer_recommendation",
                           args=(self.diag.pk,))
        edit_url = reverse("admin_customer_recommendation_edit",
                           args=(self.diag.pk,))
        send_url = reverse("admin_customer_recommendation_send",
                           args=(self.diag.pk,))

        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)

        response = self.client.get(view_url)
        self.assertEqual(response.status_code, 403)

        self.diag.set_state("submitted")

        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Add recommendation")

        data = {
            'save': 'the button save',
            'orgnl_overview': "Overview",
            'financial_health': "Financials",
            'snr_lev_commit': "Commitment",
            'support_topics': []
        }

        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['form'].errors)

        topic_1 = SupportTopic.objects.get(pk=1)
        topic_2 = SupportTopic.objects.get(pk=2)
        topic_3 = SupportTopic.objects.get(pk=3)

        pri_lev = DevPriLev.objects.get(pk=1)
        duration = DevDuration.objects.get(pk=1)
        support_type = DevSupportType.objects.get(pk=1)

        data['support_topics'] = [topic_1.pk, topic_2.pk, topic_3.pk]

        data['pri_1-topic'] = topic_1.pk
        data['pri_1-pri_lev'] = pri_lev.pk
        data['pri_1-duration'] = duration.pk
        data['pri_1-support_type'] = support_type.pk
        data['pri_1-value'] = "200"

        data['pri_2-topic'] = topic_2.pk
        data['pri_2-pri_lev'] = pri_lev.pk
        data['pri_2-duration'] = duration.pk
        data['pri_2-support_type'] = support_type.pk
        data['pri_2-value'] = ""

        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['pri_forms'][topic_1].errors)
        self.assertFalse(response.context['pri_forms'][topic_2].errors)
        self.assertTrue(response.context['pri_forms'][topic_3].errors)

        data['support_topics'] = [topic_1.pk, topic_2.pk]
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(self.diag.customerdevpri_set.count(), 2)

        # Now remove the second recommendation
        data['support_topics'] = [topic_1.pk]
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(self.diag.customerdevpri_set.count(), 1)
        self.assertEqual(self.diag.customerdevpri_set.get().topic, topic_1)

        # Put back the secong pri
        data['support_topics'] = [topic_1.pk, topic_2.pk]
        response = self.client.post(edit_url, data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(self.diag.customerdevpri_set.count(), 2)

        # Now we have saved the first recommendation
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context['form']['support_topics'].value()), [topic_1.pk, topic_2.pk])
        self.assertContains(response, "Edit recommendation")

        response = self.client.get(view_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Overview")
        self.assertContains(response, "Financials")
        self.assertContains(response, "Commitment")
        self.assertContains(response, str(topic_1))

        self.assertContains(response, edit_url)
        self.assertNotContains(response, send_url)

        self.diag = CustomerDiagnostic.objects.get(pk=self.diag.pk)
        self.diag.assessor_comments = CUSTOMER_DIAG_COMMENTS
        self.diag.save()

        response = self.client.get(view_url)
        self.assertContains(response, send_url)

        logs = CustomerCommLog.objects.count()
        response = self.client.post(send_url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(CustomerCommLog.objects.count(), logs + 1)

        # refresh
        self.diag = CustomerDiagnostic.objects.get(pk=self.diag.pk)
        self.assertEqual(self.diag.state, "recommendation_submitted")

        response = self.client.post(send_url)
        self.assertContains(response, "Cannot send to organisation")

        response = self.client.get(view_url)
        self.assertEqual(response.status_code, 200)

        self.assertNotContains(response, edit_url)
        self.assertNotContains(response, send_url)

        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)

        # Now reject the recommendation
        reject_url = reverse("customer_recommendation_reject")
        approve_url = reverse("customer_recommendation_approve")

        response = self.client.post(reject_url)
        self.assertEqual(response.status_code, 302)
        self.diag = CustomerDiagnostic.objects.get(pk=self.diag.pk)
        self.assertEqual(self.diag.state, "recommendation_rejected")
        self.assertEqual(CustomerCommLog.objects.count(), logs + 2)

        response = self.client.post(reject_url)
        self.assertEqual(response.status_code, 200)

        force_send_url = reverse("admin_customer_recommendation_send_no_consent",
                           args=(self.diag.pk,))

        response = self.client.post(force_send_url)
        self.assertEqual(response.status_code, 302)
        self.diag = CustomerDiagnostic.objects.get(pk=self.diag.pk)
        self.assertEqual(self.diag.state, "recommendation_force_submitted")
        self.assertEqual(CustomerCommLog.objects.count(), logs + 3)

        response = self.client.post(reject_url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(approve_url)
        self.assertEqual(response.status_code, 302)
        self.diag = CustomerDiagnostic.objects.get(pk=self.diag.pk)
        self.assertEqual(self.diag.state, "approved")

        # There should be one voucher only
        self.assertEqual(self.diag.cust.customervoucher_set.count(), 1)

    def test_communication_log(self):
        self.client.login(username='testuser', password='secret')
        url = reverse('admin_customer_communication', args=[self.diag.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Phone call')
        self.assertContains(response, 'Missed phone call')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Face-to-face')
        self.assertNotContains(response, 'Draft recommendation sent')
        self.assertNotContains(response, 'Draft recommendation accepted')
        self.assertNotContains(response, 'Draft recommendation rejected')

    def test_communication_log_form(self):
        self.client.login(username='testuser', password='secret')
        self.assertEqual(CustomerCommLog.objects.count(), 0)
        url = reverse('admin_customer_communication', args=[self.diag.pk])
        data = dict(
            communication_type=CustomerCommType.objects.get(comm_type='Email').pk,
            details='How are you?',
            submit='Submit'
        )
        response = self.client.post(url, data)
        self.assertRedirects(response, reverse('admin_dashboard_customer') + '?org=%s' % self.diag.cust.pk)
        self.assertEqual(CustomerCommLog.objects.count(), 1)


class AdminCustomerDiagnosticEditTest(TestCase):

    fixtures = ['types', 'xforms', 'mails']

    def setUp(self):
        super(AdminCustomerDiagnosticEditTest, self).setUp()

        self.user = User.objects.create_superuser(username='testuser',
                                        email='test@khnp.org',
                                        password='secret')

        self.person = KHNPUser(self.user).get_person()

        self.org = Organisation.objects.register(self.person, {'is_supplier': False})
        self.diag = self.org.customerdiagnostic_set.create()

    def test_wrong_state(self):
        # nonsense customer ID
        diag_url = reverse("admin_customer_diagnostic",
                           args=(99999,))

        self.client.login(username="testuser", password="secret")
        response = self.client.get(diag_url)
        self.assertEqual(response.status_code, 404)

        diag_url = reverse("admin_customer_diagnostic",
                           args=(self.org.pk,))
        model_url = reverse("admin_customer_diagnostic_xml_model",
                           args=(self.org.pk,))

        response = self.client.get(diag_url)
        self.assertEqual(response.status_code, 200)

        # check model
        response = self.client.get(model_url)
        self.assertEqual(response.status_code, 200)

    def test_diagnostic(self):
        diag_url = reverse("admin_customer_diagnostic",
                           args=(self.org.pk,))
        submit_url = reverse("admin_customer_diagnostic_submit",
                           args=(self.org.pk,))
        model_url = reverse("admin_customer_diagnostic_xml_model",
                           args=(self.org.pk,))

        self.client.login(username="testuser", password="secret")

        # submit diagnostic
        self.diag.set_state('submitted')

        response = self.client.get(diag_url)
        self.assertEqual(response.status_code, 200)
        self.failUnless(submit_url in response.content)
        self.failUnless(model_url in response.content)

        # check model
        response = self.client.get(model_url)
        self.assertEqual(response.status_code, 200)


class AdminDashboardSupplierViewTest(TestCase):

    fixtures = ['types', 'mails', 'xforms']

    def setUp(self):
        super(AdminDashboardSupplierViewTest, self).setUp()
        # create a Django user
        User.objects.create_superuser(username='admin', email='test@khnp.org', password='secret')
        user1 = User.objects.create_user(username='testuser1', email='test1@khnp.org', password='secret')
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')

        self.user1 = KHNPUser(user1).get_person()
        self.user2 = KHNPUser(user2).get_person()

    def tearDown(self):
        User.objects.all().delete()

    def test_pure_view(self):
        view = AdminDashboardSupplierView()

        s1 = Organisation.objects.register(self.user1, {'is_supplier': True})
        s2 = Organisation.objects.register(self.user2, {'is_supplier': True})

        a1 = SupplierApproval.objects.create(suppl=s1, supplier_questions='<root />')
        a2 = SupplierApproval.objects.create(suppl=s2, supplier_questions='<root />')

        a1.set_state('submitted')
        a2.set_state('in_progress')
        data = view.get_context_data()
        self.assertEqual(data['total_supplier'], 1)

        topic1 = SupportTopic.objects.get(pk=1)
        s1topic1 = SupplierTopics.objects.create(suppl=s1, topic=topic1)
        url = reverse('admin_supplier_approval_review', args=[s1.pk])
        self.client.login(username='admin', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Assessment state: Submitted')
        self.assertContains(response, 'Topics:')
        self.assertContains(response, topic1.topic + ' - not approved')
        s1topic1.is_approved = True
        s1topic1.save()
        response = self.client.get(url)
        self.assertContains(response, 'Topics:')
        self.assertContains(response, topic1.topic + ' - approved')

        url = reverse('admin_supplier_approval_comments', args=[s1.pk])
        response = self.client.get(url)
        diag = s1.get_diagnostic()
        self.assertEqual(response['Content-Type'], 'text/xml')
        self.assertEqual(diag.locked_by.username, 'admin')

        User.objects.create_superuser(username='admin2', email='test3@khnp.org', password='secret')
        self.client.login(username='admin2', password='secret')

        url = reverse('admin_supplier_approval_review', args=[s1.pk])
        unlock_url = reverse('admin_supplier_approval_unlock', args=[diag.pk])
        response = self.client.get(url)
        self.assertContains(response, unlock_url)

        self.client.post(unlock_url)
        response = self.client.get(url)
        self.assertNotContains(response, unlock_url)

    def test_review_without_approval(self):
        s1 = Organisation.objects.register(self.user1, {'is_supplier': True})
        url = reverse('admin_supplier_approval_review', args=[s1.pk])
        self.client.login(username='admin', password='secret')
        response = self.client.get(url)
        self.failUnless('Organisation profile:' in response.content)
        url = reverse('admin_supplier_approval_comments', args=[s1.pk])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('home'))
        self.failUnless('This supplier has no approval yet.' in response.cookies['messages'].value)


class AdminDashboardDatabaseViewTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(AdminDashboardDatabaseViewTest, self).setUp()
        # create a Django user
        User.objects.create_superuser(username='admin', email='test@khnp.org', password='secret')
        user1 = self.duser1 = User.objects.create_user(username='testuser1', email='test1@khnp.org', password='secret')
        user2 = self.duser2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        user3 = self.duser3 = User.objects.create_user(username='testuser3', email='test3@khnp.org', password='secret')
        user4 = self.duser4 = User.objects.create_user(username='testuser4', email='test4@khnp.org', password='secret')
        user5 = self.duser5 = User.objects.create_user(username='testuser5', email='test5@khnp.org', password='secret')

        self.user1 = KHNPUser(user1).get_person()
        self.user2 = KHNPUser(user2).get_person()
        self.user3 = KHNPUser(user3).get_person()
        self.user4 = KHNPUser(user4).get_person()
        self.user5 = KHNPUser(user5).get_person()

        self.company1 = Organisation.objects.register(self.user1, {'is_supplier': True})
        self.company2 = Organisation.objects.register(self.user2, {'is_supplier': True})
        self.company3 = Organisation.objects.register(self.user3, {'is_supplier': False})
        self.company4 = Organisation.objects.register(self.user4, {'is_supplier': False})
        self.company5 = Organisation.objects.register(self.user5, {'is_supplier': False})

        self.company1.org.displayname = 'Lorem'
        self.company1.org.save()
        self.company2.org.displayname = 'Lorem ipsum'
        self.company2.org.save()
        self.company3.org.displayname = 'Dolor Sit'
        self.company3.org.save()
        self.company4.org.displayname = 'Amet manula'
        self.company4.org.save()
        self.company5.org.displayname = 'Sit pelor'
        self.company5.org.save()

    def test_admin_user_management_remove_managers(self):
        self.client.login(username='admin', password='secret')
        url = reverse('admin_org_users', args=[self.company1.pk])
        response = self.client.get(url)
        self.assertContains(response,
                            '<input type="checkbox" name="remove" value="M1" />',
                            html=True)
        self.assertContains(response,
                            '<a href="mailto:test1@khnp.org">test1@khnp.org</a>',
                            html=True)
        self.assertContains(response,
                            '<strong>Manager</strong>',
                            html=True)
        response = self.client.post(url, {'remove': 'M1', 'remove_users': 'Remove'}, follow=True)
        self.assertContains(response, 'No organisation Manager would left.')
        self.failUnless(self.user1 in self.company1.get_members())

    def test_admin_user_management_add_users(self):
        self.client.login(username='admin', password='secret')
        url = reverse('admin_org_users', args=[self.company1.pk])
        self.assertEqual(len(self.company1.get_members()), 1)
        response = self.client.post(url, {'users': 'dummy\ninvalid@email.com', 'type': 'member'}, follow=True)
        self.assertMessageContains(response, 'No users were added.')
        self.assertMessageContains(response, "These emails weren't found in the database: dummy, invalid@email.com")
        self.assertEqual(len(self.company1.get_members()), 1)

        response = self.client.post(url, {'users': 'test1@khnp.org\ntest4@khnp.org', 'type': 'member'}, follow=True)
        self.assertMessageContains(response, 'Users added: 1')
        self.assertEqual(len(self.company1.get_members()), 2)
        self.assertEqual(len(self.company1.get_members(only_managers=True)), 1)

        # Remove test2 email from Identity table so it is found in Contact table
        Identity.objects.filter(persona='test2@khnp.org').delete()
        response = self.client.post(url, {'users': 'test2@khnp.org', 'type': 'manager'}, follow=True)
        self.assertMessageContains(response, 'Users added: 1')
        self.assertEqual(len(self.company1.get_members()), 3)
        self.assertEqual(len(self.company1.get_members(only_managers=True)), 2)

    def test_admin_user_management_remove_users(self):
        self.client.login(username='admin', password='secret')
        url = reverse('admin_org_users', args=[self.company1.pk])

        response = self.client.post(url, {'users': 'test2@khnp.org\ntest4@khnp.org', 'type': 'member'}, follow=True)
        self.assertMessageContains(response, 'Users added: 2')
        response = self.client.post(url, {'users': 'test3@khnp.org', 'type': 'manager'}, follow=True)
        self.assertMessageContains(response, 'Users added: 1')
        self.assertEqual(len(self.company1.get_members()), 4)

        response = self.client.post(url, {'remove': ['M2', 'M4'], 'remove_users': 'Remove'}, follow=True)
        self.assertMessageContains(response, 'The requested users were removed from this organisation.')
        self.assertEqual(len(self.company1.get_members()), 2)

        response = self.client.post(url, {'remove': ['M1', 'M3'], 'remove_users': 'Remove'}, follow=True)
        self.assertMessageContains(response, 'No organisation Manager would left. The removal process has been aborted.')
        self.assertEqual(len(self.company1.get_members()), 2)

        response = self.client.post(url, {'remove': ['M1'], 'remove_users': 'Remove'}, follow=True)
        self.assertMessageContains(response, 'The requested users were removed from this organisation.')
        self.assertEqual(len(self.company1.get_members()), 1)

    def test_admin_user_management_remove_multi_org(self):
        """ Test removal of users which are members of several organisations """
        self.client.login(username='admin', password='secret')
        url = reverse('admin_org_users', args=[self.company1.pk])

        response = self.client.post(url, {'users': 'test4@khnp.org', 'type': 'member'}, follow=True)
        self.assertMessageContains(response, 'Users added: 1')
        self.assertEqual(len(self.company1.get_members()), 2)
        self.assertEqual(len(self.company4.get_members()), 1)

        # User 4 is member of company 1 and company 4
        self.assertTrue(self.user4 in self.company1.get_members())
        self.assertTrue(self.user4 in self.company4.get_members())

        # Let's remove the user 4 from org1
        response = self.client.post(url, {'remove': ['M4'], 'remove_users': 'Remove'}, follow=True)
        self.assertEqual(len(self.company1.get_members()), 1)
        self.assertEqual(len(self.company4.get_members()), 1)

        # be sure user is not removed.
        self.failUnless(Person.objects.get(pk=self.user4.pk))

    def test_admin_user_management_remove_invited_users(self):
        self.client.login(username='admin', password='secret')
        InvitedUser.objects.create(org=self.company1,
                                   person=self.user1,
                                   user=self.duser1,
                                   email='dummy@khnp.org',
                                   type=RelationshipType.objects.get(pk=1))

        url = reverse('admin_org_users', args=[self.company1.pk])
        response = self.client.post(url, {'remove': ['I1'], 'remove_users': 'Remove'}, follow=True)
        self.assertMessageContains(response, 'The requested users were removed from this organisation.')
        self.failIf(InvitedUser.objects.count())

    def test_dbtab_filter(self):
        url = reverse('admin_dashboard_database')
        response = self.client.get(url)
        self.assertContains(response, 'Please register or log in')
        self.assertNotContains(response, 'Reports')

        self.client.login(username='admin', password='secret')
        response = self.client.get(url)
        self.assertContains(response, 'Search database')
        self.assertContains(response, 'Reports')

        self.assertContains(response, '<td>Lorem</td>', html=True)
        self.assertContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertContains(response, '<td>Amet manula</td>', html=True)
        self.assertContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'text': 'met', 'submit': 'Filter'})

        self.assertNotContains(response, '<td>Lorem</td>', html=True)
        self.assertNotContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertNotContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertContains(response, '<td>Amet manula</td>', html=True)
        self.assertNotContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'text': 'lor', 'submit': 'Filter'})
        self.assertContains(response, '<td>Lorem</td>', html=True)
        self.assertContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertNotContains(response, '<td>Amet manula</td>', html=True)
        self.assertContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'text': 'lor', 'org_type': 'supplier', 'submit': 'Filter'})
        self.assertContains(response, '<td>Lorem</td>', html=True)
        self.assertContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertNotContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertNotContains(response, '<td>Amet manula</td>', html=True)
        self.assertNotContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'text': 'ipsum', 'org_type': 'supplier', 'submit': 'Filter'})
        self.assertNotContains(response, '<td>Lorem</td>', html=True)
        self.assertContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertNotContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertNotContains(response, '<td>Amet manula</td>', html=True)
        self.assertNotContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'text': 'blah', 'submit': 'Filter'})
        self.assertNotContains(response, '<td>Lorem</td>', html=True)
        self.assertNotContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertNotContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertNotContains(response, '<td>Amet manula</td>', html=True)
        self.assertNotContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'org_type': 'customer', 'submit': 'Filter'})
        self.assertNotContains(response, '<td>Lorem</td>', html=True)
        self.assertNotContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertContains(response, '<td>Amet manula</td>', html=True)
        self.assertContains(response, '<td>Sit pelor</td>', html=True)

        response = self.client.get(url, data={'org_type': 'supplier', 'submit': 'Filter'})
        self.assertContains(response, '<td>Lorem</td>', html=True)
        self.assertContains(response, '<td>Lorem ipsum</td>', html=True)
        self.assertNotContains(response, '<td>Dolor Sit</td>', html=True)
        self.assertNotContains(response, '<td>Amet manula</td>', html=True)
        self.assertNotContains(response, '<td>Sit pelor</td>', html=True)

    def test_dbtab_filter_org_type(self):
        url = reverse('admin_dashboard_database')
        self.client.login(username='admin', password='secret')
        response = self.client.get(url, data={'submit': 'Filter'})
        self.assertEqual(len(response.context['object_list']), 5)
        Visit.objects.create(
                    host=self.company1,
                    title='Visit title',
                    description='Visit description'
        )
        response = self.client.get(url, data={'org_type': 'supplier', 'submit': 'Filter'})
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(url, data={'org_type': 'mentor', 'submit': 'Filter'})
        self.assertEqual(len(response.context['object_list']), 0)
        response = self.client.get(url, data={'org_type': 'visithost', 'submit': 'Filter'})
        self.assertEqual(len(response.context['object_list']), 1)

        Mentor.objects.create(
                    mentor=self.user1,
                    mentor_org=self.company4,
                    description='Visit description'
        )

        response = self.client.get(url, data={'org_type': 'mentor', 'submit': 'Filter'})
        self.assertEqual(len(response.context['object_list']), 1)

    def test_dbrep(self):
        report = Report.objects.create(title="Test report",
                                       sql="select displayname from person",
                                       enabled=True)

        url = reverse('admin_dashboard_database')
        self.client.login(username='admin', password='secret')
        response = self.client.get(url, data={'report': report.pk, 'show-report': 'Show'})

        self.assertContains(response, 'Amet manula')
        self.assertEqual(response['Content-Type'], 'text/html; charset=utf-8')

        response = self.client.get(url, data={'report': report.pk, 'download-report': 'Show'})

        self.assertContains(response, 'Amet manula')
        self.assertEqual(response['Content-Type'], 'text/csv')

        report.sql = "some sql with error"
        report.save()
        response = self.client.get(url, data={'report': report.pk, 'download-report': 'Show'})

        self.assertEqual(response['Content-Type'], 'text/csv')
        self.assertContains(response, 'Database error')


class AdminDashboardInvoiceViewTest(TestCase):

    fixtures = ['types']

    def setUp(self):
        super(AdminDashboardInvoiceViewTest, self).setUp()
        User.objects.create_superuser(username='admin', email='test@khnp.org', password='secret')
        user1 = User.objects.create_user(username='testuser1', email='test1@khnp.org', password='secret')
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')

        comm_method = CommMethod.objects.create(comm_method='Post')

        self.user1 = KHNPUser(user1).get_person()
        self.company_cust = Organisation.objects.register(self.user1, defaults={'is_supplier': False})
        self.user2 = KHNPUser(user2).get_person()
        self.company_suppl = Organisation.objects.register(self.user2, defaults={'is_supplier': True})

        project = CustomerProject.objects.create(**{
            'comm_method': comm_method,
            'cover_note': u'Hello world',
            'created': datetime(2012, 11, 20).date(),
            'proposed': datetime(2012, 11, 20).date(),
            'cust': self.company_cust,
            'description': u'Description of the project',
            'startdate': datetime(2013, 1, 1).date(),
            'enddate': datetime(2013, 1, 31).date(),
            'price': 1000,
            'voucher_value': 1000,
            'suppl': self.company_suppl,
            'state': 'completed',
        })

        self.payment = project.invoice()

    def test_view(self):
        url = reverse('admin_dashboard_invoices')
        response = self.client.get(url)

        self.assertContains(response, 'Please register or log in.')
        self.client.login(username='admin', password='secret')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.context['payments']), 1)
        pop_url = reverse('admin_add_pop_view', args=(self.payment.pk, ))
        send_url = reverse('admin_send_to_finance', args=(self.payment.pk, ))
        self.assertContains(response, pop_url)
        self.assertNotContains(response, send_url)

        response = self.client.post(send_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Cannot send to finance")

        # Add POP
        response = self.client.get(pop_url)
        self.assertEqual(response.status_code, 200)

        data = {'pop': '1234567890'}
        response = self.client.post(pop_url, data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        payment = Payment.objects.get(pk=self.payment.pk)
        self.assertEqual(payment.pop, data['pop'])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.context['payments']), 1)
        pop_url = reverse('admin_add_pop_view', args=(self.payment.pk, ))
        send_url = reverse('admin_send_to_finance', args=(self.payment.pk, ))
        self.assertNotContains(response, pop_url)
        self.assertContains(response, send_url)

        response = self.client.get(send_url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(send_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        payment = Payment.objects.get(pk=self.payment.pk)
        self.failUnless(payment.sent_to_finance)

        self.assertEqual(len(mail.outbox), 2)

        response = self.client.post(send_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Cannot send to finance")

    def test_complaint(self):
        self.client.login(username='admin', password='secret')
        url = reverse('admin_dashboard_invoices')
        response = self.client.get(url)
        self.assertContains(response, "No")

        self.payment.has_complaint = True
        self.payment.save()
        response = self.client.get(url)
        self.assertContains(response, "Yes")
