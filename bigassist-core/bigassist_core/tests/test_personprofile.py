from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from bigassist_core.user import KHNPUser
from bigassist_core.models import Person

from base import TestCase

EMAIL = 'test@khnp.org'
EMAIL2 = 'test2@khnp.org'


class PersonProfileTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(PersonProfileTest, self).setUp()
        user = User.objects.create_user(username=EMAIL, password='secret', email=EMAIL)
        self.person = Person.objects.create(type_id=1,
                                       username=EMAIL,
                                       displayname='TEST USER',
                                       firstname='TEST',
                                       surname='USER')
        self.person.set_contact('email', EMAIL)
        # prepare Identity
        KHNPUser(user).setup_identity()

    def test_profile_form(self):
        self.client.login(username=EMAIL, password='secret')
        res = self.client.get(reverse('profile'))
        self.failUnless('TEST' in res.content)
        self.failUnless(EMAIL in res.content)

    def test_profile_form_edit(self):
        self.client.login(username=EMAIL, password='secret')
        res = self.client.get(reverse('profile_edit'))
        self.failUnless('value="TEST"' in res.content)
        self.failUnless('value="%s"' % EMAIL in res.content)
        res = self.client.post(reverse('profile_edit'), dict(firstname='TEXT',
                                                             surname='',
                                                             email='test@textmatters.com',
                                                             biography='Hello world'))
        self.failUnless('This field is required' in res.content)
        res = self.client.post(reverse('profile_edit'), dict(firstname='TEXT',
                                                             surname='MATTERS',
                                                             email='testtextmatters.com',
                                                             biography='Hello world'))
        self.failUnless('Enter a valid e-mail address.' in res.content)
        res = self.client.post(reverse('profile_edit'), dict(firstname='TEXT',
                                                             surname='MATTERS',
                                                             email='test@textmatters.com',
                                                             biography='Hello world'))
        self.failIf('error' in res.content)
        person = Person.objects.get(pk=self.person.pk)
        contact = person.get_contact('email')
        self.failUnless(person.firstname == 'TEXT')
        self.failUnless(person.surname == 'MATTERS')
        self.failUnless(person.username == EMAIL)
        self.failUnless(person.biography == 'Hello world')
        self.failUnless(contact == 'test@textmatters.com')

    def test_email(self):
        self.assertEqual(self.person.get_contact_email(), EMAIL)

        self.person.del_contact('email')
        self.assertEqual(self.person.get_contact_email(), None)

        self.person.set_contact('fallback_email', EMAIL2)
        self.assertEqual(self.person.get_contact_email(), EMAIL2)

        self.person.set_contact('email', EMAIL)
        self.assertEqual(self.person.get_contact_email(), EMAIL)
        self.assertEqual(self.person.get_contact('fallback_email'), EMAIL2)

        self.person.del_contact('email')

        # Now check that the fallback one is removed
        self.client.login(username=EMAIL, password='secret')
        res = self.client.post(reverse('profile_edit'), dict(firstname='TEXT',
                                                             surname='MATTERS',
                                                             email=EMAIL,
                                                             biography='Hello world'))
        self.failIf('error' in res.content)

        self.assertEqual(self.person.get_contact_email(), EMAIL)
        self.assertEqual(self.person.get_contact('fallback_email'), None)
