import os
from datetime import datetime
from lxml import etree
import mimetypes
import time
from django.db import transaction
from django.core.files import File
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from bigassist_core.models import Contact, Organisation, SupplierApproval, OrgType
from bigassist_core.user import KHNPUser
from django_browserid.auth import default_username_algo
# /data/envs/bigassist/bigassist/import


class Command(BaseCommand):
    help = "Initialize BIGAssist database content. The basedir argument points to the directory with XML files to import"
    args = '<basedir>'

    def xmltext(self, xml, xpath):
        elements = xml.xpath(xpath, namespaces=xml.nsmap)
        if elements:
            return elements[0].text or ''
        return ''

    @transaction.atomic
    def handle(self, *args, **options):
        if not args:
            basedir = '/data/envs/bigassist/bigassist/import'
            livemode = False
        else:
            basedir = args[0]
            livemode = 'liverun' in args

        if not os.path.isdir(basedir):
            raise CommandError('The base directory %s does not exist.' % basedir)

        email_type = settings.CONTACT_TYPES['email']

        ac_files_dir = os.path.join(basedir, 'ac_files')
        inprog_xml_dir = os.path.join(basedir, 'inprog_xml')
        submitted_xml_dir = os.path.join(basedir, 'submtd_xml')
        if not os.path.isdir(ac_files_dir):
            raise CommandError('The ac_files directory %s does not exist.' % ac_files_dir)
        if not os.path.isdir(inprog_xml_dir):
            raise CommandError('The inprog_xml directory %s does not exist.' % inprog_xml_dir)
        if not os.path.isdir(submitted_xml_dir):
            raise CommandError('The submtd_xml directory %s does not exist.' % submitted_xml_dir)

        all_files = os.listdir(ac_files_dir)

        # list all users
        all_users = []
        uf = open(os.path.join(basedir, 'users', 'submtd_users.csv'), 'r')
        for row in uf.readlines():
            userid, fullname, email = row.split(',')
            # True - submitted application flag
            all_users.append((True, userid.strip(), fullname.strip(), email.strip()))
        uf.close()

        uf = open(os.path.join(basedir, 'users', 'inprog_users.csv'), 'r')
        for row in uf.readlines():
            userid, fullname, email = row.split(',')
            # False - submitted application flag
            all_users.append((False, userid.strip(), fullname.strip(), email.strip()))
        uf.close()

        org_type_c = OrgType.objects.filter(org_type='Private company').all()
        if not org_type_c:
            org_type_c = OrgType.objects.filter(org_type='Company').all()
        org_type_c = org_type_c[0]
        orgtype = dict()
        orgtype['1'] = OrgType.objects.filter(org_type='Voluntary or community organisation').all()[0]
        orgtype['2'] = org_type_c
        orgtype['3'] = OrgType.objects.filter(org_type='Sole trader').all()[0]
        orgtype['4'] = OrgType.objects.filter(org_type='Other').all()[0]

        if livemode:
            SupplierApproval.objects.all().delete()

        idx = 0
        total = len(all_users)
        for is_submitted, userid, fullname, email in all_users:
            idx += 1
            c1 = Contact.objects.filter(type=email_type['type'], value=userid).all()
            if c1:
                c1 = c1[0]
            c2 = Contact.objects.filter(type=email_type['type'], value=email).all()
            if c2:
                c2 = c2[0]
            if (c1 and c2) and (c1.pk != c2.pk):
                raise Exception('Ambiguous contact')

            try:
                u1 = User.objects.get(email=email)
            except User.DoesNotExist:
                u1 = None
            try:
                u2 = User.objects.get(username=email)
            except User.DoesNotExist:
                u2 = None
            if (u1 and u2) and (u1.pk != u2.pk):
                raise Exception('Ambiguous user')
            user = u1 or u2
            if livemode:
                if user is None:
                    if len(userid) > 30:
                        username = default_username_algo(userid)
                    else:
                        username = userid
                    user = User.objects.create(username=username, email=email)

                khnp_user = KHNPUser(user, require_authenticated=False)
                identity = khnp_user.get_identity()
                person = identity.person
                person.displayname = fullname
                person.save()
                org = Organisation.objects.register(person, {'is_supplier': True})
                company = org.org
            if is_submitted:
                xml_file = os.path.join(submitted_xml_dir, "submtd.%s.xml" % userid)
            else:
                xml_file = os.path.join(inprog_xml_dir, "%s.xml" % userid)
            if not os.path.isfile(xml_file):
                raise CommandError('XML file has not been found: %s' % xml_file)
            xml = etree.parse(xml_file)
            root = xml.getroot()
            # [('submtd', '12-10-15 15:02'), ('appl_no', '161')]
            supplier_node = xml.xpath('/tm:assist_supplier', namespaces=root.nsmap)[0]
            appl_no = int(supplier_node.attrib.get('appl_no'))
            if is_submitted:
                submtd = time.strptime(supplier_node.attrib.get('submtd'), "%y-%m-%d %H:%M")
                submitted_date = datetime.fromtimestamp(time.mktime(submtd))
            else:
                submitted_date = None

            ac_pattern = 'ac.%s_' % userid
            ac_file = None
            for fname in all_files:
                if fname.startswith(ac_pattern):
                    ac_file = os.path.join(ac_files_dir, fname)
                    break

            has_ac_file = self.xmltext(root, '/tm:assist_supplier/tm:supplier_application/tm:finance/tm:upload_accounts')
            if has_ac_file == 'Yes' and not ac_file:
                CommandError('Ambiguous ac_file. XML states there is an ac_file, but no file found. %s' % userid)
            if has_ac_file == 'No' and ac_file:
                CommandError('Ambiguous ac_file. XML states there is no ac_file, but file found. %s' % userid)

            if livemode:
                approval = SupplierApproval.objects.create(appr_id=appl_no,
                                                           cust=org,
                                                           supplier_questions=open(xml_file, 'r').read(),
                                                           submitted=submitted_date)
                if ac_file:
                    approval.ac_file.save(os.path.basename(ac_file).lstrip(ac_pattern),
                                          File(open(ac_file)))
                    _type, _encoding = mimetypes.guess_type(os.path.basename(ac_file))
                    if _type:
                        approval.ac_file_type = _type
                    else:
                        approval.ac_file_type = 'application/octet-stream'
                    approval.save()

            xorg = xml.xpath('/tm:assist_supplier/tm:organisation', namespaces=root.nsmap)[0]
            displayname = self.xmltext(xorg, './tm:org_name').strip()
            org_type_id = self.xmltext(xorg, './tm:org_type_id')
            detail = self.xmltext(xorg, './tm:detail').strip()
            address2 = self.xmltext(xorg, './tm:address').strip()
            postcode2 = self.xmltext(xorg, './tm:postcode').strip()
            address5 = self.xmltext(xorg, './tm:reg_address').strip()
            postcode5 = self.xmltext(xorg, './tm:reg_postcode').strip()
            website = self.xmltext(xorg, './tm:website').strip()
            tel_general = self.xmltext(xorg, './tm:tel_general').strip()
            email_general = self.xmltext(xorg, './tm:email_general').strip()
            contact_name = self.xmltext(xorg, './tm:contact_name').strip()
            contact_email = self.xmltext(xorg, './tm:contact_email').strip()
            contact_tel = self.xmltext(xorg, './tm:contact_tel').strip()
            shortbio = self.xmltext(root, '/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:short_description')
            longbio = self.xmltext(root, '/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:long_description')

            if livemode:
                if org_type_id:
                    org.org_type = orgtype[org_type_id]  # use the map defined above
                org.detail = detail[:50]
                org.contact_name = contact_name
                org.contact_email = contact_email
                org.contact_tel = contact_tel
                if org_type_id in ('1', '3', '4'):
                    org.is_social_ent = True
                org.save()

                company.displayname = displayname
                # company.biography = longbio
                # company.shortbiography = shortbio
                company.save()

                approval.short_desc = shortbio
                approval.long_desc = longbio
                approval.save()

                # last-time-fixes
                if postcode2 in ('As above.', 'same as above', '07970564068', '01908564123'):
                    postcode2 = ''
                if address2 in ('As above.', 'same as above'):
                    address2 = ''
                if postcode5 in ('As above.', 'same as above', '07970564068', '01908564123'):
                    postcode5 = ''
                if address5 in ('As above.', 'same as above'):
                    address5 = ''

                data = dict()
                if address2 or postcode2:
                    data['main_address'] = address2
                    data['postcode'] = postcode2
                if address5 or postcode5:
                    data['registered_address'] = address5
                    data['registered_postcode'] = postcode5
                data['website'] = website
                data['tel_general'] = tel_general
                data['email_general'] = email_general
                company.set_additional(data)
                if (idx == 1) or (idx % 50 == 0) or (idx == total):
                    print "Importing %.3d/%.3d" % (idx, total)
                # print "Supplier %-50s has been created." % displayname
            else:
                print userid, displayname, org_type_id
