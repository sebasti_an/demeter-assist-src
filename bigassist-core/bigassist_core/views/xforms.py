#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django import http
from django.views.generic import View, FormView, TemplateView
from bigassist_core.models import XForms, XFORM_TYPES, SupplierApproval
from bigassist_core.forms import XFormsFileUploadForm
from bigassist_core.user import KHNPUser
from bigassist_core.utils import get_xform
import logging
logger = logging.getLogger(__name__)


class XFormsModelView(View):

    def get(self, request, *args, **kwargs):
        formtype = self.kwargs.get('formtype')
        id = self.kwargs.get('id')
        allowed_formtypes = [x[0] for x in XFORM_TYPES]
        if formtype not in allowed_formtypes:
            return http.HttpResponseBadRequest()

    #  return files instead of object
        if id == 'model':
            if formtype == 'C':
                value = get_xform('C_customer_self_assessment_model.xml')
            elif formtype == 'S':
                value = get_xform('S_supplier_assessment_model.xml')
        else:
            if formtype == 'C':
                value = get_xform('C_customer_self_assessment_questions.xml')
            # elif: formtype == 'S'
            #     value = get_xform()

        # xform_model = get_object_or_404(XForms, formtype=formtype)
        # if id == 'model':
        #     fieldname = 'model'
        # else:
        #     fieldname = 'custom_%s' % id
        # value = getattr(xform_model, fieldname, None)

        if value is None:
            return http.HttpResponseBadRequest('Invalid field id')

        return http.HttpResponse(unicode(value).encode('utf-8'), content_type='text/xml')


class XFormsFileUpDownloadBase(object):

    def dispatch(self, request, *args, **kwargs):
        # the view can be called either by owner of the diagnostic
        # or by admin
        user = request.user
        logger.info('Current user: %s' % user)
        if not user.is_staff:
            try:
                logger.info('User is not staff.')
                logger.info('        anonymous?: %s' % user.is_anonymous())
                logger.info('    authenticated?: %s' % user.is_authenticated())
                logger.info('               pk?: %s' % getattr(user, 'pk', ''))
                logger.info('   requested diag?: %s' % kwargs.get('diag_id'))
            except Exception, e:
                logger.error('Error in logging: %s' % str(e))
            khnp_user = KHNPUser(user)
            company = khnp_user.get_company()
            if company:
                formtype = kwargs.get('formtype')
                if (formtype == 'S' and 'supplier' in company.get_roles()):
                    diagnostic = company.get_supplier_diagnostic()
                    if diagnostic:
                        if diagnostic.pk != int(kwargs.get('diag_id')):
                            logger.info('    ACCESS DENIED (diag)')
                            return http.HttpResponseForbidden('Invalid diagnostic')
                elif (formtype == 'C' and 'customer' in company.get_roles()):
                    diagnostic = company.get_customer_diagnostic()
                    if diagnostic:
                        if diagnostic.pk != int(kwargs.get('diag_id')):
                            logger.info('    ACCESS DENIED (diag)')
                            return http.HttpResponseForbidden('Invalid diagnostic')
                else:
                    logger.info('    ACCESS DENIED (company)')
                    return http.HttpResponseForbidden('Invalid company type')
        else:
            logger.info('User is staff')
        return super(XFormsFileUpDownloadBase, self).dispatch(request, *args, **kwargs)


class XFormsFileUpload(XFormsFileUpDownloadBase, FormView):
    template_name = 'xforms_upload_form.html'
    form_class = XFormsFileUploadForm

    def get_context_data(self, **kwargs):
        context = super(XFormsFileUpload, self).get_context_data(**kwargs)
        context['formtype'] = self.kwargs.get('formtype')
        context['fileilk'] = self.kwargs.get('fileilk')
        context['diag_id'] = int(self.kwargs.get('diag_id'))

        field_name = context['fileilk'] + '_file'  # eg. ac_file is the only supported atm
        context['file'] = ''
        if context['formtype'] == 'S':
            try:
                diag = get_object_or_404(SupplierApproval, pk=int(context['diag_id']))
                file = getattr(diag, field_name, None)
                filename = getattr(file, 'name', '')
                context['file'] = os.path.split(filename)[-1]
            except:
                pass
        return context

    def form_valid(self, form):
        formtype = self.kwargs.get('formtype')
        if formtype == 'S':
            diag = get_object_or_404(SupplierApproval, pk=int(self.kwargs.get('diag_id')))
            fileilk = self.kwargs.get('fileilk')
            field_name = fileilk + '_file'  # eg. ac_file is the only supported atm
            fieldtype_name = fileilk + '_file_type'  # eg. ac_file is the only supported atm
            if not hasattr(diag, field_name):
                raise http.HttpResponseBadRequest('Invalid field')
        else:
            raise http.HttpResponseBadRequest('Invalid form type')

        fp = form.cleaned_data['file']
        # remove old file and set new file
        old = getattr(diag, field_name, None)
        if old:
            old.delete()
        setattr(diag, field_name, fp)
        setattr(diag, fieldtype_name, fp.content_type)
        diag.save()
        return http.HttpResponseRedirect(reverse('xforms_fileupload', args=[formtype, fileilk, diag.pk]))


class XFormsFileDownload(XFormsFileUpDownloadBase, TemplateView):
    template_name = 'xforms_download_form.html'
    download = False

    def get_context_data(self, **kwargs):
        context = super(XFormsFileDownload, self).get_context_data(**kwargs)
        context['formtype'] = self.kwargs.get('formtype')
        context['fileilk'] = self.kwargs.get('fileilk')
        context['diag_id'] = int(self.kwargs.get('diag_id'))

        field_name = context['fileilk'] + '_file'  # eg. ac_file is the only supported atm
        fieldtype_name = context['fileilk'] + '_file_type'  # eg. ac_file is the only supported atm
        context['file'] = ''
        if context['formtype'] == 'S':
            context['diag'] = get_object_or_404(SupplierApproval, pk=int(context['diag_id']))
            context['file_object'] = getattr(context['diag'], field_name, None)
            context['file_type'] = getattr(context['diag'], fieldtype_name, None)
            filename = getattr(context['file_object'], 'name', '')
            context['file'] = os.path.split(filename)[-1]
        return context

    def get(self, request, **kwargs):
        if self.download:
            context = self.get_context_data(**kwargs)
            fp = context.get('file_object')
            if fp:
                if context['file'] == self.kwargs.get('filename'):
                    return http.HttpResponse(fp.chunks(8096), content_type=context['file_type'])
                else:
                    return http.HttpResponseBadRequest('Invalid file name')
        return super(XFormsFileDownload, self).get(request, **kwargs)
