import sys
from django.core.management.base import NoArgsCommand
from bigassist_core import models


class Command(NoArgsCommand):
    help = "Check state of all supplier approvals. Just go through all records and compare if state field contains what it should."
    failed = False

    def fail(self, obj, msg):
        print >>sys.stdout, "******* ERROR - %s: State is '%s' but %s" % (obj.pk, obj.state, msg)
        self.failed = True

    def handle_noargs(self, **options):
        self.failed = False
        print >>sys.stdout, "Checking consistency of supplier approval state"
        for obj in models.SupplierApproval.objects.all():
            state = obj.state
            if state == 'approved':
                if not obj.is_approved:
                    self.fail(obj, 'is_approved is not True')
                if not obj.submitted:
                    self.fail(obj, 'submitted is not set')
                if not obj.assessed:
                    self.fail(obj, 'assessed is not set')
                if not obj.decision:
                    self.fail(obj, 'decision is not set')
            if state == 'submitted':
                if not obj.submitted:
                    self.fail(obj, 'submitted is not set')
                if obj.decision:
                    self.fail(obj, 'decision is set')
                if obj.assessed:
                    self.fail(obj, 'assessed is set')
            if state == 'in_progress':
                if obj.submitted:
                    self.fail(obj, 'submitted is set')
                if obj.is_approved:
                    self.fail(obj, 'is_approved is set to True')
                if obj.decision:
                    self.fail(obj, 'decision is set')
                if obj.assessed:
                    self.fail(obj, 'assessed is set')
            if state == 'reviewed':
                if not obj.submitted:
                    self.fail(obj, 'submitted is not set')
                if not obj.assessed:
                    self.fail(obj, 'assessed is not set')
                if obj.decision:
                    self.fail(obj, 'decision is set')
                if not obj.assessor_comments:
                    self.fail(obj, 'assessor_comments is not set')
        if not self.failed:
            print >>sys.stdout, "There were no errors"
