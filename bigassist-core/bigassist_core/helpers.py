import datetime

"""
Please keep this module free from internal imports, like KHNPUser etc.
I've created this module to avoid circular imports in utils.py,
eg. from models.py
"""


def date_passed(date, now=None, force_date_check=False):
    if not isinstance(date, datetime.date):
        # isinstance(datetime.date) covers both date and datetime
        return False

    if now is None:
        now = datetime.datetime.now()
    if force_date_check and isinstance(date, datetime.datetime):
        date = date.date()
    if not isinstance(date, datetime.datetime):
        now = now.date()
    return now > date


def get_or_none(qs):
    """ pass a queryset to this function. It will return first record or None """
    if qs:
        return qs[0]
    return None
