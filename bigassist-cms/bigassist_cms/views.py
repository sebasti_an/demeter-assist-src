#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create your views here.

import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt  # cross-site request forgery
from django.views.decorators.http import require_POST

from django.views.generic import ListView, DetailView, TemplateView

from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404

from django.contrib.auth.decorators import login_required

from models import File, Section, Page, Event


class PageDetailView(DetailView):
    model = Page
    template_name = 'page_detail.html'

    def get_context_data(self, **kwargs):
        context = super(PageDetailView, self).get_context_data(**kwargs)
        slug = self.kwargs.get('section_slug')
        context['section'] = get_object_or_404(Section, slug=slug)

        return context


class PageListView(ListView):
    model = Page
    template_name = 'page_list.html'
    paginate_by = 10
    section = None

    def get_queryset(self):
        self.section = get_object_or_404(Section, slug=self.slug)
        if self.slug == 'news':
            return Page.objects.news()
        elif self.slug == 'guides':
            return Page.objects.guides()
        else:
            return Page.objects.public().filter(section=self.section)

    def get(self, request, *args, **kwargs):
        self.slug = kwargs.get('section_slug')
        return super(PageListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PageListView, self).get_context_data(**kwargs)

        context['section'] = self.section

        return context


class EventListView(ListView):
    model = Event
    template_name = 'event_list.html'

    def get_context_data(self, **kwargs):
        context = super(EventListView, self).get_context_data(**kwargs)
        return context


# Views for image uploads

@csrf_exempt
@require_POST
@login_required
def upload_photos(request):
    images = []
    for f in request.FILES.getlist('file'):
        obj = File.objects.create(upload=f, is_image=True)
        images.append({'filelink': obj.upload.url})
    return HttpResponse(json.dumps(images), content_type='application/json')


@login_required
def recent_photos(request):
    images = [
        {'thumb': obj.upload.url, 'image': obj.upload.url}
        for obj in File.objects.filter(is_image=True).order_by('-created_on')[:20]
    ]
    return HttpResponse(json.dumps(images), content_type='application/json')
