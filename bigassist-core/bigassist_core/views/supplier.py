#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
from datetime import datetime
import json
from django.conf import settings
from django.db import transaction
from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpResponseForbidden
from django.views.generic import View, TemplateView, FormView
from django.views.generic import DetailView, UpdateView
from django.shortcuts import get_object_or_404
from bigassist_core import models
from bigassist_cms.models import Section
from bigassist_core import forms
from bigassist_core.user import KHNPUser
from bigassist_core.utils import SupplierRequiredDecorator
from bigassist_core.utils import ConfirmationDetailView
from bigassist_core.utils import ConfirmationView
from bigassist_core.utils import get_xform
from bigassist_core.views.abstract import InviteUsersBaseView
from bigassist_core.xml import XMLManipulator
from bigassist_core.security import SupplierProjectSecurity
from bigassist_core.utils import LoginRequiredDecorator
from bigassist_core.emails import send_ba_email
from bigassist_core.security import SupplierApprovalSecurity


@LoginRequiredDecorator
class SupplierPrequalsView(TemplateView):
    template_name = "supplier/prequals.html"

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        if not company:
            return HttpResponseRedirect(reverse('home'))

        return super(SupplierPrequalsView, self).dispatch(request, *args, **kwargs)


@SupplierRequiredDecorator
class SupplierAssessmentContactView(UpdateView):
    model = models.SupplierApproval
    form_class = forms.UpdateSupplierApprovalContactForm

    success_message = 'Supplier approval contact details updated'

    template_name = 'supplier/assessment_contact_edit.html'

    diag = None

    modal = False

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(self.request.user)
        company = khnp_user.get_company()
        if company:
            # make sure we have model already, otherwise we can't generate correct URL for file upload
            self.diag = company.get_supplier_diagnostic()
        if 'modal' in self.request.GET or 'modal' in self.request.POST:
            self.modal = True
        return super(SupplierAssessmentContactView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        return self.model.objects.get(pk=self.diag.pk)

    def get_success_url(self):
        if self.modal:
            return reverse_lazy('supplier_dashboard', args=['assessment'])
        return reverse('supplier_assessment')

    def get_context_data(self, **kwargs):
        context = super(SupplierAssessmentContactView, self).get_context_data(**kwargs)
        context['is_modal'] = self.modal
        return context

    def get_initial(self):
        if self.diag.prev_appr and not self.diag.contact_name:
            prev_appr = models.SupplierApproval.objects.get(pk=self.diag.prev_appr.pk)
            initial = {
                'contact_name': prev_appr.contact_name,
                'contact_title': prev_appr.contact_title,
                'contact_phone': prev_appr.contact_phone,
                'contact_email': prev_appr.contact_email,
            }
            return initial

    def form_valid(self, form):
        form.save()

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")
        return HttpResponseRedirect(url)


@SupplierRequiredDecorator
class SupplierAssessmentView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        # we need to manipulate the form and provide fixed src attributes for the iframes
        khnp_user = KHNPUser(request.user)
        company = khnp_user.get_company()
        xml = 'N/A - Missing company'
        if company:
            # make sure we have model already, otherwise we can't generate correct URL for file upload
            diag = company.get_supplier_diagnostic()
            if diag is None:
                diag = initialize_supplier_approval(company)

            if diag:
                prog_role = models.ProgrammeRole.objects.get(prog_id=1, role='supplier')
                appr_prog_role_rel = models.SupplierApprovalProgrammeRole.objects.get_or_create(prog_role=prog_role, appr=diag)
                apr_obj, apr_created = appr_prog_role_rel
                if not apr_created:
                    apr_obj.save()

            if not diag.has_contact_info():
                return SupplierAssessmentContactView.as_view()(request, **kwargs)

            readonly = ('readonly' in request.GET) or diag.submitted
            if readonly:
                # xml = obj.xform_ro
                xml = get_xform('S_supplier_assessment_readonly.html')
            else:
                if diag.resubmission():
                    # If resubmission, use XFORM stored in custom_1 field.
                    # See https://it.fry-it.com/text-matters/dev/135#i3
                    # xml = obj.custom_1
                    xml = get_xform('S_supplier_assessment_reapplication.html')
                else:
                    # xml = obj.xform
                    xml = get_xform('S_supplier_assessment_source.html')
            if isinstance(xml, unicode):
                xml = xml.encode('utf-8')

            if readonly:
                manip = XMLManipulator(xml)
                manip.set_iframe('file-download', reverse('xforms_filedownload', args=['S', 'ac', diag.pk]))
                xml = manip.tostring()
            else:
                manip = XMLManipulator(xml)
                manip.set_iframe('file-upload', reverse('xforms_fileupload', args=['S', 'ac', diag.pk]))
                xml = manip.tostring()
        return HttpResponse(xml, content_type='text/xml')


@SupplierRequiredDecorator
class SupplierAssessmentModelView(View):

    @transaction.commit_manually
    def get(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()

        approvals = models.SupplierApproval.objects.filter(suppl=company).order_by('-created').all()
        if approvals:
            approval = approvals[0]
            model = approval.supplier_questions
        else:
            obj = initialize_supplier_approval(company)
            model = obj.supplier_questions

        transaction.commit()
        if isinstance(model, unicode):
            model = model.encode('utf-8')
        # cleanup XML whitespace. Refs. https://it.fry-it.com/text-matters/dev/029
        with transaction.commit_manually():
            xml = XMLManipulator(model, remove_blank_text=True)
            xml.insert_organisation_info(company, approval)
            xml.whitespace_cleanup()
            model = xml.tostring()
            del xml
            transaction.commit()
        return HttpResponse(model, content_type='text/xml')


@SupplierRequiredDecorator
class SupplierAssessmentPrevModelView(View):

    def get(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        company = user.get_company()

        model = None
        approval = company.get_supplier_diagnostic()
        if approval is not None:
            prev = approval.prev_appr
            if prev:
                model = prev.supplier_questions

        if model is None:
            # This should never happen
            return HttpResponse('Invalid data', content_type='text/plain')

        if isinstance(model, unicode):
            model = model.encode('utf-8')
        # cleanup XML whitespace. Refs. https://it.fry-it.com/text-matters/dev/029
        xml = XMLManipulator(model, remove_blank_text=True)
        xml.whitespace_cleanup()
        return HttpResponse(xml.tostring(), content_type='text/xml')


@SupplierRequiredDecorator
class SupplierAssessmentSaveView(View):

    def post(self, request, *args, **kwargs):
        """ currently implements quick save only
        """
        xml = request.body
        try:
            etree.XML(xml)
        except etree.XMLSyntaxError:
            return HttpResponse('ERROR: Invalid XML content')
            # return HttpResponseBadRequest('Invalid XML content')
        user = KHNPUser(request.user)
        company = user.get_company()

        approval = company.get_supplier_diagnostic()
        security = SupplierApprovalSecurity(approval)
        do_submit = kwargs.get('submit') and security.check('can_submit_approval')

        manip = XMLManipulator(xml)
        if do_submit:
            now = datetime.now()
            manip.set_attribute('/tm:assist_supplier', 'submtd', now.strftime('%y-%m-%d %H:%M'))
        xml = manip.tostring()

        if approval is not None:
            approval.supplier_questions = xml
            approval.save()
        else:
            approval = models.SupplierApproval.objects.create(suppl=company,
                                                              supplier_questions=xml)

        if do_submit:
            approval.set_state('submitted')
            send_ba_email('04_supplier_submitted_assessment', {}, sender=None, recipients=company, bcc=settings.BIGASSIST_ADMIN_EMAIL)
            messages.info(request, 'Your application has been submitted for approval.')

        return HttpResponseRedirect(reverse('home'))


@SupplierRequiredDecorator
class SupplierAssessmentResubmitView(ConfirmationView):
    template_name = 'confirmation.html'
    success_url = reverse_lazy('supplier_dashboard', args=['assessment'])
    description = "You will be able to edit and re-submit your application. Continue?"
    action = 'Yes'

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        self.company = khnp_user.get_company()
        self.diag = self.company.get_supplier_diagnostic()

        security = SupplierApprovalSecurity(self.diag)
        if not security.check('can_resubmit_approval'):
            return HttpResponseForbidden("You can't resubmit approval now.")
        return super(SupplierAssessmentResubmitView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        # create new approval record
        ac_file = self.diag.ac_file
        ac_file_type = self.diag.ac_file_type
        approval = models.SupplierApproval.objects.create(suppl=self.diag.suppl,
                                                          supplier_questions=self.diag.supplier_questions,
                                                          prev_appr=self.diag,
                                                          ac_file_type=ac_file_type,
                                                          state='in_progress')
        self.diag.next_appr = approval
        self.diag.save()

        for at in self.diag.approvaltopic_set.all():
            at_topic, created = approval.approvaltopic_set.get_or_create(topic=at.topic)
            at_topic.is_approved=at.is_approved
            at_topic.save()

        if ac_file:
            approval.ac_file.save(ac_file.name, ac_file)

        return self.render_success(request)


@SupplierRequiredDecorator
class SupplierAssessmentAppealView(FormView):

    template_name = 'supplier/appeal.html'
    form_class = forms.SupplierAssessmentAppealForm
    success_url = reverse_lazy('home')

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        self.org = khnp_user.get_company()
        approval = self.org.get_supplier_diagnostic()

        security = SupplierApprovalSecurity(approval)
        if not security.check('can_appeal_approval'):
            return HttpResponseForbidden()
        return super(SupplierAssessmentAppealView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        context = {'message': form.cleaned_data['message'],
                   'organisation': self.org}

        send_ba_email('36_supplier_appeals_rejected_assessment', context, sender=None,
                      recipients=settings.BIGASSIST_ADMIN_EMAIL)

        messages.success(self.request, "Your appeal has been lodged")
        return super(SupplierAssessmentAppealView, self).form_valid(form)


@SupplierRequiredDecorator
class SupplierDashboardView(View):

    def dispatch(self, request, *args, **kwargs):
        super(SupplierDashboardView, self).dispatch(request, *args, **kwargs)
        tab = self.kwargs.get('tab')
        if tab == 'assessment':
            return SupplierDashboardAssessmentView.as_view()(request, **kwargs)
        if tab == 'contacts':
            return SupplierContactsView.as_view()(request, **kwargs)
        if tab == 'projects':
            return SupplierProjectsView.as_view()(request, **kwargs)
        if tab == 'invoices':
            return SupplierInvoicesView.as_view()(request, **kwargs)
        if tab == 'users':
            return SupplierDashboardUsersView.as_view()(request, **kwargs)
        else:
            return HttpResponseRedirect(reverse('home'))


class SupplierDashboardBaseView(TemplateView):
    tab_name = 'assessment'

    def get_context_data(self, **kwargs):
        context = super(SupplierDashboardBaseView, self).get_context_data(**kwargs)

        context['tab'] = self.tab_name
        context['khnp_user'] = KHNPUser(self.request.user)
        org = context['org'] = context['khnp_user'].get_company()

        context['section'] = get_object_or_404(Section, slug='news')

        projects = context['org'].supplier_projects
        context['no_contacts'] = projects.potential().count()
        context['no_projects'] = projects.current().count()
        context['no_invoices'] = projects.invoiced().count()

        diag = context['org'].get_supplier_diagnostic()
        if diag and diag.state == "approved":
            if not all((org.bank_name, org.bank_ac_name, org.bank_ac_no, org.bank_sortcode, )):
                context['bank_warning'] = True

        return context


@SupplierRequiredDecorator
class SupplierDashboardAssessmentView(SupplierDashboardBaseView):
    tab_name = 'assessment'
    template_name = "supplier/dashboard_assessment.html"

    def get_context_data(self, **kwargs):
        context = super(SupplierDashboardAssessmentView, self).get_context_data(**kwargs)
        context['assessment'] = context['org'].get_supplier_diagnostic()

        context['deadlines'] = models.SupplierDeadline.objects.next_deadlines()[:2]
        return context


@SupplierRequiredDecorator
class SupplierContactsView(SupplierDashboardBaseView):
    tab_name = 'contacts'
    template_name = "supplier/dashboard_contacts.html"

    def get_context_data(self, **kwargs):
        context = super(SupplierContactsView, self).get_context_data(**kwargs)
        context['projects'] = context['org'].supplier_projects.potential()
        return context


@LoginRequiredDecorator
class SupplierDashboardUsersView(InviteUsersBaseView, SupplierDashboardBaseView):
    tab_name = 'users'
    template_name = 'org_users.html'

    def get_context_data(self, **kwargs):
        context = super(SupplierDashboardUsersView, self).get_context_data(**kwargs)
        context['base_template'] = 'supplier/dashboard.html'
        return context


@SupplierRequiredDecorator
class SupplierProjectsView(SupplierDashboardBaseView):
    tab_name = 'projects'
    template_name = "supplier/dashboard_projects.html"

    def get_context_data(self, **kwargs):
        context = super(SupplierProjectsView, self).get_context_data(**kwargs)
        context['projects'] = context['org'].supplier_projects.current()
        return context


@SupplierRequiredDecorator
class SupplierInvoicesView(SupplierDashboardBaseView):
    tab_name = 'invoices'
    template_name = "supplier/dashboard_invoices.html"

    def get_context_data(self, **kwargs):
        context = super(SupplierInvoicesView, self).get_context_data(**kwargs)
        context['projects'] = context['org'].supplier_projects.invoiced()
        return context


class SupplierProjectMixin(object):

    permission = None

    def dispatch(self, request, *args, **kwargs):
        khnp_user = KHNPUser(request.user)
        company = khnp_user.get_company()
        self.project = get_object_or_404(models.CustomerProject, pk=kwargs['pk'])

        if self.project.suppl != company:
            return HttpResponseForbidden()

        if self.permission:
            security = SupplierProjectSecurity(self.project)
            if not security.check(self.permission):
                return HttpResponseForbidden()

        return super(SupplierProjectMixin, self).dispatch(request, *args, **kwargs)


@SupplierRequiredDecorator
class SupplierPreProjectDetailView(SupplierProjectMixin, DetailView):

    model = models.CustomerProject
    context_object_name = "project"
    template_name = "supplier/preproject_detail.html"

    permission = "can_view_eoi"


@SupplierRequiredDecorator
class SupplierProjectContactsView(SupplierProjectMixin, DetailView):

    model = models.CustomerProject
    context_object_name = "project"
    template_name = "supplier/project_contacts.html"

    permission = "can_view_contacts"


@SupplierRequiredDecorator
class SupplierProjectMakeView(SupplierProjectMixin, UpdateView):

    model = models.CustomerProject
    context_object_name = "project"
    form_class = forms.SupplierMakeProject
    template_name = "supplier/project_make.html"
    success_url = reverse_lazy("supplier_dashboard", args=("contacts", ))

    permission = "can_make_project"

    @transaction.atomic
    def form_valid(self, form):
        project = form.save(commit=False)
        project.set_state("proposed", commit=False)
        project.save()

        context = {'project': project}

        send_ba_email('102_supplier_proposes_project_customer', context, sender=None, recipients=project.cust, recipient_role='customer')
        send_ba_email('101_supplier_proposes_project_supplier', context, sender=None, recipients=project.suppl, recipient_role='supplier')
        return HttpResponseRedirect(self.get_success_url())


@SupplierRequiredDecorator
class SupplierProjectEditView(SupplierProjectMixin, UpdateView):

    model = models.CustomerProject
    context_object_name = "project"
    form_class = forms.SupplierMakeProject
    template_name = "supplier/project_edit.html"
    success_url = reverse_lazy("supplier_dashboard", args=("contacts", ))

    permission = "can_edit_project"

    @transaction.atomic
    def form_valid(self, form):
        project = form.save(commit=False)
        # project.set_state("proposed", commit=False)
        project.save()

        context = {'project': project}

        messages.info(self.request, 'You have successfully updated the project.')

        send_ba_email('104_supplier_edits_project_supplier', context, sender=None, recipients=project.cust, recipient_role='customer')
        send_ba_email('105_supplier_edits_project_customer', context, sender=None, recipients=project.suppl, recipient_role='supplier')
        return HttpResponseRedirect(self.get_success_url())


@SupplierRequiredDecorator
class SupplierProjectDismissView(SupplierProjectMixin, ConfirmationDetailView):
    template_name = 'confirmation.html'
    model = models.CustomerProject
    success_url = reverse_lazy('supplier_dashboard', args=['contacts'])
    description = "You are about to dismiss this project. Continue?"
    action = 'Yes'

    permission = "can_dismiss_project"

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.project.set_state('dismissed', by_customer=False)

        context = {'project': self.project}
        send_ba_email('32_supplier_does_not_accept_project', context, sender=None, recipients=self.project.cust, recipient_role='customer')

        return self.render_success(request)


@SupplierRequiredDecorator
class SupplierProjectDetailView(SupplierProjectMixin, DetailView):

    model = models.CustomerProject
    context_object_name = "project"
    template_name = "supplier/project_detail.html"

    permission = "can_view_project"


@SupplierRequiredDecorator
class SupplierProjectCompleteView(SupplierProjectMixin, ConfirmationDetailView):
    template_name = 'confirmation.html'
    model = models.CustomerProject
    success_url = reverse_lazy('supplier_dashboard', args=['projects'])
    description = "You are about to mark this project completed. Continue?"
    action = 'Yes'

    permission = "can_complete_project"

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.project.set_state('mark_completed')
        payment = self.project.invoice()

        # remove this message as soon as emails are implemented
        messages.warning(request, 'You have marked your project as complete. '
                                  'We have sent you an email explaining how to invoice '
                                  'for the work you have done. If you do not receive '
                                  'it shortly please get in touch with the BIG Assist office.')

        context = {'project': self.project,
                   'payment': payment}

        recipients = set()
        for member in self.project.cust.get_members(only_managers=True):
            contact = member.get_contact_email()
            if contact:
                recipients.add(contact)
        send_ba_email('122_supplier_complete_project_customer', context, sender=None, recipients=recipients)

        recipients = set()
        for member in self.project.suppl.get_members(only_managers=True):
            contact = member.get_contact_email()
            if contact:
                recipients.add(contact)

        # Find mail template to send to a supplier
        if payment is None:
            mail_template = '124_supplier_complete_project_supplier_no_invoice'
        else:
            if payment.amount == self.project.price:
                mail_template = '121_supplier_complete_project_supplier'
            else:
                mail_template = '123_supplier_complete_project_supplier_partial'

        send_ba_email(mail_template, context, sender=None, recipients=recipients)
        return self.render_success(request)


@SupplierRequiredDecorator
class SupplierCustomerCapabilityReportView(SupplierProjectMixin, DetailView):
    model = models.CustomerProject
    template_name = "supplier/customer_recommendation.html"
    permission = "can_view_cust_capability_report"

    def get_context_data(self, *args, **kwargs):
        context = super(SupplierCustomerCapabilityReportView, self).get_context_data(*args, **kwargs)
        context['diag'] = self.get_object().cust.get_customer_diagnostic()
        return context


def initialize_supplier_approval(company):
    model = get_xform('S_supplier_assessment_model.xml')
    approval = models.SupplierApproval.objects.create(suppl=company, supplier_questions=model)
    appl_no = approval.pk
    manip = XMLManipulator(model)
    manip.set_attribute('/tm:assist_supplier', 'appl_no', appl_no)
    approval.supplier_questions = manip.tostring()
    approval.save()
    return approval
