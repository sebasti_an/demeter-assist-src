#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail import get_connection
from django.template import Template
from django.template import Context
from bigassist_core.models import Organisation, MailTemplate, Person

EMAIL_TEXT = {
    '01_get_in_touch': {
            'subject': 'Customer contact form enquiry',
            'message':
"""<strong>Sender:</strong> {{ sender }}
<strong>Organisation:</strong> {{ organisation }}
<strong>Message:</strong>
{{message}} 
"""
    },
    '02_supplier_registered': {
            'subject': 'You have now registered with BIG Assist',
            'message':
"""{% load absurl %}Welcome to BIG Assist

Your account username is the email you registered with.

You can log in using this and the password you chose on the registration form.

<strong>What to do now</strong>
When you're logged in to the site you can:
<ul>
<li>Fill out the <a href="{% absurl 'supplier_assessment' %}">supplier application</a></li>
{# <li>Keep up to date with <a href="{% absurl 'home' %}">your projects</a></li> #}
<li>Meet users and suppliers and share your practical knowledge in <a href="{% absurl 'marketplace' %}">Sharespace</a></li>
<li>Read the latest BIG Assist <a href="{% absurl 'home' %}">news</a></li>
</ul>
Now you have registered your <a href="{% absurl 'home' %}">personal dashboard</a> will show you any updates on your activity on the BIG Assist site.
<strong>Forgotten your password?</strong>
You can set a new password for your account by revisiting the Persona registration system and clicking on ‘<em>Forgotten your password?</em>’.
Thanks,
The BIG Assist team

"""
    },
    '03_customer_registered': {
            'subject': 'You have now registered with BIG Assist',
            'message':
"""{% load absurl %}Welcome to BIG Assist

Your account username is the email you registered with.

You can log in using this and the password you chose on the registration form.

<strong>What to do now</strong>
When you're logged in to the site you can:
<ul>
<li>Start your <a href="{% absurl 'customer_qualification' %}">Self-assessment</a></li>
{# <li>Keep up to date with <a href="FIXME">your projects</a></li> #}
<li>Meet users and suppliers and share your practical knowledge in <a href="{% absurl 'marketplace' %}">the ShareSpace</a></li>
<li>Find a mentor and arrange visits to other organisations or else invite people to see your organisation and/or become a mentor yourself in <a href="{% absurl 'marketplace' %}">the ConnectSpace</a></li>
<li>Read the latest BIG Assist <a href="{% absurl 'home' %}">news and blogs</a></li>
</ul>
Now you have registered your <a href="{% absurl 'home' %}">personal dashboard</a> will show you any updates on your activity on the BIG Assist site.
<strong>Forgotten your password?</strong>
You can set a new password for your account by revisiting the Persona registration system and clicking on ‘<em>Forgotten your password?</em>’.
Thanks,
The BIG Assist team

"""
    },
    '04_supplier_submitted_assessment': {
            'subject': 'Your BIG Assist supplier application has been submitted',
            'message':
"""{% load absurl %}Thank you for submitting your application to become a BIG Assist supplier.

Your application will now be passed to an Independent Review Panel. This Panel is made up of a range of independent individuals who have experience of the voluntary and community sector and is not influenced by either BIG or NCVO.

The review process may take several weeks, so please be patient. Following review by the Panel, we will advise you of the outcome of your application by email to the address you have entered on your application form. In the event that you are not successful, either as an organisation or in some of the services you wish to offer, we will advise you of the reasons for the Panel's decision.

If your email address changes or you have any questions about the process, let us know by emailing us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

Thanks,

The BIG Assist team

"""
    },
    '051_supplier_received_decision_approved': {
            'subject': 'You have been approved as a BIG Assist Supplier',
            'message':
"""{% load absurl %}Thank you for your application; following our assessment process we are very pleased to inform you that you have been approved.

Promotional information you provided within your application will be used to populate the <a href="{% absurl 'marketplace' %}">BIG Assist Marketplace</a>; this information will help customers review and select the supplier of their choice; no confidential and/or financial information will be shared.

Information on how you can promote yourself further can be found on our website: http://www.bigassist.org.uk/news/approved-supplier-information

If you have any queries at this stage please do not hesitate to contact the BIG Assist Team at NCVO

T: 020 7520 2418
E: <a href="mailto:supplier.assist@ncvo.org.uk">supplier.assist@ncvo.org.uk</a>.

Kind regards,
The BIG Assist Team

"""
    },
    '052_supplier_received_decision_rejected': {
            'subject': 'BIG Assist Supplier Application',
            'message':
"""{% load absurl %}Thank you for your application to become a BIG Assist Supplier.

Following our assessment process we regret to inform you that on this occasion you have not been successful in your application.

You may view the decision summary on your <a href="{% absurl 'home' %}">personal dashboard</a>.

We would of course welcome a resubmitted application from you should you wish to re-apply in the future.  Simply log into your profile on www.bigassist.org.uk and click on the ‘Edit and reapply’ button. You will then be able to re-apply using the saved information from your previous application.

Alternatively, there is an appeals process in place should you wish to appeal this decision. For further information on this process please notify us by return email within two weeks of receiving this email.

If you would like any further information or have any questions regarding this decision please do not hesitate to contact the BIG Assist Team at NCVO:

T: 020 7520 2418
E: <a href="mailto:supplier.assist@ncvo.org.uk">supplier.assist@ncvo.org.uk</a>.

Kind regards,
The BIG Assist Team
"""
    },
    '06_diagnostic_submitted': {
            'subject': 'Thank you for submitting your Self-assessment',
            'message':
"""{% load absurl %}Thank you for submitting your Self-assessment.

Once we confirm that your Self-assessment has sufficient information a reviewer will be in touch with you to arrange a meeting to discuss your self-assessment. Please note that the review process can take several weeks, but we will contact you as soon as possible.

Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '061_customer_ineligible': {
            'subject': 'Ineligible for BIG Assist',
            'message':
"""Thank you for submitting your self-assessment. Unfortunately, your organisation is not eligible for support from the BIG Assist programme because it is not an <a href="http://www.bigassist.org.uk/news/are-you-eligible">infrastructure</a> organisation.

You are welcome to use the website to assess and review consultancy and training and to benefit from our guidance articles.

However, we are unable to help you with funding any of these activities.

We do have a BIG Assist independent panel and if you have evidence which demonstrates that you are an infrastructure organisation you are entitled to appeal against this decision. Please make any appeal in writing to assist@ncvo.org.uk stating clearly why you feel the decision is wrong.

We wish you well for the future.

The BIG Assist team

"""
    },
    '07_recommendation_sent': {
            'subject': 'The results of your BIG Assist self-assessment are ready to be viewed',
            'message':
"""{% load absurl %}Your Self-assessment has been reviewed and the results are ready to be viewed.

View your results and recommendations here:

{% absurl 'home' %}

Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

If you have any questions about the results of your self assessment or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Please note that if you do not accept or reject these recommendations within 2 weeks of them being sent to you they will automatically be approved. You will then have 4 months to spend your voucher(s).

Thanks,

The BIG Assist team

"""
    },
    '081_recommendation_accepted': {
            'subject': 'We have issued your vouchers for BIG Assist',
            'message':
"""{% load absurl %}You have now accepted your recommendations and we have issued you one or more vouchers to purchase support from BIG Assist suppliers.

You must spend your vouchers by {{ voucher_expires }}. Read <a href="http://www.bigassist.org.uk/news/secret-finding-right-supplier-you"> The secret to finding the right supplier for you</a> to help with your selection.


<strong>What to do now</strong>
<ul>
{# <li>View your <a href="{% absurl 'home' %}">BIG Assist voucher</a></li> #}
<li>See BIG Assist suppliers in <a href="{% absurl 'marketplace' %}">the Marketplace</a></li>
<li>Find out and meet other Assist community members and suppliers <a href="{% absurl 'marketplace' %}">in the ShareSpace</a></li>
</ul>
Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '082_recommendation_accepted': {
            'subject': 'Diagnostic recommendations accepted',
            'message':
"""{% load absurl %}User {{ company }} has accepted their Diagnostic recommendations.
"""
    },
    '30_person_contacts_company_owner': {
            'subject': 'BIG Assist: please approve new user',
            'message':
"""{% load absurl %}Dear {{ company_owner }}

{{ person }} has asked to be added as a user of {{ organisation }}'s account on http://www.bigassist.org.uk.

To approve this request, please click this link: {% absurl 'approve_user' organisation_pk person_pk %}.

If you do not recognise {{ person }} or do not want to approve them as a user on behalf of your organisation, just ignore this email.
{% if person_email %}
User's email address: {{ person_email }}
{% endif %}{% if message %}
Message added by user:
{{ message }}
{% endif %}
Thanks,

The BIG Assist team
"""
    }, 
    '091_customer_contacts_supplier': {
            'subject': 'Your BIG Assist improvement project',
            'message':
"""{% load absurl %}You have contacted the BIG Assist supplier {{ supplier }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'customer_dashboard' 'projects' %}">personal dashboard</a>.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '092_customer_contacts_supplier': {
            'subject': 'You have been contacted by a BIG Assist customer',
            'message':
"""{% load absurl %}You have been contacted by BIG Assist customer {{ customer }}.

To view the message see: <a href="{% absurl 'supplier_preproject_detail_view' pk %}">contact details</a>.

Customers may be contacting other suppliers for a quote. If you can offer support do reply quickly - any project funded by this BIG Assist voucher must be agreed and completed by {{ voucher.expires|date:"d b, Y" }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'supplier_dashboard' 'contacts' %}">personal dashboard</a>.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '101_supplier_proposes_project_supplier': {
        'subject': 'You have submitted a project proposal',
        'message':
"""{% load absurl %}Your project proposal has been sent to BIG Assist customer {{ project.cust }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'supplier_dashboard' 'contacts' %}">your personal dashboard</a>.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '102_supplier_proposes_project_customer': {
        'subject': '{{ project.suppl }} has submitted their project proposal',
        'message':
"""{% load absurl %}{{ project.suppl }} has a project proposal ready for you to view.

View here:

<a href="{% absurl 'home' %}">{% absurl 'home' %}</a>

Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

If you have any questions about the results of your diagnostic or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '103_customer_supplier_proposed_project_reminder': {
        'subject': 'BIG Assist project proposal awaiting approval',
        'message':
"""{% load absurl %}You have a project proposal waiting for approval on <a href="{% absurl 'customer_dashboard' 'projects' %}">your dashboard</a>. Remember, if you would like to use your voucher for this project it must be completed by {{ voucher.expires|date:"d b, Y" }}.

If you have any questions please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '104_supplier_edits_project_supplier': {
        'subject': 'You have edited a project',
        'message':
"""{% load absurl %}You have edited your project proposal and the customer {{ project.cust }} has been notified by email.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'supplier_dashboard' 'contacts' %}">your personal dashboard</a>.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '105_supplier_edits_project_customer': {
        'subject': '{{ project.suppl }} has updated their project proposal',
        'message':
"""{% load absurl %}{{ project.suppl }} has updated their project proposal.

View here:

<a href="{% absurl 'home' %}">{% absurl 'home' %}</a>

Don’t forget you can check your <a href="{% absurl 'home' %}">personal dashboard</a> for any updates on your activity on the BIG Assist site.

If you have any questions about the results of your diagnostic or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '111_customer_accepts_project_supplier': {
        'subject': 'A customer has accepted your project proposal',
        'message':
"""{% load absurl %}Congratulations, your project proposal has been accepted by BIG Assist customer {{ project.cust }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'home' %}">personal dashboard</a>.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '112_customer_accepts_project_customer': {
        'subject': 'You have accepted a project proposal',
        'message':
"""{% load absurl %}You have accepted the project proposal from BIG Assist supplier {{ project.suppl }}.

<strong>What to do now</strong>

The vouchers are just part of the way BIG Assist can help you and your organisation.
<ul>
    <li>Meet users and suppliers and share your practical knowledge in the <a href="{% absurl 'marketplace' %}">ShareSpace</a></li>
    <li>Find a mentor and arrange visits to other organisations or else invite people to see your organisation and/or become a mentor yourself in the <a href="{% absurl 'marketplace' %}">ConnectSpace</a></li>
    <li>Read the latest BIG Assist <a href="{% absurl 'home' %}">news</a></li>
    <li>Keep up to date with <a href="{% absurl 'home' %}">your projects</a></li>
</ul>
We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '113_customer_chase_feedback': {
        'subject': 'Please rate your BIG Assist project',
        'message':
"""{% load absurl %}You are receiving this email because you have completed a project funded by BIG Assist and you haven’t yet given us feedback. 

Please log in and <a href="{% absurl 'customer_project_feedback' project.pk %}">rate your supplier</a>, and tell us what you got out of the project.

Please note all ratings and comments will be made public on the supplier's entry within the marketplace.


Thank you,

The BIG Assist team

"""
    },
    '114_customer_chase_feedback_thank_you': {
        'subject': 'Thank you for your feedback',
        'message':
"""{% load absurl %}Thank you for providing feedback on your BIG Assist project – it helps us, your supplier, and other site users.

Did you know that once you have spent your BIG Assist vouchers you can apply for more to take your organisation to the next level?

A 'Reapplication' section will appear on the home tab of your BIG Assist dashboard as soon as you are eligible.

If you have any questions do get in touch with the team.

Thank you,

The BIG Assist team

"""
    },
    '115_customer_voucher_expiry_prompt': {
        'subject': 'Using your BIG Assist vouchers',
        'message':
"""{% load absurl %}We’ve noticed you haven’t contacted any suppliers about your voucher for {{ voucher.area }} yet. 

If you need help choosing a supplier see our guide <a href="http://www.bigassist.org.uk/news/secret-finding-right-supplier-you">The secret to finding the right supplier for you</a> to help with your selection.

Or if you have any other questions please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Remember, you have to agree a project and complete it by <b>{{ voucher.expires|date:"d b, Y" }}</b> if you want to use your voucher to pay for it.

Read the <a href="http://www.bigassist.org.uk/news/customer-guidelines">customer guidelines</a> in full.


Thanks,

The BIG Assist team

"""
    },
    '115b_customer_voucher_expired': {
        'subject': 'Your BIG Assist voucher has expired',
        'message':
"""{% load absurl %}{{ voucher.cust.org }}

Your BIG Assist voucher for {{ voucher.area }} has expired and cannot be used.

If you would still like BIG Assist support, you can re-submit your self-assessment by clicking on Apply for more vouchers on your <a href="{% absurl 'home' %}">personal dashboard</a>.

We may be able to award you new vouchers that meet your current needs.

If you have any questions about this process please contact the BIG Assist Team on:
 
T: 0207 520 2418
E: assist@ncvo.org.uk
 
Kind regards,
 
The BIG Assist Team
"""
    },
    '116_customer_supplier_contacted': {
        'subject': 'Still deciding on a BIG Assist supplier?',
        'message':
"""{% load absurl %}Still deciding on a supplier? Remember, you have until {{ voucher.expires|date:"d b, Y" }} to choose your supplier and complete any projects for your voucher: {{ voucher.area }}.

If you have any questions please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '117_supplier_contacted_by_customer': {
        'subject': 'Customer contact request through BIG Assist',
        'message':
"""{% load absurl %}{{ voucher.cust }} has contacted you. 

If you can offer support please respond with a project proposal. {{ voucher.cust }} have {{ num_days }} days to choose a supplier and complete any project which you agree.

If you cannot offer support please dismiss the enquiry on your dashboard.

If you have any questions please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '118_customer_accepts_project_supplier_reminder': {
        'subject': 'BIG Assist project completion reminder',
        'message':
"""{% load absurl %}Just a reminder: 
{{ voucher.cust }}'s project has to be completed by {{ voucher.expires|date:"d b, Y" }} when their their BIG Assist voucher expires. 

If you have any questions please email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '119_customer_accepts_project_supplier_reminder': {
        'subject': 'BIG Assist project completion reminder',
        'message':
"""{% load absurl %}{{ voucher.cust }}'s project has has {{ num_days }} days left before their BIG Assist voucher expires.

If the project is complete, mark the project as complete on your dashboard. If it is not and there is a reason for the delay please speak to {{ voucher.cust }} and ask them to email us at <a href="mailto:assist@ncvo.org.uk>assist@ncvo.org.uk</a>

Thanks,

The BIG Assist team

"""
    },
    '121_supplier_complete_project_supplier': {
        'subject': 'You have marked a project as complete',
        'message':
"""{% load absurl %}{% load assist %}Thank you for marking your project as complete.

This project had a 'voucher value' of {{ payment.amount|currencize }}. Please send us an invoice for this amount, plus any VAT due, as a PDF which you should email to invoices@bigassist.org.uk.

Please include the name of the customer in the invoice detail.

The invoice should be addressed to 
National Council for Voluntary Organisations (NCVO). 
Society Building, 8 All Saints Street, London N1 9RL. 

Once we have received your invoice we will check it and allocate a Purchase Order number to it. We will email you when we have done this and you will normally receive full payment within 30 days.

Your feedback is very important to us so please download the <a href="http://www.scribd.com/doc/186065714/Supplier-Project-Feedback-101013/"> feedback form and send your completed form to supplier.assist@ncvo.org.uk

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'supplier_dashboard' 'projects' %}">your personal dashboard</a>.

Thanks,

The BIG Assist team
"""
    },
    '122_supplier_complete_project_customer': {
        'subject': 'Confirm the completion of your BIG Assist project',
        'message':
"""{% load absurl %}BIG Assist supplier {{ project.suppl }} has told us that their project with you is complete.

Please visit BIG Assist to confirm the project has been finished.

Alternatively if the project is not yet complete please raise this concern here.
Help us get some feedback on your project and rate this supplier.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'home' %}">personal dashboard</a>.

Thanks,

The BIG Assist team

"""
    },
    '123_supplier_complete_project_supplier_partial': {
        'subject': 'You have marked a project as complete',
        'message':
"""{% load absurl %}{% load assist %}Thank you for marking your project as complete.
Please invoice NCVO for {{ payment.amount|currencize }} (covered by the voucher scheme). Please send and invoice for the remaining {{ project.customer_value|currencize }} to {{ project.cust }}.
You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'supplier_dashboard' 'projects' %}">your personal dashboard</a>.

If you have any questions about the results of your diagnostic or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    '124_supplier_complete_project_supplier_no_invoice': {
        'subject': 'You have marked a project as complete',
        'message':
"""{% load absurl %}{% load assist %}Thank you for marking your project as complete.
Please invoice {{ project.cust }} for the full amount of {{ project.price|currencize }}.
You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'supplier_dashboard' 'projects' %}">your personal dashboard</a>.

If you have any questions about the results of your diagnostic or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    # for supplier
    '131_customer_says_project_not_complete': {
        'subject': '{{ project.cust }} has raised a concern',
        'message':
"""{% load absurl %}{{ project.cust }} has written the following concern about the project:
Further details: {{ proj_comm.details }}

If you would like to contact the BIG Assist team please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    # for customer
    '132_customer_says_project_not_complete': {
        'subject': 'Thank you for raising your concern with BIG Assist',
        'message':
"""{% load absurl %}Thank you for raising your concern about your project with {{ project.suppl }}

Further details: {{ proj_comm.details }}

BIG Assist has been notified and a member of our team we will be in touch very soon.
You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'home' %}">personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team

"""
    },
    # for admin
    '133_customer_says_project_not_complete': {
        'subject': '{{ project.cust }} has raised a concern',
        'message':
"""{% load absurl %}{{ project.cust }} has raised a concern about their project with {{ project.suppl }}:
Further details: {{ proj_comm.details }}
Please contact them.

"""
    },
    # for supplier
    '141_customer_complains_about_project': {
        'subject': '{{ project.cust }} has made a complaint',
        'message':
"""{% load absurl %}{{ project.cust }} has written the following feedback about the project:

{{ proj_comm.details }}

If you would like to contact the BIG Assist team please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    # for customer
    '142_customer_complains_about_project': {
        'subject': 'Your complaint has been received',
        'message':
"""{% load absurl %}The project supplier and the BIG Assist team have received your complaint:

{{ proj_comm.details }}

If you would like to contact the BIG Assist team please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    # for supplier
    '143_customer_complains_about_project': {
        'subject': 'A complaint about a project has been received',
        'message':
"""{% load absurl %}A complaint has been received from {{ project.cust }} regarding their project with {{ project.suppl }}:
Further details: {{ proj_comm.details }}
Please contact them.

"""
    },
    '151_visit_host_contacted_by_visitor': {
        'subject': 'You\'ve contacted {{host}} regarding a visit',
        'message':
"""{% load absurl %}Your request has been sent to {{host}}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to contact the BIG Assist team directly please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '152_visit_host_contacted_by_visitor': {
        'subject': 'Someone has contacted you regarding a visit',
        'message':
"""{% load absurl %}{{ visitor.visitor_org }} has sent you a message regarding a visit:

{{ visitor.contact_msg }}

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'visitors' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at assist@ncvo.org.uk.

Thanks,
The BIG Assist team

"""
    },
    '161_visitor_applies_for_bursary': {
        'subject': 'Your request for a sponsorship has been received',
        'message':
"""{% load absurl %}The BIG Assist team have received your sponsorship request.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to contact the BIG Assist team directly please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '162_visitor_applies_for_bursary': {
        'subject': 'A user has requested a sponsorship',
        'message':
"""{% load absurl %}{{ visitor.visitor_org }} has requested a sponsorship for a visit.

"""
    },
    '171_admin_decided_on_bursary_accepted': {
        'subject': 'BIG Assist sponsorship decision',
        'message':
"""{% load absurl %}BIG Assist has approved your request for sponsorship.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '172_admin_decided_on_bursary_rejected': {
        'subject': 'BIG Assist sponsorship decision',
        'message':
"""{% load absurl %}BIG Assist has declined your request for sponsorship.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to get in contact with BIG Assist about the decision please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '173_admin_decided_on_bursary_accepted': {
        'subject': 'BIG Assist sponsorship decision',
        'message':
"""{% load absurl %}BIG Assist has approved the request by {{ visitor_org }} for sponsorship.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'visitors' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '174_admin_decided_on_bursary_rejected': {
        'subject': 'BIG Assist sponsorship decision',
        'message':
"""{% load absurl %}BIG Assist has rejected the request by {{ visitor_org }} for sponsorship.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'visitors' %}">your personal dashboard</a>.

If you would like to get in contact with BIG Assist about the decision please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '181_visit_host_decision_accept': {
        'subject': 'Your request for a visit has been accepted',
        'message':
"""{% load absurl %}{{ visitor.visit.host }} has accepted your request for a visit.

The next step is to apply to BIG Assist for sponsorship. Please log on to <a href=" {% absurl 'customer_dashboard' 'connectspace' %}">your dashboard</a> and, under the ConnectSpace tab, apply for sponsorship.

You must apply for sponsorship and have it approved by the BIG Assit team before the visit takes place. If your sponsorship request has not been approved, we'll be unable to pay either the host or the visitor.

If you would like to contact the BIG Assist team directly, please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist Team

"""
    },
    '182_visit_host_decision_reject': {
        'subject': 'Your request for a visit has been decline',
        'message':
"""{% load absurl %}{{ visitor.visit.host }} has rejected your request for a visit.

The next step is to apply for sponsorship from BIG Assist for this visit.

Log on to your dashboard and click on the ‘connectspace’ tab where you will see an option to ‘apply for sponsorship’.

If you have any queries please contact BIG Assist team directly at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '183_visit_host_decision_accept': {
        'subject': 'You have accepted a visit request',
        'message':
"""{% load absurl %}You have accepted the visit request from {{ visitor.visitor_org }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to contact the BIG Assist team directly please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '184_visit_host_decision_reject': {
        'subject': 'You have declined a visit request',
        'message':
"""{% load absurl %}You have declined the visit request from {{ visitor.visitor_org }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to contact the BIG Assist team directly please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '191_visit_complete_host': {
        'subject': 'Visit complete - sponsorship invoice request',
        'message':
"""{% load absurl %}{% load assist %}The visit entitled {{ visit.title }} was scheduled to take place on {{ visit_instance.date }}.

As a host of this visit, you are sponsored to the value of {{ payment.amount|currencize }}. Please send us an invoice for this amount (flat rate not subject to VAT) as a PDF which you should email to invoices@bigassist.org.uk.

Please include the name of the visitor(s) in the invoice detail.

The invoice should be addressed to
National Council for Voluntary Organisations (NCVO).
Society Building, 8 All Saints Street, London N1 9RL.

Once we have received your invoice we will check it and allocate a Purchase Order number to it. We will email you when we have done this and you will normally receive full payment within 30 days.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

Thanks,
The BIG Assist team

"""
    },
    '192_visit_complete_visitor': {
        'subject': 'Visit complete - sponsorship invoice request',
        'message':
"""{% load absurl %}{% load assist %}The visit entitled {{ visit.title }} was scheduled to take place on {{ visit_instance.date }}.

As a visitor, you are sponsored to the value of {{ payment.amount|currencize }}. Please send us an invoice for this amount (flat rate not subject to VAT) as a PDF which you should email to invoices@bigassist.org.uk.

Please include the name of the visit host in the invoice detail.

The invoice should be addressed to
National Council for Voluntary Organisations (NCVO).
Society Building, 8 All Saints Street, London N1 9RL.

Once we have received your invoice we will check it and allocate a Purchase Order number to it. We will email you when we have done this and you will normally receive full payment within 30 days.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

Thanks,

The BIG Assist team

"""
    },
    '201_customer_contacts_mentor_cust': {
        'subject': 'You have contacted a mentor',
        'message':
"""{% load absurl %}You have contacted mentor {{ mentor }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '202_customer_contacts_mentor_mentor': {
        'subject': 'A BIG Assist customer has contacted you about mentoring',
        'message':
"""{% load absurl %}{{ customer }} has sent you a message regarding mentoring:
{{ message|linebreaksbr }}

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team

"""
    },
    '211_mentee_applies_for_bursary': {
        'subject': 'Your application for sponsorship has been sent',
        'message':
"""{% load absurl %}BIG Assist has received your sponsorship request.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you would like to contact the BIG Assist team directly please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '212_mentee_applies_for_bursary': {
        'subject': 'Sponsorship application has been received',
        'message':
"""{% load absurl %}{{ mentee.mentee_org }} has requested sponsorship. Please review the sponsorship and contact them.

"""
    },
    '221_admin_decide_on_bursary': {
        'subject': 'BIG Assist bursary decision',
        'message':
"""{% load absurl %}BIG Assist reached a decision on the bursary you applied for. View the decision on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you want to contact BIG Assist to discuss the decision please email <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '222_admin_decide_on_bursary': {
        'subject': 'A customer and mentor have been informed of a bursary decision',
        'message':
"""{% load absurl %}{{ mentee.mentee_org }} and {{ mentee.mentor }} ({{ mentee.mentor.mentor_org }}) have been informed of the bursary decision.
"""
    },
    '223_admin_decide_on_bursary': {
        'subject': 'BIG Assist bursary decision',
        'message':
"""{% load absurl %}{{ mentee.mentee_org }} has received a decision on their bursary application.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'mentoring' %}">your personal dashboard</a>.

If you want to contact BIG Assist to discuss the decision please email <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '231_mentor_accepts_customer_mentee': {
        'subject': 'BIG Assist Mentoring decision',
        'message':
"""{% load absurl %}Your mentoring request has been accepted by {{ mentor }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '232_mentor_accepts_customer_mentor': {
        'subject': 'BIG Assist Mentoring decision',
        'message':
"""{% load absurl %}You have accepted {{ mentee }} mentoring request.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

We’re always interested in hearing what you think about BIG Assist; please email any feedback to us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '241_mentor_adds_date_mentee': {
        'subject': 'Mentoring date added',
        'message':
"""{{% load absurl %}{{ mentor }} has added a date for mentoring. View the decision on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you want to contact BIG Assist to discuss the decision please email <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '242_mentor_adds_date_mentor': {
        'subject': 'You have added a mentoring date',
        'message':
"""{% load absurl %}You have added date {{ date }} as your mentoring date. If this date is incorrect please contact BIG Assist team by email at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'mentoring' %}">your personal dashboard</a>.

Thanks,
The BIG Assist team
"""
    },
    '251_mentoring_complete_mentor': {
        'subject': 'Mentoring complete - sponsorship invoice request',
        'message':
"""{% load absurl %}{% load assist %}Thank you for completing your mentoring with {{ mentee.mentee_org }}.

As a mentor, you are sponsored to the value of {{ payment.amount|currencize }}. Please send us an invoice for this amount, inclusive of VAT, as a PDF which you should email to invoices@bigassist.org.uk.

Please include the name of the customer in the invoice detail.

The invoice should be addressed to
National Council for Voluntary Organisations (NCVO).
Society Building, 8 All Saints Street, London N1 9RL.

Once we have received your invoice we will check it and allocate a Purchase Order number to it. We will email you when we have done this and you will normally receive full payment within 30 days.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

Thanks,
The BIG Assist team
"""
    },
    '252_mentoring_complete_mentee': {
        'subject': 'Mentoring complete',
        'message':
"""{% load absurl %}{% load assist %}Thank you for completing your mentoring with {{ mentee.mentor.mentor_org }}.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

If you have any questions about this or any other query to do with BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,
The BIG Assist team
"""
    },
    '261_customer_applies_as_host': {
        'subject': 'You have applied to host a visit',
        'message':
"""{% load absurl %}BIG Assist is reviewing your application to host a visit and will be in touch shortly.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

Thanks,
The BIG Assist team
"""
    },
    '262_customer_applies_as_host': {
        'subject': 'A customer has applied to host a visit',
        'message':
"""{% load absurl %}{{ visit.host }} has applied to host a visit. Review the request and contact them.
"""
    },
    '27_customer_rejected_as_host': {
        'subject': 'Your application to host a visit has been rejected',
        'message':
"""{% load absurl %}Your application to host a visit has been rejected. If you would like to find out more about the decision please contact us by email at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'home' %}">personal dashboard</a>.

Thanks,

The BIG Assist team
"""
    },
    '281_customer_applies_as_mentor': {
        'subject': 'Your application to become a mentor has been received',
        'message':
"""{% load absurl %}Your application is being review and we will be in touch shortly.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on <a href="{% absurl 'customer_dashboard' 'connectspace' %}">your personal dashboard</a>.

Thanks,
The BIG Assist team
"""
    },
    '282_customer_applies_as_mentor': {
        'subject': 'A customer has applied to be a mentor',
        'message':
"""{% load absurl %}{{ mentor.mentor_org }} has applied to become a mentor. Review the request and contact them.
"""
    },
    '29_customer_rejected_as_mentor': {
        'subject': 'Your application to become a mentor has been rejected',
        'message':
"""{% load absurl %}Your application to become a mentor has been rejected. If you would like to find out more about the decision please contact us by email at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

You can see the progress of this and any other projects as well as check for other BIG Assist updates and activities on your <a href="{% absurl 'home' %}">personal dashboard</a>.

Thanks,

The BIG Assist team
"""
    },
    '30_person_contacts_company_owner': {
        'subject': 'BIG Assist: please approve new user',
        'message':
"""{% load absurl %}Dear {{ company_owner }}

{{ person }} has asked to be added as a user of {{ organisation }}'s account on http://www.bigassist.org.uk.

To approve this request, please click this link: {% absurl 'approve_user' organisation_pk person_pk %}.

If you do not recognise {{ person }} or do not want to approve them as a user on behalf of your organisation, just ignore this email.
{% if person_email %}
User's email address: {{ person_email }}
{% endif %}{% if message %}
Message added by user:
{{ message }}
{% endif %}
Thanks,

The BIG Assist team
"""
    },
    # for supplier
    '31_customer_dismisses_project': {
        'subject': 'BIG Assist project - {{ customer }}',
        'message':
"""{% load absurl %}Many thanks for the information provided to {{ customer }}.

{{ customer }} have however decided not to pursue this project any further.

This will be automatically updated in your <a href="{% absurl 'home' %}">personal dashboard</a> where you can also see the progress of any other projects, as well as check for other BIG Assist updates and activities.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team"""
    },
    '32_supplier_does_not_accept_project': {
        'subject': 'BIG Assist project - {{ project.suppl }}',
        'message':
"""{% load absurl %}Many thanks for the information provided to {{ project.suppl }}.

{{ project.suppl }} have however decided not to pursue this project any further.

This will be automatically updated in your <a href="{% absurl 'home' %}">personal dashboard</a> where you can also see the progress of any other projects, as well as check for other BIG Assist updates and activities.

If you have any questions about BIG Assist please email us at <a href="mailto:assist@ncvo.org.uk">assist@ncvo.org.uk</a>.

Thanks,

The BIG Assist team
"""
    },
    '33_user_invited': {
        'subject': 'You have been invited to join {{ company }} on BIG Assist',
        'message':
"""{% load absurl %}Hi,
{{ person }} has invited you to join them as a member of {{ company }} on BIG Assist. To become a member you just need to click on this link:

<a href="{% absurl 'join_bigassist' pk %}">{% absurl 'join_bigassist' pk %}</a>.

If you don't want to join, you can ignore this email.

Thanks,

The BIG Assist team
"""
    },
    '34_send_to_finance': {
        'subject': 'Payment is waiting to be processed',
        'message':
"""Hi,
<p>There is a request for the payment to be processed.</p>
<ul>
<li>Organisation: {{ payment.payee_org }}</li>
<li>Amount: {{ payment.amount }}</li>
<li>POP: {{ payment.pop }}</li>
<li>Invoice date: {{ payment.invoice_date }}</li>
</ul>

Thanks,
The BIG Assist team
"""
    },
    '341_send_to_finance_supplier': {
        'subject': 'Your invoice to BIG Assist',
        'message':
"""{% load assist %}
Dear Sir/Madam

We have received your invoice for work with {{ payment.proj.cust }} for {{ payment.amount|currencize }} plus VAT (if applicable).

We have allocated Purchase Order number {{ payment.pop }} to your invoice and passed it to our Finance Department. It will normally be paid into your bank within 30 days using the sort code and account number which you gave us.

If you have any questions or problems, please email us at invoices@bigassist.org.uk. Remember to quote the Purchase Order number above in any query.

Thanks

The BIG Assist team
"""
    },
    '35_profile_updated': {
        'subject': 'BIG Assist organisation profile changed',
        'message':
"""{% load absurl %}{{ organisation }} have updated their profile.

See details at {% absurl 'admin_organisation_profile' organisation.pk %}
"""
    },
    '36_supplier_appeals_rejected_assessment': {
        'subject': 'Supplier {{ organisation }} appeals rejected assessment',
        'message':
"""{{ organisation }} appeals rejected assessment with the following message:
<p>{{ message }}</p>
"""
    },
    '43_request_membership': {
        'subject': 'User {{ user.email }} requests membership',
        'message':
"""{% load absurl %}User {{ khnpuser }} ({{ user.email }}) has requested to become a member of {{ person.displayname }} which you manage on BIG Assist.


If you agree with this request, please go to BIG Assist web site {% absurl 'org_users' %} to add the user as a member or manager.
Thanks,
The BIG Assist team.
"""
    },
    '50_customer_appeals_recommendation': {
        'subject': 'You have appealed your recommendation',
        'message':
"""{% load absurl %}
You have started the appeal process for your recommendation.

You have indicated that you would like to appeal the decision that the customer consultant has made following your diagnostic.

All BIG Assist appeals are undertaken by the Independent Panel*; to make your appeal please email assist@ncvo.org.uk for the attention of Nicola North, Project Manager.

Please clearly state within your email why you feel the decision taken was wrong, and provide any additional evidence which you would like to be considered.

In accordance with the appeal process the completed self-assessment, report of the review call and the additional information you have provided will be sent through to our independent panel for their review. 

Unfortunately through the BIG Assist programme we do not have the funds to be able to offer funding through a voucher to all eligible organisations; there is a scoring mechanism in place that helps to determine the organisations that are awarded and those that aren’t and we do prioritise the organisations that are most:

•   Ready to change/can evidence that they are embracing change
•   Able to capture the impact of the work undertaken
•   Prepared to share their learning; passing on their learning and knowledge to other organisations
•   Champion and promote the BIG Assist approach

The independent panel members will consider the all information provided against this scoring mechanism as well as ensure that all internal processes were followed. You will be notified of the outcome of your appeal by email; you will also receive further recommendations via the BIG Assist website in accordance with the outcome. There will be no option to reject the recommendation following appeal.

For further information do contact the BIG Assist Team on 020 7520 2418.

Kind regards,

BIG Assist Team

*The BIG Assist Panel is made up of 5 independent members including representatives from local and national infrastructure as well as the private sector. The BIG Assist Independent Panel is designed to provide strategic independent advice and recommendations to the NCVO and the BLF to help achieve the best outcomes from the programme. The expertise provide by the panel will play a strategic role in helping develop an effective efficient market place of high quality relevant suppliers of support for voluntary sector infrastructure organisations, to best support infrastructure organisations (IOs) be more effective, sustainable and able to adapt to change.
 
"""
    },
    '51_customer_appeals_recommendation_admin': {
        'subject': 'Customer has appealed their recommendation',
        'message':
"""{% load absurl %}
Customer {{ customer }} has appealed their recommendations. 

Please log into the <a href="{% absurl 'admin_dashboard_customer' %}">admin dashboard</a> to see the customer.


"""
    },
    '52_customer_receives_revised_recommendation': {
        'subject': 'Your revised recommendation is ready to view',
        'message':
"""{% load absurl %}
Your recommendation is ready to view. 

Following your rejection of the initial recommendation and subsequent appeal, your final recommendation has now been issued. To view this please log into your dashboard.

This recommendation takes into account the decision from the appeal process; the outcome of which is final. Rejection of these recommendations will therefore not result in any further action being taken. [Recommendations will automatically be accepted within 2 weeks of issue].

If you have any questions about this please contact the BIG Assist Team on 020 7520 2418 assist@ncvo.org.uk
"""
    },
    '53_customer_rejects_final_recommendation': {
        'subject': 'You have rejected your revised recommendations',
        'message':
"""{% load absurl %}
You have rejected your recommendation

This recommendation has been issued following a formal appeal process; the decision of which is final – we are therefore unable to take any further action.

Unfortunately through the BIG Assist programme we do not have the funds to be able to offer funding through a voucher to all eligible organisations; there is a scoring mechanism in place that helps to determine the organisations that are awarded and those that aren’t and we do prioritise the organisations that are most:

• Ready to change/can evidence that they are embracing change
• Able to capture the impact of the work undertaken
• Prepared to share their learning; passing on their learning and knowledge to other organisations
• Champion and promote the BIG Assist approach

All organisations are however able to re-submit their self-assessments should their circumstances change and they are better able to meet the programme criteria. You can do this via the application tab on your dashboard.

If you have any questions about this or would like additional information please contact the BIG Assist Team on 020 7520 2418 assist@ncvo.org.uk

"""
    },
    '54_customer_rejects_final_recommendation_admin': {
        'subject': 'Customer has rejected their revised recommendation',
        'message':
"""{% load absurl %}
Customer {{ customer }} has rejected their revised recommendations. 

They will no longer appear in the customer tab of the admin dashboard.

"""
    },
    '55_customer_rejects_recommendation': {
        'subject': 'You have rejected your recommendations',
        'message':
"""{% load absurl %}
You have rejected your recommendation

Unfortunately through the BIG Assist programme we do not have the funds to be able to offer funding through a voucher to all eligible organisations; there is a scoring mechanism in place that helps to determine the organisations that are awarded and those that aren’t and we do prioritise the organisations that are most:

• Ready to change/can evidence that they are embracing change
• Able to capture the impact of the work undertaken
• Prepared to share their learning; passing on their learning and knowledge to other organisations
• Champion and promote the BIG Assist approach

All organisations are however able to re-submit their self-assessments should their circumstances change and they are better able to meet the programme criteria. You can do this via the application tab on your dashboard.

If you have any questions about this or would like additional information please contact the BIG Assist Team on 020 7520 2418 assist@ncvo.org.uk

"""
    },
    '56_customer_rejects_recommendation_admin': {
        'subject': 'Customer has rejected their recommendation',
        'message':
"""
Customer {{ customer }} has rejected their recommendations after starting the appeal process.

They will no longer appear in the customer tab of the admin dashboard.

"""
    },
    '99_new_topic_added': {
        'subject': 'New topic {{ topic }} added',
        'message':
"""{% load absurl %}New topic has been added:

Title: {{ topic }}
URL: {% absurl 'pybb:topic' topic.pk %}
"""
    },
}


class HTMLEmailMessage(EmailMessage):

    content_subtype = 'html'


def send_ba_email(typ, data, sender=None, recipients=None, bcc=None, cc=None, recipient_role=None):
    """ if sender is none, settings.SERVER_EMAIL is used.
        You can also pass an Organisation object to recipients argument. """

    info = EMAIL_TEXT.get(typ)
    subject = info['subject']
    message = info['message']

    if not sender:
        sender = settings.SERVER_EMAIL

    if not recipients:
        # email is sent to bigassist because it is supposed to be sent there (recipient is None)
        # or the recipient has no email set (weird)
        recipients = settings.BIGASSIST_ADMIN_EMAIL
    if isinstance(recipients, Organisation):
        if recipient_role is 'supplier':
            emails = recipients.get_contact_emails('supplier')
        elif recipient_role is 'customer':
            emails = recipients.get_contact_emails('customer')
        else:
            emails = recipients.get_contact_emails()
        recipients = emails['main']
        if cc is None:
            cc = emails['members']
    if isinstance(recipients, Person):
        emails = recipients.get_contact_emails()
        recipients = emails['main']
        if cc is None:
            cc = emails['members']
    if isinstance(recipients, basestring):
        recipients = [recipients]
    if isinstance(cc, basestring):
        cc = [cc]
    if isinstance(bcc, basestring):
        bcc = [bcc]

    template = Template(subject)
    subject = template.render(Context(data))

    msg_text = message.replace('\r', '').replace('\n', '<br />\n')
    template = Template(msg_text)
    message = template.render(Context(data))

    connection = get_connection()
    return HTMLEmailMessage(subject, message, sender, recipients,
                            connection=connection, bcc=bcc, cc=cc).send()
