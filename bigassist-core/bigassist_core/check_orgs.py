from django.db import connection


def check_orgs(orig_data):
#               db,              # sqlalchemy session
#               ncvomem=None,    # NCVO membership id (string)
#               companyno=None,  # company no (integer)
#               charityno=None,  # charity no (integer)
#               orgname='',      # organisation name
#               url='',          # organisation website url
#               postcode=''      # organisation postcode
#               ):
    """Finds organisations in database that match organisation to check.

Takes one or more parameters of an organisation to check.
Returns a list of organisations which match, in order of liklihood of match.
    """

    # make a local copy
    data = dict(orig_data)

    # query to select relevant items from database
    sql = \
"""
SELECT person.id AS orgid, '' as ncvomem, companyno, charityno,
   displayname AS orgname, value AS url, postcode
FROM person
LEFT JOIN contact ON contact.type=3 AND person.id=contact.person
LEFT JOIN address ON address.use<>5 AND person.id=address.person
WHERE person.type=2
"""

    filters = []
    if data.get('ncvomem'):
        filters.append("ncvomem = %(ncvomem)s")

    if data.get('companyno'):
        filters.append("companyno = %(companyno)s")

    if data.get('charityno'):
        filters.append("charityno = %(charityno)s")

    if data.get('displayname'):
        data['displayname'] = data['displayname'].lower()
        if data['displayname'].startswith('the '):
            data['displayname'] = data['displayname'][4:]
        filters.append("regexp_replace(lower(displayname), '^the ', '') = %(displayname)s")

    if data.get('website'):
        data['website'] = data['website'].lower()
        filters.append("lower(value) = %(website)s")

    if data.get("postcode"):
        data['postcode'] = data['postcode'].replace(' ', '').lower()
        filters.append("lower(replace(postcode, ' ', '')) = %(postcode)s")

    if filters:
        sql = "%s and (%s)" % (sql, ' OR '.join(filters))

    # get list of matches
    cursor = connection.cursor()

    cursor.execute(sql, data)
    
    def dictfetchall(cursor):
        "Returns all rows from a cursor as a dict"
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    qry = dictfetchall(cursor)

    # sort list into order of likelihood
    def sort_key(item, req):
        return -int(item == req) if item else 0
    # the order of these sorts is significant
    if data.get('postcode'):
        qry.sort(key=lambda q: sort_key(q['postcode'].replace(' ', '').lower()
                                        if q['postcode'] else None, data['postcode']))
    if data.get('website'):
        qry.sort(key=lambda q: sort_key(q['url'].lower() if q['url'] else None, data['website']))
    if data.get('displayname'):
        qry.sort(key=lambda q: sort_key(q['orgname'].lower()[4:]
                                        if q['orgname'].lower().startswith('the ')
                                        else q['orgname'].lower(), data['displayname']))
    if data.get('charityno'):
        qry.sort(key=lambda q: sort_key(q['charityno'], data['charityno']))
    if data.get('companyno'):
        qry.sort(key=lambda q: sort_key(q['companyno'], data['companyno']))
    if data.get('ncvomem'):
        qry.sort(key=lambda q: sort_key(q['ncvomem'], data['ncvomem']))

    def is_true_match(org, data):
        if org['ncvomem'] == data['ncvomem']:
            return True
        if org['companyno'] == data['companyno']:
            return True
        if org['charityno'] == data['charityno']:
            return True
        return False

    # if some of them match return only matched ones
    #matched = [q['orgid'] for q in qry if is_true_match(q, data)]
    #if len(matched) > 0:
    #    return matched

    # otherwise return all of them
    qry = [q['orgid'] for q in qry]
    return qry
