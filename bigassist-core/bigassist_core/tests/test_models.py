from bigassist_core.user import KHNPUser
from django.contrib.auth.models import User

from base import TestCase

from bigassist_core.models import Organisation

EMAIL = 'test@khnp.org'


class OrganizationManagerTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(OrganizationManagerTest, self).setUp()
        # create a Django user
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.client.login(username='testuser', password='secret')
        user = KHNPUser(self.user)
        person = user.get_person()
        self.supplier = Organisation.objects.register(person,
                                                      defaults={'is_supplier': True})

    def test_supplier_manager(self):
        self.assertEqual(Organisation.objects.suppliers().count(), 1)
        self.assertEqual(Organisation.objects.approved_suppliers().count(), 0)
        self.assertEqual(Organisation.objects.customers().count(), 0)
        self.assertEqual(Organisation.objects.approved_customers().count(), 0)
