import os

from subprocess import Popen
from subprocess import PIPE
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


class PDFCommandError(Exception):
    pass


def _run_wkhtmltopdf_command(input_url, output_filename,
                             tmp_dir,
                             **extra_options):

    if not os.path.isdir(os.path.dirname(output_filename)):
        raise PDFCommandError("%s does not exist" % os.path.dirname(output_filename))

    if os.path.exists(output_filename):
        # old copy from a previous crash maybe
        os.unlink(output_filename)

    parameters = dict(input_filename=input_url,
                      output_filename=output_filename,
                      tmp_dir=tmp_dir)

    command = "%s  -q " % settings.WKHTMLTOPDF_BINARY

    for key in extra_options:
        if extra_options[key] is None:
            command += '--%s ' % key
        else:
            command += '--%s "%s" ' % (key, extra_options[key])

    command += '--encoding utf-8 "%(input_filename)s" "%(output_filename)s"'
    command = command % parameters
    logging.info("COMMAND: %s" % command)
    proc = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    stdoutdata, stderrdata = proc.communicate()
    if stderrdata and os.path.exists(output_filename):
        # The error is in fact a warning because the file has been created, so
        # we can ignore it
        stderrdata = ""

    if stderrdata:
        logging.error("Got error when trying to generate PDF from this command: (%s)" % command)
        logging.error(stderrdata)

    logging.warning("wkhtmltopdf process test %r" % proc.poll())

    return stdoutdata, stderrdata


def pdf(url, filename, tmp_dir, **extra_options):
    """ Save PDF to the file """
    # result = None

    output, error = _run_wkhtmltopdf_command(url,
                                    filename, tmp_dir,
                                    **extra_options
                                    )

    if error:
        logger.warn("COMMAND ERROR:\n%s" % error)
        return None

    # else:
    #     result = file(filename, 'rb').read()

    # if result:
    #         response = HttpResponse(result, content_type='application/pdf')
    #         filename = filename.replace('"', '')
    #         response['Content-Disposition'] = 'attachment; filename="%s"' % filename
    #         return response
    return filename
