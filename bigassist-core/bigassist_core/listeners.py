from django.conf import settings
from django.dispatch import receiver
from django_browserid.signals import user_created
from django.db.backends.signals import connection_created
from django.db.models.signals import post_save

from pybb.models import Topic

from bigassist_core.emails import send_ba_email
from bigassist_core.user import KHNPUser


@receiver(connection_created, dispatch_uid='db_connection_created')
def db_connection_created(sender, connection, **kwargs):
    """ activate sql logger even if DEBUG is set to False """
    connection.use_debug_cursor = True


@receiver(user_created, dispatch_uid='persona_user_created')
def persona_user_created(sender, **kwargs):
    """ This method is called when user is logged in through Mozilla Persona
        to this Django app for the first time.
        We need to find or create user's account in the KHNP database
    """
    obj = kwargs['user']

    khnp_user = KHNPUser(obj)

    # If the user is new the identity is not set up yet
    khnp_user.setup_identity()


@receiver(post_save, sender=Topic, dispatch_uid='topic_created')
def topic_created(sender, **kwargs):

    if not kwargs.get('created'):
        return

    topic = kwargs['instance']
    send_ba_email('99_new_topic_added', {'topic': topic}, sender=None,
                  recipients=settings.BIGASSIST_ADMIN_EMAIL)
