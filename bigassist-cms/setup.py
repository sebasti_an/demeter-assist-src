#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from setuptools import find_packages
from setuptools import setup

from bigassist_cms import __versionstr__

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'readme.md')).read()

setup(name='bigassist-cms',
      version=__versionstr__,
      description='CMS package for BIG Assist',
      long_description=README,
      classifiers=[
          'Environment :: Web Environment',
          'Framework :: Django',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
      ],
      keywords='',
      author='Text Matters',
      author_email='post@textmatters.com',
      url='',
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
        'setuptools',
      ],
)
