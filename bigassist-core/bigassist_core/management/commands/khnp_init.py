#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
import sys
from django.conf import settings
from django.core.management.base import NoArgsCommand
from bigassist_core import models
from bigassist_core.emails import EMAIL_TEXT


DATABASE_DATA = [
    {
        'model': models.Use,
        'rows': [
            {'get_query': {'pk': settings.KHNP_ADDRESS_REGISTERED},
             'fields': {'pk': settings.KHNP_ADDRESS_REGISTERED, 'name': 'Registered'},
            }
        ]

    },
    {
        'model': models.OrgType,
        'rows': [
            {'get_query': {'pk': 1},
             'fields': {'pk': 1, 'org_type': 'Voluntary or community organisation'},
            },
            {'get_query': {'pk': 2},
             'fields': {'pk': 2, 'org_type': 'Company'},
            },
            {'get_query': {'pk': 3},
             'fields': {'pk': 3, 'org_type': 'Sole trader'},
            },
            {'get_query': {'pk': 4},
             'fields': {'pk': 4, 'org_type': 'Other'},
            },
        ]
    },
    {
        'model': models.SupportArea,
        'update': True,
        'rows': [
            {'get_query': {'pk': 1},
             'fields': {'pk': 1, 'area': 'Strategy, planning and managing change', 'area_key': 'strategy'},
            },
            {'get_query': {'pk': 2},
             'fields': {'pk': 2, 'area': 'Financial sustainability', 'area_key': 'finance'},
            },
            {'get_query': {'pk': 3},
             'fields': {'pk': 3, 'area': 'Innovation, new products and ways of working', 'area_key': 'innovation'},
            },
            {'get_query': {'pk': 4},
             'fields': {'pk': 4, 'area': 'Marketing and strategic relationships', 'area_key': 'relationships'},
            },
            {'get_query': {'pk': 5},
             'fields': {'pk': 5, 'area': 'Supporting and developing people and organisational culture', 'area_key': 'people'},
            },
        ]
    },
    {
        'model': models.CommMethod,
        'rows': [
            {'comm_method': 'Phone'},
            {'comm_method': 'Email'},
            {'comm_method': 'Post'},
        ]
    },
    {
        'model': models.XForms,
        'rows': [
            {'get_query': {'formtype': 'SA'},
             'fields': {'formtype': 'SA', 'xform': '', 'model': ''},
            },
            {'get_query': {'formtype': 'SRA'},
             'fields': {'formtype': 'SRA', 'xform': '', 'model': ''},
            },
        ]
    },
    {
        'model': models.CustomerCommType,
        'rows': [
            {'comm_type_id': settings.KHNP_COMMLOG_MADE_INELIGIBLE_ID, 'comm_type': 'Customer made ineligible'},
            {'comm_type_id': settings.KHNP_COMMLOG_RECOMMENDATION_SENT_ID, 'comm_type': 'Draft recommendation sent'},
            {'comm_type_id': settings.KHNP_COMMLOG_RECOMMENDATION_ACCEPTED_ID, 'comm_type': 'Draft recommendation accepted'},
            {'comm_type_id': settings.KHNP_COMMLOG_RECOMMENDATION_REJECTED_ID, 'comm_type': 'Draft recommendation rejected'},
        ]

    },
    {
        'model': models.Region,
        'rows': [
            {'get_query': {'code': 'NE'},
             'fields': {'code': 'NE', 'name': 'North East'},
            },
            {'get_query': {'code': 'NW'},
             'fields': {'code': 'NW', 'name': 'North West'},
            },
            {'get_query': {'code': 'Yorks'},
             'fields': {'code': 'Yorks', 'name': 'Yorkshire and The Humber'},
            },
            {'get_query': {'code': 'EM'},
             'fields': {'code': 'EM', 'name': 'East Midlands'},
            },
            {'get_query': {'code': 'WM'},
             'fields': {'code': 'WM', 'name': 'West Midlands'},
            },
            {'get_query': {'code': 'E'},
             'fields': {'code': 'E', 'name': 'East of England'},
            },
            {'get_query': {'code': 'SE'},
             'fields': {'code': 'SE', 'name': 'South East'},
            },
            {'get_query': {'code': 'SW'},
             'fields': {'code': 'SW', 'name': 'South West'},
            },
            {'get_query': {'code': 'Lon'},
             'fields': {'code': 'Lon', 'name': 'London'},
            },
        ]

    },
    {
        'model': models.BursaryTypes,
        'rows': [
            {'get_query': {'pk': 1},
             'fields': {'pk': 1, 'name': 'mentor', 'value': 400},
            },
            {'get_query': {'pk': 2},
             'fields': {'pk': 2, 'name': 'visitor', 'value': 250},
            },
            {'get_query': {'pk': 3},
             'fields': {'pk': 3, 'name': 'host', 'value': 400},
            },
        ]
    },
]


class Command(NoArgsCommand):
    help = "Initialize BIGAssist database content"

    def handle_noargs(self, **options):
        ok = created = mails_created = 0
        for item in DATABASE_DATA:
            model = item['model']
            for row in item['rows']:
                fields = row.get('fields')
                if not fields:
                    fields = row
                get_query = row.get('get_query')
                if not get_query:
                    get_query = row
                try:
                    obj = model.objects.get(**get_query)
                    ok += 1
                    # print >>sys.stdout, "record OK '%s' Query: %s Data: %s" % (model._meta.object_name, str(get_query), str(fields))
                except model.DoesNotExist:
                    # create
                    print >>sys.stdout, "Creating new record in %s - %s" % (model._meta.object_name, str(fields))
                    obj = model.objects.create(**fields)
                    created += 1
                    # get must pass now, otherwise something went wrong
                    assert model.objects.get(**get_query).pk == obj.pk
                else:
                    if item.get('update'):
                        print >>sys.stdout, "Updating record in %s - %s" % (model._meta.object_name, str(fields))
                        for key, val in fields.items():
                            setattr(obj, key, val)
                        obj.save()

        if not User.objects.filter(username='admin').exists():
            admin = User.objects.create_superuser('admin', 'radim@fry-it.com', 'secret')
            # common Fry password
            admin.password = u'pbkdf2_sha256$10000$01OAHFemES3t$g6/cuwAG0XVWVX+lr9vG6Bb+ldpp7GjC9VVI4tKAJfs='
            admin.save()
            created += 1
            print >>sys.stdout, "Created admin user"
        else:
            ok += 1

        if not User.objects.filter(username='textmatters').exists():
            tm = User.objects.create_superuser('textmatters', 'peter.adams@textmatters.com', 'secret')
            # text1290
            tm.password = u'pbkdf2_sha256$10000$Xufi5OONOwTz$utJAmYJd2cyu1qWjhzuyl5f1QiiwDl1Q96BPR3hginc='
            tm.save()
            created += 1
            print >>sys.stdout, "Created textmatters user"
        else:
            ok += 1

        # One time update for CustomerCommType
        if models.CustomerCommType.objects.all()[0].available_in_form is None:
            for row in models.CustomerCommType.objects.all():
                row.available_in_form = False
                row.save()

        # Update emails
        for code, value in EMAIL_TEXT.iteritems():
            subject = value['subject']
            message = value['message']
            try:
                models.MailTemplate.objects.get(code=code)
            except models.MailTemplate.DoesNotExist:
                models.MailTemplate.objects.create(code=code, subject=subject, message=message)
                mails_created += 1

        print >>sys.stdout, "khnp_init finished. Created: %d items, checked: %d items. Created %d emails." % (created, ok, mails_created)
