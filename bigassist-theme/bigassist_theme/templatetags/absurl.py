import urlparse
from django.template import Library
from django.template.defaulttags import URLNode
# {% load url from future %}  This is just a marker for search as soon as we will want to change it to {% load url %}
# from django.templatetags.future import url
from django.template.defaulttags import url
from django.contrib.sites.models import Site

register = Library()


class AbsoluteURLNode(URLNode):
    def render(self, context):
        path = super(AbsoluteURLNode, self).render(context)
        domain = "http://%s" % Site.objects.get_current().domain
        return urlparse.urljoin(domain, path)


def absurl(parser, token, node_cls=AbsoluteURLNode):
    """Just like {% url %} but ads the domain of the current site."""
    node_instance = url(parser, token)
    return node_cls(view_name=node_instance.view_name,
        args=node_instance.args,
        kwargs=node_instance.kwargs,
        asvar=node_instance.asvar)
absurl = register.tag(absurl)
