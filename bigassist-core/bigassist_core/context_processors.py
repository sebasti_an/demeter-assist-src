#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bigassist_core.user import KHNPUser
from django.http import QueryDict
from bigassist_core.forms import ChooseCompanyForm, ChooseRoleForm

def bigassist(request):
    result = dict()
    if request.user.is_authenticated():
        user = KHNPUser(request.user)
        result['khnpuser'] = user
        company = user.get_company()
        if company:
            result['khnp_organisation'] = company

            orgs = user.get_companies()
            if len(list(orgs)) > 1:
                initial = {'company': company.pk}
                result['choose_company_form'] = ChooseCompanyForm(user, initial=initial)

            roles = company.get_roles()
            if len(roles) > 1:
                if 'current_role' in request.session:
                    initial = {'role': request.session.get('current_role')}
                    result['choose_role_form'] = ChooseRoleForm(user, initial=initial)
                    result['current_role'] = request.session.get('current_role')

    result['is_marketplace'] = request.path and (
                                        request.path.startswith('/marketplace')
                                     or request.path.startswith('/forum')
                                     or request.path.startswith('/customer/supplier/profile/')
                                     or request.path.startswith('/customer/get-in-touch/')
                               )
    result['is_cms'] = request.path and request.path.startswith('/cms')

    qs = QueryDict(request.GET.urlencode(), mutable=True)
    if 'page' in qs:
        qs.pop('page')
    result['url_suffix'] = qs.urlencode()
    return result


