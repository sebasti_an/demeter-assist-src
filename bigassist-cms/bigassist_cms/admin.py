#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Count

from django.contrib import admin
from django.forms import ModelForm
from django import forms

from models import Page, Section, File, Event, AgendaItem, Venue

from redactor.widgets import AdminRedactorEditor


class PageAdminForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PageAdminForm, self).__init__(*args, **kwargs)
        self.fields['introduction'].widget = AdminRedactorEditor(
            redactor_css="/static/css/main.css",
            redactor_settings={
                'lang': 'en',
                'load': True,
                'path': False,
                'focus': False,
                'iframe': True,
                'minHeight': '200',
                'buttons': ['html', 'bold', 'italic', 'link', ],  # 'image', 'video',  '|', 'image'
                'italicTag': 'i',
                'boldTag': 'b',
                'autoresize': True,
            }
            )
        self.fields['body'].widget = AdminRedactorEditor(
            redactor_css="/static/css/main.css",
            redactor_settings={
                'lang': 'en',
                'load': True,
                'path': False,
                'focus': False,
                'iframe': True,
                'minHeight': '350',
                'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link',  '|', 'image', ],  # 'image', 'video',  '|', 'image'
                'italicTag': 'i',
                'boldTag': 'b',
                # 'plugins': ['clips'],
                'autoresize': True,
                # 'css': '/styles/your_stylesheet_file.css',
                'imageUpload': '/cms/photos/upload/',
                'imageGetJson': '/cms/photos/recent/',
            }
            )


class PageAdmin(admin.ModelAdmin):

    form = PageAdminForm

    prepopulated_fields = {"slug": ("title",)}
    # inlines = [ EntryImageInline, ]
    list_display = ('title', 'created_on', 'published_on', 'public', 'order', )
    list_filter = ['created_on', 'modified_on', 'section', ]
    list_editable = ('public', 'order',)
    search_fields = ('title', 'body')

    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if db_field.name == 'thumbnail':
    #         request = kwargs.pop("request", None)
    #         kwargs['widget'] = AdminImageWidget
    #         return db_field.formfield(**kwargs)
    #     return super(EntryAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    fieldsets = (
        (None, {
            'fields': (
                ('title', 'slug'),
                'section',
                'order',
                'introduction',
                'body',
                # 'created_on',
                'published_on',
                'public',
                )
        }),
        ('Page settings', {
            'classes': ('collapse',),
            'fields': ('parent',),
        })
    )


class SectionAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'public', 'in_navigation', 'page_count', )
    list_editable = ('public', 'in_navigation', )

    def get_queryset(self, request):
        return Section.objects.annotate(page_count=Count('page'))

    def page_count(self, inst):
        return inst.page_count
    page_count.short_description = "Pages in section"


class AgendaItemInline(admin.TabularInline):
    model = AgendaItem

    exclude = ('order',)

    extra = 1

    formfield_overrides = {
        models.TextField: {'widget': AdminRedactorEditor(
            redactor_settings={
                'lang': 'en',
                'load': True,
                'path': False,
                'focus': False,
                'iframe': False,
                'minHeight': '100',
                'buttons': ['html', 'formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'link', ],  # 'image', 'video',  '|', 'image'
                'italicTag': 'i',
                'boldTag': 'b',
                # 'plugins': ['clips'],
                'autoresize': True,
                # 'css': '/styles/your_stylesheet_file.css',
                'imageUpload': '/cms/photos/upload/',
                'imageGetJson': '/cms/photos/recent/',
                }
            )},
        }


class EventAdminForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventAdminForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget = AdminRedactorEditor(
            # redactor_css="/static/css/main.css",
            redactor_settings={
                'lang': 'en',
                'load': True,
                'path': False,
                'focus': False,
                'iframe': False,
                'minHeight': '200',
                'buttons': ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link',  '|', 'image', ],  # 'image', 'video',  '|', 'image'
                'italicTag': 'i',
                'boldTag': 'b',
                'autoresize': True,
                # 'css': '/styles/your_stylesheet_file.css',
                'imageUpload': '/cms/photos/upload/',
                'imageGetJson': '/cms/photos/recent/',
            }
            )
        self.fields['organiser'].widget = forms.TextInput(
            attrs={
                'style': 'width: 450px; ',
            }
            )
        self.fields['times'].widget = forms.Textarea(
            attrs={
                'cols': '40',
                'rows': '6',
                # 'style': 'width: 450px; height: 60px;',
            }
            )
        self.fields['audience'].widget = forms.Textarea(
            attrs={
                'cols': '40',
                'rows': '6',
                # 'style': 'width: 450px; height: 60px;',
            }
            )


class VenueAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class EventAdmin(admin.ModelAdmin):

    form = EventAdminForm

    prepopulated_fields = {"slug": ("title",)}
    inlines = [AgendaItemInline, ]
    list_display = ('title', 'created_on', 'published_on', 'public', )
    list_filter = ['created_on', 'modified_on', 'venue']
    list_editable = ('public', )
    search_fields = ('title', 'text')

    fieldsets = (
        (None, {
            'fields': (
                ('title', 'slug'),
                'organiser',
                ('date', ),
                'times',
                'venue',
                'audience',
                'eventbrite_link',
                'text',
                # 'created_on',
                # 'published_on',
                # 'public',
                )
        }),
    )


admin.site.register(Section, SectionAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Venue, VenueAdmin)
