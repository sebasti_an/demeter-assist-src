Because of the unique way that django stores static files, making changes here won't filter through to the live site.

Do make the change here, so we have a stored copy.

The real files are in usr/local/bigassist/bigassist/static/css/

So your changes need to be there to make a difference.

*************** CM Nov 2013 ****************
