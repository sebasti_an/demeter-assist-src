from django.db import connection
from django.test import TestCase as BaseTestCase
from lxml import etree
from bigassist_core.models import GlobalPermission

class TestCase(BaseTestCase):

    fixture = ['types']

    def setUp(self):
        cursor = connection.cursor()
        cursor.execute("ALTER TABLE relationship DROP CONSTRAINT relationship_pkey")
        cursor.execute("ALTER TABLE relationship ADD CONSTRAINT relationship_pkey PRIMARY KEY(subject, object, type);")

        self.can_view_admin = GlobalPermission.objects.create(codename='can_view_admin', name='Can view admin')
        self.can_modify_admin = GlobalPermission.objects.create(codename='can_modify_admin', name='Can modify admin')

    def assertMessageCount(self, response, expect_num):
        """
        Asserts that exactly the given number of messages have been sent.
        Requires full context so use follow=True for client.post
        """

        actual_num = len(response.context['messages'])
        if actual_num != expect_num:
            self.fail('Message count was %d, expected %d' %
                (actual_num, expect_num))

    def assertMessageContains(self, response, text, level=None):
        """
        Asserts that there is exactly one message containing the given text.
        Requires full context so use follow=True for client.post
        """

        messages = response.context['messages']

        matches = [m for m in messages if text in m.message]

        if len(matches) == 1:
            msg = matches[0]
            if level is not None and msg.level != level:
                self.fail('There was one matching message but with different'
                    'level: %s != %s' % (msg.level, level))

            return

        elif len(matches) == 0:
            messages_str = ", ".join('"%s"' % m for m in messages)
            self.fail('No message contained text "%s", messages were: %s' %
                (text, messages_str))
        else:
            self.fail('Multiple messages contained text "%s": %s' %
                (text, ", ".join(('"%s"' % m) for m in matches)))

    def assertMessageNotContains(self, response, text):
        """ Assert that no message contains the given text.
        Requires full context so use follow=True for client.post
        """

        messages = response.context['messages']

        matches = [m for m in messages if text in m.message]

        if len(matches) > 0:
            self.fail('Message(s) contained text "%s": %s' %
                (text, ", ".join(('"%s"' % m) for m in matches)))


class XMLTestHelper(object):

    def __init__(self, xml):
        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        parser = etree.XMLParser(remove_pis=False, remove_comments=False)
        self.tree = etree.XML(xml, parser=parser)

        nsmap = self.tree.nsmap
        if None in nsmap:
            nsmap['xhtml'] = nsmap[None]
            nsmap.pop(None)
        self.nsmap = nsmap

    def get_text(self, xpath):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            return element.text or ''

    def get_attribute(self, xpath, attribute):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            return element.get(attribute)

    def set_text(self, xpath, value):
        for element in self.tree.xpath(xpath, namespaces=self.nsmap):
            element.text = value

    def tostring(self, **kwargs):
        if 'xml_declaration' not in kwargs:
            kwargs['xml_declaration'] = True
        if 'encoding' not in kwargs:
            kwargs['encoding'] = 'utf-8'
        # flushing self.tree.getroottree() to preserve stylesheet processing instructions
        return etree.tostring(self.tree.getroottree(), **kwargs)
