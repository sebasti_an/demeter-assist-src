from django.contrib.auth.models import User
from bigassist_core.user import KHNPUser
from bigassist_core.models import Contact
from bigassist_core.models import Identity
from bigassist_core.models import Person
from bigassist_core.models import Organisation

from base import TestCase

EMAIL = 'test@khnp.org'


class KHNPUserTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(KHNPUserTest, self).setUp()
        # create a Django user
        self.user = User.objects.create(username='testuser', email=EMAIL)

    def tearDown(self):
        # Delete all exisitng references to test@khnp.org from database
        Person.objects.filter(username=EMAIL).delete()
        Contact.objects.filter(value=EMAIL).delete()
        Identity.objects.filter(persona=EMAIL).delete()
        User.objects.all().delete()

    def test_identity(self):
        """ Check creating user identity, including Contact and Person records
        This simulates situation when new user is logged in to the site
        for the very first time.
        """
        user = KHNPUser(self.user)
        identity = user.get_identity()
        self.failIf(identity is None)
        # There is a single Person which has the same username as identity.person
        self.failUnless(Person.objects.get(username=EMAIL).pk == identity.person.pk)
        # Contact email is the same as user's email
        self.failUnless(identity.person.contact_set.all()[0].value == self.user.email)

    def test_identity_with_contact(self):
        """ Test assignment of Person/Contact to identity
            This simulates sitation when existing KHNP user is logged in
            using Persona for the very first time
        """
        person = Person.objects.create(type_id=1, username=EMAIL)
        person.set_contact('email', EMAIL)
        self.failIf(Identity.objects.filter(persona__iexact=EMAIL).exists())
        # there is single contact
        self.failUnless(Contact.objects.filter(value=EMAIL).count() == 1)
        user = KHNPUser(self.user)
        identity = user.get_identity()
        self.failIf(identity is None)
        self.failUnless(identity.persona == EMAIL)
        self.failUnless(identity.person.pk == person.pk)
        # there has to be still exactly one Contact
        self.failUnless(Contact.objects.filter(value=EMAIL).count() == 1)

    def test_identity_with_contacts(self):
        """ Test assignment of Person/Contact to identity
            This simulates sitation when existing KHNP user is logged in
            using Persona for the very first time, but, apart of previous one,
            there are two the same emails in the Contact database so we don't know
            which person to use -> we must create a new person
        """
        # WARNING: if username would be EMAIL, the test would fail on duplicate value for USERNAME field.
        # I've asked Peter how to handle this - https://it.fry-it.com/text-matters/dev/001
        person1 = Person.objects.create(type_id=1, username='dummy1')
        person2 = Person.objects.create(type_id=1, username='dummy2')
        person1.set_contact('email', EMAIL)
        person2.set_contact('email', EMAIL)

        self.failUnless(Contact.objects.filter(value=EMAIL).count() == 2)

        user = KHNPUser(self.user)
        identity = user.get_identity()
        self.failIf(identity is None)
        self.failUnless(identity.persona == EMAIL)
        # the identity is not assigned to any of person1/person2 but new person is created.
        self.failIf(identity.person.pk == person1.pk)
        self.failIf(identity.person.pk == person2.pk)
        # there will be three perosns and contacts now
        self.failUnless(Contact.objects.filter(value=EMAIL).count() == 3)
        self.failUnless(Person.objects.count() == 3)

    def test_company_blank_setup(self):
        user = KHNPUser(self.user)
        person = user.get_person()
        self.failIf(person is None)

        company = user.get_company()
        self.failUnless(company is None)

        self.assertRaises(AssertionError, Organisation.objects.register, person, {})

        defaults = {'is_supplier': True}
        company = Organisation.objects.register(person, defaults)
        self.failIf(company is None)

        self.assertTrue(company.is_supplier)
