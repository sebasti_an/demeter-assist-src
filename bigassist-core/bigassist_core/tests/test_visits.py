from django.conf import settings
from datetime import datetime
from django.core import mail
from django.core.urlresolvers import reverse
from bigassist_core.views.customer import initialize_customer_diagnostic
from bigassist_core.user import KHNPUser
from bigassist_core import models
from django.contrib.auth.models import User

from .base import TestCase

EMAIL = 'test@khnp.org'
CUST_EMAIL = 'test_cust@khnp.org'


class VisitsTest(TestCase):

    fixtures = ['types', 'mails', 'xforms']

    def tearDown(self):
        User.objects.all().delete()
        models.Organisation.objects.all().delete()

    def _setup_users(self):
        self.comm_method = models.CommMethod.objects.create(comm_method='Email')
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.topic = models.SupportTopic.objects.all()[0]
        pri_lev = models.DevPriLev.objects.get(pk=1)
        support_type = models.DevSupportType.objects.get(pk=1)

        user = KHNPUser(self.user)
        person = user.get_person()
        self.company = models.Organisation.objects.register(person,
                                                defaults={'is_supplier': False})
        self.company.org.displayname = 'Company 1'
        self.company.org.save()

        initialize_customer_diagnostic(self.company)
        diag = self.company.get_diagnostic()
        diag.set_state('approved')
        diag.customerdevpri_set.create(topic=self.topic,
                                       pri_lev=pri_lev,
                                       support_type=support_type,
                                       value=1000)

    def test_visit_crud(self):
        self._setup_users()
        visits_url = reverse('customer_dashboard', args=['visitors'])
        meet_url = reverse('customer_dashboard', args=['connectspace'])

        self.client.login(username='testuser', password='secret')
        res = self.client.get(visits_url)
        self.assertContains(res, '<h2>You don\'t host any visits</h2>', html=True)

        res = self.client.get(meet_url)
        self.assertContains(res, '<h2>You have no visits</h2>', html=True)

        url = reverse('customer_host_a_visit')
        res = self.client.get(url)
        self.assertTemplateUsed(res, 'customer/host_a_visit_pre.html')

        res = self.client.post(url, {'disclaimer': '1'}, follow=True)
        self.assertTemplateUsed(res, 'customer/host_a_visit.html')

        data = dict(
            host=self.company.pk,
            title='Visit title',
            description='Visit description',
            region=2,  # primary key
            date_type='anytime',
            topics=['10', '11', '21'],
        )
        url = reverse('customer_host_a_visit2')
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 302)
        self.assertEqual(models.Visit.objects.count(), 1)

        # list visits hosted by organisation
        res = self.client.get(visits_url)
        visit = models.Visit.objects.all()[0]
        self.assertContains(res, '<h3>%s</h3>' % visit.title, html=True)
        self.assertContains(res, '<span>anytime by arrangement</span>', html=True)
        self.assertContains(res, 'Edit details')
        self.assertContains(res, 'Remove visit')
        topics = sorted([x.pk for x in visit.topics.all()])
        regions = sorted([x.pk for x in visit.region.all()])
        self.assertEqual(regions, [2])
        self.assertEqual(topics, [10, 11, 21])

        # edit form
        url = reverse('visit_edit', args=[visit.pk])
        data = dict(
            host=self.company.pk,
            title='Visit title2',
            description='Visit description2',
            date_type='fixed',
            date='12 Jan 2013',
            region=[3, 4],  # primary key
            topics=['11', '21'],
        )
        res = self.client.post(url, data)
        self.assertContains(res, 'Visit date can&#39;t be in the past.')
        data['date'] = '12 Jan 2099'
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 302)
        visit = models.Visit.objects.all()[0]
        topics = sorted([x.pk for x in visit.topics.all()])
        regions = sorted([x.pk for x in visit.region.all()])
        self.assertEqual(visit.title, 'Visit title2')
        self.assertEqual(visit.description, 'Visit description2')
        self.assertEqual(regions, [3, 4])
        self.assertEqual(topics, [11, 21])

        url = reverse('visit_remove', args=[visit.pk])
        res = self.client.post(url)
        self.assertEqual(models.Visit.objects.count(), 0)

    def test_visit_visitor(self):
        self._setup_users()

        # create a Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit title',
                    description='Visit description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        # we need also a Visitor
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        user2 = KHNPUser(user2)
        person2 = user2.get_person()
        company2 = models.Organisation.objects.register(person2,
                                           defaults={'is_supplier': False})
        company2.org.displayname = 'Company 2'
        company2.org.save()
        initialize_customer_diagnostic(company2)

        self.client.login(username='testuser2', password='secret')
        url = reverse('visit_get_in_touch', args=[visit.pk])
        res = self.client.get(url)
        self.assertContains(res, '<label class="form-label">Recipient</label>', html=True)
        self.assertContains(res, '<span class="form-input">%s</span>' % self.company.org.displayname, html=True)

        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        self.assertEqual(res.status_code, 302)
        self.assertEqual(models.Visitor.objects.count(), 1)
        visitor = models.Visitor.objects.all()[0]
        self.assertEqual(visitor.state, 'pending')
        # test also a VisitorManager behavior
        self.assertEqual(models.Visitor.objects.potential().count(), 1)
        self.assertEqual(models.Visitor.objects.confirmed().count(), 0)
        self.assertEqual(models.Visitor.objects.completed().count(), 0)
        self.assertEqual(visitor.visitor_org, company2)
        self.assertEqual(visitor.contact_msg, 'I want to visit your company, please')
        self.assertEqual(len(mail.outbox), 2)
        self.failUnless(mail.outbox[0].subject == 'You\'ve contacted %s regarding a visit' % self.company)
        self.failUnless(mail.outbox[1].subject == 'Someone has contacted you regarding a visit')

        # check Meetspace tab
        res = self.client.get(reverse('customer_dashboard', args=['connectspace']))
        self.assertContains(res, '1 potential visit')
        self.assertContains(res, 'Apply for sponsorship')

        # The visitor must appear on Host dashboard - Visitors tab
        self.client.login(username='testuser', password='secret')
        res = self.client.get(reverse('customer_dashboard', args=['visitors']))
        self.assertContains(res, '<h3>Visit title</h3>', html=True)
        self.assertContains(res, '<td>Company 2</td>', html=True)
        self.assertContains(res, 'Accept')
        self.assertContains(res, 'Decline')

        # finally discard the visit as a Visitor
        # first try to discard visit as visit host - this should not be possible because
        # the host is not a Visitor (VisitorSecurity.discard_visit should reject it)
        url = reverse('visitor_discard', args=[visitor.pk])
        self.client.login(username='testuser', password='secret')
        res = self.client.post(url, follow=True)
        self.assertEqual(models.Visitor.objects.count(), 1)
        self.assertContains(res, 'This visit can&#39;t be discarded.')
        # let's do it as a visitor now
        self.client.login(username='testuser2', password='secret')
        res = self.client.post(url, follow=True)
        self.assertEqual(models.Visitor.objects.count(), 0)
        self.assertContains(res, "The visit has been discarded")

    def test_visit_confirm(self):
        self._setup_users()

        # create a Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit title',
                    description='Visit description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        # we need also a Visitor
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        user2 = KHNPUser(user2)
        person2 = user2.get_person()
        company2 = models.Organisation.objects.register(person2,
                                           defaults={'is_supplier': False})
        company2.org.displayname = 'Company 2'
        company2.org.save()

        self.client.login(username='testuser2', password='secret')
        url = reverse('visit_get_in_touch', args=[visit.pk])
        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        # Forbidden - user does not have a diagnostic yet
        self.assertEqual(res.status_code, 403)

        initialize_customer_diagnostic(company2)
        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        self.assertEqual(res.status_code, 302)

        # The visitor must appear on Host dashboard - Visitors tab
        self.client.login(username='testuser', password='secret')
        res = self.client.get(reverse('customer_dashboard', args=['visitors']))
        self.assertContains(res, 'Accept')
        self.assertEqual(models.Visitor.objects.count(), 1)
        visitor = models.Visitor.objects.all()[0]
        url = reverse('visitor_accept', args=[visitor.pk])
        res = self.client.get(url)
        self.assertContains(res, 'Applicant')
        self.assertContains(res, unicode(visitor.visitor_org))
        dt = datetime(2100, 10, 10)
        dts = dt.strftime(settings.DATE_FORMAT_P)
        res = self.client.post(url, {})
        self.assertContains(res, "Please select either Date or existing date.")
        visit_instance = models.VisitInstance.objects.create(visit=visit, date=datetime(2100, 10, 11))
        data = dict(date=dts, existing=visit_instance.pk)
        res = self.client.post(url, data)
        self.assertContains(res, "Please select either Date or existing date, not both.")
        data = dict(date=datetime(2100, 10, 11).strftime(settings.DATE_FORMAT_P), existing=visit_instance.pk)
        res = self.client.post(url, data)
        self.assertContains(res, "Visit instance at that date already exists. Please select from dropdown below.")
        data = dict(date=dts)
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 302)

        url = reverse('customer_dashboard', args=['visitors'])
        res = self.client.get(url)
        self.assertContains(res, 'Visit agreed for %s' % dts)

        # Visitor point of view
        self.client.login(username='testuser2', password='secret')
        url = reverse('customer_dashboard', args=['connectspace'])
        res = self.client.get(url)
        self.assertContains(res, 'Confirmed')

        # check bursary
        self.assertContains(res, 'Apply for sponsorship')
        url = reverse('visitor_bursary', args=[visitor.pk])
        res = self.client.get(url)
        self.assertContains(res, '<h1>Apply for sponsorship</h1>', html=True)
        res = self.client.post(url, {'benefit': "I need help", 'purpose': 'I expect I will be better.'})
        self.assertEqual(res.status_code, 302)

        url = reverse('customer_dashboard', args=['connectspace'])
        res = self.client.get(url)
        self.assertNotContains(res, 'Apply for sponsorship')
        self.assertContains(res, 'Requested:')

        url = reverse('visit_detail', args=[visitor.visit.pk])
        res = self.client.get(url)
        self.assertNotContains(res, 'Apply for sponsorship')
        self.assertContains(res, 'contacted')

    def test_visit_feedback(self):
        self._setup_users()

        # create a Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit title',
                    description='Visit description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        # we need also a Visitor
        user2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        user2 = KHNPUser(user2)
        person2 = user2.get_person()
        company2 = models.Organisation.objects.register(person2,
                                           defaults={'is_supplier': False})
        company2.org.displayname = 'Company 2'
        company2.org.save()
        initialize_customer_diagnostic(company2)

        self.client.login(username='testuser2', password='secret')
        url = reverse('visit_get_in_touch', args=[visit.pk])
        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        self.assertEqual(res.status_code, 302)

        self.client.login(username='testuser', password='secret')
        visitor = models.Visitor.objects.all()[0]
        url = reverse('visitor_accept', args=[visitor.pk])
        dt = datetime(2011, 10, 10)
        dts = dt.strftime(settings.DATE_FORMAT_P)
        data = dict(date=dts)
        res = self.client.post(url, data)
        self.assertContains(res, 'Visit date can&#39;t be in the past.')

        dt = datetime.now().date()
        dts = dt.strftime(settings.DATE_FORMAT_P)
        data = dict(date=dts)
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 302)

        # This can't be tested properly, because it works with datetime.now()
        # # Visitor point of view - the visit date has passed so the visit is completed.
        # self.client.login(username='testuser2', password='secret')
        # url = reverse('customer_dashboard', args=['connectspace'])
        # res = self.client.get(url)
        # self.assertContains(res, 'Completed')
        # self.assertContains(res, 'Leave feedback')

        # url = reverse('visitor_feedback', args=[visitor.pk])
        # res = self.client.post(url, {'fb_score': 1, 'fb_comment': 'Everything OK'})
        # self.assertEqual(res.status_code, 302)
        # url = reverse('customer_dashboard', args=['connectspace'])
        # res = self.client.get(url)
        # self.assertContains(res, 'Feedback given (1)')

    def test_visit_admin(self):
        self._setup_users()

        self.admin = User.objects.create_superuser(username='admin', email='admin@khnp.org', password='secret')
        response = self.client.get(reverse('admin_dashboard_meetspace'))
        self.assertContains(response, 'Register')
        self.client.login(username='admin', password='secret')
        response = self.client.get(reverse('admin_dashboard_meetspace'))
        self.assertContains(response, 'There aren\'t any unmoderated visits or mentoring')

        # create a Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit title',
                    description='Visit description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        self.assertEqual(visit.available, None)

        response = self.client.get(reverse('admin_dashboard_meetspace'))
        self.assertContains(response, '<h1>Unmoderated items</h1>', html=True)
        self.assertContains(response, unicode(visit))
        self.assertContains(response, '>Approve</a>')
        self.assertContains(response, '>Reject</a>')

        # Also check the visit is available on Marketplace
        response = self.client.get(reverse('marketplace'))
        self.assertContains(response, '<h2>Connect<span>Space</span> <span class="count">(0)</span></h2>', html=True)
        response = self.client.get(reverse('connectsgroup'))
        # since admin doesn't have an organisation, there is no 'contacted' marker
        self.assertContains(response, '<h3>VISIT: %s</h3>' % unicode(visit), html=True)
        count = response.content.count('<h3>VISIT:')
        self.assertEqual(count, 1)

        # Approve visit
        response = self.client.post(reverse('admin_visit_approve', args=[visit.pk]), follow=True)
        self.assertEqual(len(mail.outbox), 0)
        self.assertContains(response, "The visit has been approved")
        self.assertEqual(models.Visit.objects.get(pk=visit.pk).available, True)

        response = self.client.get(reverse('admin_dashboard_meetspace'))
        self.assertContains(response, 'There aren\'t any unmoderated visits or mentoring')

        # create another Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit2 title',
                    description='Visit2 description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        # Visit is available on marketplace
        response = self.client.get(reverse('connectsgroup'))
        count = response.content.count('<h3>VISIT:')
        self.assertEqual(count, 2)

        # Approve visit
        response = self.client.post(reverse('admin_visit_reject', args=[visit.pk]), follow=True)
        self.assertEqual(len(mail.outbox), 1)
        self.assertContains(response, "The visit has been reject")
        self.assertEqual(models.Visit.objects.get(pk=visit.pk).available, False)

        # Visit is not on marketplace
        response = self.client.get(reverse('marketplace'))
        self.assertContains(response, '<h2>Connect<span>Space</span> <span class="count">(0)</span></h2>', html=True)
        response = self.client.get(reverse('connectsgroup'))
        count = response.content.count('<h3>VISIT:')
        self.assertEqual(count, 1)

    def test_visit_sponsorship(self):
        self._setup_users()
        self.admin = User.objects.create_superuser(username='admin', email='admin@khnp.org', password='secret')

        # create another company which will act as a visitor
        duser2 = User.objects.create_user(username='testuser2', email='test2@khnp.org', password='secret')
        pri_lev = models.DevPriLev.objects.get(pk=1)
        support_type = models.DevSupportType.objects.get(pk=1)

        user2 = KHNPUser(duser2)
        person2 = user2.get_person()
        company2 = models.Organisation.objects.register(person2,
                                                defaults={'is_supplier': False})
        company2.org.displayname = 'Company 2'
        company2.org.save()

        initialize_customer_diagnostic(company2)
        diag = company2.get_diagnostic()
        diag.set_state('approved')
        diag.customerdevpri_set.create(topic=self.topic,
                                       pri_lev=pri_lev,
                                       support_type=support_type,
                                       value=2000)

        # create a Visit
        visit = models.Visit.objects.create(
                    host=self.company,
                    title='Visit title',
                    description='Visit description'
        )
        visit.region.add(models.Region.objects.all()[0])
        visit.topics.add(*models.SupportTopic.objects.all()[:4])

        # create visitor
        self.client.login(username='testuser2', password='secret')
        url = reverse('visit_get_in_touch', args=[visit.pk])
        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        self.assertEqual(res.status_code, 302)
        self.assertTrue(models.Visitor.objects.count(), 1)

        SPONSORSHIP_VISITOR = models.BursaryTypes.objects.get(name='visitor').value
        SPONSORSHIP_HOST = models.BursaryTypes.objects.get(name='host').value

        visitor = models.Visitor.objects.all()[0]
        # ask for sponsorship (as a visitor)
        url = reverse('visitor_bursary', args=[visitor.pk])
        self.client.post(url, {'purpose': 'Purpose of the visit', 'benefit': 'Benefit of the sponsorship'})
        self.assertEqual(res.status_code, 302)
        visitor = models.Visitor.objects.all()[0]
        self.assertEqual(visitor.amount, SPONSORSHIP_VISITOR)
        self.assertEqual(visitor.bursary, None)

        # there are 2 options how sponsorship can be handled.
        # 1) sponsorship is granted before visitor is assigned to Visit Instance
        # 2) sponsorship is granted after visitor is assigned to Visit Instance
        # in both cases, visit host should get a sponsorship for host (400 by default) if at least
        # one visitor gets sponsored
        # 3) no sponsroship for visit host
        # let's try to test all three options

        # 1) sponsorship is granted before visitor is assigned to Visit Instance
        self.client.login(username='admin', password='secret')
        self.client.post(reverse('admin_sponsorship_grant', args=['visitor', visitor.pk]))
        visitor = models.Visitor.objects.all()[0]
        self.assertEqual(visitor.bursary, True)
        self.assertEqual(visitor.visit_instance, None)  # not assigned yet
        self.assertEqual(models.VisitInstance.objects.count(), 0)
        # check total in admin sponsorship history
        res = self.client.get(reverse('admin_dashboard_bursary_history'))
        self.assertEqual(res.context_data['total_sum'], SPONSORSHIP_VISITOR)
        self.assertEqual(res.context_data['total_count'], 1)
        # as soon as visitor's visit date is arranged, the host should get sponsorship as well
        self.client.login(username='testuser', password='secret')
        self.client.post(reverse('visitor_accept', args=[visitor.pk]),
                         {'date': '1 Jan 2050'})
        self.assertEqual(models.VisitInstance.objects.count(), 1)
        self.client.login(username='admin', password='secret')
        res = self.client.get(reverse('admin_dashboard_bursary_history'))
        self.assertEqual(res.context_data['total_sum'], SPONSORSHIP_VISITOR + SPONSORSHIP_HOST)
        self.assertEqual(res.context_data['total_count'], 2)

        # 2) sponsorship is granted after visitor is assigned to Visit Instance
        models.VisitInstance.objects.all().delete()
        visitor.delete()
        # create visitor again, but don't request bursary
        self.client.login(username='testuser2', password='secret')
        url = reverse('visit_get_in_touch', args=[visit.pk])
        res = self.client.post(url, {'contact_msg': 'I want to visit your company, please'})
        self.assertEqual(res.status_code, 302)
        visitor = models.Visitor.objects.all()[0]
        # arrange a visit
        self.client.login(username='testuser', password='secret')
        self.client.post(reverse('visitor_accept', args=[visitor.pk]),
                         {'date': '1 Jan 2050'})
        self.assertEqual(res.status_code, 302)
        # check current status of sponsorship
        self.client.login(username='admin', password='secret')
        res = self.client.get(reverse('admin_dashboard_bursary_history'))
        self.assertEqual(res.context_data['total_sum'], 0)
        self.assertEqual(res.context_data['total_count'], 0)

        # request sponsorship
        self.client.login(username='testuser2', password='secret')
        url = reverse('visitor_bursary', args=[visitor.pk])
        res = self.client.post(url, {'purpose': 'Purpose of the visit', 'benefit': 'Benefit of the sponsorship'})
        self.assertEqual(res.status_code, 302)

        # accept sponsorship
        self.client.login(username='admin', password='secret')
        self.client.post(reverse('admin_sponsorship_grant', args=['visitor', visitor.pk]))
        visitor = models.Visitor.objects.all()[0]
        # both visitor and host should be sponsored
        self.client.login(username='admin', password='secret')
        res = self.client.get(reverse('admin_dashboard_bursary_history'))
        self.assertEqual(res.context_data['total_sum'], SPONSORSHIP_VISITOR + SPONSORSHIP_HOST)
        self.assertEqual(res.context_data['total_count'], 2)
