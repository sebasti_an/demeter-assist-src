#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.validators import validate_email
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.http import HttpResponseForbidden
from django.core.urlresolvers import reverse
from bigassist_core.user import KHNPUser
from bigassist_core.models import InvitedUser, RelationshipType
from bigassist_core.emails import send_ba_email
from bigassist_core.security import UserSecurity


class InviteUsersBaseView(TemplateView):
    # abstract view

    def dispatch(self, request, *args, **kwargs):
        self.user = KHNPUser(request.user)
        self.company = self.user.get_company()
        self.person = self.user.get_person()

        # This check is not needed, because all members of the company
        # can access the page
        # found = False
        # for member in self.company.get_members(only_managers=True):
        #     if member.pk == self.person.pk:
        #         found = True
        # if not found:
        #     return HttpResponseForbidden('You are not allowed to manage users')

        return super(InviteUsersBaseView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InviteUsersBaseView, self).get_context_data(**kwargs)

        khnp_person_pk = self.person.pk

        members = list()
        context['members'] = list()
        for member in self.company.get_members():
            pk = member.pk
            email = member.get_contact_email()
            if pk in members:
                continue
            suffix = ''
            if pk == khnp_person_pk:
                suffix = ' (you)'
            is_manager = member.rel_subject_set.filter(object=self.company.pk, type__id=settings.KHNP_MANAGER_TYPE_ID).exists()
            # Had to change 'object=self.company, …' to ref the PK because django 1.6 looks for id field
            if is_manager:
                suffix += ' Manager'
            context['members'].append("%s <%s>%s" % (member.displayname, email, suffix))

        for record in InvitedUser.objects.filter(org=self.company, registered__isnull=True):
            suffix = ' (email invitation sent)'
            if record.type.id == settings.KHNP_MANAGER_TYPE_ID:
                suffix += ' Manager'
            context['members'].append("<%s>%s" % (record.email, suffix))

        return context

    def post(self, request, *args, **kwargs):

        security = UserSecurity(request.user)
        if not security.can('manage_users'):
            return HttpResponseForbidden('You are not allowed to manage users')

        emails = request.POST.get('users', '').split()
        emails = [x.strip() for x in emails]

        typ = request.POST.get('type', 'member')
        if typ == 'manager':
            rel_type_id = settings.KHNP_MANAGER_TYPE_ID
        else:
            rel_type_id = settings.KHNP_MEMBER_TYPE_ID
        rel_type = RelationshipType.objects.get(pk=rel_type_id)

        members = []
        for member in self.company.get_members():
            members.append(member.get_contact_email())

        for email in emails:
            # check if email is not member of the organisation already
            if email in members:
                continue

            try:
                validate_email(email)
            except ValidationError:
                continue

            row, created = InvitedUser.objects.get_or_create(email=email,
                                                             org=self.company,
                                                             defaults=dict(person=self.person,
                                                                           user=request.user,
                                                                           org=self.company,
                                                                           type=rel_type,
                                                                           email=email))
            if created:
                data = dict(
                    company=self.company,
                    person=self.person,
                    pk=row.pk
                )
                send_ba_email('33_user_invited', data, sender=None, recipients=[email])
        return HttpResponseRedirect(reverse('org_users'))
