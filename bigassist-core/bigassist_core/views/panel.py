import datetime
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.db.models import Count
from django.db import transaction
from django.views.generic import TemplateView, UpdateView

from bigassist_core.utils import StaffRequiredDecorator, AdminRequiredDecorator, ROAdminRequiredDecorator
from bigassist_core.emails import send_ba_email
from bigassist_core.models import OrgType, SupportArea, SupplierApproval, \
    SupplierDeadline
from bigassist_core.forms import ApprovalReviewForm
from bigassist_core.tasks import get_celery_result
from bigassist_core.security import PanelSecurity, SupplierApprovalSecurity


@ROAdminRequiredDecorator
class PanelDashboardView(TemplateView):

    template_name = "panel/dashboard.html"

    def dispatch(self, request, *args, **kwargs):
        self.deadline = SupplierDeadline.objects.last_deadline()
        return super(PanelDashboardView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PanelDashboardView, self).get_context_data(**kwargs)

        deadline = self.deadline
        context['deadline'] = deadline

        if deadline is None:
            return context

        approvals = deadline.get_approvals()
        context['approvals'] = approvals

        stats = {}
        stats['total'] = approvals.count()

        stats['new_submissions'] = approvals.filter(prev_appr__isnull=True).count()
        stats['re_submissions'] = approvals.filter(prev_appr__isnull=False).count()

        stats['org_types'] = OrgType.objects.filter(organisation__supplierapproval__in=approvals)\
            .annotate(count=Count('organisation')).values()
        stats['areas'] = SupportArea.objects.filter(supporttopic__approvaltopic__appr__in=approvals)\
            .annotate(count=Count('supporttopic__approvaltopic__appr__suppl')).values()

        completed = approvals.filter(decision__isnull=False)
        stats['completed'] = completed.count()
        stats['in_progress'] = approvals.filter(decision__isnull=True).count()

        stats['passed'] = completed.filter(is_approved=True).count()
        stats['failed'] = completed.filter(is_approved=False).count()

        stats['completed_org_types'] = OrgType.objects.filter(organisation__supplierapproval__in=completed)\
            .annotate(count=Count('organisation')).values()
        stats['completed_areas'] = SupportArea.objects.filter(supporttopic__approvaltopic__appr__in=completed)\
            .annotate(count=Count('supporttopic__approvaltopic__appr__suppl')).values()

        # PDF file
        context['pdf_ready'] = False
        context['pdf_task_success'] = False
        context['pdf_task_message'] = ''
        task_id = deadline.pdf_task_id
        if task_id:
            task_result = get_celery_result(task_id)
            context['pdf_state'] = task_result.state
            if task_result.ready():
                # READY is when state is any of : ['FAILURE', 'REVOKED', 'SUCCESS']
                context['pdf_ready'] = True
                context['pdf_task_success'] = task_result.successful()
                context['pdf_task_message'] = task_result.result  # None or message
            else:
                context['pdf_ready'] = False
                if task_result.state == 'PROGRESS':
                    # PROGRESS is a custom state set by current_task.update_state
                    context['pdf_progress'] = '%(current)d/%(total)d' % task_result.info
                elif task_result.state == 'PENDING':
                    context['pdf_progress'] = 'waiting for start'
        context.update(stats)

        return context

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        
        if not request.user.has_perm('bigassist_core.can_review_panel'):
            messages.warning(request, "Your current user role does not allow you to proceed")
            url = reverse('panel_dashboard')
            return HttpResponseRedirect(url)

        security = PanelSecurity(self.deadline)

        if 'go_to_submission' in request.POST:
            approval = get_object_or_404(SupplierApproval, pk=request.POST.get('approval'))
            url = reverse('panel_approval_review', args=(approval.pk, ))
            return HttpResponseRedirect(url)

        if 'start_panel' in request.POST:
            
            if security.check("begin_review"):
                self.deadline.set_state('in_progress')

            approvals = self.deadline.get_approvals().filter(decision__isnull=True)

            if approvals.count() > 0:
                url = reverse('panel_approval_review', args=(approvals[0].pk, ))
            else:
                messages.success(self.request, "No more unprocessed approvals")
                url = reverse('panel_dashboard')
            return HttpResponseRedirect(url)

        if 'complete_panel' in request.POST and security.check("complete_review"):
            self.deadline.set_state('completed')
            approvals = self.deadline.get_approvals()
            for approval in approvals:
                if approval.is_approved:
                    approval.set_state('approved')
                    send_ba_email('051_supplier_received_decision_approved', {}, sender=None, recipients=approval.suppl, recipient_role='supplier')
                else:
                    if approval.to_be_removed:
                        # FIXME - send an email to say sorry we have deleted your company?
                        approval.suppl.delete()
                    else:
                        approval.set_state('rejected')
                        send_ba_email('052_supplier_received_decision_rejected', {}, sender=None, recipients=approval.suppl, recipient_role='supplier')

            url = reverse('panel_dashboard')
            return HttpResponseRedirect(url)

        return HttpResponseForbidden()


@ROAdminRequiredDecorator
class PanelApprovalReviewView(UpdateView):

    model = SupplierApproval
    form_class = ApprovalReviewForm
    template_name = "panel/approval_review.html"
    context_object_name = "approval"

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        security = SupplierApprovalSecurity(self.get_object())
        if not request.user.has_perm('bigassist_core.can_review_panel'):
            return HttpResponseForbidden()

        if not security.check("can_panel_review"):
            return HttpResponseForbidden()

        return super(PanelApprovalReviewView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        approval = self.get_object()
        topics = approval.approvaltopic_set.filter(is_approved=True)
        initial = {'topics': [x.pk for x in topics]}
        if approval.is_approved is not None:
            approved = int(approval.is_approved)
            if approval.to_be_removed:
                approved = -1
            initial['approved'] = approved
        return initial

    def get_form(self, form_class):
        return form_class(self.get_object(), **self.get_form_kwargs())

    @transaction.atomic
    def form_valid(self, form):
        approval = form.save(commit=False)
        approval.decision = datetime.date.today()
        approval.is_approved = form.cleaned_data['is_approved']
        approval.to_be_removed = form.cleaned_data['to_be_removed']
        approval.save()

        approval.approvaltopic_set.all().update(is_approved=False)
        for topic in approval.approvaltopic_set.all():
            if topic in form.cleaned_data['topics']:
                topic.is_approved = True
                topic.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        approval = self.get_object()
        deadline = approval.get_deadline()
        approvals = deadline.get_approvals().filter(decision__isnull=True)

        if approvals.count() > 0:
            url = reverse('panel_approval_review', args=(approvals[0].pk, ))
        else:
            url = reverse('panel_dashboard')
        return url
