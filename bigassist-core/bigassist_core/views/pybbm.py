from django.http import Http404
from django.shortcuts import get_object_or_404
from bigassist_core.user import KHNPUser
from pybb import compat, defaults, util
from pybb.views import AddPostView as BaseAddPostView
from pybb.models import Post, Topic, Forum
from pybb.permissions import perms


class AddPostView(BaseAddPostView):
    """We are overwriting the whole dispatch method of pybb's AddPostView
    So we can pass in a single small alteration. See comment below"""

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            self.user = request.user
        else:
            if defaults.PYBB_ENABLE_ANONYMOUS_POST:
                self.user, new = User.objects.get_or_create(**{username_field: defaults.PYBB_ANONYMOUS_USERNAME})
            else:
                from django.contrib.auth.views import redirect_to_login
                return redirect_to_login(request.get_full_path())

        self.forum = None
        self.topic = None
        if 'forum_id' in kwargs:
            self.forum = get_object_or_404(perms.filter_forums(request.user, Forum.objects.all()), pk=kwargs['forum_id'])
            if not perms.may_create_topic(self.user, self.forum):
                raise PermissionDenied
        elif 'topic_id' in kwargs:
            self.topic = get_object_or_404(perms.filter_topics(request.user, Topic.objects.all()), pk=kwargs['topic_id'])
            if not perms.may_create_post(self.user, self.topic):
                raise PermissionDenied

            self.quote = ''
            if 'quote_id' in request.GET:
                try:
                    quote_id = int(request.GET.get('quote_id'))
                except TypeError:
                    raise Http404
                else:
                    post = get_object_or_404(Post, pk=quote_id)
                    # profile = util.get_pybb_profile(post.user)
                    # self.quote = util._get_markup_quoter(defaults.PYBB_MARKUP)(post.body, profile.get_display_name())

                    # we are no longer using the profile variable, but instead
                    # passing in the khnp_user's get_display_name
                    khnp_user = KHNPUser(post.user, require_authenticated=False)
                    self.quote = util._get_markup_quoter(defaults.PYBB_MARKUP)(post.body, unicode(khnp_user.get_person()))

                if self.quote and request.is_ajax():
                    return HttpResponse(self.quote)
        return super(AddPostView, self).dispatch(request, *args, **kwargs)


# class OldAddPostView(BaseAddPostView):
#     """
#     Overriding this view (get_form_kwargs method) because we need different behavior
#     of the quote.
#     Original:
#     quote = defaults.PYBB_QUOTE_ENGINES[defaults.PYBB_MARKUP](post.body, post.user.username)
#     """
#     def get_form_kwargs(self):
#         ip = self.request.META.get('REMOTE_ADDR', '')
#         form_kwargs = super(AddPostView, self).get_form_kwargs()
#         form_kwargs.update(dict(topic=self.topic, forum=self.forum, user=self.user,
#                        ip=ip, initial={}))
#         if 'quote_id' in self.request.GET:
#             try:
#                 quote_id = int(self.request.GET.get('quote_id'))
#             except TypeError:
#                 raise Http404
#             else:
#                 post = get_object_or_404(Post, pk=quote_id)
#                 khnp_user = KHNPUser(post.user, require_authenticated=False)
#                 quote = defaults.PYBB_QUOTE_ENGINES[defaults.PYBB_MARKUP](post.body, unicode(khnp_user.get_person()))
#                 form_kwargs['initial']['body'] = quote
#         if self.user.is_staff:
#             form_kwargs['initial']['login'] = self.user.username
#         return form_kwargs
