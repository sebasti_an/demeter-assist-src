#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db.models import Q
import os
import datetime
import json
import csv
import cStringIO
import codecs
from dateutil.relativedelta import relativedelta
from django.db import connection
from django.conf import settings
from django.db import transaction
from django.db import DatabaseError
from django.db.models import Sum, Count
from django.views.generic import TemplateView, UpdateView, DetailView, View
from django.views.generic import FormView, ListView
from django.http import HttpResponseRedirect, HttpResponse, \
    HttpResponseForbidden
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.safestring import mark_safe
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from lxml import etree
from bigassist_core.helpers import get_or_none
from bigassist_core.models import Organisation, CustomerDiagnostic, \
    CustomerDevPri, SupportTopic, XForms, CustomerCommLog, CustomerCommType, \
    SupplierApproval, CustomerPrequal, CustomerVoucher, CustomerProject, \
    InvitedUser, Identity, Payment, SupplierDeadline, Visit, Report, \
    Mentor, Visitor, Mentee, Contact, VisitInstance, Relationship
from bigassist_core.utils import StaffRequiredDecorator, ConfirmationView, ConfirmationDetailView, \
    AdminRequiredDecorator, ROAdminRequiredDecorator
from bigassist_core.utils import check_token, CSVRespondMixin
from bigassist_core.utils import get_xform
from bigassist_core.forms import RecommendationEditForm, DevelopmentPriorityForm
from bigassist_core.forms import CustomerCommunicationLogForm, RemoveUserForm, AddPOPForm, \
    AdminDeadlineForm, OrganisationForm, OrganisationPaymentForm, \
    AdminDatabaseFilterForm, \
    AdminBursaryHistoryForm, UpdateSupplierApprovalContactForm, \
    UpdateCustomerDiagnosticContactForm, SupplierOrganisationForm
from bigassist_core.xml import XMLManipulator
from bigassist_core.emails import send_ba_email
from bigassist_core.utils import get_detail_fieldname, PaginationMixin
from bigassist_core.security import CustomerDiagnosticSecurity
from bigassist_core.security import PaymentSecurity
from bigassist_core.security import SupplierApprovalSecurity
from bigassist_core.tasks import celery_make_pdf


import logging
logger = logging.getLogger(__name__)


class AdminDashboardViewMixin(object):
    tab_name = ''

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardViewMixin, self).get_context_data(**kwargs)
        context['tab'] = self.tab_name
        # old_customers = Organisation.objects.customers().exclude(customerdiagnostic__state__in=('in_progress', 'approved', 'ineligible')) \
        #                                             .exclude(customerdiagnostic__isnull=True) \
        #                                             .order_by('customerdiagnostic__state',
        #                                                       'customerdiagnostic__agreed',
        #                                                       'customerdiagnostic__submitted',
        #                                                       'customerdiagnostic__created')
        cust_query = Q(customerdiagnostic__state__in=(
                        'submitted',
                        'recommendation_started',
                        'recommendation_submitted',
                        'review_started',
                        'recommendation_force_submitted',
                        'recommendation_appealed',
                        'recommendation_revised'
                        ))
        cust_query = cust_query & Q(customerdiagnostic__next_diag__isnull=True)
        customers = Organisation.objects.customers() \
                        .filter(cust_query) \
                        .order_by('customerdiagnostic__state',
                                  'org__displayname') \
                        .distinct()


        query = Q(supplierapproval__state="submitted")
        query = query & Q(supplierapproval__next_appr__isnull=True)
        suppliers = Organisation.objects.suppliers() \
                           .filter(query) \
                           .order_by('supplierapproval__state',
                                     'supplierapproval__decision',
                                     'supplierapproval__assessed',
                                     'supplierapproval__submitted',
                                     'supplierapproval__created')

        context['inactive_delta'] = datetime.datetime.now() - relativedelta(weeks=2)
        context['customers'] = customers
        context['suppliers'] = suppliers
        context['total_customer'] = customers.count()
        context['total_supplier'] = suppliers.count()
        context['total_invoices'] = Payment.objects.filter(sent_to_finance__isnull=True).count()

        return context


@ROAdminRequiredDecorator
class AdminDashboardDatabaseView(AdminDashboardViewMixin, PaginationMixin, ListView, CSVRespondMixin):
    template_name = "khnpadmin/dashboard_database.html"
    tab_name = 'database'
    paginate_by = 10
    filename = "report.csv"
    csv_object_name = "data"

    def get_queryset(self):
        data = {}

        # Matous - changed from submit to org-type
        if 'text' in self.request.GET or 'org_type' in self.request.GET:
            self.form = AdminDatabaseFilterForm(data=self.request.GET)
            if self.form.is_valid():
                data = self.form.cleaned_data
        else:
            self.form = AdminDatabaseFilterForm()

        order_by = '-created'
        query = Q()
        org_type = ''

        if data:
            org_type = data['org_type']
            if org_type == 'supplier':
                query &= Q(prog__role='supplier')
            elif org_type == 'customer':
                query &= Q(prog__role='customer')
            elif org_type == 'visithost':
                query &= Q(visit__id__isnull=False)
            elif org_type == 'mentor':
                query &= Q(mentor__mentor_org__isnull=False)
            text = data['text'].strip()
            if text:
                query &= Q(org__displayname__icontains=text)
            order = data['order_by']
            if order == 'created':
                order_by = 'created'
            elif order == 'displayname':
                order_by = 'org__displayname'
            elif order == '-created':
                order_by = '-created'
            elif order == '-displayname':
                order_by = '-org__displayname'

        # order by 'pk' as second field because 'created' is just a 'date' so order of
        # organisations created at the same day is unspecified and it can show the same organisation
        # on different pages
        # Matous - changed the second to alphabetical with the same creation date
        result = Organisation.objects.filter(query).distinct().order_by(order_by, 'org__displayname')
        return result

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardDatabaseView, self).get_context_data(**kwargs)
        context['form'] = self.form
        context['get_params'] = self.request.GET
        context['reports'] = Report.objects.filter(enabled=True)
        report = self.request.GET.get('report')
        context['report_error'] = ''
        if report:
            report = get_object_or_404(Report, pk=report)
            try:
                columns, data = report.data()
                report_table = report.as_table(columns, data)
                context['report_result'] = report_table
                context['columns'] = columns
                context['data'] = [[val[fld] for fld in columns] for val in data]
            except DatabaseError, e:
                err_msg = 'Database error: %s' % str(e)
                context['report_error'] = err_msg
                context['columns'] = []
                context['data'] = [[err_msg, ], ]
        return context

    def header(self, context):
        return context['columns']

    def render_to_response(self, context, **response_kwargs):
        if 'download-report' in self.request.GET:
            return self.render_to_csv(context, **response_kwargs)

        return super(AdminDashboardDatabaseView, self).render_to_response(context, **response_kwargs)


@ROAdminRequiredDecorator
class AdminDashboardMeetspaceView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/dashboard_meetspace.html"
    tab_name = 'connectspace'

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardMeetspaceView, self).get_context_data(**kwargs)
        context['viments'] = []
        for visit in Visit.objects.filter(available__isnull=True):
            context['viments'].append(visit)
        for mentor in Mentor.objects.filter(available__isnull=True):
            context['viments'].append(mentor)
        return context


@ROAdminRequiredDecorator
class AdminDashboardBursaryView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/dashboard_bursary.html"
    tab_name = 'bursary'

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardBursaryView, self).get_context_data(**kwargs)
        rows = []
        for row in Visitor.objects.filter(bursary__isnull=True, amount__gt=0):
            diag = row.visitor_org.get_customer_diagnostic()
            if diag and diag.state != 'approved':
                diag = None
            # additional_info will be displayed in second row and contains
            # info about Visit host sponsorship. Refs. #188
            if row.visit.date:
                # visit has a fixed date
                if row.visit_instance and row.visit_instance.bursary:
                    host_amount = 0  # overwrite host_amount if another visitor for this instance has already been granted a bursary
                else:
                        host_amount = row.visit.get_host_sponsorship_amount()
            else:
                host_amount = row.visit.get_host_sponsorship_amount()
            total_amount = row.amount + host_amount
            rows.append((row.created, dict(pk=row.pk,
                                           applicant=row.visitor_org,
                                           name=row.visit.title,
                                           host=row.visit.host,
                                           date=row.created.strftime(settings.DATE_FORMAT_P),
                                           applicant_amount=row.amount,
                                           host_amount=host_amount,
                                           total_amount=total_amount,
                                           benefit=row.benefit,
                                           purpose=row.purpose,
                                           host_email=row.visit.get_contact_email(),
                                           visitor_email=row.get_contact_email(),
                                           has_diagnostic=not not diag,
                                           typ='visitor',
                                           key='visitor-%s' % row.pk,
                                           )))
        for row in Mentee.objects.filter(bursary__isnull=True, amount__gt=0):
            diag = row.mentee_org.get_customer_diagnostic()
            if diag and diag.state != 'approved':
                diag = None

            rows.append((row.created, dict(pk=row.pk,
                                           applicant=row.mentee_org,
                                           host="%s (%s)" % (row.mentor, row.mentor.mentor_org),
                                           date=row.created.strftime(settings.DATE_FORMAT_P),
                                           amount=row.amount,
                                           benefit=row.benefit,
                                           purpose=row.purpose,
                                           has_diagnostic=not not diag,
                                           bursary_rcmd=diag and diag.customerdevpri_set.filter(bursary_rcmd=True, topic__in=row.mentor.topics.all()).exists(),
                                           typ='mentee',
                                           key='mentee-%s' % row.pk,
                                           additional='',
                                           )))
        context['results'] = [x[1] for x in sorted(rows)]
        return context


@ROAdminRequiredDecorator
class AdminDashboardBursaryHistoryView(FormView):
    template_name = "khnpadmin/bursary_history.html"
    form_class = AdminBursaryHistoryForm

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardBursaryHistoryView,
                        self).get_context_data(**kwargs)

        if 'total_count' not in context:
            context.update(self.get_bursaries({'weeks': 1}))
        return context

    def get_bursaries(self, delta):
        mentees = Mentee.objects.filter(bursary=True)
        visitors = Visitor.objects.filter(bursary=True)
        hosts = VisitInstance.objects.filter(bursary=True)

        if delta is not None:
            now = datetime.datetime.now()
            dt_from = now - relativedelta(**delta)
            mentees = mentees.filter(granted__gte=dt_from)
            visitors = visitors.filter(granted__gte=dt_from)
            hosts = hosts.filter(granted__gte=dt_from)

        mentees = mentees.aggregate(sum=Sum('amount'), count=Count('amount'))
        visitors = visitors.aggregate(sum=Sum('amount'), count=Count('amount'))
        hosts = hosts.aggregate(sum=Sum('amount'), count=Count('amount'))

        total_sum = (mentees['sum'] or 0) + (visitors['sum'] or 0) + (hosts['sum'] or 0)
        total_count = (mentees['count'] or 0) + (visitors['count'] or 0) + (hosts['count'] or 0)
        return {'total_sum': total_sum, 'total_count': total_count}

    def form_valid(self, form):
        delta = form.cleaned_data['delta']
        burs = self.get_bursaries(delta)

        return self.render_to_response(self.get_context_data(form=form, **burs))


@ROAdminRequiredDecorator
class AdminDashboardCustomerView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/dashboard_customer.html"
    tab_name = 'customer'


@ROAdminRequiredDecorator
class AdminDashboardSupplierView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/dashboard_supplier.html"
    tab_name = 'supplier'

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardSupplierView, self).get_context_data(**kwargs)
        deadlines = SupplierDeadline.objects.active_deadlines()

        context['deadlines'] = deadlines
        context['last_deadline'] = SupplierDeadline.objects.last_deadline()
        context['next_deadline'] = SupplierDeadline.objects.next_deadline()
        context['last_approvals'] = context['last_deadline'] and context['last_deadline'].get_potential_approvals()
        context['next_approvals'] = context['last_deadline'] and context['last_deadline'].get_newer_approvals()

        if 'add_deadline_form' not in context:
            context['add_deadline_form'] = AdminDeadlineForm()
        return context

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        if 'add_deadline' in self.request.POST:
            form = AdminDeadlineForm(data=self.request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Deadline added")
                return HttpResponseRedirect(reverse('admin_dashboard_supplier'))

            return self.render_to_response(self.get_context_data(add_deadline_form=form))
        return HttpResponseBadRequest()


@ROAdminRequiredDecorator
class AdminSupplierReviewView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/supplier_review.html"
    tab_name = 'supplier'

    def get_context_data(self, **kwargs):
        context = super(AdminSupplierReviewView, self).get_context_data(**kwargs)
        context['supplier'] = get_object_or_404(Organisation, pk=int(self.kwargs['pk']))
        context['assessment'] = context['supplier'].get_supplier_diagnostic()
        if context['assessment']:
            context['locked'] = context['assessment'].locked(self.request.user)
        return context


@ROAdminRequiredDecorator
class AdminDashboardInvoicesView(AdminDashboardViewMixin, TemplateView):
    template_name = "khnpadmin/dashboard_invoices.html"
    tab_name = 'invoices'

    def get_context_data(self, **kwargs):
        context = super(AdminDashboardInvoicesView, self).get_context_data(**kwargs)
        context['payments'] = Payment.objects.filter(sent_to_finance__isnull=True)
        return context



@ROAdminRequiredDecorator
class AdminCustomerRecommendationView(DetailView):

    model = CustomerDiagnostic
    template_name = "khnpadmin/recommendation.html"
    context_object_name = "diag"

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        security = CustomerDiagnosticSecurity(self.get_object())
        if not security.check("view_recommendation"):
            return HttpResponseForbidden()

        return super(AdminCustomerRecommendationView, self).dispatch(
            request, *args, **kwargs)


@AdminRequiredDecorator
class AdminCustomerRecommendationEdit(UpdateView):

    model = CustomerDiagnostic
    template_name = "khnpadmin/recommendation_edit.html"
    context_object_name = "diag"
    form_class = RecommendationEditForm

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        security = CustomerDiagnosticSecurity(self.get_object())
        if not security.check("add_recommendation") and \
                not security.check("edit_recommendation") and \
                not security.check("revise_recommendation"):
            return HttpResponseForbidden()

        return super(AdminCustomerRecommendationEdit, self).dispatch(
            request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # It's here to stop form validation
        if 'cancel' in request.POST:
            messages.warning(request, "Edit cancelled")
            return HttpResponseRedirect(reverse('admin_dashboard_customer'))

        return super(AdminCustomerRecommendationEdit, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminCustomerRecommendationEdit,
                        self).get_context_data(**kwargs)

        if 'pri_forms' not in kwargs:
            context['pri_forms'] = self.get_forms()

        return context

    def get_initial(self):
        diag = self.get_object()

        initial = {}
        initial['support_topics'] = diag.customerdevpri_set.all()\
            .values_list('topic', flat=True)
        return initial

    def get_forms(self):
        diag = self.get_object()
        topics = SupportTopic.objects.all()
        pri_forms = {}
        for topic in topics:
            try:
                diag_pri = diag.customerdevpri_set.get(topic=topic)
            except CustomerDevPri.DoesNotExist:
                diag_pri = None

            kwargs = {
                'instance': diag_pri,
                'prefix': "pri_%s" % topic.pk,
            }

            if self.request.method == "POST":
                kwargs['data'] = self.request.POST
            else:
                kwargs['initial'] = {'topic': topic}

            topic_form = DevelopmentPriorityForm(**kwargs)
            pri_forms[topic] = topic_form

        return pri_forms

    @transaction.atomic
    def form_valid(self, form):
        support_topics = [SupportTopic.objects.get(pk=x) for x in form.cleaned_data['support_topics']]

        pri_forms = self.get_forms()
        filtered_pri_forms = [val for key, val in pri_forms.items()
                              if key in support_topics]

        # validate all
        validations = [x.is_valid() for x in filtered_pri_forms]
        if not all(validations):
            return self.render_to_response(self.get_context_data(form=form,
                                           pri_forms=pri_forms))

        diag = form.save(commit=False)

        if diag.assessor is None:
            diag.assessor = self.request.user

        if diag.state in ["submitted", "review_started"]:
            # In any case it should be at least started
            diag.set_state('recommendation_started', commit=False)

        diag.save()

        existing = diag.customerdevpri_set.all()
        removed_pri_forms = [x for x in existing if x.topic not in support_topics]
        for dev_pri in removed_pri_forms:
            dev_pri.delete()

        for pri_form in filtered_pri_forms:
            pri = pri_form.save(commit=False)
            pri.diag = diag
            pri.save()

        if 'save' in self.request.POST:
            return HttpResponseRedirect(reverse('admin_dashboard_customer'))
        elif 'save_preview' in self.request.POST:
            return HttpResponseRedirect(reverse('admin_customer_recommendation', args=(diag.pk,)))
        else:
            raise NotImplementedError


@AdminRequiredDecorator
class AdminCustomerRecommendationRevise(AdminCustomerRecommendationEdit):
    pass


@AdminRequiredDecorator
class AdminCustomerRecommendationApprove(ConfirmationView):

    description = "You are about to approve a customer’s recommendation."
    action = "Approve"
    layout = "bottom"
    success_url = reverse_lazy("home")

    @transaction.atomic
    def post(self, request, *args, **kwargs):

        diag = get_object_or_404(CustomerDiagnostic, pk=kwargs['pk'])

        diag.set_state("approved")
        # voucher_expires = datetime.datetime.now() + relativedelta(months=4)
        # send_ba_email('081_recommendation_accepted', {'voucher_expires': voucher_expires}, sender=None, recipients=user.get_contact())
        # send_ba_email('082_recommendation_accepted', {'company': user.get_company()}, sender=None, recipients=None)
        CustomerCommLog.objects.create(diag=diag,
                                       comm_type=CustomerCommType.objects.get(
                                                     pk=settings.KHNP_COMMLOG_RECOMMENDATION_ACCEPTED_ID),
                                       details='Admin approved recommendation for customer')
        msg = "Recommendation for customer has been approved"
        messages.success(request, msg)

        return self.render_success(request)


@AdminRequiredDecorator
class AdminCustomerRecommendationReject(ConfirmationView):

    description = "You are about to reject a customer’s recommendation."
    action = "Reject"
    layout = "bottom"
    success_url = reverse_lazy("home")

    @transaction.atomic
    def post(self, request, *args, **kwargs):

        diag = get_object_or_404(CustomerDiagnostic, pk=kwargs['pk'])

        diag.set_state("recommendation_rejected")
        msg = "Recommendation for customer has been rejected"
        CustomerCommLog.objects.create(diag=diag,
                                       comm_type=CustomerCommType.objects.get(
                                                     pk=settings.KHNP_COMMLOG_RECOMMENDATION_REJECTED_ID),
                                       details='Admin rejected recommendation for customer')
        messages.success(request, msg)
        return self.render_success(request)


@AdminRequiredDecorator
class AdminCustomerRecommendationSend(ConfirmationDetailView):

    model = CustomerDiagnostic
    template_name = "confirmation.html"
    context_object_name = "diag"

    # If True it is sent without consent - to be set in urls
    force = False
    description = ''
    action = "Send"
    success_url = reverse_lazy("admin_dashboard_customer")

    def post(self, request, *args, **kwargs):
        diag = self.object = self.get_object()

        if self.force:
            permission = "force_send_recommendation"
            state = "recommendation_force_submitted"
        else:
            permission = "send_recommendation"
            state = "recommendation_submitted"

        security = CustomerDiagnosticSecurity(diag)
        error = security.error(permission)
        if error:
            msg = "Cannot send to organisation: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        diag.set_state(state)
        send_ba_email('07_recommendation_sent', {}, sender=None, recipients=diag.cust, recipient_role='customer')
        # also log as a communication
        CustomerCommLog.objects.create(diag=diag,
                                       comm_type=CustomerCommType.objects.get(pk=settings.KHNP_COMMLOG_RECOMMENDATION_SENT_ID),
                                       details='')
        msg = "Recommendation has been sent to the organisation"
        messages.success(request, msg)

        return self.render_success(request)


@AdminRequiredDecorator
class AdminCustomerRecommendationRevisedSend(ConfirmationDetailView):

    model = CustomerDiagnostic
    template_name = "confirmation.html"
    context_object_name = "diag"

    # If True it is sent without consent - to be set in urls
    force = False
    description = "You are about to submit the revised recommendation to the customer?"
    action = "Send revised recommendation"
    success_url = reverse_lazy("admin_dashboard_customer")

    def post(self, request, *args, **kwargs):
        diag = self.object = self.get_object()

        permission = "send_recommendation"
        state = "recommendation_revised"

        security = CustomerDiagnosticSecurity(diag)
        error = security.error(permission)
        if error:
            msg = "Cannot send to organisation: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        diag.set_state(state)
        send_ba_email('52_customer_receives_revised_recommendation', {}, sender=None, recipients=diag.cust)
        # also log as a communication
        CustomerCommLog.objects.create(diag=diag,
                                       comm_type=CustomerCommType.objects.get(pk=settings.KHNP_COMMLOG_RECOMMENDATION_SENT_ID),
                                       details='Revised recommendation sent to customer')
        msg = "Revised ecommendation has been sent to the organisation"
        messages.success(request, msg)

        return self.render_success(request)



@AdminRequiredDecorator
class AdminCustomerMakeIneligibleView(ConfirmationDetailView):
    '''
    View to make customer/diagnostic ineligible
    '''

    model = CustomerDiagnostic
    template_name = "confirmation.html"
    context_object_name = "diag"

    # If True it is sent without consent - to be set in urls
    # force = False
    description = ''
    action = "Yes, make customer ineligible"
    # success_url = reverse_lazy("admin_dashboard_customer")
    success_url = reverse_lazy("admin_dashboard_customer")

    def post(self, request, *args, **kwargs):
        diag = self.object = self.get_object()

        # permission = "make_ineligible" #from security.py, checks if application is not submitter and not review_started
        state = "ineligible"

        # security = CustomerDiagnosticSecurity(diag)
        # error = security.error(permission)
        # if error:
        #     msg = "You cannot make this customer ineligible: %s" % error
        #     messages.error(request, msg)
        #     return self.render_to_response(self.get_context_data())

        diag.set_state(state)

        # SEB fix -- MAke new email template
        send_ba_email('061_customer_ineligible', {}, sender=None, recipients=diag.cust, recipient_role='customer')
        # also log as a communication - make new communication type and log
        CustomerCommLog.objects.create(diag=diag,
                                       comm_type=CustomerCommType.objects.get(pk=settings.KHNP_COMMLOG_MADE_INELIGIBLE_ID),
                                       details='')
        msg = "Organisation has been notified of their status as ineligible."
        messages.success(request, msg)

        return self.render_success(request)


@ROAdminRequiredDecorator
class AdminCustomerCommLogListView(ListView):
    model = CustomerCommLog
    template_name = "khnpadmin/commlog_list.html"
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.company = get_object_or_404(Organisation, pk=self.kwargs['organisation_id'])
        return super(AdminCustomerCommLogListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(AdminCustomerCommLogListView, self).get_context_data(*args, **kwargs)
        context['customer'] = self.company
        return context

    def get_queryset(self):
        diag = self.company.get_customer_diagnostic()
        if diag is None:
            return []
        return CustomerCommLog.objects.filter(diag=diag)


@ROAdminRequiredDecorator
class AdminCustomerDiagnosticView(TemplateView):

    editable = False

    def dispatch(self, request, *args, **kwargs):
        customer_pk = int(kwargs.get('pk'))
        company = get_object_or_404(Organisation, pk=customer_pk)

        diag = company.get_customer_diagnostic()
        security = CustomerDiagnosticSecurity(diag)

        # An admin can edit the xform from the profile page - using 'C'
        # if from review than using 'CA'
        def is_not_readonly(my_security):
            return my_security and request.user.has_perm('bigassist_core.can_modify_admin')

        if self.editable:
            form_type = 'C'
            if is_not_readonly(security.can("change_by_admin")):
                xml = get_xform('C_customer_self_assessment_source.html')
            else:
                xml = get_xform('C_customer_self_assessment_readonly.html')
        elif diag.resubmission():
            form_type = 'CRA'
            if is_not_readonly(security.can("edit_comments")):
                xml = get_xform('CRA_customer_admin_source_resubmission.html')
            else:
                xml = get_xform('CRA_customer_admin_readonly_resubmission.html')
        else:
            form_type = 'CA'
            if is_not_readonly(security.can("edit_comments")):
                xml = get_xform('CA_customer_admin_source.html')
            else:
                xml = get_xform('CA_customer_admin_readonly.html')



        # manipulate submit action and model URL of the form
        manip = XMLManipulator(xml)
        manip.set_xfinstance('data-set', reverse('admin_customer_diagnostic_xml_model', args=[company.pk]))
        manip.set_xfinstance('old-data-set', reverse('admin_customer_diagnostic_xml_prev_model', args=[company.pk]))
        manip.set_xfinstance('question-set', reverse('xforms_model', args=['C', '1']))  # custom_1 field of Diagnostic form
        manip.set_xfinstance('comments-set', reverse('admin_customer_diagnostic_comments_model', args=[company.pk]))
        # There is no difference between Save and Submit for admin.
        if form_type == 'C':
            manip.set_xfsubmission('submit', reverse('admin_customer_diagnostic_submit', args=[company.pk]))
            manip.set_xfsubmission('finalsubmit', reverse('admin_customer_diagnostic_submit', args=[company.pk]))
        else:
            manip.set_xfsubmission('submit', reverse('admin_customer_diagnostic_submit_comments', args=[company.pk]))
            manip.set_xfsubmission('finalsubmit', reverse('admin_customer_diagnostic_submit_comments', args=[company.pk]))
        xml = manip.tostring()
        del manip

        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        return HttpResponse(xml, content_type='text/xml')


@ROAdminRequiredDecorator
class AdminCustomerDiagnosticModelView(View):

    def get(self, request, *args, **kwargs):
        customer_pk = int(kwargs.get('pk'))
        company = get_object_or_404(Organisation, pk=customer_pk)

        diag = company.get_customer_diagnostic()

        if kwargs.get('diag_id'):
            diag_id = int(kwargs.get('diag_id'))
            diag = CustomerDiagnostic.objects.get(pk=diag_id)
            model = diag.diag_questions
        else:
            model = diag.diag_questions

        if isinstance(model, unicode):
            model = model.encode('utf-8')

        if model:
            xml = XMLManipulator(model, remove_blank_text=True)
            xml.insert_organisation_info(company, diag)
            # xml.insert_organisation_info(company)
            xml.whitespace_cleanup()
            model = xml.tostring()

        return HttpResponse(model, content_type='text/xml')


@ROAdminRequiredDecorator
class AdminCustomerDiagnosticPrevModelView(View):

    def get(self, request, *args, **kwargs):
        customer_pk = int(kwargs.get('pk'))
        company = get_object_or_404(Organisation, pk=customer_pk)

        diagnostic = company.get_customer_diagnostic()
        model = diagnostic.prev_diag.diag_questions


        if isinstance(model, unicode):
            model = model.encode('utf-8')

        if model:
            xml = XMLManipulator(model, remove_blank_text=True)
            xml.insert_organisation_info(company, diagnostic.prev_diag)
            xml.whitespace_cleanup()
            model = xml.tostring()

        return HttpResponse(model, content_type='text/xml')


class AdminCustomerDiagnosticCommentsModelView(DetailView):
    """ Comments provided by the admin """
    model = Organisation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.is_authenticated():
            return super(AdminCustomerDiagnosticCommentsModelView, self).dispatch(request, *args, **kwargs)
        else:
            token = request.GET.get('t')
            if check_token(token, self.get_object()):
                return super(AdminCustomerDiagnosticCommentsModelView, self).dispatch(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

    def get(self, request, *args, **kwargs):
        company = self.get_object()
        if 'customer' not in company.get_roles():
            return HttpResponseForbidden("The requested company is not a customer.")

        diag = company.get_customer_diagnostic()
        model = diag.assessor_comments

        if not model:
            # retrieve the comments model from XForms table
            # https://fry-it.basecamphq.com/projects/9419045-text-matters/posts/68390121/comments#comment_207832746
            model = get_xform('CA_customer_admin_model.xml')


        # insert organisation info - issue #054
        if model:
            manip = XMLManipulator(model)
            manip.insert_organisation_info(company, diag)
            manip.whitespace_cleanup()
            model = manip.tostring()
            del manip

        if isinstance(model, unicode):
            model = model.encode('utf-8')
        return HttpResponse(model, content_type='text/xml')


@AdminRequiredDecorator
class AdminCustomerDiagnosticSubmitView(View):

    comments = False

    def post(self, request, *args, **kwargs):
        xml = request.body
        try:
            etree.XML(xml)
        except etree.XMLSyntaxError:
            return HttpResponseBadRequest('Invalid XML content')

        customer_pk = int(kwargs.get('pk'))
        company = get_object_or_404(Organisation, pk=customer_pk)
        diag = company.get_customer_diagnostic()

        security = CustomerDiagnosticSecurity(diag)
        if not security.can("change_by_admin"):
            return HttpResponseForbidden()

        if not request.user.has_perm('bigassist_core.can_modify_admin'):
            return HttpResponseForbidden()

        if self.comments:
            diag.assessor_comments = xml
        else:
            diag.diag_questions = xml
        diag.save()

        return HttpResponseRedirect(reverse('admin_organisation_profile', args=[company.pk]))


@AdminRequiredDecorator
class AdminCustomerCommunicationView(FormView):
    form_class = CustomerCommunicationLogForm
    template_name = "khnpadmin/communication_log.html"

    def dispatch(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            diagnostic = get_object_or_404(CustomerDiagnostic, pk=int(kwargs.get('diag_id')))
            return HttpResponseRedirect(reverse('admin_dashboard_customer') + '?org=%s' % diagnostic.pk)

        return super(AdminCustomerCommunicationView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        submit = 'submit' in self.request.POST
        if submit:
            diag = form.cleaned_data['diagnostic']
            CustomerCommLog.objects.create(diag=diag,
                                           comm_type=form.cleaned_data['communication_type'],
                                           details=form.cleaned_data['details'])
            pk = form.cleaned_data['organisation']
            if diag.assessor is None:
                diag.assessor = self.request.user
            if diag.state == 'submitted':
                diag.set_state('review_started', commit=False)

            diag.save()

            messages.info(self.request, 'The communication log has been added.')
        return HttpResponseRedirect(reverse('admin_dashboard_customer') + '?org=%s' % pk)

    def get_form_kwargs(self):
        kwargs = super(AdminCustomerCommunicationView, self).get_form_kwargs()
        # copy data from initial to 'data' because the form requires these
        if 'data' in kwargs:
            kwargs['data'] = kwargs['data'].copy()
            kwargs['data']['organisation'] = kwargs['initial']['organisation'].pk
            kwargs['data']['diagnostic'] = kwargs['initial']['diagnostic'].pk
        return kwargs

    def get_initial(self):
        diagnostic = get_object_or_404(CustomerDiagnostic, pk=int(self.kwargs.get('diag_id')))

        initial = {}
        initial['organisation'] = diagnostic.cust
        # diagnostic is a hidden field which holds CustomerDiagnostic ID
        initial['diagnostic'] = diagnostic
        return initial

    def get_context_data(self, **kwargs):
        context = super(AdminCustomerCommunicationView,
                        self).get_context_data(**kwargs)
        context['diag_id'] = self.kwargs.get('diag_id')
        return context


# @StaffRequiredDecorator
# class AdminRemoveUserView(FormView):
#     template_name = "khnpadmin/remove_user.html"
#     form_class = RemoveUserForm

#     def what_to_remove(self, email):
#         result = []
#         for identity in Identity.objects.filter(persona__iexact=email).all():
#             result.append(dict(
#                 table=identity._meta.db_table,
#                 to_remove=identity,
#                 pk=identity.pk,
#                 description=unicode(identity),
#             ))
#             person = identity.person
#             result.append(dict(
#                 table=person._meta.db_table,
#                 to_remove=person,
#                 pk=person.pk,
#                 description=unicode(person),
#             ))
#             for contact in person.contact_set.all():
#                 result.append(dict(
#                     table=contact._meta.db_table,
#                     to_remove=contact,
#                     pk=contact.pk,
#                     description=unicode(contact),
#                 ))

#             query = """SELECT DISTINCT o.* FROM organisation o
#                        LEFT JOIN person po ON o.org_id = po.id
#                        LEFT JOIN relationship r ON r.object = po.id
#                        LEFT JOIN person pp ON pp.id = r.subject
#                        WHERE pp.id = %s AND r.type IN (5,6)"""
#             orgs = Organisation.objects.raw(query, [person.pk, ])

#             for rel in person.rel_subject_set.all():
#                 result.append(dict(
#                     table=rel._meta.db_table,
#                     to_remove=rel,
#                     pk=rel.pk,
#                     description=unicode(rel),
#                 ))
#             for rel in person.rel_object_set.all():
#                 result.append(dict(
#                     table=rel._meta.db_table,
#                     to_remove=rel,
#                     pk=rel.pk,
#                     description=unicode(rel),
#                 ))
#             for org in orgs:
#                 result.append(dict(
#                     table=org._meta.db_table,
#                     to_remove=org,
#                     pk=org.pk,
#                     description=unicode(org),
#                 ))
#                 for contact in org.org.contact_set.all():
#                     result.append(dict(
#                         table=contact._meta.db_table,
#                         to_remove=contact,
#                         pk=contact.pk,
#                         description=unicode(contact),
#                     ))
#                     # remove all cm_feedback records. Refs. https://it.fry-it.com/text-matters/dev/024
#                     for feedback in contact.feedbacks.all():
#                         result.append(dict(
#                             table=feedback._meta.db_table,
#                             to_remove=feedback,
#                             pk=feedback.pk,
#                             description=unicode(feedback),
#                         ))
#                 for diag in CustomerPrequal.objects.filter(cust=org):
#                     result.append(dict(
#                         table=diag._meta.db_table,
#                         to_remove=diag,
#                         pk=diag.pk,
#                         description=unicode(diag),
#                     ))
#                 for diag in CustomerDiagnostic.objects.filter(cust=org):
#                     result.append(dict(
#                         table=diag._meta.db_table,
#                         to_remove=diag,
#                         pk=diag.pk,
#                         description=unicode(diag),
#                     ))
#                     for log in CustomerCommLog.objects.filter(diag=diag):
#                         result.append(dict(
#                             table=log._meta.db_table,
#                             to_remove=log,
#                             pk=log.pk,
#                             description=unicode(log),
#                         ))
#                     for devpri in CustomerDevPri.objects.filter(diag=diag):
#                         result.append(dict(
#                             table=devpri._meta.db_table,
#                             to_remove=devpri,
#                             pk=devpri.pk,
#                             description=unicode(devpri),
#                         ))
#                     for voucher in CustomerVoucher.objects.filter(cust=org):
#                         result.append(dict(
#                             table=voucher._meta.db_table,
#                             to_remove=voucher,
#                             pk=voucher.pk,
#                             description=unicode(voucher),
#                         ))
#                 for diag in SupplierApproval.objects.filter(suppl=org):
#                     result.append(dict(
#                         table=diag._meta.db_table,
#                         to_remove=diag,
#                         pk=diag.pk,
#                         description=unicode(diag),
#                     ))

#         return result

#     @transaction.atomic
#     def form_valid(self, form):
#         data = form.cleaned_data
#         email = data['email']
#         remove = self.request.POST.get('remove') == '1'
#         if remove:
#             to_remove = self.what_to_remove(email)
#             to_remove.reverse()
#             for row in to_remove:
#                 row['to_remove'].delete()
#             messages.info(self.request, 'User %s has been completely removed' % email)
#             return HttpResponseRedirect(reverse('admin_remove_user'))
#         else:
#             return HttpResponseRedirect(reverse('admin_remove_user') + '?email=%s' % email)

#     def get_context_data(self, **kwargs):
#         context = super(AdminRemoveUserView,
#                         self).get_context_data(**kwargs)
#         email = self.request.GET.get('email')
#         if email:
#             context['email'] = email
#             context['to_remove'] = self.what_to_remove(email)
#         return context


@ROAdminRequiredDecorator
class AdminSupplierApprovalView(View):

    editable = False

    def get(self, request, *args, **kwargs):
        supplier_pk = kwargs.get('pk')
        company = get_object_or_404(Organisation, pk=supplier_pk)
        diag = company.get_supplier_diagnostic()
        security = SupplierApprovalSecurity(diag)

        if self.editable:
            readonly = not security.can("change_by_admin")
        else:
            readonly = ('readonly' in request.GET) or not security.can("change_by_admin")

        # last break
        readonly = readonly and request.user.has_perm('bigassist_core.can_modify_admin')

        if readonly:
            xml = get_xform('S_supplier_assessment_readonly.html')
            # if diag.resubmission():
            #     xml = get_xform('SRA_supplier_admin_readonly_reapplication.html')
            # else:
            #     xml = get_xform('SA_supplier_admin_readonly.html')
        else:
            if diag.resubmission():
                xml = get_xform('S_supplier_assessment_reapplication.html')
            else:
                xml = get_xform('S_supplier_assessment_source.html')

        # manipulate submit action and model URL of the form
        manip = XMLManipulator(xml)
        manip.set_xfinstance('data-set', reverse('admin_supplier_approval_xml_model', args=[company.pk]))
        manip.set_xfinstance('old-data-set', reverse('admin_supplier_approval_xml_prevmodel', args=[company.pk]))
        # https://it.fry-it.com/text-matters/dev/135/#i1
        manip.set_xfinstance('compare-sets', reverse('xforms_model', args=['S', 'model']))
        manip.set_xfsubmission('submit', reverse('admin_supplier_approval_submit', args=[company.pk]))
        manip.set_xfsubmission('finalsubmit', reverse('admin_supplier_approval_submit', args=[company.pk]))
        if readonly:
            manip.set_iframe('file-download', reverse('xforms_filedownload', args=['S', 'ac', diag.pk]))
        else:
            manip.set_iframe('file-upload', reverse('xforms_fileupload', args=['S', 'ac', diag.pk]))

        xml = manip.tostring()
        del manip

        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        return HttpResponse(xml, content_type='text/xml')


@ROAdminRequiredDecorator
class AdminSupplierApprovalCommentsView(View):

    def get(self, request, *args, **kwargs):
        supplier_pk = kwargs.get('pk')
        company = get_object_or_404(Organisation, pk=supplier_pk)
        diag = company.get_supplier_diagnostic()
        if diag is None:
            messages.warning(request, 'This supplier has no approval yet.')
            return HttpResponseRedirect(reverse('home'))

        if diag.locked(request.user):
            return HttpResponseRedirect(reverse('admin_supplier_approval_review', args=[company.pk]))

        readonly = diag.state != 'submitted'

        readonly = readonly and request.user.has_perm('bigassist_core.can_modify_admin')

        if diag.resubmission():
            # Refs. https://it.fry-it.com/text-matters/dev/135
            if readonly:
                xml = get_xform('SRA_supplier_admin_readonly_reapplication.html')
            else:
                xml = get_xform('SRA_supplier_admin_source_reapplication.html')
                diag.lock(request.user)
        else:
            if readonly:
                xml = get_xform('SA_supplier_admin_readonly.html')
            else:
                diag.lock(request.user)
                xml = get_xform('SA_supplier_admin_source.html')



        # manipulate submit action and model URL of the form
        manip = XMLManipulator(xml)
        manip.set_xfinstance('data-set', reverse('admin_supplier_approval_xml_model', args=[company.pk]))
        manip.set_xfinstance('old-data-set', reverse('admin_supplier_approval_xml_prevmodel', args=[company.pk]))
        # https://it.fry-it.com/text-matters/dev/135/#i1
        manip.set_xfinstance('compare-sets', reverse('xforms_model', args=['S', 'model']))
        manip.set_xfinstance('comments-set', reverse('admin_supplier_approval_comments_model', args=[company.pk]))

        # There is no difference between Save and Submit for admin.
        manip.set_xfsubmission('submit', reverse('admin_supplier_approval_submit_comments', args=[company.pk]))
        manip.set_xfsubmission('finalsubmit', reverse('admin_supplier_approval_submit_comments', args=[company.pk]))
        manip.set_iframe('file-download', reverse('xforms_filedownload', args=['S', 'ac', diag.pk]))

        xml = manip.tostring()
        del manip

        if isinstance(xml, unicode):
            xml = xml.encode('utf-8')
        return HttpResponse(xml, content_type='text/xml')


class AdminSupplierApprovalModelView(DetailView):
    """ Data provided by the supplier """
    model = Organisation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.has_perm('bigassist_core.can_view_admin'):
            return super(AdminSupplierApprovalModelView, self).dispatch(request, *args, **kwargs)
        else:
            token = request.GET.get('t')
            if check_token(token, self.get_object()):
                return super(AdminSupplierApprovalModelView, self).dispatch(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

    def get(self, request, *args, **kwargs):
        company = self.get_object()
        if 'supplier' not in company.get_roles():
            return HttpResponseForbidden("The requested company is not a supplier.")

        diag = company.get_supplier_diagnostic()
        model = diag.supplier_questions

        if isinstance(model, unicode):
            model = model.encode('utf-8')

        if model:
            xml = XMLManipulator(model, remove_blank_text=True)
            xml.insert_organisation_info(company, diag)
            xml.whitespace_cleanup()
            model = xml.tostring()

        return HttpResponse(model, content_type='text/xml')


class AdminSupplierApprovalPrevModelView(DetailView):
    """ Data provided by the supplier """
    model = Organisation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.has_perm('bigassist_core.can_view_admin'):
            return super(AdminSupplierApprovalPrevModelView, self).dispatch(request, *args, **kwargs)
        else:
            token = request.GET.get('t')
            if check_token(token, self.get_object()):
                return super(AdminSupplierApprovalPrevModelView, self).dispatch(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

    def get(self, request, *args, **kwargs):
        company = self.get_object()
        if 'supplier' not in company.get_roles():
            return HttpResponseForbidden("The requested company is not a supplier.")

        diag = company.get_supplier_diagnostic()
        if diag and diag.prev_appr:
            model = diag.prev_appr.supplier_questions
        else:
            return HttpResponse('Invalid data', content_type='text/plain')

        if isinstance(model, unicode):
            model = model.encode('utf-8')
        return HttpResponse(model, content_type='text/xml')


class AdminSupplierApprovalCommentsModelView(DetailView):
    """ Comments provided by the admin """
    model = Organisation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.is_authenticated():
            return super(AdminSupplierApprovalCommentsModelView, self).dispatch(request, *args, **kwargs)
        else:
            token = request.GET.get('t')
            if check_token(token, self.get_object()):
                return super(AdminSupplierApprovalCommentsModelView, self).dispatch(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

    def get(self, request, *args, **kwargs):
        company = self.get_object()
        if not company.is_supplier:
            return HttpResponseForbidden("The requested company is not a supplier.")

        diag = company.get_supplier_diagnostic()
        model = diag.assessor_comments
        if not model:
            # retrieve the comments model from XForms table
            # https://fry-it.basecamphq.com/projects/9419045-text-matters/posts/68390121/comments#comment_207832746
            model = get_xform('SA_supplier_admin_model.xml')

        # insert organisation info - issue #054
        if model:
            manip = XMLManipulator(model)
            manip.insert_organisation_info(company, diag)
            manip.whitespace_cleanup()
            model = manip.tostring()
            del manip

        if isinstance(model, unicode):
            model = model.encode('utf-8')
        return HttpResponse(model, content_type='text/xml')


@AdminRequiredDecorator
class AdminSupplierApprovalSubmitView(View):
    comments = False

    def post(self, request, *args, **kwargs):
        xml = request.body
        try:
            etree.XML(xml)
        except etree.XMLSyntaxError:
            return HttpResponseBadRequest('Invalid XML content')

        pk = kwargs.get('pk')
        company = get_object_or_404(Organisation, pk=pk)
        diag = company.get_supplier_diagnostic()

        if diag.locked(request.user):
            return HttpResponseBadRequest('This approval is locked by a different user')

        security = SupplierApprovalSecurity(diag)
        if not security.can("change_by_admin"):
            return HttpResponseForbidden()

        if self.comments:
            diag.assessor_comments = xml
        else:
            diag.supplier_questions = xml

        diag.save()
        diag.unlock()

        if self.comments:
            return HttpResponseRedirect(reverse('admin_supplier_approval_review', args=[pk]))
        else:
            return HttpResponseRedirect(reverse('admin_organisation_profile', args=[company.pk]))


@AdminRequiredDecorator
class AdminSupplierApprovalReviewView(ConfirmationDetailView):

    model = Organisation
    template_name = "confirmation.html"
    context_object_name = "assessment"

    description = "Do you really want to mark the assessment as reviewed?"
    action = "Mark as reviewed"

    def get_success_url(self):
        return reverse('admin_supplier_approval_review', args=[self.object.pk])

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        diag = self.object.get_supplier_diagnostic()
        security = SupplierApprovalSecurity(diag)
        error = security.error('can_review_approval')
        if not error:
            diag.set_state("reviewed")
            messages.success(request, "The assessment has been marked as reviewed.")
        else:
            messages.error(request, error)

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@AdminRequiredDecorator
class AdminSupplierApprovalUnlockView(ConfirmationDetailView):

    model = SupplierApproval
    template_name = "confirmation.html"
    context_object_name = "assessment"

    # If True it is sent without consent - to be set in urls
    force = False

    description = "Do you really want to force unlock the assessment?"
    action = "Force unlock"

    def get_success_url(self):
        return reverse("admin_supplier_approval_review", args=[self.object.suppl.pk])

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.unlock()
        messages.success(request, "The assessment has been unlocked.")
        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@ROAdminRequiredDecorator
class AdminOrganisationProfileView(DetailView):
    model = Organisation
    template_name = 'khnpadmin/organisation_profile.html'

    def get_context_data(self, **kwargs):
        context = super(AdminOrganisationProfileView, self).get_context_data(**kwargs)
        obj = self.get_object()
        context['vouchers'] = CustomerVoucher.objects.filter(cust=obj).order_by('pk')
        context['projects'] = CustomerProject.objects.current().filter(cust=obj).order_by('pk')

        context['supplier_appr'] = obj.get_supplier_diagnostic()
        context['customer_diag'] = obj.get_customer_diagnostic()

        members = list()
        context['members'] = list()
        for member in obj.get_members():
            pk = member.pk
            email = member.get_contact_email()
            if pk in members:
                continue
            context['members'].append((member.displayname, email))

        for record in InvitedUser.objects.filter(org=obj, registered__isnull=True):
            context['members'].append(('--- waiting ---', record.email))

        return context


@AdminRequiredDecorator
class AdminAddPOPView(UpdateView):

    model = Payment
    template_name = "khnpadmin/add_pop.html"
    form_class = AddPOPForm
    success_url = reverse_lazy('admin_dashboard_invoices')

    def form_valid(self, form):
        form.save()

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@AdminRequiredDecorator
class AdminSendToFinanceView(ConfirmationDetailView):

    model = Payment
    description = "Send this invoice to finance"
    action = "Send"
    success_url = reverse_lazy('admin_dashboard_invoices')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        security = PaymentSecurity(self.object)
        error = security.error("send_to_finance")
        if error:
            msg = "Cannot send to finance: %s" % error
            messages.error(request, msg)
            return self.render_to_response(self.get_context_data())

        self.object.sent_to_finance = datetime.datetime.now()
        self.object.save()

        send_ba_email('34_send_to_finance', {'payment': self.object}, sender=None, recipients=settings.FINANCE_EMAIL)

        recipients = set()
        for member in self.object.payee_org.get_members(only_managers=True):
            contact = member.get_contact_email()
            if contact:
                recipients.add(contact)

        send_ba_email('341_send_to_finance_supplier', {'payment': self.object}, sender=None, recipients=recipients)

        msg = "Invoice has been sent to finance"
        messages.success(request, msg)

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@AdminRequiredDecorator
class AdminApproveRejectVisitView(ConfirmationDetailView):
    approve = True
    model = Visit
    success_url = reverse_lazy('admin_dashboard_meetspace')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.available = self.approve
        self.object.save()

        if self.approve:
            msg = "The visit has been approved"
        else:
            send_ba_email('27_customer_rejected_as_host', {}, recipients=self.object.host, recipient_role='customer')
            msg = "The visit has been rejected"
        messages.success(request, msg)

        return self.render_success(request)


@AdminRequiredDecorator
class AdminApproveRejectMentorView(ConfirmationDetailView):
    approve = True
    model = Mentor
    success_url = reverse_lazy('admin_dashboard_meetspace')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.available = self.approve
        self.object.save()

        if self.approve:
            msg = "The mentor has been approved"
        else:
            send_ba_email('29_customer_rejected_as_mentor', {}, recipients=self.object.mentor_org, recipient_role='customer')
            msg = "The mentor has been rejected"
        messages.success(request, msg)
        return self.render_success(request)


@ROAdminRequiredDecorator
class AdminSupplierApprovalContactEditView(SuccessMessageMixin, UpdateView):
    model = SupplierApproval
    form_class = UpdateSupplierApprovalContactForm
    diag_type = 'supplier'

    success_message = 'Supplier approval contact details updated'

    template_name = 'khnpadmin/diagnostic_contact_edit.html'

    def get_success_url(self):
        org_pk = self.kwargs.get('org_pk')
        return reverse('admin_organisation_profile', kwargs={
            'pk': int(org_pk)
            })

    def get_context_data(self, **kwargs):
        context = super(AdminSupplierApprovalContactEditView, self).get_context_data(**kwargs)
        context['diag_type'] = self.diag_type
        return context

    def form_valid(self, form):
        form.save()

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")
        return HttpResponseRedirect(url)


@ROAdminRequiredDecorator
class AdminCustomerDiagnosticContactEditView(AdminSupplierApprovalContactEditView):
    model = CustomerDiagnostic
    form_class = UpdateCustomerDiagnosticContactForm
    diag_type = 'customer'

    success_message = 'Customer diagnostic contact details updated'


@AdminRequiredDecorator
class AdminOrganisationEditView(FormView):

    template_name = 'khnpadmin/organisation_edit.html'
    # form_class - see get_form_class mehod

    def dispatch(self, request, *args, **kwargs):
        self.org = get_object_or_404(Organisation, pk=kwargs['pk'])
        # Commented out below: is this necessary?
        # Why do we need the diag here, and if org is both cust and suppl
        # which should we pick?
        # self.diag = self.org.get_diagnostic()
        self.customer_diag = self.org.get_customer_diagnostic()
        self.supplier_appr = self.org.get_supplier_diagnostic()

        return super(AdminOrganisationEditView, self).dispatch(
            request, *args, **kwargs)

    def show_payment(self):
        return 'supplier' in self.org.get_roles()

    def get_success_url(self):
        return reverse('admin_organisation_edit', args=[self.org.pk])

    def get_form_class(self):
        if self.show_payment():
            form_class = OrganisationPaymentForm
        else:
            if self.supplier_appr:
                form_class = SupplierOrganisationForm
            else:
                form_class = OrganisationForm
        return form_class

    def get_form(self, form_class):
        form = form_class(**self.get_form_kwargs())
        if self.show_payment():
            form['bank_name'].field.required = False
            form['bank_ac_name'].field.required = False
            form['bank_ac_no'].field.required = False
            form['bank_sortcode'].field.required = False
        return form

    def get_initial(self):
        initial = {}

        # organisation
        org = self.org
        initial['org_type'] = org.org_type and org.org_type.pk
        initial['year_founded'] = org.year_founded
        # org_type can be None (old records)
        detail_field = initial['org_type'] and get_detail_fieldname(org.org_type.org_type)
        if detail_field:
            initial[detail_field] = org.detail

        if self.show_payment():
            initial['bank_name'] = org.bank_name
            initial['bank_ac_name'] = org.bank_ac_name
            initial['bank_ac_no'] = org.bank_ac_no
            initial['bank_sortcode'] = org.bank_sortcode

        # company
        company = org.org
        initial['displayname'] = company.displayname
        initial['twitter'] = company.get_contact('twitter')
        initial['linkedin'] = company.get_contact('linkedin')
        # initial['maincontactname'] = company.maincontactname
        # initial['maincontacttitle'] = company.maincontacttitle
        initial.update(company.get_additional())

        return initial

    def get_context_data(self, **kwargs):
        context = super(AdminOrganisationEditView, self).get_context_data(**kwargs)
        context['show_payment'] = self.show_payment()
        context['org_pk'] = self.org.pk
        context['organisation_type_str'] = unicode(self.org.org_type)
        context['is_supplier'] = self.supplier_appr or False

        detail = self.org.get_detail()
        if detail:
            context['organisation_type_str'] += u' / ' + unicode(detail)
        return context

    @transaction.atomic
    def form_valid(self, form):
        data = form.cleaned_data

        org = self.org
        company = org.org

        org.org_type = data['org_type']
        org.detail = data['detail']

        if self.show_payment():
            org.bank_name = data['bank_name']
            org.bank_ac_name = data['bank_ac_name']
            org.bank_ac_no = data['bank_ac_no']
            org.bank_sortcode = data['bank_sortcode']

        if self.supplier_appr:
            org.year_founded = data['year_founded']

        # It does not matter if we send in more data as it picks only needed
        company.set_additional(data['company'])
        if data['displayname']:
            company.displayname = data['displayname']
        # company.maincontactname = data['maincontactname']
        # company.maincontacttitle = data['maincontacttitle']

        org.save()
        company.save()

        return super(AdminOrganisationEditView, self).form_valid(form)


# This is AJAX POST request from dashboard_mentoring
class VoucherChangeDate(View):

    def dispatch(self, request, *args, **kwargs):
        vid = request.POST.get('voucher')
        date_id = request.POST.get('did')  # A or E
        date = request.POST.get('value')
        try:
            self.value = datetime.datetime.strptime(date, settings.DATE_FORMAT_P)
        except:
            self.value = None

        self.voucher = get_object_or_404(CustomerVoucher, pk=vid)
        if date_id in ('A', 'E'):
            self.date_id = date_id
            return super(VoucherChangeDate, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden('Invalid date type')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        if self.value:
            if self.date_id == 'A':
                self.voucher.assigned = self.value
            elif self.date_id == 'E':
                self.voucher.expires = self.value
            self.voucher.save()
            str_date = self.value.strftime(settings.DATE_FORMAT_P)
            return HttpResponse(str_date)
        else:
            # javascript will handle empty value
            return HttpResponse('')


class AdminSupplierAssessmentHTMLView(DetailView):
    model = Organisation

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        if request.user.has_perm('bigassist_core.can_view_admin'):
            return super(AdminSupplierAssessmentHTMLView, self).dispatch(request, *args, **kwargs)
        else:
            token = request.GET.get('t')
            if check_token(token, self.get_object()):
                self.token = token
                return super(AdminSupplierAssessmentHTMLView, self).dispatch(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

    def get(self, request, *args, **kwargs):
        company = self.get_object()
        diag = company.get_supplier_diagnostic()
        if diag is None:
            return HttpResponse('Approval does not exist')

        manip = XMLManipulator(get_xform('SA_supplier_admin_readonly.html'))

        manip.set_xfinstance('data-set', reverse('admin_supplier_approval_xml_model', args=[company.pk]) + '?t=%s' % self.token)
        manip.set_xfinstance('old-data-set', reverse('admin_supplier_approval_xml_prevmodel', args=[company.pk]) + '?t=%s' % self.token)
        manip.set_xfinstance('comments-set', reverse('admin_supplier_approval_comments_model', args=[company.pk]) + '?t=%s' % self.token)
        # There is no difference between Save and Submit for admin.
        manip.set_xfsubmission('submit', '')
        manip.set_xfsubmission('finalsubmit', '')
        manip.set_iframe('file-download', '')
        # make a tranformation
        transformed = manip.make_transformation()
        return HttpResponse(transformed['full_result'], content_type='text/html')


@ROAdminRequiredDecorator
class AdminSupplierAssessmentsPDFView(TemplateView):
    template_name = 'khnpadmin/assessments_pdf_error.html'

    def get(self, request, *args, **kwargs):
        """ Either return a PDF file or show an error template """
        deadline = SupplierDeadline.objects.last_deadline()
        approvals = None
        if deadline:
            approvals = deadline.get_approvals()

        if (deadline is None) or (approvals is None):
            kw = kwargs.copy()
            kw.update({'err_msg': 'There are no approvals to process'})
            return super(AdminSupplierAssessmentsPDFView, self).get(request, *args, **kw)

        # Send ids of approvals as parameters to Celery task queue
        ids = [a.pk for a in approvals]
        result = celery_make_pdf.delay(deadline.pk, ids)
        deadline.pdf_task_id = result.task_id
        deadline.save()
        messages.info(request, 'Your request for PDF has been accepted. '
                               'The PDF will be generated in couple of minutes. '
                               'Please refresh this page to see the progress.')
        return HttpResponseRedirect(reverse('panel_dashboard'))


@ROAdminRequiredDecorator
class AdminSupplierAssessmentsPDFDownloadView(DetailView):
    model = SupplierDeadline

    def get(self, request, *args, **kwargs):
        deadline = self.get_object()
        if deadline.pdf_file.url:
            fname = os.path.join(settings.MEDIA_ROOT, deadline.pdf_file.url)
            response = HttpResponse(open(fname, 'rb'), content_type='text/pdf')
            response['Content-Disposition'] = 'attachment; filename="assessments.pdf"'
        else:
            response = HttpResponseForbidden('Invalid request')
        return response


@ROAdminRequiredDecorator
class AdminSelfAssessmentCSVDownloadView(View):
    class UnicodeWriter:
        """
        A CSV writer which will write rows to CSV file "f",
        which is encoded in the given encoding.
        """

        def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
            # Redirect output to a queue
            self.queue = cStringIO.StringIO()
            self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
            self.stream = f
            self.encoder = codecs.getincrementalencoder(encoding)()

        def writerow(self, row):
            self.writer.writerow([s.encode("utf-8") for s in row])
            # Fetch UTF-8 output from the queue ...
            data = self.queue.getvalue()
            data = data.decode("utf-8")
            # ... and reencode it into the target encoding
            data = self.encoder.encode(data)
            # write to the target stream
            self.stream.write(data)
            # empty queue
            self.queue.truncate(0)

        def writerows(self, rows):
            for row in rows:
                self.writerow(row)


    model = CustomerDiagnostic
    qlist = [
        ('organisation/org_name', '1',),
        ('organisation/org_type_id', '2',),
        ('organisation/org_type_other', '2a',),
        ('organisation/org_flavour_other', '2b',),
        ('organisation/detail', '2c',),
        ('organisation/address', '3',),
        ('organisation/postcode', '4',),
        ('organisation/reg_address', '5',),
        ('organisation/reg_postcode', '6',),
        ('organisation/tel_general', '7',),
        ('organisation/website', '8',),
        ('organisation/contact_name', '9',),
        ('organisation/short_desc', '10',),
        ('organisation/contact_tel', '11',),
        ('organisation/contact_email', '12',),

        ('spares/spare1','13a',),
        ('strategy/understandGoals','14',),
        ('strategy/understandMeans','15',),
        ('strategy/understandBeneficiaries','16',),
        ('strategy/understandBeneNeeds','17',),
        ('strategy/stratLastReviewed','18',),
        ('strategy/farLookFrequency','19',),
        ('strategy/whoReviewsStrategy','20',),
        ('strategy/haveOpPlans','21',),
        ('strategy/opPlansSetGoals','22',),
        ('strategy/opPlanSetDeliveryResp','23',),
        ('strategy/riskOppsAssess','24',),
        ('strategy/orgEffectivenessOpinion','25',),
        ('spares/spare2','25a',),
        ('strategy/capInfoOutputs','26a',),
        ('strategy/capInfoOutcomes','26b',),
        ('strategy/capInfoSatis','26c',),
        ('strategy/infoCapUse/infoEval','27a',),
        ('strategy/infoCapUse/infoLearn','27b',),
        ('strategy/infoCapUse/infoCommInt','27c',),
        ('strategy/infoCapUse/infoCommExt','27d',),
        ('strategy/changeInPast5yrs','28',),
        ('strategy/descMajorChangesPast5yrs','29',),
        ('strategy/changeInNextXyrs','30',),
        ('strategy/descBigChangesAhead','31',),
        ('strategy/speedBigChangesAhead','32',),
        ('strategy/whoMakesChange','33',),

        ('spares/spare3','33a',),
        ('spares/spare4','33b',),
        ('spares/spare5','33c',),
        ('finance/lastReview','34',),
        ('finance/whoPlansIncomeopps','35',),
        ('finance/incomeSources/donations','36a',),
        ('finance/incomeSources/grants','36b',),
        ('finance/incomeSources/contracts','36c',),
        ('finance/incomeSources/trading','36d',),
        ('finance/incomeSources/socialInvestment','36e',),
        ('finance/incomeSources/investment','36f',),
        ('finance/incomeSources/other','36g',),
        ('finance/futureIncomeSources/donations','37a',),
        ('finance/futureIncomeSources/grants','37b',),
        ('finance/futureIncomeSources/contracts','37c',),
        ('finance/futureIncomeSources/trading','37d',),
        ('finance/futureIncomeSources/socialInvestment','37e',),
        ('finance/futureIncomeSources/investment','37f',),
        ('finance/futureIncomeSources/other','37g',),
        ('finance/incomeMeetsFutureNeeds','38',),
        ('finance/incomeShortfallScale','39',),
        ('finance/cashflowSystems','40',),
        ('finance/costSkills','41',),
        ('finance/lastSpendReview','42',),
        ('finance/recentSkillsOpps','43',),
        ('finance/ownBuilding','44',),
        ('finance/buildingMakesMoney','45',),
        ('finance/mainFinancialChallenges','46',),
        ('finance/mainFinancialOpps','47',),

        ('innovation/innovateJudgement','48',),
        ('innovation/innoExample','49',),
        ('innovation/innoCurrent','50',),
        ('innovation/innoSkills','51',),
        ('innovation/innoEffectiveness/usingFailure','52a',),
        ('innovation/innoEffectiveness/cutRedTape','52b',),
        ('innovation/innoEffectiveness/analyseRisk','52c',),
        ('innovation/innoEffectiveness/rewardRiskTakers','52d',),
        ('innovation/innoEffectiveness/openToIdeas','52e',),
        ('innovation/innoEffectiveness/encourageSuggestions','52f',),
        ('innovation/innoEffectiveness/noNIH','52g',),
        ('innovation/innoEffectiveness/breakSilos','52h',),
        ('innovation/innoEffectiveness/valueCreativity','52i',),
        ('innovation/workWithStakeholders','53',),
        ('innovation/canCoproduce','54',),

        ('relationships/knowMarkets','55',),
        ('relationships/haveMktgStrategy','56',),
        ('relationships/lastReviewMarComms','57',),
        ('relationships/currentAndPriorityCommChannels','58',),
        ('relationships/analysedMarkets','59',),
        ('relationships/whoMakesStratRels','60',),
        ('relationships/needNewRels','61',),
        ('relationships/whoKeyNewRels','62',),
        ('relationships/relEffectiveness/pubSectorAgencies','63a',),
        ('relationships/relEffectiveness/privSectorOrgs','63b',),
        ('relationships/relEffectiveness/funders','63c',),
        ('relationships/relEffectiveness/policyMakers','63d',),
        ('relationships/relEffectiveness/volSectorOrgs','63e',),
        ('relationships/howValuedBy/pubSectorAgencies','64a',),
        ('relationships/howValuedBy/privSectorOrgs','64b',),
        ('relationships/howValuedBy/funders','64c',),
        ('relationships/howValuedBy/policyMakers','64d',),
        ('relationships/howValuedBy/volSectorOrgs','64e',),
        ('relationships/howEffectiveCommWith/pubSectorAgencies','65a',),
        ('relationships/howEffectiveCommWith/privSectorOrgs','65b',),
        ('relationships/howEffectiveCommWith/funders','65c',),
        ('relationships/howEffectiveCommWith/policyMakers','65d',),
        ('relationships/howEffectiveCommWith/volSectorOrgs','65e',),
        ('relationships/considerCollab','66',),
        ('relationships/considerShareBackOffice','67',),
        ('relationships/considerJointWorking','68',),
        ('relationships/considerMerger','69',),
        ('relationships/reasonsToJWorMerge','70',),
        ('relationships/skillsToMerge','71',),

        ('people/knowMission','72',),
        ('people/staffValued/staff','73a',),
        ('people/staffValued/volunteers','73b',),
        ('people/staffValued/trustees','73c',),
        ('people/staffValued/CEO','73d',),
        ('people/creativeCulture','74',),
        ('people/skillsForChange','75',),
        ('people/skillsGaps','76',),
        ('people/rightStructures','77',),
        ('people/boardWorkWithMgmt','78',),

        ('mostEffectiveSupport','79',),
        ('whatSuccessLooksLike','80',),

        ('sign-off','81',),
        ]



    def get(self, request, *args, **kwargs):

        # response = HttpResponse(content_type='text/plain')
        response = HttpResponse(content_type='text/csv')
        now = datetime.datetime.now()
        filename = now.strftime('%Y-%m-%d_bigassist_self-assessments.csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        qlist = self.qlist
        writer = self.UnicodeWriter(response)

        xml = os.path.join(os.path.dirname(__file__), 'xforms', 'C_customer_self_assessment_questions.xml')
        qtree = etree.parse(xml)

        tm = '{http://textmatters.com}'

        questions = ['diag_id', 'Region']
        for key, qnum in qlist:
            path = '%sq%s/%slabel' % (tm, qnum, tm)
            question = qtree.findtext(path, 'MISSING')
            questions.append(question)
        writer.writerow(questions)

        qry = CustomerDiagnostic.objects.order_by('diag_id').all()

        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')

        for row in qry:
            xml = row.diag_questions
            atree = etree.fromstring(xml.encode('utf-8'), parser=parser)
            answers = [str(row.diag_id), row.get_region()]
            for key, qnum in qlist:
                path = tm + key.replace('/', '/' + tm)
                answer = atree.findtext(path, 'MISSING')
                options = qtree.findall('%sq%s/%soption' % (tm, qnum, tm))
                translation = [q.text for q in options if q.attrib.get('store') == answer]
                if translation:
                    answer = translation[0]
                answers.append(answer)

            writer.writerow(answers)


        # response = HttpResponse('testing')
        return response


@ROAdminRequiredDecorator
class AdminCustomerCapabilityReportView(DetailView):
    model = Organisation
    template_name = "supplier/customer_recommendation.html"

    def get_context_data(self, *args, **kwargs):
        context = super(AdminCustomerCapabilityReportView, self).get_context_data(*args, **kwargs)
        context['diag'] = self.get_object().get_customer_diagnostic()
        return context


@AdminRequiredDecorator
class AdminApproveRejectSponsorshipView(ConfirmationDetailView):
    approve = None
    description = ''
    success_url = reverse_lazy('admin_dashboard_bursary')

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get('typ') not in ('visitor', 'mentee'):
            return HttpResponseBadRequest()
        return super(AdminApproveRejectSponsorshipView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if self.kwargs['typ'] == 'mentee':
            return Mentee.objects.all()
        elif self.kwargs['typ'] == 'visitor':
            return Visitor.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(AdminApproveRejectSponsorshipView, self).get_context_data(*args, **kwargs)
        if self.approve:
            context['description'] = mark_safe('Do you really want to grant sponsorship?')
        else:
            context['description'] = mark_safe('Do you really want to reject sponsorship?')
        return context

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.bursary = self.approve
        now = datetime.datetime.now()
        if self.approve:
            self.object.granted = now
        self.object.save()

        typ = self.kwargs['typ']
        context = {}
        if typ == 'visitor':
            if self.approve:
                # if the sponsorship is granted to at least one visitor
                # we can approve sponsorship for visit host (by approvin visitinstance row, if any)
                # If there is no visitinstance yet, it will be approved as soon as
                # visit host assigns visitor to the instance. Refs. #188
                # Please note, very similar code is in CustomerVisitorAcceptView
                instance = self.object.visit_instance
                if instance and not instance.bursary:
                    instance.bursary = True
                    instance.granted = now
                    instance.save()
            message_user_target = self.object.visitor_org
            message_host_target = self.object.visit.host
            context['visitor_org'] = self.object.visitor_org
            if self.approve:
                message_user_id = '171_admin_decided_on_bursary_accepted'
                message_host_id = '173_admin_decided_on_bursary_accepted'
            else:
                message_user_id = '172_admin_decided_on_bursary_rejected'
                message_host_id = '174_admin_decided_on_bursary_rejected'
        elif typ == 'mentee':
            message_user_id = '221_admin_decide_on_bursary'
            message_user_target = self.object.mentee_org

            message_host_id = '223_admin_decide_on_bursary'
            message_host_target = self.object.mentor.mentor_org

            context['mentee'] = self.object
            # an email for admin (not sent for Visits)
            send_ba_email('222_admin_decide_on_bursary', context)

        if self.approve:
            msg = "The sponsorship has been granted"
        else:
            msg = "The sponsorship has been rejected"

        send_ba_email(message_user_id, context, recipients=message_user_target, recipient_role='customer')
        send_ba_email(message_host_id, context, recipients=message_host_target, recipient_role='customer')
        messages.success(request, msg)
        return self.render_success(request)


@AdminRequiredDecorator
class AdminOrganisationUsersView(DetailView):
    model = Organisation
    template_name = "khnpadmin/organisation_users.html"

    def get_context_data(self, *args, **kwargs):
        context = super(AdminOrganisationUsersView, self).get_context_data(*args, **kwargs)

        members = list()
        context['members'] = list()
        company = self.get_object()
        for member in company.get_members():
            pk = member.pk
            if pk in members:
                continue
            email = member.get_contact_email()

            personas = member.identity_set.all()

            suffix = ''
            is_manager = member.rel_subject_set.filter(object=company.pk, type__id=settings.KHNP_MANAGER_TYPE_ID).exists()
            if is_manager:
                suffix += ' <strong>Manager</strong>'
            key = 'M%s' % member.pk
            if personas.count() > 1:
                persona_list = str()
                for index, p in enumerate(personas):
                    if index == 0:
                        persona_list += p.persona
                    else:
                        persona_list += ', ' + p.persona
                context['members'].append([key, mark_safe('%s &lt;<a href="mailto:%s">%s</a>&gt; (Personas: %s)%s' % (member.displayname, email, email, persona_list, suffix))])
            else:
                context['members'].append([key, mark_safe('%s &lt;<a href="mailto:%s">%s</a>&gt;%s' % (member.displayname, email, email, suffix))])

        for record in InvitedUser.objects.filter(org=company, registered__isnull=True):
            suffix = ' <em>(email invitation sent)</em>'
            if record.type.id == settings.KHNP_MANAGER_TYPE_ID:
                suffix += ' <strong>Manager</strong>'
            key = 'I%s' % member.pk
            context['members'].append([key, mark_safe('&lt;<a href="mailto:%s">%s</a>&gt;%s' % (record.email, record.email, suffix))])

        return context

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        company = self.get_object()

        if request.POST.get('remove_users'):
            # be sure at least one manager left
            # Process real users first, then procewss InvitedUsers
            to_remove = [int(x[1:]) for x in request.POST.getlist('remove') if x[0] == 'M']
            managers = []
            objects_to_remove = []
            users_removed = False
            if to_remove:
                for member in company.get_members():
                    pk = member.pk
                    if pk in to_remove:
                        objects_to_remove.append(member)
                    else:
                        # update list of organisation managers to be sure at least one Manager left
                        is_manager = member.rel_subject_set.filter(object=company.pk, type__id=settings.KHNP_MANAGER_TYPE_ID).exists()
                        if is_manager:
                            managers.append(member.pk)
                # if there is no manager left, stop processing and return error message
                if not managers:
                    messages.warning(request, 'No organisation Manager would left. The removal process has been aborted.')
                    return HttpResponseRedirect(reverse('admin_org_users', args=[self.kwargs['pk']]))
                # if at least one manager left, remove user's relationship with the Organisation
                for member in objects_to_remove:
                    cursor = connection.cursor()
                    query = """DELETE FROM relationship WHERE subject = %s and object = %s"""
                    cursor.execute(query, [member.pk, company.pk])
                    users_removed = True

            to_remove = [int(x[1:]) for x in request.POST.getlist('remove') if x[0] == 'I']
            if to_remove:
                for member in InvitedUser.objects.filter(org=company, registered__isnull=True):
                    if member.pk in to_remove:
                        member.delete()
                        users_removed = True
            if users_removed:
                messages.info(request, 'The requested users were removed from this organisation.')
            else:
                messages.info(request, 'No users were removed.')
        else:
            emails = request.POST.get('users', '').split()
            emails = [x.strip() for x in emails]
            typ = request.POST.get('type', 'member')

            members = []
            for member in company.get_members():
                members.append(member.get_contact_email())

            not_found = []
            added = 0
            exists = 0
            for email in emails:
                # check if email is not member of the organisation already
                if email in members:
                    continue

                # let's search for Person object. We should search Identity table first and then
                # look to the Contact table. See #159#i3
                person = None
                identity = get_or_none(Identity.objects.filter(persona__iexact=email))
                if identity:
                    person = identity.person
                else:
                    q = dict(value__iexact=email, person__type=settings.KHNP_PERSON_TYPE)
                    q.update(settings.PERSON_CONTACT_TYPES['email'])
                    contact = get_or_none(Contact.objects.filter(**q))
                    if not contact:
                        q.update(settings.PERSON_CONTACT_TYPES['fallback_email'])
                        contact = get_or_none(Contact.objects.filter(**q))
                    if contact:
                        person = contact.person

                if not person:
                    not_found.append(email)
                else:
                    # add user to the company
                    added += 1
                    cursor = connection.cursor()

                    try:
                        Relationship.objects.get(subject=person.pk, object=company.pk, type=settings.KHNP_MEMBER_TYPE_ID)
                        added -= 1
                        exists += 1
                        query = """UPDATE relationship SET (subjectapproved, objectapproved) = (True, True)
                                    WHERE subject=%s AND object=%s AND type=%s"""
                        cursor.execute(query, [person.pk, company.pk, settings.KHNP_MEMBER_TYPE_ID])
                    except Relationship.DoesNotExist:
                        query = """INSERT INTO relationship
                                   (subject, object, type, subjectapproved, objectapproved)
                                   VALUES (%s, %s, %s, True, True)"""
                        cursor.execute(query, [person.pk, company.pk,
                                               settings.KHNP_MEMBER_TYPE_ID])

                    if typ == 'manager':
                        try:
                            Relationship.objects.get(subject=person.pk, object=company.pk, type=settings.KHNP_MANAGER_TYPE_ID)
                        except Relationship.DoesNotExist:
                            query = """INSERT INTO relationship
                                       (subject, object, type, subjectapproved, objectapproved)
                                       VALUES (%s, %s, %s, True, True)"""
                            cursor.execute(query, [person.pk, company.pk, settings.KHNP_MANAGER_TYPE_ID])

                    cursor.close()

            if added:
                messages.info(request, 'Users added: %d' % added)
            else:
                messages.warning(request, 'No users were added.')

            if exists:
                messages.info(request, '%d user(s) already exist' % exists)

            if not_found:
                messages.warning(request, "These emails weren't found in the database: %s" % ', '.join(not_found))
                return HttpResponseRedirect(reverse('admin_org_users', args=[self.kwargs['pk']]))

        return HttpResponseRedirect(reverse('admin_organisation_profile', args=[self.kwargs['pk']]))
