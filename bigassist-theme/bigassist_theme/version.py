#!/usr/bin/env python
# -*- coding: utf-8 -*-

VERSION = (4, 1, 2)
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))
