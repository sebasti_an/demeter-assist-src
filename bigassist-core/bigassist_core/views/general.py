#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import urllib2
from datetime import datetime
from django.db import transaction
from django.db import connection
from django.conf import settings
from django.http import HttpResponseRedirect, Http404, HttpResponse, HttpResponseForbidden
from django.views.generic import TemplateView, FormView, View, DetailView, ListView
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth import logout
from django.contrib import messages
from django.shortcuts import get_object_or_404

from pybb.models import Forum

from bigassist_core.models import Organisation, NewsItem, InvitedUser, \
                                  Person, Relationship, ProgrammeRole, \
                                  OrganisationProgrammeRoleRelationship, \
                                  EmailDomain, get_email_domain_blacklist
from bigassist_cms.models import Section, Page
from bigassist_core.forms import RegistrationForm, KnownUserRegistrationForm, PersonProfileForm, \
    OrganisationForm, ContactCompanyOwnerForm, OrganisationPaymentForm, \
    SimpleRegistrationForm, JustTextWidget, ChooseCompanyForm, \
    ChooseRoleForm, KnownUserManagerRegistrationForm, SupplierOrganisationForm
from bigassist_core.user import KHNPUser
from bigassist_core.views.customer import CustomerDashboardDiagnosticView, \
                                          CustomerDashboardUsersView
from bigassist_core.views.supplier import SupplierDashboardAssessmentView, \
                                          SupplierDashboardUsersView
from bigassist_core.views.admin import AdminDashboardDatabaseView
from bigassist_core.utils import LoginRequiredDecorator, \
                                 ConfirmationDetailView, \
                                 ConfirmationView
from bigassist_core.utils import get_detail_fieldname
from bigassist_core.security import UserSecurity
from bigassist_core.emails import send_ba_email

from bigassist_core.check_orgs import check_orgs


import logging
logger = logging.getLogger(__name__)


def home_view_dispatcher(request, **kwargs):
    user = request.user
    if user.is_anonymous():
        return HomeAnonView.as_view()(request, **kwargs)
    elif user.is_staff or user.has_perm('bigassist_core.can_view_admin'):
        return AdminDashboardDatabaseView.as_view()(request, **kwargs)
    else:
        khnp_user = KHNPUser(user)
        company = khnp_user.get_company()
        if not company:
            # This cannot go to the register view as we do not know what to
            # register as - customer or supplier
            # return HomeAnonView.as_view()(request, **kwargs)
            # We should now redirect to the generic registration view.
            return RegistrationView.as_view(
                user_type=None,
                signin_url=reverse_lazy('generic_signin')
                )(request, **kwargs)
        else:
            # Checks if an org is both supplier and customer
            # and redirects to the correct view
            if set(['supplier', 'customer']) <= company.get_roles():
                if 'current_role' in request.session:
                    if 'supplier' in request.session.get('current_role'):
                        return SupplierDashboardAssessmentView.as_view()(request, **kwargs)
                    elif 'customer' in request.session.get('current_role'):
                        return CustomerDashboardDiagnosticView.as_view()(request, **kwargs)
                else:
                    return ChooseRoleView.as_view()(request, **kwargs)
            if 'supplier' in company.get_roles():
                return SupplierDashboardAssessmentView.as_view()(request, **kwargs)
            elif 'customer' in company.get_roles():
                return CustomerDashboardDiagnosticView.as_view()(request, **kwargs)
            else:
                return GenericDashboardView.as_view()(request, **kwargs)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))


class LoginView(TemplateView):

    template_name = 'registration/notallowed.html'


class HomeAnonView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeAnonView, self).get_context_data(**kwargs)
        context['section'] = get_object_or_404(Section, slug='news')
        context['home_page_news'] = Page.objects.news()[:5]
        context['topics'] = []
        for f in Forum.objects.all():
            context['topics'].append({'forum':f.name,'post':f.last_post})
        return context


class SigninView(TemplateView):

    template_name = "signin.html"
    next_url = None
    user_type = None

    def get_context_data(self, **kwargs):
        context = super(SigninView, self).get_context_data(**kwargs)
        context['persona_next_url'] = self.next_url
        return context


# Do not decorate with LoginRequired view. We need this view
# to handle different signin urls
class RegistrationView(FormView):

    template_name = 'register.html'
    form_class = RegistrationForm
    success_url = reverse_lazy('home')
    signin_url = None
    user_type = None
    known_org = None
    known_orgs = None
    new_org = None

    def dispatch(self, request, *args, **kwargs):
        user = request.user

        self.known_org = int(kwargs.get('organisation_id', False))

        if user.is_anonymous():
            return HttpResponseRedirect(self.signin_url)

        self.khnp_user = KHNPUser(request.user)
        self.person = self.khnp_user.get_person()

        if self.khnp_user.has_registered_company():
            return HttpResponseRedirect(self.success_url)

        if self.known_org:
            assert self.known_org in self.khnp_user.get_all_possible_orgs(), \
                "User does not have the permission to register this organisation"

        self.known_orgs = self.khnp_user.get_all_orgs()

        if len(self.known_orgs) >= 1:
            return super(RegistrationView, self).dispatch(request, *args, **kwargs)

        if not self.known_org:
            if not self.new_org:
                # Check whether khnp_user has associated email domains
                self.emaildomains = self.khnp_user.get_email_domains()
                if self.emaildomains.count() > 0:
                    # pass
                    return RegistrationMatchEmailDomainView.as_view()(request, **kwargs)

        return super(RegistrationView, self).dispatch(request, *args, **kwargs)

    def get_initial(self, **kwargs):
        initial = {}
        initial.update(self.person.__dict__)

        if self.kwargs.get('organisation_id'):
            test_org = Person.objects.get(id=self.kwargs.get('organisation_id'))
            initial.update({'displayname': test_org.displayname,
                # 'maincontactname': test_org.maincontactname, 
                # 'maincontacttitle': test_org.maincontacttitle, 
                'org_id': test_org.id, 
                })
            initial.update(test_org.get_additional())

        return initial

    def is_forced(self):
        return 'force_create' in self.request.POST

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.
        """
        kwargs = {'initial': self.get_initial()}
        if self.request.method in ('POST', 'PUT'):
            if self.is_forced():
                data = self.request.session.get('data', {})
            else:
                data = self.request.POST

            kwargs.update({
                'data': data,
                'files': self.request.FILES,
            })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['khnp_user'] = self.khnp_user
        # is_supplier = self.user_type == 'supplier'
        # context['is_supplier'] = is_supplier
        # Org role = customer or supplier, not True or False as is_supplier
        org_role = self.user_type
        context['org_role'] = org_role

        context['khnp_user_orgs'] = self.khnp_user.get_all_orgs()

        if self.kwargs.get('organisation_id'):
            context['current_org'] = Person.objects.get(id=self.kwargs.get('organisation_id'))

        return context

    def form_valid(self, form):
        # assert self.user_type in ['customer', 'supplier', None], \
        #     "The user type is not specified, cannot decide whether it's a customer or a supplier"
        # is_supplier = self.user_type == 'supplier'
        # As in get_context_data
        org_role = self.user_type

        data = form.cleaned_data

        if self.is_forced():
            # self.save_form(data, is_supplier)
            # we now need to pass in org_role instead of is_supplier
            self.save_form(data, org_role)
            return super(RegistrationView, self).form_valid(form)

        # similar = self.get_similar_orgs(data)
        # if len(similar) == 0:
        #     self.save_form(data, is_supplier)
        #     return super(RegistrationView, self).form_valid(form)

        else:
            person = self.person
            person.firstname = data['firstname']
            person.surname = data['surname']
            person.displayname = person.firstname + u' ' + person.surname
            person.save()

            self.request.session['data'] = form.data.dict()

            self.save_form(data, org_role)
            return super(RegistrationView, self).form_valid(form)

            # return self.render_to_response(self.get_context_data(similar=similar))

    @transaction.atomic
    def save_form(self, data, org_role):
        person = self.person
        org = self.khnp_user.get_company()
        user_email_domain = self.khnp_user.get_contact_email_domain()
        if org is None:
            org = Organisation.objects.register(person,
                                                defaults={'org_role': org_role})

        company = org.org

        person.firstname = data['firstname']
        person.surname = data['surname']
        person.displayname = person.firstname + u' ' + person.surname

        org.org_type = data['org_type']
        org.detail = data['detail']

        # REMOVED
        # org.contact_name = data['maincontactname']
        # org.contact_email = data['email_direct']
        # org.contact_tel = data['tel_general']

        # It does not matter if we send in more data as it picks only needed
        company.set_additional(data['company'])
        company.displayname = data['displayname']
        # company.maincontactname = data['maincontactname']
        # company.maincontacttitle = data['maincontacttitle']

        # Here we need to get or create the emaildomain object for the org
        if user_email_domain not in get_email_domain_blacklist():
            EmailDomain.objects.get_or_create(object=person, domain=user_email_domain)

        person.save()
        org.save()
        company.save()

        # send email to customer/supplier about a successfull registration
        if org_role == 'supplier':
            send_ba_email('02_supplier_registered', data, sender=None, recipients=self.khnp_user.get_contact())
        else:
            send_ba_email('03_customer_registered', data, sender=None, recipients=self.khnp_user.get_contact())

    def get_similar_orgs(self, data):
        is_supplier = self.user_type == 'supplier'
        ids = check_orgs(data)
        persons = [Person.objects.get(id=x) for x in ids]
        def is_true_match(person):
            if person.ncvomem == data['ncvomem']:
                return True
            if person.companyno == data['companyno']:
                return True
            if person.charityno == data['charityno']:
                return True
            return False


        result = []
        for person in persons:
            dt = {'person' : person}
            dt['match'] = is_true_match(person)
            dt['has_members'] = len(person.get_members()) > 0

            try:
                org = person.organisation_set.get()
                dt['org'] = org
            except Organisation.DoesNotExist:
                result.append(dt)
                continue

            if is_supplier == org.is_supplier:
                result.append(dt)


        # If there are any with a match then return them
        matched = [x for x in result if x['match'] == True]
        if len(matched) > 0:
            return matched

        # if not return everythin
        return [x for x in result]


class KnownUserRegistrationView(RegistrationView):
    template_name = 'register.html'

    def get_form_class(self):
        try:
            Relationship.objects.get(subject=self.person.pk, object=self.known_org, type=settings.KHNP_MANAGER_TYPE_ID)
            return KnownUserManagerRegistrationForm
        except Relationship.DoesNotExist:
            return KnownUserRegistrationForm

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.

        # assert self.user_type in ['customer', 'supplier'], \
        #     "The user type is not specified, cannot decide whether it's a customer or a supplier"
        # is_supplier = self.user_type == 'supplier'
        # context['is_supplier'] = is_supplier
        # Org role = customer or supplier, not True or False as is_supplier
        org_role = self.user_type

        data = form.cleaned_data

        self.save_form(data, org_role)

        return super(RegistrationView, self).form_valid(form)

    @transaction.atomic
    def save_form(self, data, org_role):
        ''' We need to overwrite this method to get the org
        from the current PK. Currently org returns None and
        the save form method registers a new organisation
        with person's PK as it's ID.
        '''

        person = self.person
        org = self.khnp_user.get_company()
        org_id = data['org_id']

        if org is None:
            org = Organisation.objects.update_organisation(org_id, person,
                                       defaults={'org_role': org_role})

        company = org.org

        person.firstname = data['firstname']
        person.surname = data['surname']
        person.displayname = person.firstname + u' ' + person.surname

        org.org_type = data['org_type']
        org.detail = data['detail']

        # SK - Make the registering user a member (type 5) of
        # the organisation if it doesn't already exist
        try:
            Relationship.objects.get(subject=person.pk, object=company.pk, type=settings.KHNP_MEMBER_TYPE_ID)
        except Relationship.DoesNotExist:
            cursor = connection.cursor()
            query = """INSERT INTO relationship
                       (subject, object, type, subjectapproved, objectapproved)
                       VALUES (%s, %s, %s, True, True)"""
            cursor.execute(query, [person.pk, company.pk,
                                   settings.KHNP_MEMBER_TYPE_ID])

        # REMOVED
        #org.contact_name = data['maincontactname']
        #org.contact_email = data['email_direct']
        #org.contact_tel = data['tel_general']

        # It does not matter if we send in more data as it picks only needed
        # SK - We no longer want to update the company details here.
        # The fields are marked as read-only in the Form class
        # but we do not want to resend the data and hit the DB for this,
        # as it will change the updated date and is open for html hacks.
        # if the user is manager, we allow saving
        try:
            Relationship.objects.get(subject=self.person.pk, object=self.known_org, type=settings.KHNP_MANAGER_TYPE_ID)

            company.set_additional(data['company'])
            company.displayname = data['displayname']
            # company.maincontactname = data['maincontactname']
            # company.maincontacttitle = data['maincontacttitle']
            company.save()
        except:
            pass

        person.save()
        org.save()

        # send email to customer/supplier about a successfull registration
        if org_role == 'supplier':
            send_ba_email('02_supplier_registered', data, sender=None, recipients=self.khnp_user.get_contact())
        else:
            send_ba_email('03_customer_registered', data, sender=None, recipients=self.khnp_user.get_contact())


class RegistrationOrganisationSearchView(ListView):
    ''' This view searches for matching organisations based on name.
    Customer users will only get BA customers, and
    Supplier users will only get BA suppliers
    '''
    model = Person
    # context_object_name = 'organisations'
    template_name = "registration/match_organisation.html"
    user_type = None

    def get_queryset(self, **kwargs):

        organisations = Person.objects.filter(type=2)

        name = self.request.GET.get('name', False)

        if name is not False:
            object_list = organisations.filter(displayname__icontains=name).order_by('displayname')[:20]
        else:
            object_list = organisations.none()
        return object_list

    def get_context_data(self, **kwargs):
        context = super(RegistrationOrganisationSearchView, self).get_context_data(**kwargs)
        # context['test'] = self.request.GET.get('name')
        context['my_user_type'] = self.user_type
        return context


class RegistrationMatchEmailDomainView(TemplateView):
    ''' This view searches for matching email domains based on email address.
    If a user has a match, we check if the matched org is registered on BA,
    if it is, we should send an email to the org admin and ask if the user
    is allowed to become a member of the org.
    If the org is known to cyclops, but not BA, we create a BA org, 
    and a relationship to the KHNP org.
    '''
    # context_object_name = 'organisations'
    template_name = "registration/match_email_domain.html"
    user_type = None

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_anonymous():
            return HttpResponseRedirect(self.signin_url)

        self.khnp_user = KHNPUser(request.user)
        self.person = self.khnp_user.get_person()

        self.user_email_domain = self.khnp_user.get_contact_email_domain

        return super(RegistrationMatchEmailDomainView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistrationMatchEmailDomainView, self).get_context_data(**kwargs)
        # context['test'] = self.request.GET.get('name')
        context['my_user_type'] = self.user_type
        context['khnp_user'] = self.khnp_user
        return context


@LoginRequiredDecorator
class OrganisationRequestMembershipView(ConfirmationDetailView):

    model = Person
    description = "Request membership"
    action = "Request"
    success_url = reverse_lazy('home')

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        khnpuser = KHNPUser(request.user).get_person()

        send_ba_email('43_request_membership', {'user': request.user, 'person': obj, 'khnpuser': khnpuser}, sender=None, recipients=obj)

        msg = "Membership request has been sent"
        messages.success(request, msg)

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@LoginRequiredDecorator
class OrganisationBecomeManagerView(ConfirmationDetailView):

    model = Person
    description = "Become a manager of the company"
    action = "Submit"
    success_url = reverse_lazy('home')
    user_type = None

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        company = self.get_object()
        # is_supplier = self.user_type == 'supplier'
        # context['is_supplier'] = is_supplier
        # Org role = customer or supplier, not True or False as is_supplier
        org_role = self.user_type

        khnp_user = KHNPUser(request.user)
        person = khnp_user.get_person()

        if len(company.get_members()) > 0:
            messages.error(request, "You cannot become a manager of this company as there are already some members.")
            return self.render_to_response(self.get_context_data())

        # become member
        cursor = connection.cursor()
        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [person.pk, company.pk,
                               settings.KHNP_MEMBER_TYPE_ID])

        # become manager
        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [person.pk, company.pk,
                               settings.KHNP_MANAGER_TYPE_ID])

        org, created = company.organisation_set.get_or_create(defaults={'org_role': org_role})


        form = RegistrationForm(request.session.get('data', []))
        if not form.is_valid():
            messages.error(request, "You cannot become a manager of this company as there are already some members.")
            return self.render_to_response(self.get_context_data())

        data = form.cleaned_data
        person.firstname = data['firstname']
        person.surname = data['surname']
        person.displayname = person.firstname + u' ' + person.surname

        org.org_type = data['org_type']
        org.detail = data['detail']

        company.set_additional(data['company'])
        company.displayname = data['displayname']
        # company.maincontactname = data['maincontactname']
        # company.maincontacttitle = data['maincontacttitle']

        person.save()
        org.save()
        company.save()

        if org_role == 'supplier':
            send_ba_email('02_supplier_registered', data, sender=None, recipients=khnp_user.get_contact())
        else:
            send_ba_email('03_customer_registered', data, sender=None, recipients=khnp_user.get_contact())

        msg = "You have registered"
        messages.success(request, msg)

        url = str(self.get_success_url())
        if self.request.is_ajax():
            return HttpResponse(json.dumps({"url": url}),
                                content_type="application/json")

        return HttpResponseRedirect(url)


@LoginRequiredDecorator
class OrganisationEditView(FormView):

    template_name = 'org_profile.html'
    form_class = OrganisationForm
    success_url = reverse_lazy('home')
    register_url = None
    user_type = None

    def dispatch(self, request, *args, **kwargs):
        self.khnp_user = KHNPUser(request.user)
        self.person = self.khnp_user.get_person()

        if not self.khnp_user.has_registered_company():
            return HttpResponseRedirect(self.register_url)

        # TM-23 - if supplier assessment process is pending review, Organization can't be changed.
        self.org = self.khnp_user.get_company()
        self.appr = self.org.get_supplier_diagnostic()

        # if self.appr and self.appr.state not in ['in_progress', 'approved']:
        #     messages.warning(request, "The assessment is pending review. Organisation details can't be changed at this time.")
        #     return HttpResponseRedirect(reverse('home'))

        self.readonly = not UserSecurity(request.user).can('edit_organisation_profile')
        return super(OrganisationEditView, self).dispatch(
            request, *args, **kwargs)

    def show_payment(self):
        # if self.user_type == "supplier":
        if 'supplier' in self.org.get_roles():
            if self.appr is not None and self.appr.state == "approved":
                return True
        return False

    def get_form_class(self):
        if self.show_payment():
            form_class = OrganisationPaymentForm
        else:
            if self.user_type == 'supplier':
                form_class = SupplierOrganisationForm
            else:
                form_class = OrganisationForm
        return form_class

    def get_form(self, form_class):
        form = form_class(**self.get_form_kwargs())
        if not self.appr:
            return form

        if self.appr.state == "approved":
            form['displayname'].field.required = False
            form['displayname'].field.widget = JustTextWidget()

        if self.readonly:
            for field in form.fields.values():
                field.widget = JustTextWidget()
                field.required = False
        return form

    def get_initial(self):
        initial = {}

        # organisation
        org = self.org
        initial['org_type'] = org.org_type and org.org_type.pk
        # org_type can be None (old records)

        if self.show_payment():
            initial['bank_name'] = org.bank_name
            initial['bank_ac_name'] = org.bank_ac_name
            initial['bank_ac_no'] = org.bank_ac_no
            initial['bank_sortcode'] = org.bank_sortcode

        if self.user_type == 'supplier':
            initial['year_founded'] = org.year_founded

        # company
        company = org.org
        initial['displayname'] = company.displayname
        initial['detail'] = org.detail
        # initial['maincontactname'] = company.maincontactname
        # initial['maincontacttitle'] = company.maincontacttitle
        initial.update(company.get_additional())

        return initial

    def get_context_data(self, **kwargs):
        context = super(OrganisationEditView, self).get_context_data(**kwargs)
        context['khnp_user'] = self.khnp_user
        context['show_payment'] = self.show_payment()
        context['is_supplier'] = self.user_type == 'supplier'
        context['readonly'] = self.readonly

        context['organisation_type_str'] = unicode(self.org.org_type)
        detail = self.org.get_detail()
        if detail:
            context['organisation_type_str'] += u' / ' + unicode(detail)
        return context

    @transaction.atomic
    def form_valid(self, form):
        if not self.readonly:
            data = form.cleaned_data

            person = self.person
            org = self.khnp_user.get_company()
            company = org.org

            org.org_type = data['org_type']
            org.detail = data['detail']

            if self.show_payment():
                org.bank_name = data['bank_name']
                org.bank_ac_name = data['bank_ac_name']
                org.bank_ac_no = data['bank_ac_no']
                org.bank_sortcode = data['bank_sortcode']

            if self.user_type == 'supplier':
                org.year_founded = data['year_founded']

            # It does not matter if we send in more data as it picks only needed
            company.set_additional(data['company'])
            if data['displayname']:
                company.displayname = data['displayname']
            # company.maincontactname = data['maincontactname']
            # company.maincontacttitle = data['maincontacttitle']

            # FIXME - remove
            #org.contact_name = data['maincontactname']
            #org.contact_email = data['email_direct']
            #org.contact_tel = data['tel_direct']

            person.save()
            org.save()
            company.save()

            # Send email to NVCO - issue #139
            send_ba_email('35_profile_updated', {'organisation': org})
            messages.info(self.request, 'Organisation details have been updated.')

        return super(OrganisationEditView, self).form_valid(form)


@LoginRequiredDecorator
class ProfileView(TemplateView):
    template_name = 'profile.html'

    def dispatch(self, request, *args, **kwargs):
        self.khnp_user = KHNPUser(request.user)
        self.person = self.khnp_user.get_person()
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        org_name = ''
        org = self.khnp_user.get_company()
        if org:
            org_name = org.org.displayname
        email = self.khnp_user.get_person().get_contact_email()
        initial = dict(
            firstname=self.person.firstname,
            surname=self.person.surname,
            email=email,
            biography=self.person.biography,
        )
        # The form is rendered as read-only fields/labels. Form is no processed in this view.
        context['form'] = PersonProfileForm(initial=initial)
        context['organization'] = org_name
        return context


@LoginRequiredDecorator
class ProfileEditView(FormView):

    template_name = 'profile_edit.html'
    form_class = PersonProfileForm
    success_url = reverse_lazy('profile')

    def dispatch(self, request, *args, **kwargs):
        self.khnp_user = KHNPUser(request.user)
        self.person = self.khnp_user.get_person()
        return super(ProfileEditView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        email = self.khnp_user.get_person().get_contact_email()

        initial = dict(
            firstname=self.person.firstname,
            surname=self.person.surname,
            email=email,
            biography=self.person.biography,
        )
        return initial

    def get_context_data(self, **kwargs):
        context = super(ProfileEditView, self).get_context_data(**kwargs)
        org_name = ''
        org = self.khnp_user.get_company()
        if org:
            org_name = org.org.displayname
        context['organization'] = org_name
        return context

    @transaction.atomic
    def form_valid(self, form):
        self.person.firstname = form.cleaned_data['firstname']
        self.person.surname = form.cleaned_data['surname']
        self.person.displayname = "%(firstname)s %(surname)s" % form.cleaned_data
        self.person.biography = form.cleaned_data['biography']
        self.person.save()

        # https://it.fry-it.com/text-matters/dev/152 - always delete the fallback email
        self.person.del_contact('fallback_email')

        self.person.set_contact('email', form.cleaned_data['email'])
        return super(ProfileEditView, self).form_valid(form)


class LookupCompany(View):

    LOOKUP_URL = 'http://opencorporates.com/companies/gb/'

    def get(self, request, *args, **kwargs):
        comp_id = kwargs.get('id', '').strip()
        if not comp_id:
            raise Http404

        url = "%s%s.json" % (self.LOOKUP_URL, comp_id)
        try:
            data = urllib2.urlopen(url).read()
        except urllib2.HTTPError:
            raise Http404

        return HttpResponse(data, content_type='application/json')


class LookupCharity(View):

    LOOKUP_URL = 'http://opencharities.org/charities/'

    def get(self, request, *args, **kwargs):
        charity_id = kwargs.get('id', '').strip()
        if not charity_id:
            raise Http404

        url = "%s%s.json" % (self.LOOKUP_URL, charity_id)
        try:
            data = urllib2.urlopen(url).read()
        except urllib2.HTTPError:
            raise Http404

        return HttpResponse(data, content_type='application/json')


class CheckCompany(TemplateView):

    template_name = "check_company.html"

    def get_context_data(self, **kwargs):
        context = super(CheckCompany, self).get_context_data(**kwargs)

        org_type = int(self.request.GET.get('org_type', '0'))
        companyno = self.request.GET.get('companyno', '').strip()

        org = None

        if org_type and companyno:
            if org_type == 1:
                orgs = Organisation.objects.filter(org_type__org_type_id=org_type,
                                               org__charityno__iexact=companyno)
            elif org_type == 2:
                orgs = Organisation.objects.filter(org_type__org_type_id=org_type,
                                               org__companyno__iexact=companyno)
            elif org_type == -1:
                orgs = Organisation.objects.filter(org__ncvomem__iexact=companyno)
            else:
                orgs = Organisation.objects.filter(org_type__org_type_id=org_type,
                                               detail__iexact=companyno)

            if len(orgs) > 0:
                org = orgs[0]

        if org:
            context['org'] = org
            context['person'] = org.org
            context['additional'] = org.org.get_additional()
        return context


class ContactCompanyOwnerView(FormView):

    form_class = ContactCompanyOwnerForm
    template_name = 'contact_company_owner.html'

    def get_initial(self):
        initial = {}
        person_pk = int(self.kwargs.get('org_id'))
        initial['person'] = get_object_or_404(Person, pk=person_pk)
        return initial

    def get_context_data(self, **kwargs):
        context = super(ContactCompanyOwnerView, self).get_context_data(**kwargs)
        person_pk = int(self.kwargs.get('org_id'))
        context['person'] = get_object_or_404(Person, pk=person_pk)
        context['is_registration'] = self.request.GET.get('registration') == 'true'
        return context

    def form_valid(self, form):
        # contact
        user = self.request.user
        person = KHNPUser(user).get_person()
        organisation = form.cleaned_data['person'] # it's the person org, not a person person
        contact_email = organisation.get_contact_email()
        manager = None
        members = organisation.get_members(only_managers=True)
        if members:
            manager = members[0]
            if not contact_email:
                contact_email = manager.get_contact_email()

        if manager is None or not contact_email:
            messages.error(self.request, 'Unable to contact organisation. No manager contact found. %s/%s' % (manager, contact_email))
        else:
            person_email = person.get_contact_email()
            data = dict(
                # release email address even if user does not allow it. See issue #058
                person=person.displayname or person.get_contact_email(),
                person_email=person_email,
                company_owner=unicode(manager),
                organisation=unicode(organisation),
                organisation_pk=organisation.pk,
                person_pk=person.pk,
                message=form.cleaned_data['message'],
            )
            send_ba_email('30_person_contacts_company_owner', data, sender=None, recipients=[contact_email])
            messages.info(self.request, 'Organisation has been contacted by email.')
        return HttpResponseRedirect(reverse('home'))


@LoginRequiredDecorator
class ApproveUserView(View):

    def get(self, request, *args, **kwargs):
        org = get_object_or_404(Person, pk=kwargs.get('organisation_id'))
        person = get_object_or_404(Person, pk=kwargs.get('person_id'))

        khnp_user = KHNPUser(self.request.user)
        khnp_person = khnp_user.get_person()

        if khnp_person not in org.get_members(only_managers=True):
            return HttpResponseForbidden()

        cursor = connection.cursor()
        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [person.pk, org.pk,
                               settings.KHNP_MEMBER_TYPE_ID])

        messages.info(request, "New member %s added to your organisation" % person)

        return HttpResponseRedirect(reverse('home'))


class JoinView(FormView):
    model = InvitedUser
    form_class = SimpleRegistrationForm
    template_name = 'join_company.html'

    def get_object(self):
        return get_object_or_404(InvitedUser, pk=self.kwargs['pk'])

    def get_initial(self):
        initial = {}
        user = self.request.user
        if not user.is_anonymous():
            person = KHNPUser(user).get_person()
            initial['firstname'] = person.firstname
            initial['surname'] = person.surname
        return initial

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        user = request.user
        if user.is_anonymous():
            return SigninView.as_view(next_url=reverse('join_bigassist', args=[kwargs['pk']]))(request, **kwargs)
        if self.get_object().registered:
            messages.warning(request, 'This invite has been finished already')
            return HttpResponseRedirect(reverse('home'))
        return super(JoinView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(JoinView, self).get_context_data(*args, **kwargs)
        invite = self.get_object()
        context['object'] = invite
        if not self.request.user.is_anonymous():
            if self.request.user.email != invite.email:
                context['message'] = "Your email is not valid for this invite."
        return context

    @transaction.atomic
    def form_valid(self, form):
        invite = self.get_object()
        user = self.request.user
        if user.email == invite.email \
           and self.request.POST.get('submit'):
            khnp_user = KHNPUser(user)
            person = khnp_user.get_person()
            # add user to the company
            cursor = connection.cursor()
            try:
                Relationship.objects.get(subject=person.pk, object=invite.org.pk, type=settings.KHNP_MEMBER_TYPE_ID)
            except Relationship.DoesNotExist:
                query = """INSERT INTO relationship
                           (subject, object, type, subjectapproved, objectapproved)
                           VALUES (%s, %s, %s, True, True)"""
                cursor.execute(query, [person.pk, invite.org.pk,
                                       settings.KHNP_MEMBER_TYPE_ID])

            if invite.type.id == settings.KHNP_MANAGER_TYPE_ID:
                try:
                    Relationship.objects.get(subject=person.pk, object=invite.org.pk, type=settings.KHNP_MANAGER_TYPE_ID)
                except Relationship.DoesNotExist:
                    query = """INSERT INTO relationship
                               (subject, object, type, subjectapproved, objectapproved)
                               VALUES (%s, %s, %s, True, True)"""
                    cursor.execute(query, [person.pk, invite.org.pk,
                                           settings.KHNP_MANAGER_TYPE_ID])

            cursor.close()

            person.firstname = form.cleaned_data['firstname']
            person.surname = form.cleaned_data['surname']
            person.displayname = person.firstname + u' ' + person.surname
            person.save()

            invite.registered = datetime.now()
            invite.save()
        return HttpResponseRedirect(reverse('home'))


@LoginRequiredDecorator
class ChooseCompany(FormView):
    form_class = ChooseCompanyForm
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        return HttpResponseForbidden()

    def get_form(self, form_class):
        user = KHNPUser(self.request.user)
        return form_class(user, **self.get_form_kwargs())

    def form_valid(self, form):
        user = KHNPUser(self.request.user)
        org = int(form.cleaned_data['company'])

        if 'marketplace_filter' in self.request.session:
            del self.request.session['marketplace_filter']

        if 'current_role' in self.request.session:
            del self.request.session['current_role']

        if org in [x.pk for x in user.get_companies()]:
            self.request.session['khnp_current_company'] = org
        else:
            messages.error(self, "You cannot change to this company")
        return super(ChooseCompany, self).form_valid(form)


@LoginRequiredDecorator
class ChooseRoleView(FormView):
    form_class = ChooseRoleForm
    success_url = reverse_lazy('home')
    template_name = 'choose_role.html'

    # def get(self, request, *args, **kwargs):
    #     return HttpResponseForbidden()

    def get_context_data(self, **kwargs):
        context = super(ChooseRoleView, self).get_context_data(**kwargs)
        context['current_role'] = self.request.session.get('current_role')
        context['org'] = KHNPUser(self.request.user).get_company()
        return context

    def get_form(self, form_class):
        user = KHNPUser(self.request.user)
        return form_class(user, **self.get_form_kwargs())

    def form_valid(self, form):
        user = KHNPUser(self.request.user).get_company()
        role = str(form.cleaned_data['role'])

        if 'marketplace_filter' in self.request.session:
            del self.request.session['marketplace_filter']

        if role in user.get_roles():
            self.request.session['current_role'] = role
        else:
            messages.error(self, "You cannot change to this role")

        return super(ChooseRoleView, self).form_valid(form)


class GenericDashboardBaseView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        user = KHNPUser(request.user)
        self.person = user.get_person()
        return super(GenericDashboardBaseView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GenericDashboardBaseView, self).get_context_data(**kwargs)
        now = datetime.now()

        today = now.date()

        context['today'] = today

        context['khnp_user'] = KHNPUser(self.request.user)
        person = context['khnp_user'].get_person()
        context['object'] = context['khnp_user'].get_company()

        context['person'] = person

        context['section'] = get_object_or_404(Section, slug='news')

        return context


@LoginRequiredDecorator
class GenericDashboardView(GenericDashboardBaseView):
    template_name = "generic/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(GenericDashboardView, self).get_context_data(**kwargs)
        # Above loads context from superclass
        # we can define specific context in here
        return context


@LoginRequiredDecorator
class OrganisationAddRoleView(ConfirmationView):
    template_name = 'confirmation.html'
    success_url = reverse_lazy('home')
    org_role = None

    description = "You are about to create a new role for your organisation. Do you want to continue?"
    layout = 'bottom'
    action = 'Yes'

    # def dispatch(self, request, *args, **kwargs):
    #     user = KHNPUser(request.user)
    #     company = user.get_company()
    #     # self.diag = company.get_diagnostic()

    #     security = CustomerDiagnosticSecurity(self.diag)
    #     # if not security.check('can_resubmit'):
    #     # if security.check('can_resubmit'):
    #     #     return HttpResponseForbidden("You can't resubmit self-assessment now.")
    #     return super(OrganisationBecomeSupplierView, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        """
        This view needs to take the current org (customer or supplier) and
        create a new org_prog_role_relationship object
        """
        user = KHNPUser(request.user)
        company = user.get_company()

        try:
            logger.info('DEMETER-21 Creating org_prog_role_rel...')
            logger.info('           User: %s' % user)
            logger.info('    User person: %s' % user.get_person())
            logger.info('Session company: %s' % self.request.session.get('khnp_current_company'))
            logger.info('        Company: %s' % company)
        except Exception, e:
            logger.error('Error in logging: %s' % str(e))

        prog_role = ProgrammeRole.objects.get(prog_id=1, role=self.org_role)

        try:
            logger.info('DEMETER-21')
            logger.info('Programme Role: %s' % prog_role)
        except Exception, e:
            logger.error('Error in logging: %s' % str(e))

        org_prog_role_rel = OrganisationProgrammeRoleRelationship.objects.create(org=company, prog_role=prog_role)
        org_prog_role_rel.save()

        if 'marketplace_filter' in request.session:
            del request.session['marketplace_filter']

        self.request.session['current_role'] = self.org_role

        return self.render_success(request)


@LoginRequiredDecorator
class OrganisationUsersWrapperView(View):

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        company = KHNPUser(user).get_company()
        if company:
            if company.is_supplier:
                return SupplierDashboardUsersView.as_view()(request, **kwargs)
            else:
                return CustomerDashboardUsersView.as_view()(request, **kwargs)
        else:
            return HttpResponseRedirect(reverse('home'))


class NewsItemView(DetailView):

    model = NewsItem
    template_name = "newsitem_view.html"
    context_object_name = "newsitem"
