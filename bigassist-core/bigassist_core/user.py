#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from bigassist_core.models import Identity, Contact, Person
from bigassist_core.models import Organisation, EmailDomain, get_email_domain_blacklist
from bigassist_core.middlewares import get_current_request
import logging
logger = logging.getLogger(__name__)


class KHNPUser(object):

    def __init__(self, user, require_authenticated=True):
        if require_authenticated:
            assert user.is_authenticated()
        self.user = user

    ## Methods for resolving a related Person object
    def _setup_identity(self):
        email = self.user.email
        # we need to create a new Identity for new or existing user
        contacts = Contact.objects.filter(value__iexact=email,
                                          type=settings.KHNP_EMAIL_TYPE_ID,
                                          person__type=settings.KHNP_PERSON_TYPE)
        if contacts.count() == 1:
            # existing contact. Create a Identity, linked to correct person
            contact = contacts[0]
            person = contact.person
        else:
            # there is no such email or more than one.
            # Create new contact and Person
            # Type: 1 - user, 2 - organization
            person, created = Person.objects.get_or_create(type_id=1, username__iexact=email,
                                                  defaults={'username': email})
            if not created:
                logger.warn('Person %d, username %s has no contact record. Setting new contact email to %s',
                             person.pk, person.username, email)
            person.set_contact('email', email)

        identity = Identity.objects.create(person=person,
                                           persona=email)
        return identity

    def setup_identity(self):
        if Identity.objects.filter(persona__iexact=self.user.email).exists():
            return
        self._setup_identity()

    def get_identity(self):
        try:
            identity = Identity.objects.get(persona__iexact=self.user.email)
        except Identity.DoesNotExist:
            identity = self._setup_identity()
        return identity

    def get_person(self):
        # FIXME - hey - cache me, please
        return self.get_identity().person

    # Methods for resolving assist organisation object
    def get_all_orgs(self):
        """ Return list of all orgs that user is a member of (PA)
        """
        person = self.get_person()
        query = """SELECT DISTINCT po.* FROM person po
                   LEFT JOIN relationship r ON r.object = po.id
                   LEFT JOIN person pp ON pp.id = r.subject
                   WHERE pp.id = %s AND r.type IN (5,6)"""
        return list(Person.objects.raw(query, [person.pk, ]))

    def get_companies(self):
        # FIXME - The organisation type needs to be set
        person = self.get_person()
        """
        # This is probably a bad way to do
        possible_types = RelationshipType.objects.filter(
            id__in=[settings.KHNP_MEMBER_TYPE_ID, settings.KHNP_MANAGER_TYPE_ID])
        rels = person.rel_subject_set.filter(type__in=possible_types)
        orgs = Person.objects.filter(rel_object_set__in=rels,
                                     organisation__isnull=False)

        """
        query = """SELECT DISTINCT o.* FROM organisation o
                   LEFT JOIN person po ON o.org_id = po.id
                   LEFT JOIN relationship r ON r.object = po.id
                   LEFT JOIN person pp ON pp.id = r.subject
                   WHERE pp.id = %s AND r.type IN (5,6)"""
        return Organisation.objects.raw(query, [person.pk, ])

    def get_company(self, request=None):
        """ Return a company
            - if does not exists, create a new one if possible
            - if not possible, return None (or raise an error?)
            - if creating, specify org_type 'customer', 'supplier'
        """

        if request is None:
            request = get_current_request()

        orgs = self.get_companies()
        count = len(list(orgs))

        if count == 0:
            return None

        if request:
            curr_org = request.session.get('khnp_current_company')
            if curr_org:
                orgs = [x for x in orgs if x.pk == curr_org]

        count = len(list(orgs))
        return count and orgs[0] or None

    def has_registered_company(self):
        return self.get_company() is not None

    def get_contact(self):
        """ Returns first record from the Contact table which is Email type.
            This record is considered as main email contact address and can be
            modified using Profile form.
            This method can be used for retrieving or modifying the contact record.
        """
        person = self.get_person()
        return person.get_contact_email()

    def get_contact_email_domain(self):
        email = self.get_person().get_contact_email()
        domain = email.split('@')[1]
        return domain.lower()

    def get_email_domains(self):
        """ Returns queryset with matching email domains """
        BLACKLIST = get_email_domain_blacklist()

        email_domain = self.get_contact_email_domain()
        if email_domain not in BLACKLIST:
            return EmailDomain.objects.filter(domain__iexact=email_domain)
        return EmailDomain.objects.none()

    def get_all_possible_orgs(self):
        pk_list = list()
        for d in self.get_email_domains():
            pk_list.append(d.object.pk)
        for o in self.get_all_orgs():
            pk_list.append(o.pk)
        return pk_list
