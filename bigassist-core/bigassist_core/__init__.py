import logging
from bigassist_core import version
from bigassist_core.middlewares import get_current_request

VERSION = version.VERSION
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))


class SQLLogFilter(logging.Filter):

    def filter(self, record):
        """ filter out SELECT queries and also django_session queries """
        sql = getattr(record, 'sql', '')
        if not sql:
            return False
        sql = sql.lower()
        if 'django_session' in sql:
            return False
        if sql.startswith('release savepoint'):
            return False
        if sql.startswith('savepoint'):
            return False
        if sql.startswith('select'):
            return False
        return True


class UserIdLogFormatter(logging.Formatter):

    def format(self, record):
        request = get_current_request()
        user = getattr(request, 'user', None)
        if user is None:
            user_id = 0
        elif user.is_anonymous():
            user_id = 0
            # user_email = ''
        else:
            user_id = user.pk
            # user_email = user.email
        setattr(record, 'user_id', user_id)
        # setattr(record, 'user_email', user_email)
        return super(UserIdLogFormatter, self).format(record)
