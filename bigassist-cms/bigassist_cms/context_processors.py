#!/usr/bin/env python
# -*- coding: utf-8 -*-


from bigassist_cms.models import Section, Page

def cms_sections(request):
    return {
        'cms_sections': Section.objects.available_in_nav()
        # 'site': SiteInformation.objects.select_related('site').get_current()
    }