#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from datetime import date as DateType
from django.conf import settings
from django.utils import formats
from django import forms
from django.core.exceptions import ValidationError
from django.forms.widgets import HiddenInput, Widget, CheckboxSelectMultiple
from bigassist_core.widgets import GroupCheckboxSelectMultiple
from django.forms.util import flatatt
from django.utils.safestring import mark_safe
from bigassist_core.models import OrgType, CustomerDiagnostic, \
    CustomerDevPri, CustomerCommType, Organisation, Identity, CommMethod, \
    CustomerProject, CustomerProjectComm, SupportArea, OrgRegion, Payment, SupplierApproval, \
    Visit, Visitor, VisitInstance, Mentor, Mentee, SupplierDeadline, Person, \
    ProgrammeRole
from bigassist_core.emails import send_ba_email
from bigassist_core.widgets import AccessibleCheckboxSelectMultiple
from tinymce.widgets import TinyMCE


POSTCODE_REGEX = r'^[A-Z]{1,2}[0-9][A-Z0-9]? ?[0-9][ABD-HJLNP-UW-Z]{2}$'
PHONE_REGEX = r'[\d|\+|\(]+[\)|\d|\s|-|\(]*[\d]$'
CHARITYNO_REGEX = r"^[\w\d]{3}\d{3,5}(-\d)?$"
COMPANYNO_REGEX = r"^[\w\d]{0,2}\d{6}$"
NCVOMEM_REGEX = r"^MEMBER[VPCE]{0,2}/\d{4,5}$"


class JustTextWidget(Widget):
    """
    Just a simple <span>value</span>
    """
    def __init__(self, attrs=None, choices=()):
        super(JustTextWidget, self).__init__(attrs)
        # if widget should support choices, pass them as init parameter
        self.choices = list(choices)

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        if isinstance(value, DateType):
            value = value.strftime(settings.DATE_FORMAT_P)
        for choice in self.choices:
            if value == choice[0]:
                value = choice[1]
                break
        final_attrs = self.build_attrs(attrs, **{'class': 'just-text-widget'})
        return mark_safe(u'<div%s>%s</div>' % (flatatt(final_attrs), value))


class PostcodeField(forms.RegexField):
    "Ensure uppercase, takes RegexField arguments"
    def to_python(self, value):
        # Return empty string if no input was given.
        if not value:
            return u''
        return value.upper()


class TwitterField(forms.CharField):
    """ Subclass of CharField to validate twitter username.
    from/based on https://github.com/dragonfly-science/seabirds.net/blob/master/seabirds/profile/forms.py"""

    def to_python(self, value):
        if value:
            return value.strip()

    def validate(self, value):
        super(TwitterField, self).validate(value)
        if value:
            if not value.startswith('@'):
                raise ValidationError('Prefix your username with @.')

            if len(value.strip().split()) > 1:
                raise ValidationError('There must be no spaces in your user name.')


class SimpleRegistrationForm(forms.Form):

    firstname = forms.CharField(max_length=150)
    surname = forms.CharField(max_length=150)


class ChooseCompanyForm(forms.Form):
    company = forms.ChoiceField(choices=[])

    def __init__(self, user, *args, **kwargs):
        super(ChooseCompanyForm, self).__init__(*args, **kwargs)
        companies = [i for i in user.get_companies()]
        self.fields['company'].choices = sorted([(x.pk, str(x)) for x in companies],key=lambda c: c[1])


class ChooseRoleForm(forms.Form):
    role = forms.ChoiceField(choices=[])

    def __init__(self, user, *args, **kwargs):
        super(ChooseRoleForm, self).__init__(*args, **kwargs)
        current_company_roles = user.get_company().get_roles()
        roles = [i for i in current_company_roles]

        from models import get_programme_roles_tuple

        self.fields['role'].label = 'Select role'
        self.fields['role'].choices = sorted([(x, dict(get_programme_roles_tuple()).get(x)) for x in roles])


class UpdateSupplierApprovalContactForm(forms.ModelForm):
    class Meta:
        model = SupplierApproval
        fields = ['contact_name', 'contact_title', 'contact_phone', 'contact_email']

    contact_name = forms.CharField()
    contact_title = forms.CharField(required=False)
    contact_phone = forms.CharField()
    contact_email = forms.EmailField()


class UpdateCustomerDiagnosticContactForm(UpdateSupplierApprovalContactForm):
    class Meta:
        model = CustomerDiagnostic
        fields = ['contact_name', 'contact_title', 'contact_phone', 'contact_email']


class OrganisationForm(forms.Form):

    displayname = forms.CharField(label="Organisation name",
                                  widget=forms.TextInput(attrs={'size': 60}))
    org_type = forms.ModelChoiceField(OrgType.objects.all(),
                                      label="Organisation type",
                                      widget=forms.RadioSelect,
                                      empty_label=None)

    ncvomem = forms.RegexField(NCVOMEM_REGEX, label="NCVO membership id", required=False, error_message="Not a valid NCVO membership id")
    companyno = forms.RegexField(COMPANYNO_REGEX, label="Company number", required=False, error_message="Not a valid company number")
    charityno = forms.RegexField(CHARITYNO_REGEX, label="Charity number", required=False, error_message="Not a valid charity number")

    # detail_1 = forms.CharField(label="Registered charity number", required=False)
    # detail_2 = forms.CharField(label="Registration number", required=False)
    # detail_3 = forms.CharField(label="Describe", required=False)
    detail = forms.CharField(label="Describe", required=False)

    main_address = forms.CharField(widget=forms.Textarea, required=False)
    postcode = PostcodeField(POSTCODE_REGEX, required=False, error_message="Not a valid UK postcode")
    registered_address = forms.CharField(widget=forms.Textarea, required=False)
    registered_postcode = PostcodeField(POSTCODE_REGEX, required=False, error_message="Not a valid UK postcode")

    tel_general = forms.RegexField(PHONE_REGEX, label="Telephone (general)", required=True, error_message="Not a valid UK phone number")
    # tel_direct = forms.RegexField(PHONE_REGEX, label="Telephone (direct)", required=True, error_message="Not a valid UK phone number")

    # email_direct = forms.EmailField(required=True, label="E-mail (direct)")
    email_general = forms.EmailField(required=True, label="E-mail (general)")

    website = forms.URLField(required=False)
    # maincontactname = forms.CharField(label="Contact name", required=True)
    # maincontacttitle = forms.CharField(label="Contact job title", required=True)

    def clean(self):
        data = self.cleaned_data
        org_type = data.get('org_type')
        if not org_type:
            return data

        # if org_type.org_type_id == 1 and not data.get('charityno'):
        #     self._errors['charityno'] = self.error_class(['This field is required'])

        if org_type.org_type_id == 2 and not data.get('companyno'):
            self._errors['companyno'] = self.error_class(['This field is required'])

        # Separate data by models
        data['person'] = {
            'firstname': data.get('firstname'),
            'surname': data.get('surname')
        }

        data['organisation'] = {
            'org_type': data.get('org_type'),
            'detail': data.get('detail'),
        }
        """When multiple programmes are introduced, we want to separate
        out tel_direct and email_direct from this form. They should
        be stored in the diagnostic or approval table"""
        data['company'] = {
            'displayname': data.get('displayname'),
            # 'maincontactname': data.get('maincontactname'),
            # 'maincontacttitle': data.get('maincontacttitle'),
            'main_address': data.get('main_address'),
            'postcode': data.get('postcode'),
            'ncvomem': data.get('ncvomem'),
            'companyno': data.get('companyno'),
            'charityno': data.get('charityno'),
            'registered_address': data.get('registered_address'),
            'registered_postcode': data.get('registered_postcode'),
            'tel_general': data.get('tel_general'),
            # 'tel_direct': data.get('tel_direct'),
            # 'email_direct': data.get('email_direct'),
            'email_general': data.get('email_general'),
            'website': data.get('website'),
            'twitter': data.get('twitter'),
            'linkedin': data.get('linkedin'),
        }

        return data


class RegistrationForm(OrganisationForm):

    firstname = forms.CharField()
    surname = forms.CharField()


class KnownUserManagerRegistrationForm(RegistrationForm):

    org_id = forms.IntegerField(widget=forms.HiddenInput())
    displayname = forms.CharField(label="Organisation name",
                                  widget=forms.TextInput(
                                    attrs={'size': 60,
                                           'readonly': 'readonly', }))


class KnownUserRegistrationForm(KnownUserManagerRegistrationForm):

    def __init__(self, *args, **kwargs):
        super(KnownUserRegistrationForm, self).__init__(*args, **kwargs)

        """Make these fields readonly, as we don't want the member to
        edit these when they set up a BA Org. Changes would be reflected
        all over NCVO/KHNP"""
        self.fields['main_address'].widget.attrs['readonly'] = 'readonly'
        self.fields['postcode'].widget.attrs['readonly'] = 'readonly'

        self.fields['registered_address'].widget.attrs['readonly'] = 'readonly'
        self.fields['registered_postcode'].widget.attrs['readonly'] = 'readonly'

        self.fields['tel_general'].widget.attrs['readonly'] = 'readonly'
        self.fields['email_general'].widget.attrs['readonly'] = 'readonly'

        self.fields['website'].widget.attrs['readonly'] = 'readonly'
        # self.fields['maincontactname'].widget.attrs['readonly'] = 'readonly'
        # self.fields['maincontacttitle'].widget.attrs['readonly'] = 'readonly'

        # Turn off required on these fields
        # self.fields['maincontactname'].required = False
        # self.fields['maincontacttitle'].required = False


class SupplierOrganisationForm(OrganisationForm):

    year_founded = forms.IntegerField(label='Year founded (YYYY)')
    twitter = TwitterField(label='Twitter username', required=False)
    linkedin = forms.URLField(label='URL to your LinkedIn account', required=False)


class OrganisationPaymentForm(SupplierOrganisationForm):

    bank_name = forms.CharField(label='Bank/Building Society')
    bank_ac_name = forms.CharField(label='Account name')
    bank_ac_no = forms.CharField(label='Account number')
    bank_sortcode = forms.CharField(label='Sort code')


class PersonProfileForm(forms.Form):

    firstname = forms.CharField()
    surname = forms.CharField()
    email = forms.EmailField(required=False)
    biography = forms.CharField(label='About you', widget=forms.Textarea, required=False)


class CustomerGetInTouchForm(forms.Form):

    recipient = forms.CharField(label='Recipient',
                                widget=JustTextWidget)
    organisation = forms.CharField(label='Organisation name')
    message = forms.CharField(label='Message', widget=forms.Textarea)

    def send_email(self, sender):
        self.cleaned_data.update(sender=sender)
        send_ba_email('01_get_in_touch', self.cleaned_data)


class CustomerGetInTouchFormAnon(CustomerGetInTouchForm):

    email = forms.EmailField(label='Your email')

    def __init__(self, *args, **kwargs):
        super(CustomerGetInTouchFormAnon, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['recipient', 'organisation', 'email', 'message']


class RecommendationEditForm(forms.ModelForm):

    # support_topics = forms.ModelMultipleChoiceField(label="Development priorities",
    #                                                queryset=SupportTopic.objects,
    #                                                widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        super(RecommendationEditForm, self).__init__(*args, **kwargs)
        self.fields['support_topics'].choices = self.topicareas_as_choices()

    def topicareas_as_choices(self):
        result = []
        for area in SupportArea.objects.all():
            result.append((area.area, [(x.pk, x.topic) for x in area.supporttopic_set.all()]))
        return result

    support_topics = forms.MultipleChoiceField(label="Development priorities",
                                               choices=[],
                                               widget=GroupCheckboxSelectMultiple)

    class Meta:
        model = CustomerDiagnostic
        fields = ('orgnl_overview', 'financial_health', 'snr_lev_commit',
                  'support_topics')


class DevelopmentPriorityForm(forms.ModelForm):

    class Meta:
        model = CustomerDevPri
        exclude = ('diag', )
        widgets = {
            'topic': forms.HiddenInput(),
            'value': forms.TextInput(),
        }


class CustomerCommunicationLogForm(forms.Form):

    organisation = forms.CharField(label='Organisation', widget=JustTextWidget)
    diagnostic = forms.ModelChoiceField(queryset=CustomerDiagnostic.objects, widget=HiddenInput)
    communication_type = forms.ModelChoiceField(label='Communication type',
                                                queryset=CustomerCommType.objects.filter(available_in_form=True))
    details = forms.CharField(label='Call minutes/email text', widget=forms.Textarea)


class ContactCompanyOwnerForm(forms.Form):

    person = forms.ModelChoiceField(queryset=Person.objects, widget=HiddenInput)
    message = forms.CharField(label='Message', widget=forms.Textarea)
    #release_email = forms.BooleanField(label='Release my email address to the recipient', required=False)


class RemoveUserForm(forms.Form):

    email = forms.EmailField()

    def clean_email(self):
        email = self.cleaned_data['email']
        if not Identity.objects.filter(persona__iexact=email).exists():
            raise forms.ValidationError('The email is not a valid Persona identity')
        return email


class XFormsFileUploadForm(forms.Form):

    file = forms.FileField(label="Select file", max_length=200)


class CustomerHostVisitPreForm(forms.Form):

    disclaimer = forms.BooleanField(label='I accept these terms and conditions')


class CustomerHostVisitForm(forms.ModelForm):

    date_type = forms.ChoiceField(choices=(('anytime', 'Anytime by arrangement'),
                                           ('fixed', 'Fixed date (pick date below)')),
                                  label='Date',
                                  widget=forms.RadioSelect)
    date = forms.DateField(label='Select date (only if "Fixed date" has been set above)',
                           required=False)

    def __init__(self, *args, **kwargs):
        super(CustomerHostVisitForm, self).__init__(*args, **kwargs)
        self.fields['topics'].choices = self.topicareas_as_choices()
        self.fields['topics'].label = 'Please categorise this visit'
        self.fields['description'].label = 'Describe the topic and the experience ' \
                                           'and expertise you are sharing. This information ' \
                                           'will be displayed to organisations looking for ' \
                                           'support in ConnectSpace.'
        self.fields['date'].widget = forms.DateInput(attrs={'class': 'datepicker', 'data-minDate': '0'})
        instance = kwargs.get('instance')
        if instance and instance.visitinstance_set.exists():
            self.fields['date'].widget = JustTextWidget()
            self.fields['date_type'].widget = JustTextWidget(choices=self.fields['date_type'].choices)
            self.fields['date_type'].required = False
        else:
            self.fields['date_type'].required = True

    def clean_date(self):
        value = self.cleaned_data['date']
        typ = self.cleaned_data.get('date_type', 'anytime' if not value else 'fixed')
        if typ == 'anytime':
            # do nothing
            return value

        if value:
            now = datetime.now()
            if value < now.date():
                raise forms.ValidationError("Visit date can't be in the past.")
        return value

    def topicareas_as_choices(self):
        result = []
        for area in SupportArea.objects.all():
            result.append((area.area, [(x.pk, x.topic) for x in area.supporttopic_set.all()]))
        return result

    class Meta:
        model = Visit
        # Specify fields due to specific order. Refs. #144
        fields = ('topics', 'title', 'description', 'address', 'region', 'date_type', 'date')
        widgets = {
            'region': CheckboxSelectMultiple(),
            'topics': GroupCheckboxSelectMultiple(render_group_tickboxes=True),
        }


class VisitGetInTouchForm(forms.ModelForm):

    class Meta:
        model = Visitor
        fields = ('contact_msg',)


class MentorGetInTouchForm(forms.ModelForm):

    class Meta:
        model = Mentee
        fields = ('contact_msg',)


class VisitorArrangeMeetingForm(forms.Form):

    date = forms.CharField(required=False)
    existing = forms.ChoiceField(label='or select from existing dates',
                                 choices=[],
                                 required=False)

    def __init__(self, *args, **kwargs):
        self.visit = visit = kwargs.pop('visit')
        super(VisitorArrangeMeetingForm, self).__init__(*args, **kwargs)
        self.fields['existing'].choices = self.visit_instances_as_choices(visit)
        if visit.date:
            del self.fields['date']
            self.fields['existing'].label = 'Available dates'
        else:
            self.fields['date'].widget = forms.DateInput(attrs={'class': 'datepicker', 'data-minDate': '0'})

    def visit_instances_as_choices(self, visit):
        result = []

        now = datetime.now()

        if visit.date and visit.date > now.date():
            strdate = visit.date.strftime(settings.DATE_FORMAT_P)
            result.append(('0', "%s" % strdate))
        else:
            result.append(('', '---'))
            for instance in visit.visitinstance_set.all():
                if instance.date:
                    strdate = instance.date.strftime(settings.DATE_FORMAT_P)
                    visitors = instance.visitor_set.count()
                    result.append((str(instance.pk), "%s (%s)" % (strdate, visitors)))
        return result

    def clean_date(self):
        value = self.cleaned_data['date']
        if value:
            try:
                now = datetime.now()
                date = datetime.strptime(value, settings.DATE_FORMAT_P).date()
                if self.visit.visitinstance_set.filter(date=date).exists():
                    raise forms.ValidationError("Visit instance at that date already exists. Please select from dropdown below.")
                if date < now.date():
                    raise forms.ValidationError("Visit date can't be in the past.")
            except ValueError:
                raise forms.ValidationError("Invalid date format")
        return value

    def clean(self):
        cleaned_data = super(VisitorArrangeMeetingForm, self).clean()
        data = self.cleaned_data
        if not data.get('date') and not data.get('existing'):
            raise forms.ValidationError("Please select either Date or existing date.")
        if data.get('date') and data.get('existing'):
            raise forms.ValidationError("Please select either Date or existing date, not both.")
        return cleaned_data


class AdminBursaryHistoryForm(forms.Form):

    CHOICES = (("last_1w", "Last week"),
               ("last_1m", "Last month"),
               ("last_2m", "Last 2 month"),
               ("last_3m", "Last 3 month"),
               ("last_6m", "Last 6 month"),
               ("last_9m", "Last 9 month"),
               ("last_1y", "Last year"),
               ("forever", "Forever"),)

    DELTAS = {
        'last_1w': {'weeks': 1},
        'last_1m': {'months': 1},
        'last_2m': {'months': 2},
        'last_3m': {'months': 3},
        'last_6m': {'months': 6},
        'last_9m': {'months': 9},
        'last_1y': {'years': 1},
    }

    period = forms.ChoiceField(choices=CHOICES)

    def clean(self):
        period = self.cleaned_data.get('period')
        if not period:
            return self.cleaned_data

        if period == 'forever':
            delta = None
        else:
            delta = self.DELTAS[period]

        self.cleaned_data['delta'] = delta
        return self.cleaned_data


class VisitorBursaryForm(forms.Form):

    purpose = forms.CharField(label='Purpose of the visit/mentoring including your aims and learning objectives (max 200 words)',
                              widget=forms.Textarea)
    benefit = forms.CharField(label='Describe what you hope to learn from this visit/mentoring (max 200 words)',
                              widget=forms.Textarea)


class VisitorFeedbackForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(VisitorFeedbackForm, self).__init__(*args, **kwargs)
        instance = kwargs['instance']
        if instance and not instance.bursary:
            if 'fb_original_purpose' in self.fields:
                del self.fields['fb_original_purpose']
        else:
            self.fields['fb_original_purpose'].required = True
            self.fields['fb_original_purpose'].label = 'Describe how the visit/mentoring opportunity '\
                                                       'met the original purpose given for sponsorship'

        self.fields['fb_score'].required = True
        self.fields['fb_comment'].required = True
        self.fields['fb_comment'].label = 'Provide feedback on learning and how this will be put into practice'

    class Meta:
        SCORE_CHOICE = (
            (5, u'5 – excellent'),
            (4, '4'),
            (3, '3'),
            (2, '2'),
            (1, u'1 – poor'),
        )

        model = Visitor
        fields = ('fb_score', 'fb_original_purpose', 'fb_comment')
        widgets = {
            'fb_score': forms.RadioSelect(choices=SCORE_CHOICE),
            'fb_comment': forms.Textarea(),
        }


class CustomerGetInTouchSupplierForm(forms.Form):

    provider = forms.CharField(label='Supplier', required=False, widget=JustTextWidget)
    report = forms.CharField(label='Recommendations',
                             required=False,
                             widget=JustTextWidget)
    message = forms.CharField(label='Project brief', widget=forms.Textarea)
    typ = forms.ModelChoiceField(CommMethod.objects.all(),
                                 label='How would you like them to get in touch?',
                                 widget=forms.RadioSelect,
                                 empty_label=None)


class SupplierMakeProject(forms.ModelForm):

    confirm_enddate_message = u'Warning, you have chosen an end date ' \
                'which is later than the customer voucher expiry date. ' \
                'The customer will not be able to use their voucher to ' \
                'pay for the project. Please check the box to confirm ' \
                'you want to continue.'

    confirm_enddate = forms.BooleanField(label='Confirm dates',error_messages={'required': confirm_enddate_message})

    class Meta:
        model = CustomerProject
        fields = ('price', 'description', 'startdate', 'enddate')
        widgets = {
            'startdate': forms.DateInput(attrs={'class': 'datepicker', 'data-minDate': '0'}),
            'enddate': forms.DateInput(attrs={'class': 'datepicker', 'data-minDate': '0'}),
        }

    def __init__(self, *args, **kwargs):
        super(SupplierMakeProject, self).__init__(*args, **kwargs)
        self.fields['price'].required = True
        self.fields['description'].required = True
        possible_vouchers = kwargs['instance'].applicable_vouchers()
        if possible_vouchers:
            voucher_exp = possible_vouchers[0].expires
            pretty_date = voucher_exp.strftime(settings.DATE_FORMAT_P)
            self.fields['startdate'].help_text = 'customer voucher expires %s' % pretty_date
            self.fields['enddate'].help_text = 'customer voucher expires %s' % pretty_date
            self.fields['enddate'].widget = forms.DateInput(attrs={'class': 'datepicker', 'data-minDate': '0', 'data-voucher-expiry': pretty_date})

    def clean_startdate(self):
        start = self.cleaned_data.get("startdate")
        if not start:
            return
        now = datetime.now()
        if start < now.date():
            raise forms.ValidationError("Start date can't be in the past")
        return start

    def clean_enddate(self):
        end = self.cleaned_data.get("enddate")
        if not end:
            return
        now = datetime.now()
        if end < now.date():
            raise forms.ValidationError("End date can't be in the past")
        return end

    def clean(self):
        cleaned_data = super(SupplierMakeProject, self).clean()
        start = cleaned_data.get("startdate")
        end = cleaned_data.get("enddate")

        if all((start, end)) and end < start:
            msg = u"End date must be after start date"
            self._errors["startdate"] = self.error_class([msg])
            self._errors["enddate"] = self.error_class([msg])

            # These fields are no longer valid. Remove them from the cleaned data.
            del cleaned_data["startdate"]
            del cleaned_data["enddate"]
        return cleaned_data


class SupplierAssessmentAppealForm(forms.Form):

    message = forms.CharField(label="Message", widget=forms.Textarea)


class CustomerProjectFeedbackForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomerProjectFeedbackForm, self).__init__(*args, **kwargs)
        self.fields['fb_score'].required = True
        self.fields['fb_comment'].required = True
        self.fields['fb_work_again'].required = True

    class Meta:
        YES_NO_CHOICE = (
            (True, 'Yes'),
            (False, 'No'),
        )

        SCORE_CHOICE = (
            (5, u'5 – excellent'),
            (4, '4'),
            (3, '3'),
            (2, '2'),
            (1, u'1 – poor'),
        )

        model = CustomerProject
        fields = ('fb_score', 'fb_comment', 'fb_work_again')
        widgets = {
            'fb_score': forms.RadioSelect(choices=SCORE_CHOICE),
            'fb_work_again': forms.RadioSelect(choices=YES_NO_CHOICE),
        }


class CustomerProjectNotCompleteForm(forms.ModelForm):

    class Meta:
        fields = ('details', )
        model = CustomerProjectComm


class CustomerProjectProblemsForm(forms.ModelForm):

    recipient = forms.CharField(label='Recipient',
                                widget=JustTextWidget)

    class Meta:
        fields = ('recipient', 'details', )
        model = CustomerProjectComm


class BaseMarketplaceFilterForm(forms.Form):

    REGIONS = OrgRegion.REGIONS

    search = forms.CharField(required=False, 
                                        widget=forms.HiddenInput(),
                                        label=None)

    regions = forms.MultipleChoiceField(choices=REGIONS,
                                        widget=AccessibleCheckboxSelectMultiple,
                                        required=False)
    sections = forms.ModelMultipleChoiceField(queryset=OrgType.objects.all(),
                                        widget=AccessibleCheckboxSelectMultiple,
                                        required=False)



class MarketplaceFilterForm(BaseMarketplaceFilterForm):

    topics = forms.ModelMultipleChoiceField(queryset=SupportArea.objects.all(),
                                            widget=AccessibleCheckboxSelectMultiple,
                                            required=False)


class TailoredMarketplaceFilterForm(BaseMarketplaceFilterForm):

    topic = forms.ModelChoiceField(queryset=SupportArea.objects.all(),
                                            widget=forms.RadioSelect,
                                            required=False,
                                            empty_label=None)


class AddPOPForm(forms.ModelForm):

    class Meta:
        fields = ('pop', )
        model = Payment


class ApprovalReviewForm(forms.ModelForm):

    BOOL_CHOICES = ((1, 'Passed'), (0, 'Failed'))

    approved = forms.TypedChoiceField(label="This submission has...", choices=BOOL_CHOICES,
                                         empty_value=None, widget=forms.RadioSelect)

    topics = forms.ModelMultipleChoiceField(queryset=None,
                                            widget=forms.CheckboxSelectMultiple,
                                            required=False)

    class Meta:
        model = SupplierApproval
        fields = ('approved', 'topics', 'panel_text')

    def __init__(self, approval, *args, **kwargs):
        super(ApprovalReviewForm, self).__init__(*args, **kwargs)

        if approval.resubmission():
            self.fields['approved'].choices = ((1, 'Passed'),
                                          (0, 'Failed - revert to previous details'),
                                          (-1, 'Failed - remove organisation from assist'),)
        self.fields['panel_text'].required = True

        self.fields['topics'].queryset = approval.approvaltopic_set.all()

    def clean(self):
        cleaned_data = super(ApprovalReviewForm, self).clean()

        if 'approved' in cleaned_data:
            approved = int(cleaned_data.pop('approved'))
            cleaned_data['is_approved'] = approved == 1
            cleaned_data['to_be_removed'] = approved == -1

        return cleaned_data


class CustomerBecomeMentorPreForm(forms.Form):

    disclaimer = forms.BooleanField(label='I accept these terms and conditions')


class CustomerBecomeMentorForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomerBecomeMentorForm, self).__init__(*args, **kwargs)
        self.fields['topics'].choices = self.topicareas_as_choices()
        self.fields['topics'].label = 'Please categorise this mentoring opportunity'

    def topicareas_as_choices(self):
        result = []
        for area in SupportArea.objects.all():
            result.append((area.area, [(x.pk, x.topic) for x in area.supporttopic_set.all()]))
        return result

    class Meta:
        model = Mentor
        fields = ('topics', 'mentor_job_title', 'description', 'region')
        widgets = {
            'region': CheckboxSelectMultiple(),
            'topics': GroupCheckboxSelectMultiple(render_group_tickboxes=True),
        }


class AdminDeadlineForm(forms.ModelForm):

    class Meta:
        model = SupplierDeadline
        fields = ('deadline', )
        widgets = {
            'deadline': forms.DateInput(attrs={'class': 'datepicker'})
        }


class BatchSizeField(forms.ChoiceField):

    CHOICES = ((5, 'Page size: 5'), (10, 'Page size: 10'), (20, 'Page size: 20'), (0, 'All on one page'))

    def __init__(self, choices=(), required=True, widget=None, label=None,
             initial=10, help_text=None, *args, **kwargs):
        if not choices:
            choices = self.CHOICES
        super(BatchSizeField, self).__init__(choices=choices, required=required, widget=widget, label=label,
                                    initial=initial, help_text=help_text, *args, **kwargs)

    def clean(self, value):
        value = super(BatchSizeField, self).clean(value)
        try:
            value = int(value)
        except (ValueError, TypeError):
            value = 10  # return initial if wrong

        if value == 0:
            value = None

        return value


class AdminDatabaseFilterForm(forms.Form):

    ORG_TYPES = (('', 'Any org type'),
                 ('supplier', 'Supplier'),
                 ('customer', 'Customer'),
                 ('visithost', 'Visit host'),
                 ('mentor', 'Mentor'))

    ORDERS = (('-created', 'Sort by creation date (newest first)'),
              ('created', 'Sort by creation date (oldest first)'),
              ('displayname', 'Sort by display name (A-Z)'),
              ('-displayname', 'Sort by display name (Z-A)'))

    text = forms.CharField(required=False)
    org_type = forms.ChoiceField(choices=ORG_TYPES, required=False)
    order_by = forms.ChoiceField(choices=ORDERS, required=False)
    #batch_size = BatchSizeField(required=False)


class BatchSizeForm(forms.Form):

    batch_size = BatchSizeField(required=False)

