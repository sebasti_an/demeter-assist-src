#!/usr/bin/env python
# -*- coding: utf-8 -*-

VERSION = (0, 0, 1)
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))
