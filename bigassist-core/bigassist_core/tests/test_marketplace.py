from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from bigassist_core.user import KHNPUser
from bigassist_core.models import Organisation, CommMethod, SupportTopic, OrgType, \
    DevPriLev, DevSupportType, Visit
from bigassist_core.views.supplier import initialize_supplier_approval
from bigassist_core.views.customer import initialize_customer_diagnostic
from base import TestCase

EMAIL = 'test@khnp.org'
CUST_EMAIL = 'test_cust@khnp.org'
COMPANY = 'Private company'


class MarketplaceTest(TestCase):

    fixtures = ['types', 'xforms', 'mails']

    def tearDown(self):
        User.objects.all().delete()
        Organisation.objects.all().delete()

    def _setup_users(self):
        self.comm_method = CommMethod.objects.create(comm_method='Email')
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.topic = SupportTopic.objects.all()[0]
        org_type = OrgType.objects.get(org_type=COMPANY)
        pri_lev = DevPriLev.objects.get(pk=1)
        support_type = DevSupportType.objects.get(pk=1)

        user = KHNPUser(self.user)
        person = user.get_person()
        self.company = Organisation.objects.register(person,
                                                defaults={'is_supplier': False})
        initialize_customer_diagnostic(self.company)
        diag = self.company.get_diagnostic()
        diag.set_state('approved')
        diag.customerdevpri_set.create(topic=self.topic,
                                       pri_lev=pri_lev,
                                       support_type=support_type,
                                       value=1000)

        suppl_user = User.objects.create_user(username='testsuppl', email='test2@khnp.org', password='secret')
        suser = KHNPUser(suppl_user, require_authenticated=False)
        self.scompany = Organisation.objects.register(suser.get_person(),
                                                 defaults={'is_supplier': True,
                                                           'org_type': org_type})
        initialize_supplier_approval(self.scompany)
        assessment = self.scompany.get_diagnostic()
        assessment.set_state('approved')
        self.scompany.suppliertopics_set.create(topic=self.topic, is_approved=True)
        self.scompany.orgregion_set.create(region="NE")

    def test_anonymous(self):
        url = reverse('marketplace')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'panel_general')
        self.assertNotContains(response, 'panel_tailored')

    def test_customer(self):
        self._setup_users()
        self.client.login(username='testuser', password='secret')

        url = reverse('marketplace')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'panel_general')
        self.assertContains(response, 'panel_tailored')

        choices = response.context['tailored_form']['topic'].field.choices

        self.assertEqual(len(choices), 1)
        self.assertEqual(choices.queryset[0], self.topic.area)
        # set filter
        data = {
            'topic': [self.topic.area.pk, ],
            'tailored': 1,
        }
        mp_url = reverse('marketplace')
        response = self.client.post(mp_url, data)

        self.assertEqual(self.client.session['marketplace_filter']['topics'], [self.topic.area.pk, ])

    def test_not_approved_customer(self):
        user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        khnp_user = KHNPUser(user)
        person = khnp_user.get_person()
        self.company = Organisation.objects.register(person,
                                                defaults={'is_supplier': False})

        self.client.login(username='testuser', password='secret')
        url = reverse('marketplace')
        response = self.client.get(url)
        self.assertContains(response, 'panel_general')
        self.assertNotContains(response, 'panel_tailored')

    def test_library(self):
        url = reverse('librarygroup')

        # set filter
        data = {
            'topics': [1, ]
        }

        mp_url = reverse('marketplace')
        response = self.client.post(mp_url, data)

        self.assertEqual(self.client.session['marketplace_filter']['topics'], [1, ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_experts(self):
        self._setup_users()

        topic_other = SupportTopic.objects.exclude(area=self.topic.area)[0]

        url = reverse('expertsgroup')
        response = self.client.get(url)

        profile_url = reverse('supplier_full_profile', args=(self.scompany.pk, ))
        self.assertContains(response, profile_url)
        org_type = OrgType.objects.get(org_type=COMPANY)

        data = {
            'topics': [self.topic.area.pk, ],
            'region': ['NE', 'NW'],
            'sections': [org_type.pk, ]
        }

        response = self.client.get(url)
        mp_url = reverse('marketplace')
        response = self.client.post(mp_url, data)

        response = self.client.get(url)
        self.assertContains(response, profile_url)

        data = {
            'topics': [topic_other.area.pk, ]
        }

        response = self.client.get(url)
        mp_url = reverse('marketplace')
        response = self.client.post(mp_url, data)

        response = self.client.get(url)
        self.assertNotContains(response, profile_url)

    def test_visits(self):
        self._setup_users()
        Visit.objects.create(host=self.company, title='Test visit', description='Test visit')
        # I'm anonymous. The listing must not fail
        response = self.client.get(reverse('marketplace'))
        self.assertContains(response, '<h2>Connect<span>Space</span> <span class="count">(0)</span></h2>', html=True)
