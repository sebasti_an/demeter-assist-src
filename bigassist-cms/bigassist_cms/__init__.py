#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bigassist_cms import version

VERSION = version.VERSION
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))
