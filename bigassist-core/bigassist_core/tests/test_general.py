from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core import mail

from base import TestCase

from bigassist_core.user import KHNPUser
from bigassist_core.models import OrgType, Organisation, NewsItem
from bigassist_core.middlewares import clear_current_request

EMAIL = 'test@khnp.org'


class RegisterTest(TestCase):

    fixtures = ['types', 'mails']

    def setUp(self):
        super(RegisterTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')

    def tearDown(self):
        User.objects.all().delete()
        clear_current_request()

    def test_register_customer(self):
        url = reverse("customer_register")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        signin_url = reverse("customer_signin")
        self.assertTrue(signin_url in response['Location'])

        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = {
            'firstname': 'John',
            'surname': 'Doe',
            'displayname': 'My organisation',
            'org_type': OrgType.objects.get(org_type="Private company").pk,
            'companyno': 111,
            'main_address': 'Main Address',
            'registered_address': 'Registered Address',
            'registered_postcode': 'SE1 9PG',
            'tel_direct': '22222222',
            'email_direct': 'test@example.com',
            'email_general': 'general@example.com',
            'website': 'http://example.com/',
            'contact_name': 'Contact',
            'maincontactname': 'Contact'
        }

        data['postcode'] = '123456789'
        response = self.client.post(url, data)
        self.failUnless('Not a valid UK postcode' in response.content)

        data['tel_general'] = '12345 CX'
        response = self.client.post(url, data)
        self.failUnless('Not a valid UK phone number' in response.content)

        data['tel_general'] = '11111111'
        data['postcode'] = 'SE1 9PG'
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 302)

        user = KHNPUser(self.user)
        person = user.get_person()
        self.assertEqual(person.firstname, "John")
        self.assertEqual(person.surname, "Doe")
        self.assertEqual(person.registered_by.code, "ba")

        org = user.get_company()
        company = org.org
        self.assertEqual(company.displayname, "My organisation")
        self.assertEqual(org.is_supplier, False)
        self.assertNotEqual(person.displayname, "My organisation")

        self.assertEqual(company.get_contact('tel_general'), data['tel_general'])
        self.assertEqual(company.get_contact('tel_direct'), data['tel_direct'])
        self.assertEqual(company.get_contact('email_direct'), data['email_direct'])
        self.assertEqual(company.get_contact('email_general'), data['email_general'])
        self.assertEqual(company.get_contact('website'), data['website'])
        self.assertEqual(company.get_address().postcode, data['postcode'])
        self.assertEqual(company.get_address(use=settings.KHNP_ADDRESS_REGISTERED).postcode, data['registered_postcode'])

    # def test_lookup_company(self):
    #     url = reverse('lookup_company', args=['04091032'])
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, 200)
    #     self.failUnless('FRY-IT LIMITED' in response.content)

    # def test_lookup_charity(self):
    #     url = reverse('lookup_charity', args=['276035'])
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, 200)
    #     self.failUnless('PLAN INTERNATIONAL UK' in response.content)

    def test_register_duplicate(self):
        # register a company first
        url = reverse("customer_register")
        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = {
            'firstname': 'John',
            'surname': 'Doe',
            'displayname': 'My organisation',
            'org_type': OrgType.objects.get(org_type="Private company").pk,
            'companyno': 111,
            'main_address': 'Main Address',
            'postcode': 'SE1 9PG',
            'registered_address': 'Registered Address',
            'registered_postcode': 'SE1 9PG',
            'tel_general': '11111111',
            'tel_direct': '22222222',
            'email_direct': 'test@example.com',
            'website': 'http://example.com/',
            'contact_name': 'Contact',
            'maincontactname': 'Contact',
        }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        org = Organisation.objects.get(org__companyno='111')
        url = reverse('contact_company_owner', args=[org.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless('value="Send message"' in response.content)
        #self.failUnless('Release my email address to the recipient' in response.content)
        # release email is not added to the message so it should not appear on mail message
        data = dict(
            person=org.org.pk,
            message='Hello world',
        )
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.failUnless('Organisation has been contacted by email' in response.content)
        # first email is registration email thus outbox has 2 emails now
        self.failUnlessEqual(len(mail.outbox), 2)
        mailtext = mail.outbox[-1]
        self.failUnless(data['message'] in mailtext.body)
        self.failUnless(self.user.email in mailtext.body)

        data = dict(
            person=org.org.pk,
            message='Hello world',
            release_email='1'
        )
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.failUnless('Organisation has been contacted by email' in response.content)
        self.failUnlessEqual(len(mail.outbox), 3)
        mailtext = mail.outbox[-1]
        self.failUnless(data['message'] in mailtext.body)
        self.failUnless(self.user.email in mailtext.body)

    def test_switch_company(self):
        url = reverse('home')

        self.client.login(username='testuser', password='secret')

        khnpuser = KHNPUser(self.user)
        person = khnpuser.get_person()

        suppl = Organisation.objects.register(person,
                                      defaults={'is_supplier': True,
                                                'org_type_id': 2})
        suppl.org.displayname = "Supplier org"
        suppl.org.save()

        response = self.client.get(url)
        self.assertFalse('choose_company_form' in response.context)

        cust = Organisation.objects.register(person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 2})
        cust.org.displayname = "Customer org"
        cust.org.save()

        self.assertEqual(len(list(khnpuser.get_companies())), 2)

        response = self.client.get(url)

        self.assertTrue(response.context['choose_company_form'])

        change_url = reverse("choose_company")
        data = {'company': suppl.pk}
        self.client.post(change_url, data)

        self.assertEqual(khnpuser.get_company(), suppl)
        response = self.client.get(url)
        self.assertContains(response, reverse("supplier_org_profile"))

        data = {'company': cust.pk}
        self.client.post(change_url, data)

        self.assertEqual(khnpuser.get_company(), cust)
        response = self.client.get(url)
        self.assertContains(response, reverse("customer_org_profile"))


class NewsItemTest(TestCase):

    def test_newsitems(self):
        newsitem = NewsItem.objects.create(title="Test news item",
                                           slug="test",
                                           content="Example <b>content</b>")

        response = self.client.get(newsitem.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test news item")
        # This tests the html is rendered using "safe"
        self.assertContains(response, "<b>content</b>")

        response = self.client.get(reverse('home'))
        self.assertContains(response, "Test news item")
        self.assertContains(response, newsitem.get_absolute_url())
