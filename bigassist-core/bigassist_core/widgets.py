from itertools import chain
from django.forms import widgets
from django.utils.encoding import force_unicode
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe


class GroupCheckboxSelectMultiple(widgets.CheckboxSelectMultiple):

    class Media:
        js = ('js/groupcheckboxselectmultiple.js',)

    def __init__(self, attrs=None, choices=(), render_group_tickboxes=False):
        super(GroupCheckboxSelectMultiple, self).__init__(attrs, choices)
        self.render_group_tickboxes = render_group_tickboxes

    def render(self, name, value, attrs=None, choices=()):
        """ this is almost copied from CheckboxSelectMultiple but expects option_label
            is a list/tuple and option_value is label(s) of groups
            Group 1
                 checkbox1
                 checkbox2
            Group 2
                 checkbox3
                 checkbox4
        """
        if value is None:
            value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u'<dl class="group-checkbox-select-multiple">']
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        group_id = i = 0
        for group_id, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # render group headers a DT
            tickbox = ''
            css_class = "%s_group_%s" % (name, group_id)
            final_attrs.update({'class': css_class})
            if self.render_group_tickboxes:
                tickbox = ' <input type="checkbox" data-itemsclass="%(class)s" id="id_%(class)s" />' % final_attrs
            output.append(u'<dt><label>%s%s</label></dt>' % (tickbox, option_value))
            for (o_value, o_label) in option_label:
                # If an ID attribute was given, add a numeric index as a suffix,
                # so that the checkboxes don't all have the same ID attribute.
                if has_id:
                    final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                    label_for = u' for="%s"' % final_attrs['id']
                else:
                    label_for = ''

                o_value = str(o_value)
                cb = widgets.CheckboxInput(final_attrs, check_test=lambda value: o_value in str_values)
                option_value = force_unicode(o_value)
                rendered_cb = cb.render(name, o_value)
                o_label = conditional_escape(force_unicode(o_label))
                output.append(u'<dd><label%s>%s %s</label></dd>' % (label_for, rendered_cb, o_label))
                i += 1

        output.append(u'</dl>')
        return mark_safe(u'\n'.join(output))


class AccessibleCheckboxSelectMultiple(widgets.CheckboxSelectMultiple):

    def id_for_label(self, id_):
        # see https://it.fry-it.com/text-matters/dev/129
        return ""
