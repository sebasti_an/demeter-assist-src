#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from django.contrib.auth.models import User
from django.db import models, connection
from django.db import connections, transaction
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.datastructures import SortedDict
import django_tables2 as tables
from dateutil.relativedelta import relativedelta
import logging
logger = logging.getLogger('default')

XFORM_TYPES = (
    ('P', u'Customer prequal form'),
    ('C', u'Customer self-assessment'),
    ('CA', u'Customer self-assessment (with comments)'),
    ('CRA', u'Customer self-assessment resubmission admin (with comments)'),
    ('S', u'Supplier assessment'),
    ('SA', u'Supplier admin (with comments)'),
    ('SRA', u'Supplier reapplication admin (with comments)'),
)


def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


class GenericReportTable(tables.Table):

    def __init__(self, columns, data, *args, **kwargs):
        super(GenericReportTable, self).__init__(data, *args, **kwargs)
        # weird init, because tables.Table does not support custom columns
        # It supports fixed attributes or model based columns only
        self.base_columns = SortedDict()
        for c in columns:
            self.base_columns[c] = tables.Column(verbose_name=c)
        self._sequence = tables.utils.Sequence()
        self._sequence.expand(self.base_columns.keys())
        self.columns = tables.columns.BoundColumns(self)

    # class Meta:
    #     attrs = {"class": "paleblue"}


class MailTemplate(models.Model):

    code = models.CharField(max_length=150, unique=True)
    subject = models.CharField(max_length=255)
    message = models.TextField(default='{% load absurl %}')

    class Meta:
        ordering = ['code']
        db_table = u'mail_templates'

    def __unicode__(self):
        return self.code


class ContactType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50, blank=True)

    class Meta:
        db_table = u'contacttype'

    def __unicode__(self):
        return self.name


class PersonType(models.Model):
    id = models.AutoField('ID', primary_key=True)
    name = models.CharField('Name', max_length=50)

    class Meta:
        verbose_name = 'Person type'
        verbose_name_plural = 'Person types'
        db_table = 'persontype'

    def __unicode__(self):
        return self.name


class ApplicationsManager(models.Manager):

    def bigassist_app(self):
        app, created = self.get_or_create(code="ba", defaults={'name': 'Big Assist'})
        return app


class Applications(models.Model):

    code = models.CharField(primary_key=True, max_length=10)
    name = models.CharField(max_length=100)

    objects = ApplicationsManager()

    class Meta:
        db_table = 'register_source'

    def __unicode__(self):
        return self.name


class Person(models.Model):

    id = models.AutoField('ID', primary_key=True)
    firstname = models.CharField('First name', max_length=50)
    surname = models.CharField('Surname', max_length=50)
    displayname = models.CharField('Display name', max_length=150)
    type = models.ForeignKey(PersonType, db_column='type')
    username = models.CharField('Username', max_length=150, default=None, null=True, blank=True, unique=True)
    shortbiography = models.TextField('Short biography', null=True, blank=True, default=u'')
    biography = models.TextField('About you', null=True, blank=True, default=u'')
    # maincontactname = models.CharField(max_length=255, blank=True)
    # maincontacttitle = models.CharField(max_length=255, blank=True)

    companyno = models.CharField('Company number', max_length=50, null=True, blank=True)
    charityno = models.CharField('Charity number', max_length=50, null=True, blank=True)
    # ncvomem = models.CharField('NVCO membership id', max_length=16, null=True, blank=True)

    registered_by = models.ForeignKey(Applications, null=True, blank=True, default=Applications.objects.bigassist_app)
    region = models.CharField('Region', max_length=50, null=True)

    class Meta:
        verbose_name = 'Person'
        verbose_name_plural = 'Persons'
        db_table = 'person'

    def __unicode__(self):
        if self.username and (not self.displayname) and ('@' in self.username):
            # do not expose email addresses - mainly in Forum
            parts = self.username.split('@')
            first = '...' if len(parts[0]) <= 5 else parts[0][:3] + '...'
            last = '...' + (parts[1][-8:] if len(parts[1]) >= 13 else parts[1][-6:])
            return unicode(first + u'@' + last)
        else:
            return unicode(self.displayname or self.id)

    def get_contact_types(self):
        if self.type_id == settings.KHNP_PERSON_TYPE:
            return settings.PERSON_CONTACT_TYPES
        else:
            return settings.CONTACT_TYPES

    def set_contact(self, contact_type, value):
        contact_types = self.get_contact_types()

        assert contact_type in contact_types, "Unknown contact type"
        ctype = contact_types[contact_type]
        type_obj = ContactType.objects.get(pk=ctype['type'])
        use_obj = Use.objects.get(pk=ctype['use'])
        if len(value) > 200:
            logger.error('Unable to store contact value %s for person id %s. Value is too long. Truncating to 200 chars.' % (value, self.pk))
            value = value[:200]

        try:
            contact, created = self.contact_set.get_or_create(type=type_obj,
                                                          use=use_obj,
                                                          defaults={'value': value})
        except Contact.MultipleObjectsReturned:
            contact = self.contact_set.get_or_create(type=type_obj, use=use_obj)[0]

        if not created:
            contact.value = value
            contact.save()

    def get_contact(self, contact_type):
        contact_types = self.get_contact_types()
        assert contact_type in contact_types, "Unknown contact type"

        ctype = contact_types[contact_type]
        result = self.contact_set.filter(type__id=ctype['type'],
                                         use__id=ctype['use']).order_by('id').all()
        if result:
            return result[0].value

    def del_contact(self, contact_type):
        contact_types = self.get_contact_types()
        assert contact_type in contact_types, "Unknown contact type"

        ctype = contact_types[contact_type]
        result = self.contact_set.filter(type__id=ctype['type'],
                                        use__id=ctype['use']).order_by('id').all()
        if result:
            return result[0].delete()

    def get_contact_email(self):
        """ Returns contact email regardless of person type """
        if self.type_id == settings.KHNP_PERSON_TYPE:
            email = self.get_contact('email')
            if not email:
                email = self.get_contact('fallback_email')
            return email
        else:
            return self.get_contact('email_general')

    def get_contact_website(self):
        return self.get_contact('website') or None

    def get_contact_twitter(self):
        return self.get_contact('twitter') or None

    def get_contact_linkedin(self):
        return self.get_contact('linkedin') or None

    def get_twitter_link(self):
        return 'https://twitter.com/' + self.get_contact_twitter()

    def set_address(self, use, **kwargs):
        use_obj = Use.objects.get(pk=use)
        address, created = self.address_set.get_or_create(use=use_obj, defaults=kwargs)

        if not created:
            for key, val in kwargs.items():
                setattr(address, key, val)
            address.save()

    def get_address(self, use=settings.KHNP_ADDRESS_MAIN):
        result = self.address_set.filter(use__id=use)
        if result:
            return result[0]

    def set_additional(self, data):
        if 'main_address' in data or 'postcode' in data:
            addr = {}
            addr['address'] = data.get('main_address')
            addr['postcode'] = data.get('postcode')
            self.set_address(use=settings.KHNP_ADDRESS_MAIN, **addr)

        if 'registered_address' in data or 'registered_postcode' in data:
            addr = {}
            addr['address'] = data.get('registered_address')
            addr['postcode'] = data.get('registered_postcode')
            self.set_address(use=settings.KHNP_ADDRESS_REGISTERED, **addr)

        contact_types = self.get_contact_types()
        for key, val in data.items():
            if val and key in contact_types:
                self.set_contact(key, val)

        self.ncvomem = data.get('ncvomem')
        self.companyno = data.get('companyno')
        self.charityno = data.get('charityno')

    def get_additional(self):
        """ Helper method to receive additional data from other tables """
        data = {}

        address = self.get_address(use=settings.KHNP_ADDRESS_MAIN)
        if address:
            data['main_address'] = address.address
            data['postcode'] = address.postcode

        address = self.get_address(use=settings.KHNP_ADDRESS_REGISTERED)
        if address:
            data['registered_address'] = address.address
            data['registered_postcode'] = address.postcode

        contact_types = self.get_contact_types()
        for ctype in contact_types.keys():
            data[ctype] = self.get_contact(ctype)

        # data['ncvomem'] = self.ncvomem
        data['companyno'] = self.companyno
        data['charityno'] = self.charityno

        return data

    def get_members(self, only_managers=False):
        r_types = [settings.KHNP_MANAGER_TYPE_ID]
        if not only_managers:
            r_types.append(settings.KHNP_MEMBER_TYPE_ID)

        query = """SELECT distinct pp.* from person pp
                   LEFT JOIN relationship r on r.subject = pp.id
                   LEFT JOIN person po on po.id = r.object
                   WHERE po.id = %%s and r.type in (%s)
                """ % ','.join([str(x) for x in r_types])
        persons = Person.objects.raw(query, [self.pk])
        return list(persons)

    def get_contact_emails(self):
        cc = set()
        recipient = self.get_contact_email()
        for member in self.get_members(only_managers=True):
            contact = member.get_contact_email()
            if contact and contact != recipient:
                cc.add(contact)
        return dict(main=recipient,
                    members=cc)


class Identity(models.Model):
    id = models.AutoField(primary_key=True)
    persona = models.CharField('Persona', max_length=150, db_index=True)
    person = models.ForeignKey(Person, db_column='khnp_id')

    class Meta:
        verbose_name = 'Identity (Persona)'
        verbose_name_plural = 'Identities (Persona)'
        db_table = 'identity'

    def __unicode__(self):
        return self.persona


class Use(models.Model):
    name = models.CharField(max_length=50, blank=True)

    class Meta:
        db_table = u'use'

    def __unicode__(self):
        return self.name


class Contact(models.Model):
    type = models.ForeignKey(ContactType, db_column='type')
    value = models.CharField(max_length=200, blank=True)
    person = models.ForeignKey(Person, null=True, db_column='person', blank=True)
    use = models.ForeignKey(Use, null=True, db_column='use', blank=True)
    created = models.DateTimeField(null=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = 'Contact'
        verbose_name_plural = 'Contacts'
        db_table = 'contact'

    def __unicode__(self):
        return unicode(self.person) + u' - ' + self.value


class LocalAuthority(models.Model):
    id = models.CharField(max_length=4, primary_key=True)
    name = models.CharField(max_length=60)
    region = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = 'Local authority'
        verbose_name_plural = 'Local authorities'
        db_table = 'local_authority'

    def __unicode__(self):
        return self.name


class Address(models.Model):
    address = models.TextField(blank=True)
    postcode = models.CharField(max_length=8, blank=True)
    country = models.CharField(max_length=2, blank=True, null=True, default='GB')
    person = models.ForeignKey(Person, db_column='person')
    use = models.ForeignKey(Use, db_column='use')
    local_authority = models.ForeignKey(LocalAuthority, null=True, blank=True)
    created = models.DateTimeField(null=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'address'

    def formatted_address(self):
        ## for use on HTML pages with address|linebreaksbr
        result = []
        if self.address:
            result.extend(self.address.split(','))
        if self.postcode:
            result.append(self.postcode)
        if self.country:
            result.append(self.country)
        return '\n'.join(result)


class RelationshipType(models.Model):
    """ This does not really work as we need primary key """

    name = models.CharField(max_length=150)
    reversename = models.CharField(max_length=150)
    subjecttype = models.ForeignKey(PersonType,
                                    db_column="subjecttype",
                                    related_name="subjecttype_set")
    objecttype = models.ForeignKey(PersonType,
                                   db_column="objecttype",
                                   related_name="objecttype_set")

    class Meta:
        verbose_name = 'Relationship type'
        verbose_name_plural = 'Relationship types'
        db_table = 'relationshiptype'

    def __unicode__(self):
        return self.name


class Relationship(models.Model):
    """ Note: The primary key is a dummy one, there is a composite key
              in postgres and it is handled manually in our TestCase
    """

    subject = models.ForeignKey(Person, db_column="subject",
                                related_name="rel_subject_set",
                                primary_key=True)
    object = models.ForeignKey(Person, db_column="object",
                               related_name="rel_object_set")
    type = models.ForeignKey(RelationshipType, db_column="type")

    subjectapproved = models.BooleanField(default=False)
    objectapproved = models.BooleanField(default=False)

    details = models.CharField(max_length=255, blank=True, null=True)

    # There is no auto_add as it is already specified in the db
    created = models.DateTimeField(null=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'relationship'

    def __unicode__(self):
        return "%s is %s of %s" % (self.subject, self.type, self.object)


class OrgType(models.Model):

    org_type_id = models.AutoField('ID', primary_key=True)
    org_type = models.CharField('Organisation type', max_length=100)

    class Meta:
        ordering = ['org_type_id']
        verbose_name = 'Organisation type'
        verbose_name_plural = 'Organisation types'
        db_table = 'org_type'

    def __unicode__(self):
        return self.org_type


class OrganisationManager(models.Manager):

    def register(self, manager, defaults):
        # assert 'is_supplier' in defaults, \
        #     "Cannot create organisation without is_supplier"
        # Use org_role instead of is_supplier
        assert 'org_role' in defaults, \
            "Cannot create organisation without org_role"

        assert isinstance(manager, Person), "Manager has to be a Person instance"

        # Lookup or create person - type Organisation
        person_type = PersonType.objects.get(pk=settings.KHNP_COMPANY_TYPE)
        company = Person.objects.create(type=person_type)

        # Setup relationships
        cursor = connection.cursor()

        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [manager.pk, company.pk,
                               settings.KHNP_MANAGER_TYPE_ID])

        # Also setup a Member relationship - https://it.fry-it.com/text-matters/dev/020/#i36
        query = """INSERT INTO relationship
                   (subject, object, type, subjectapproved, objectapproved)
                   VALUES (%s, %s, %s, True, True)"""
        cursor.execute(query, [manager.pk, company.pk,
                               settings.KHNP_MEMBER_TYPE_ID])

        org = self.create(org=company)
        # We should create a new relationship object here.
        # First get the org role instance (prog_id=1 is BIGASSIST)
        # FIXME, when introducing new programmes, remove hard-coded prog_id here
        # and base it on value in urls.py
        if defaults.get('org_role') is not None:
            # Only create relationship if org_role is selected.
            # If using generic registration, we do not create the rel. yet
            prog_role = ProgrammeRole.objects.get(prog_id=1, role=defaults.get('org_role'))
            org_prog_role_rel, opr_created = OrganisationProgrammeRoleRelationship.objects.get_or_create(org=org, prog_role=prog_role)
            if not opr_created:
                org_prog_role_rel.save()

        for key, val in defaults.iteritems():
            if hasattr(org, key):
                setattr(org, key, val)
        org.save()
        return org

    def update_organisation(self, org_id, manager, defaults):
        # assert 'is_supplier' in defaults, \
        #     "Cannot create organisation without is_supplier"
        # Use org_role instead of is_supplier
        assert 'org_role' in defaults, \
            "Cannot update organisation without org_role"

        assert isinstance(manager, Person), "Manager has to be a Person instance"

        company = Person.objects.get(pk=org_id)

        org = self.create(org=company)

        if defaults.get('org_role') is not None:
            # Only create relationship if org_role is selected.
            # If using generic registration, we do not create the rel. yet
            prog_role = ProgrammeRole.objects.get(prog_id=1, role=defaults.get('org_role'))
            org_prog_role_rel, opr_created = OrganisationProgrammeRoleRelationship.objects.get_or_create(org=org, prog_role=prog_role)
            if not opr_created:
                org_prog_role_rel.save()

        for key, val in defaults.iteritems():
            if hasattr(org, key):
                setattr(org, key, val)
        org.save()
        return org

    def suppliers(self):
        # return self.get_queryset().filter(is_supplier=True)
        return self.get_queryset().filter(prog__role='supplier')

    def approved_suppliers(self):
        return self.suppliers().filter(supplierapproval__is_approved=True, supplierapproval__expertise__isnull=False).distinct()

    def customers(self):
        # return self.get_queryset().filter(is_supplier=False)
        return self.get_queryset().filter(prog__role='customer')

    def approved_customers(self):
        return self.customers().filter(customerdiagnostic__is_approved=True).distinct()

    # Method for sorting complete project state
    def approved_suppliers_with_project_state_completed(self):
        return self.approved_suppliers().filter(supplier_projects__state='completed').distinct()

    def approved_suppliers_with_project_state_not_completed(self):
        return self.approved_suppliers().exclude(supplier_projects__state='completed').distinct()

    def approved_suppliers_with_project_feedback_given(self):
        return self.approved_suppliers().filter(supplier_projects__feedback__isnull=False, supplier_projects__state='completed').distinct()

    def approved_suppliers_with_project_feedback_not_given(self):
        return self.approved_suppliers().exclude(supplier_projects__feedback__isnull=False).distinct()


class Organisation(models.Model):

    STATES = {
        'not_started': 'Not started',
        'in_progress': 'In progress',
        'review_in_progress': 'Review in progress',
        'submitted': 'Submitted',
        'closed': 'Closed',
    }

    # This should link to a Person of a type 'Organisation'
    org = models.ForeignKey(Person, primary_key=True, db_column='org_id', related_name='my_org')
    org_type = models.ForeignKey(OrgType, null=True, blank=True)
    detail = models.CharField('Detail', max_length=255, blank=True)
    is_social_ent = models.BooleanField('Social Enterprise?', default=False)
    # is_supplier = models.BooleanField('BIG Assist supplier', default=False)
    #contact_name = models.CharField('Contact name', max_length=150, null=True, blank=True)
    #contact_email = models.EmailField('Contact email', null=True, blank=True)
    #contact_tel = models.EmailField('Contact telephone', null=True, blank=True)
    created = models.DateField('Created at', auto_now_add=True, editable=False)
    updated = models.DateField('Modified at', auto_now=True, editable=False)

    bank_name = models.CharField('Bank/Building Society', max_length=255)
    bank_ac_name = models.CharField('Account name', max_length=255)
    bank_ac_no = models.CharField('Account number', max_length=255)
    bank_sortcode = models.CharField('Sort code', max_length=255)

    vat_no = models.CharField('VAT number', max_length=20, null=True, blank=True)

    year_founded = models.IntegerField('Year founded', max_length=4, null=True, blank=True)

    prog = models.ManyToManyField('ProgrammeRole', through='OrganisationProgrammeRoleRelationship')

    objects = OrganisationManager()

    class Meta:
        verbose_name = 'Organisation'
        verbose_name_plural = 'Organisations'
        db_table = 'organisation'

    def __unicode__(self):
        return unicode(self.org)

    def is_supplier(self):
        role = self.prog.first().role
        if role == "supplier":
            return True

    def get_roles(self):
        programme_roles = self.prog.all()
        return set(p.role for p in programme_roles)

    def get_diagnostic(self, role):
        """Since users can now have both roles, we need to explicitly ask
        for a supplier or customer diagnostic.
        """
        if 'supplier' in self.get_roles() and role == 'supplier':
            approvals = self.supplierapproval_set.order_by('-appr_id').all()
            if approvals.count() > 0:
                return approvals[0]
        elif 'customer' in self.get_roles() and role == 'customer':
            diags = self.customerdiagnostic_set.order_by('-diag_id').all()
            if diags.count() > 0:
                return diags[0]

    def get_supplier_diagnostic(self):
        return self.get_diagnostic('supplier')

    def get_customer_diagnostic(self):
        return self.get_diagnostic('customer')

    def get_state(self, role=None):
        if role == 'customer':
            diag = self.get_customer_diagnostic()
        else:
            diag = self.get_supplier_diagnostic()

        if diag is None:
            return "not_started"
        if 'supplier' in self.get_roles():
            if diag.submitted and diag.is_approved is None:
                return "submitted"

            if diag.is_approved is None:
                return "in_progress"

            return "closed"
        else:
            if diag.submitted is None:
                return "not_started"

            if diag.submitted and not diag.agreed:
                review_started = diag.customercommlog_set.count() > 0
                if review_started:
                    return "review_in_progress"
                else:
                    return "in_progress"

            if diag.agreed:
                return "closed"

    def get_state_display(self):
        state = self.get_state()
        return state and self.STATES[state]

    def get_customer_state_display(self):
        state = self.get_state('customer')
        return state and self.STATES[state]

    def get_members(self, only_managers=False):
        return self.org.get_members(only_managers)

    def get_contact_emails(self, role=None):
        if role is None:
            return self.org.get_contact_emails()
        elif role is 'supplier':
            recipient = self.get_supplier_diagnostic().contact_email
        elif role is 'customer':
            recipient = self.get_customer_diagnostic().contact_email

        return dict(main=recipient,
                    members=self.org.get_contact_emails().get('cc'))

    def get_role_for_display(self):
        """ Returns Supplier/Customer/Visit host/Mentor
            Used in dashboard database display
        """
        result = []
        if self.mentor_set.exists():
            result.append('Mentor')
        if self.visit_set.exists():
            result.append('Visit host')
        if 'supplier' in self.get_roles():
            result.append('Supplier')
        if 'customer' in self.get_roles():
            result.append('Customer')
        # else:
        #     result.append('Customer')
        return ', '.join(result)

    def get_detail(self):
        """ Return detail companyno, charityno or detail """
        val = None
        org_type_id = self.org_type and self.org_type.org_type_id

        if org_type_id == 1:
            val = self.org.charityno
        elif org_type_id == 2:
            val = self.org.companyno
        else:
            val = self.detail
        return val


class CustomerPrequal(models.Model):

    cust = models.ForeignKey(Organisation, db_column='org_id')
    prequal_questions = models.TextField('Prequal answers')
    created = models.DateField('Created', auto_now_add=True)

    class Meta:
        verbose_name = 'Customer Prequal'
        verbose_name_plural = 'Customer Prequals'
        db_table = 'customer_prequal'

    def __unicode__(self):
        return unicode(self.cust)


class CustomerDiagnostic(models.Model):

    STATES = (('prequalified', 'Pre-qualification complete, self-assesment not started'),
        ('in_progress', 'Self-assessment in progress'),
        ('submitted', 'Self-assessment submitted'),
        ('review_started', 'Review in progress'),
        ('recommendation_started', 'Recommendations draft in progress'),
        ('recommendation_submitted', 'Our recommendations are awaiting your approval'),
        ('recommendation_force_submitted', 'Recommendation awaiting customer approval (without consent)'),
        ('recommendation_rejected', 'Recommendations rejected'),  # This is as well 'Recommendations received'
        ('recommendation_appealed', 'Recommendations appealed'),
        ('recommendation_revised', 'Our revised recommendations are awaiting your approval'),
        ('approved', 'Application complete'),
        ('ineligible', 'Ineligible for programme'),)

    ADMIN_STATES = (('prequalified', 'Customer has finished pre-qualification'),
        ('in_progress', 'Self-assessment in progress'),
        ('submitted', 'Self-assessment submitted, review not started'),
        ('review_started', 'Review in progress'),
        ('recommendation_started', 'Recommendation draft in progress'),
        ('recommendation_submitted', 'Recommendation sent to customer (waiting)'),
        ('recommendation_force_submitted', 'Recommendation sent to customer (waiting)'),
        ('recommendation_rejected', 'Customer rejected recommendation'),
        ('recommendation_appealed', 'Recommendations appealed (waiting)'),
        ('recommendation_revised', 'Revised recommendation sent to customer (waiting)'),
        ('approved', 'Approved'),
        ('ineligible', 'Customer is ineligible'),)

    diag_id = models.AutoField('Diagnostic ID', primary_key=True)
    cust = models.ForeignKey(Organisation)
    diag_questions = models.TextField('Diagnostic answers')
    assessor_comments = models.TextField('Assessor comments', null=True, blank=True)

    created = models.DateField('Created', auto_now_add=True)
    submitted = models.DateField('Submitted', null=True, blank=True)
    agreed = models.DateField('Agreed', null=True, blank=True)
    is_approved = models.NullBooleanField('Accepted?', null=True, blank=True)

    prev_diag = models.ForeignKey("self", db_column='prev_diag_id', related_name="prev_diags", null=True, blank=True)
    next_diag = models.ForeignKey("self", db_column='next_diag_id', related_name="next_diags", null=True, blank=True)

    orgnl_overview = models.TextField("Organisational overview", null=True)
    financial_health = models.TextField("Priority areas for support", null=True)
    snr_lev_commit = models.TextField("Recommendation for support", null=True)
    conditions_accepted = models.DateField('Voucher conditions accepted at', null=True, blank=True)

    assessor = models.ForeignKey(User, null=True, blank=True)
    state = models.CharField(max_length=40, choices=STATES, default='in_progress')

    prog_role = models.ManyToManyField('ProgrammeRole', through='CustomerDiagnosticProgrammeRole')

    contact_name = models.CharField('Contact name', max_length=255, null=True, blank=True)
    contact_title = models.CharField('Contact title', max_length=255, null=True, blank=True)
    contact_phone = models.CharField('Contact telephone', max_length=255, null=True, blank=True)
    contact_email = models.EmailField('Contact email', max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = 'Customer Diagnostic'
        verbose_name_plural = 'Customer Diagnostics'
        db_table = 'customer_diagnostic'

    def __unicode__(self):
        return unicode(self.cust)

    def has_contact_info(self):
        return self.contact_name is not None

    def set_state(self, state, commit=True):
        assert state in [x[0] for x in self.STATES]
        self.state = state

        now = datetime.datetime.now()
        # FIXME - set the dates of all other
        if state == "submitted":
            self.submitted = now
        elif state == "approved":
            self.agreed = now
        elif state == "ineligible":
            self.agreed = now

        self.is_approved = state == "approved"
        if self.is_approved:
            # Create vouchers from CustomerDevPri
            # The approved state should be set exactly one time only. The following situation,
            # where I remove existing vouchers, should happen only during testing.
            # PA: prevent voucher being deleted (also deletes payment and project)
            #CustomerVoucher.objects.filter(cust=self.cust).delete()
            for devpri in CustomerDevPri.objects.filter(diag=self).all():
                if not devpri.value:
                    continue
                CustomerVoucher.objects.create(cust=self.cust,
                                               area=devpri.topic.area,
                                               assigned=now,
                                               expires=now + relativedelta(months=4),
                                               value=devpri.value,
                                               allocated=0,
                                               spent=0)

        if commit:
            self.save()

    def get_admin_state_display(self):
        states = dict(self.ADMIN_STATES)
        title = states[self.state]
        return title

    def get_rough_state(self):
        if self.state == "in_progress":
            return "form_in_progress"
        elif self.state in ['submitted',
                            'review_started',
                            'recommendation_started',
                            'recommendation_submitted',
                            'recommendation_force_submitted']:
            return "recommendation_in_progress"
        elif self.state == "approved":
            return "approved"

    def has_comments(self):
        from bigassist_core.xml import XMLManipulator
        comments = self.assessor_comments
        has_comments = False
        if comments:
            xmlm = XMLManipulator(comments)
            has_comments = bool(xmlm.get_text('.//tm:summary'))
        return has_comments

    def resubmission(self):
        return self.prev_diag is not None

    def get_region(self):
        """ region if exists or empty string
        """
        return Person.objects.get(pk=self.cust.org_id).region or ''

    def get_recommendation_sent_date(self):
        try:
            qs = CustomerCommLog.objects.filter(
                    diag_id=self.pk,
                    comm_type_id=settings.KHNP_COMMLOG_RECOMMENDATION_SENT_ID
                    ).order_by('-created').first()
            date = qs.created
        except:
            date = None
        return date

    def recommendation_overdue(self):
        if self.get_recommendation_sent_date():
            now = datetime.datetime.now()
            today = now.date()
            tomorrow = today - datetime.timedelta(weeks=2)
            if self.get_recommendation_sent_date() < tomorrow and self.state != 'recommendation_appealed':
                return 'overdue'
            else:
                return None


class CustomerDevPri(models.Model):

    diag = models.ForeignKey('CustomerDiagnostic')
    topic = models.ForeignKey('SupportTopic')
    pri_lev = models.ForeignKey('DevPriLev')
    # Not needed. See https://it.fry-it.com/text-matters/dev/013/#i1
    # duration = models.ForeignKey('DevDuration')
    support_type = models.ForeignKey('DevSupportType')
    value = models.IntegerField(null=True, blank=True)
    bursary_rcmd = models.BooleanField('Recommended for sponsorship', default=False)

    class Meta:
        db_table = 'customer_dev_pri'


class CommMethod(models.Model):
    comm_method_id = models.AutoField(primary_key=True)
    comm_method = models.CharField(max_length=255)

    class Meta:
        db_table = u'comm_method'

    def __unicode__(self):
        return self.comm_method


class CustomerCommType(models.Model):
    # WARNING - this is not auto increment field in the database
    comm_type_id = models.IntegerField(primary_key=True)
    comm_type = models.CharField(max_length=255)
    available_in_form = models.BooleanField('Show this record in communication log form?',
                                            default=True,
                                            help_text=u'If checked, this record will be shown in the "Add new communication log entry" form.')

    class Meta:
        db_table = u'customer_comm_type'

    def __unicode__(self):
        return self.comm_type


class CustomerCommLog(models.Model):
    comm_log_id = models.AutoField(primary_key=True)
    diag = models.ForeignKey(CustomerDiagnostic, db_column="diag_id")
    comm_type = models.ForeignKey(CustomerCommType, db_column="comm_type_id")
    details = models.TextField('Communication details')
    created = models.DateField('Created', auto_now_add=True, editable=False)

    class Meta:
        db_table = u'customer_comm_log'
        ordering = ['comm_log_id']

    def __unicode__(self):
        return unicode(self.diag) + u' ' + unicode(self.comm_type)

    def iteminfo(self):
        """ return pre-processed log info """
        customer_originated_log_types = (settings.KHNP_COMMLOG_RECOMMENDATION_ACCEPTED_ID,
                                         settings.KHNP_COMMLOG_RECOMMENDATION_REJECTED_ID)
        if self.comm_type.pk in customer_originated_log_types:
            sender = unicode(self.diag.cust)
        else:
            sender = 'Assessor'
        preview = None
        if self.comm_type.pk == settings.KHNP_COMMLOG_RECOMMENDATION_SENT_ID:
            preview = True

        return dict(
            sender=sender,
            created=self.created,
            comm_type=unicode(self.comm_type),
            preview=preview,
            pk=self.pk,
            details=self.details,
        )


class DevDuration(models.Model):
    duration_id = models.AutoField(primary_key=True)
    duration = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = u'dev_duration'

    def __unicode__(self):
        return self.duration


class DevPriLev(models.Model):
    pri_lev_id = models.AutoField(primary_key=True)
    pri_lev = models.CharField(max_length=255)

    class Meta:
        db_table = u'dev_pri_lev'

    def __unicode__(self):
        return self.pri_lev


class DevSupportType(models.Model):
    support_type_id = models.AutoField(primary_key=True)
    support_type = models.CharField(max_length=255)

    class Meta:
        db_table = u'dev_support_type'

    def __unicode__(self):
        return self.support_type


class SupportTopic(models.Model):

    topic_id = models.AutoField('Topic ID', primary_key=True)
    area = models.ForeignKey("SupportArea", db_column='area_id')
    topic = models.CharField('Topic', max_length=150)  # should it have choices ?

    class Meta:
        verbose_name = 'Support Topic'
        verbose_name_plural = 'Support Topics'
        db_table = 'support_topic'

    def __unicode__(self):
        return self.topic + u' / ' + unicode(self.area)


class CustomerVoucherManager(models.Manager):
    use_for_related_fields = True

    def available(self):
        now = datetime.datetime.now()
        return self.get_queryset().filter(value__gt=models.F('allocated'),
                                           expires__gte=now)


class CustomerVoucher(models.Model):

    voucher_id = models.AutoField('Voucher ID', primary_key=True)
    cust = models.ForeignKey(Organisation, db_column='cust_id')
    area = models.ForeignKey("SupportArea", db_column='area_id')
    assigned = models.DateField('Assigned', auto_now_add=True)
    expires = models.DateField('Expires', null=True, blank=True)
    value = models.IntegerField('Value of voucher')
    allocated = models.IntegerField('Amount allocated to a project', default=0)
    spent = models.IntegerField('Amount already paid to supplier', default=0)

    objects = CustomerVoucherManager()

    class Meta:
        verbose_name = 'Customer voucher'
        verbose_name_plural = 'Customer vouchers'
        db_table = 'customer_voucher'

    def number(self):
        return self.voucher_id + 1000

    def assigned_by(self):
        # https://it.fry-it.com/text-matters/dev/013/#i1
        # - assigned by date (customer_voucher.assigned + 3 months)
        return self.assigned + relativedelta(months=3)

    def get_state(self):
        now = datetime.datetime.now()
        today = now.date()
        tomorrow = today + datetime.timedelta(days=1)
        if self.expires < tomorrow:
            return 'expired'
        elif self.assigned <= today <= self.expires:
            return 'current'
        else:
            return ''

    def get_residual_value(self):
        return self.value - (self.allocated or 0)

    def __unicode__(self):
        return unicode(self.cust) + u' / ' + unicode(self.area) + u' / ' + unicode(self.value)


""" Suppliers models """


class SupplierApprovalManager(models.Manager):

    def active(self):
        return self.get_queryset().filter(next_appr__isnull=True)


class SupplierApproval(models.Model):

    STATES = (('in_progress', 'Approval in progress'),
        ('submitted', 'Approval submitted'),
        ('reviewed', 'Approval reviewed'),
        ('rejected', 'Approval rejected'),
        ('approved', 'Approved'))

    ADMIN_STATES = (('in_progress', 'In progress'),
        ('submitted', 'Submitted'),
        ('reviewed', 'Reviewed'),
        ('rejected', 'Rejected'),
        ('approved', 'Approved'))

    appr_id = models.AutoField('Approval ID', primary_key=True)
    suppl = models.ForeignKey(Organisation, db_column='suppl_id')
    supplier_questions = models.TextField('Assessment answers')
    assessor_comments = models.TextField('Assessor comments', null=True, blank=True)
    created = models.DateField('Created', auto_now_add=True)
    submitted = models.DateField('Submitted', null=True, blank=True)
    assessed = models.DateField('Assessed', null=True, blank=True)
    decision = models.DateField('Decision', null=True, blank=True)
    is_approved = models.NullBooleanField('Approved?', null=True, blank=True)
    to_be_removed = models.BooleanField('To be removed', default=None)    # Only when marked as to be removed during panel decission
    panel_text = models.TextField("Decision summary", null=True, blank=True)
    prev_appr = models.ForeignKey("self", db_column='prev_appr_id', related_name="prev_apprs", null=True, blank=True)
    next_appr = models.ForeignKey("self", db_column='next_appr_id', related_name="next_apprs", null=True, blank=True)
    ac_file = models.FileField(upload_to='ac_files', max_length=200, null=True, blank=True)
    ac_file_type = models.CharField(max_length=100, null=True, blank=True)
    assessor = models.ForeignKey(User, null=True, blank=True)
    state = models.CharField(max_length=40, choices=STATES, default='in_progress')
    # who delivers support - extracted from XML - max length is used in xml.py so if you change it, change it there as well
    delivery_people = models.CharField(max_length=200, null=True, blank=True)
    short_desc = models.TextField("Short description", help_text='Plain text. No styling or links', null=True, blank=True)
    # long_desc should be removed once all suppliers have updated their profiles 08 Apr 15
    long_desc = models.TextField("Long description", help_text='Rich text. This can have headings, styling and links to external web pages', null=True, blank=True)
    #topics = models.ManyToManyField(SupportTopic, through="ApprovalTopic")

    expertise = models.TextField('Expertise', null=True, blank=True)
    employee_no = models.CharField('Number of employees', max_length=255, null=True, blank=True)
    io_types_worked_with = models.TextField('Infrastructure organisations worked with', null=True, blank=True)
    pc_turnover_from = models.TextField('Percentage of total turnover from customer type', null=True, blank=True)
    blogs_link = models.URLField('Link to blog/post', null=True, blank=True)

    locked_by = models.ForeignKey(User, null=True, blank=True, related_name='locked_approvals')
    locked_at = models.DateTimeField(null=True, blank=True)

    prog_role = models.ManyToManyField('ProgrammeRole', through='SupplierApprovalProgrammeRole')

    contact_name = models.CharField('Contact name', max_length=255, null=True, blank=True)
    contact_title = models.CharField('Contact title', max_length=255, null=True, blank=True)
    contact_phone = models.CharField('Contact telephone', max_length=255, null=True, blank=True)
    contact_email = models.EmailField('Contact email', max_length=255, null=True, blank=True)

    objects = SupplierApprovalManager()

    class Meta:
        verbose_name = 'Supplier Approval'
        verbose_name_plural = 'Supplier Approvals'
        db_table = 'supplier_approval'

    def __unicode__(self):
        return unicode(self.suppl)

    def has_contact_info(self):
        return self.contact_name is not None

    def get_io_types_worked_with_list(self):
        io_types = self.io_types_worked_with  # list()
        io_types = io_types.split()
        io_types_list = list()
        for io_type in io_types:
            io_types_list.append(io_type.replace('_', ' '))
        return io_types_list

    def get_pc_turnover_from_dict(self):
        if self.pc_turnover_from:
            import ast
            return ast.literal_eval(self.pc_turnover_from)

    def get_blogs_link(self):
        return self.blogs_link if "://" in self.blogs_link else "http://" + self.blogs_link

    def get_blogs_link_display(self):
        from urlparse import urlparse
        url = urlparse(self.get_blogs_link())
        return url.netloc

    def set_state(self, state, commit=True):
        assert state in [x[0] for x in self.STATES]
        self.state = state

        now = datetime.datetime.now()
        if state == "submitted":
            self.submitted = now
            self.assessed = None
            self.decision = None
        if state == "reviewed":
            self.assessed = now
            self.decision = None
        if state in ("approved", "rejected", "removed"):
            self.decision = now

            # Unlock application
            self.locked_by = None
            self.locked_at = None

        if state == 'approved':
            self.is_approved = True
            sts = self.suppl.suppliertopics_set
            ats = self.approvaltopic_set.filter(is_approved=True)

            sts.all().delete()
            for at in ats:
                sts.get_or_create(topic=at.topic)

        elif state in ['rejected', 'removed']:
            self.is_approved = False
            self.suppl.suppliertopics_set.all().delete()
        else:
            self.is_approved = None

        if commit:
            self.save()

    def get_admin_state_display(self):
        if self.state is None:
            return dict(self.ADMIN_STATES)['in_progress']
        return dict(self.ADMIN_STATES)[self.state]

    def resubmission(self):
        return self.prev_appr is not None

    def get_deadline(self):
        return SupplierDeadline.objects.last_deadline()

    def is_partially_approved(self):
        return self.is_approved and self.approvaltopic_set.filter(is_approved=False).exists()

    def lock(self, user):
        self.locked_by = user
        self.locked_at = datetime.datetime.now()
        self.save()

    def unlock(self):
        self.locked_by = None
        self.locked_at = None
        self.save()

    def locked(self, user=None):
        if self.locked_by is None:
            return False

        return self.locked_by != user

    def save(self, force_insert=False, force_update=False, using=None, force_xml_update=False):
        # Update approval fields from XML
        from bigassist_core.xml import XMLManipulator
        old_xml = ''
        if self.pk is not None:
            old_xml = SupplierApproval.objects.get(pk=self.pk).supplier_questions

        # If new approval, we need to have the primary key first
        if self.pk is None:
            super(SupplierApproval, self).save(force_insert, force_update, using)

        if force_xml_update or (old_xml != self.supplier_questions):
            manip = XMLManipulator(self.supplier_questions)
            manip.update_company(self)
        super(SupplierApproval, self).save(False, True, using)


class SupportArea(models.Model):

    area_id = models.AutoField('Area ID', primary_key=True)
    area_key = models.CharField(max_length=30)
    area = models.CharField('Area', max_length=150)

    class Meta:
        verbose_name = 'Support Area'
        verbose_name_plural = 'Support Areas'
        db_table = 'support_area'
        ordering = ['area']

    def __unicode__(self):
        return self.area


class SupplierTopics(models.Model):
    suppl = models.ForeignKey(Organisation, db_column='suppl_id')
    topic = models.ForeignKey(SupportTopic, db_column='topic_id')
    is_approved = models.BooleanField('Approved by panel?', default=False)

    class Meta:
        verbose_name = 'Supplier Topic'
        verbose_name_plural = 'Supplier Topics'
        db_table = 'supplier_topics'

    def __unicode__(self):
        return unicode(self.suppl) + u' - ' + unicode(self.topic)


class ApprovalTopic(models.Model):
    appr = models.ForeignKey(SupplierApproval)
    topic = models.ForeignKey(SupportTopic, db_column='topic_id')
    is_approved = models.BooleanField('Approved by panel?', default=False)

    class Meta:
        db_table = 'approval_topic'
        unique_together = ('appr', 'topic')

    def __unicode__(self):
        return unicode(self.topic.topic)


class XForms(models.Model):

    formtype = models.CharField('Form type', max_length=10, choices=XFORM_TYPES, unique=True)
    xform = models.TextField('XForm source')
    model = models.TextField('XForm model', help_text='XForm data model (source for xf:instance)')
    xform_ro = models.TextField('Read only version of the XForm',
                                blank=True, default='',
                                help_text='This XForm will be displayed when Customer, Admin or Supplier requests view-only version of the form.')
    custom_1 = models.TextField('Additional XML field',
                                help_text='This is a Question set XML for Customer XForm but a Previous answers XForm for Supplier XForm',
                                blank=True, default='')
    custom_2 = models.TextField('Customer XForm resubmission source',
                                help_text='This is the XForm source for customer resubmission of self-assessment',
                                blank=True, default='')

    class Meta:
        verbose_name = 'XForms'
        verbose_name_plural = 'XForms'
        db_table = 'xforms'

    def __unicode__(self):
        return "%s - (%s)" % (self.get_formtype_display(), self.formtype)


class CMFeedback(models.Model):
    id = models.ForeignKey(Contact, primary_key=True, db_column='id', related_name='feedbacks')

    def __unicode__(self):
        return str(id)

    class Meta:
        db_table = 'cm_feedback'


class NewsItemManager(models.Manager):

    def current(self):
        date = datetime.datetime.now() - relativedelta(years=1)
        return self.get_queryset().filter(date__gte=date)


class NewsItem(models.Model):

    title = models.CharField(max_length=255,
                             help_text="Title of the news item, used as the wording in the link to the item.")
    slug = models.SlugField(max_length=255,
                            help_text="URL page name (filled automatically, but can be overriden).")
    content = models.TextField(help_text="Content of the news item, can be HTML encoded.")
    date = models.DateTimeField(default=datetime.datetime.now,
                               help_text="News items are displayed most recent first. Items over a year old are not displayed. Date can be modified to control both whether an item is displayed and the order it appears.")

    objects = NewsItemManager()

    class Meta:
        db_table = 'news'
        verbose_name_plural = "News"
        ordering = ('-date', )

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('newsitem', [self.slug, ])


class Payment(models.Model):
    payment_id = models.AutoField(primary_key=True)
    proj = models.ForeignKey("CustomerProject", null=True, blank=True)
    visitor = models.ForeignKey("Visitor", null=True, blank=True)
    mentee = models.ForeignKey("Mentee", null=True, blank=True)
    visit_instance = models.ForeignKey("VisitInstance", null=True, blank=True)

    payee_org = models.ForeignKey(Organisation)
    amount = models.IntegerField()
    has_complaint = models.BooleanField(default=False)
    pop = models.CharField(max_length=100)

    invoice_date = models.DateTimeField(auto_now_add=True)
    sent_to_finance = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = "payment"

    def __unicode__(self):
        return "%s - %s" % (self.payee_org, self.amount)

    def payment_due(self):
        return self.invoice_date + datetime.datetime.timedelta(days=30)

    def get_related_object(self):
        """ Return related object - either proj, visitor or mentee """
        if self.proj is not None:
            return self.proj
        elif self.visitor is not None:
            return self.visitor
        elif self.mentee is not None:
            return self.mentee
        elif self.visit_instance is not None:
            return self.visit_instance
        else:
            raise RuntimeError("No related object. Are we missing the not null constraint?")


class CustomerProjectManager(models.Manager):
    use_for_related_fields = True

    def created(self):
        return self.get_queryset().filter(state__in=['created', ])

    def proposed(self):
        return self.get_queryset().filter(state__in=['proposed', ])

    def potential(self):
        return self.get_queryset().filter(state__in=['created', 'proposed'])

    def accepted(self):
        return self.get_queryset().filter(state__in=['accepted', ])

    def current(self):
        return self.get_queryset().filter(state__in=['accepted', 'mark_completed'])

    def invoiced(self):
        return self.get_queryset().filter(state__in=['completed', ])

    def dismissed(self):
        return self.get_queryset().filter(state__in=['dismissed', ]).order_by('-dismissed')


class CustomerProject(models.Model):

    STATES = (('created', 'Contact made'),
              ('proposed', 'Offer received'),
              ('accepted', 'Offer accepted'),
              ('mark_completed', 'Completed'),
              ('completed', 'Completed'),
              ('dismissed', 'Dismissed'),)

    SUPPLIER_STATES = (('created', 'Potential contact'),
                       ('proposed', 'Project proposed'),
                       ('accepted', 'Current'),
                       ('mark_completed', 'Completed'),
                       ('completed', 'Completed'),
                       ('dismissed', 'Dismissed'),)

    proj_id = models.AutoField(primary_key=True)
    cust = models.ForeignKey(Organisation, db_column='cust_id', related_name='customer_projects')
    suppl = models.ForeignKey(Organisation, db_column='suppl_id', related_name='supplier_projects')
    cover_note = models.TextField()
    comm_method = models.ForeignKey(CommMethod, db_column='comm_method_id')
    created = models.DateField('Created at', auto_now_add=True, editable=False)
    dismissed = models.DateField('Dismissed at', null=True, blank=True)
    dismissed_by_cust = models.NullBooleanField('Dismissed by customer?', null=True, blank=True)
    price = models.IntegerField(u'Price (£)', null=True, blank=True)
    description = models.TextField('Description of work', null=True, blank=True)
    startdate = models.DateField('Project start date', null=True, blank=True)
    enddate = models.DateField('Project end date', null=True, blank=True)
    proposed = models.DateField('When proposed by supplier', null=True, blank=True)
    responded = models.DateField('When responded by customer', null=True, blank=True)
    accepted = models.DateField('When accepted by customer', null=True, blank=True)
    voucher = models.ForeignKey(CustomerVoucher, null=True, blank=True, db_column='voucher_id')
    voucher_value = models.IntegerField(default=0)
    feedback = models.DateField('When feedback provided by customer', null=True, blank=True)
    fb_score = models.IntegerField('Feedback score', default=0)
    fb_comment = models.TextField('Feedback comment', null=True, blank=True)
    fb_work_again = models.NullBooleanField('Would you work with this supplier again?', null=True, blank=True)

    # vouchers = models.ManyToManyField(CustomerVoucher, related_name='multivouchers')

    state = models.CharField(max_length=40, choices=STATES, default='created')

    objects = CustomerProjectManager()

    class Meta:
        db_table = 'customer_project'
        verbose_name = "Customer project"
        verbose_name_plural = "Customer projects"

    def __unicode__(self):
        return u"%s - %s" % (self.cust, self.suppl)

    def get_customer_state_display(self):
        return dict(self.STATES).get(self.state, self.state)

    def get_supplier_state_display(self):
        return dict(self.SUPPLIER_STATES).get(self.state, self.state)

    def set_state(self, state, commit=True, **kwargs):
        assert state in [x[0] for x in self.STATES]
        self.state = state

        now = datetime.datetime.now()
        if state == "proposed":
            self.proposed = now
        elif state == "accepted":
            self.accepted = now
            self.responded = now
        elif state == "completed":
            # feedback provided by customer
            self.feedback = now
        elif state == "dismissed":
            assert 'by_customer' in kwargs
            self.dismissed = now
            self.dismissed_by_cust = not not kwargs['by_customer']
            if self.dismissed_by_cust:
                # responded field - set only by customer
                self.responded = now

        if commit:
            self.save()

    def cust_contact(self):
        method = self.comm_method.comm_method
        if isinstance(method, basestring):
            method = method.lower()
        if method == "phone":
            # return self.cust.org.get_contact('tel_direct')
            diag = self.cust.get_customer_diagnostic()
            return diag.contact_phone
        elif method == "email":
            return self.cust.org.get_contact_email()
        elif method == "post":
            return self.cust.org.get_address().formatted_address()
        else:
            return ""

    def invoice(self):
        """ Creates payment """
        assert self.payment_set.count() == 0, "The project is already invoiced"

        if not self.voucher_value:
            # Nothing to invoice
            return

        payment = self.payment_set.create(payee_org=self.suppl,
                                amount=min(self.price, self.voucher_value))

        if self.voucher:
            if self.voucher.spent is None:
                self.voucher.spent = 0
            self.voucher.spent += min(self.price, self.voucher_value)
            self.voucher.save()
        return payment

    def applicable_vouchers(self):
        supplier_areas = SupportArea.objects.filter(supporttopic__suppliertopics__suppl=self.suppl).distinct()
        return self.cust.customervoucher_set.available().filter(area__in=supplier_areas)

    def apply_voucher(self, voucher):
        assert voucher in self.applicable_vouchers()

        self.voucher = voucher
        self.voucher_value = min(voucher.get_residual_value(), self.price)
        self.save()

        if voucher.allocated is None:
            voucher.allocated = 0

        voucher.allocated += self.voucher_value
        voucher.save()

    def customer_value(self):
        return self.price - self.voucher_value


class ProjCommType(models.Model):

    id = models.AutoField(primary_key=True, db_column='proj_comm_type_id')
    title = models.CharField('Project communication type', max_length=255, db_column='proj_comm_type')

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'proj_comm_type'


class CustomerProjectComm(models.Model):

    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(CustomerProject, db_column='proj_id')
    details = models.TextField('Further details')
    created = models.DateTimeField('Created', auto_now_add=True)

    def __unicode__(self):
        return u"%s" % self.project

    class Meta:
        db_table = 'customer_project_comm'


class InvitedUser(models.Model):

    id = models.AutoField(primary_key=True)
    # invited to
    org = models.ForeignKey(Organisation)
    # invited by
    person = models.ForeignKey(Person)
    # invited by
    user = models.ForeignKey(User)
    # who has been invited
    email = models.EmailField('Email', db_index=True)
    # type of invited user (Manager, Member)
    type = models.ForeignKey(RelationshipType)
    created = models.DateTimeField('Created', auto_now_add=True)
    # user successfully registered
    registered = models.DateTimeField('Registered at', null=True, blank=True)

    def __unicode__(self):
        finished = self.registered and " (finished)" or ''
        return "%s to %s%s" % (self.email, self.org, finished)

    class Meta:
        db_table = 'invited_user'


class OrgRegion(models.Model):

    REGIONS = (('NE', 'North East'),
               ('NW', 'North West'),
               ('Yorks', 'Yorkshire and The Humber'),
               ('EM', 'East Midlands'),
               ('WM', 'West Midlands'),
               ('E', 'East of England'),
               ('SE', 'South East'),
               ('SW', 'South West'),
               ('Lon', 'London'))

    org = models.ForeignKey(Organisation)
    region = models.CharField(max_length=10, choices=REGIONS)

    class Meta:
        db_table = 'org_region'
        unique_together = ('org', 'region')


class Region(models.Model):

    code = models.CharField('Region code', max_length=10)
    name = models.CharField('Full name', max_length=50)

    class Meta:
        db_table = 'region'
        ordering = ['pk']

    def __unicode__(self):
        return self.name


class BursaryTypes(models.Model):

    value = models.IntegerField('Amount')
    name = models.CharField(max_length=30, blank=True, default='')

    class Meta:
        db_table = 'bursary_types'

    def __unicode__(self):
        return u'%s (£%s)' % (self.name, self.value)


class VisitManager(models.Manager):
    use_for_related_fields = True

    def potential(self):
        return self.get_queryset().filter(available__isnull=True)

    def completed(self):
        return self.get_queryset().filter(available=False)


# class VisitTopic(models.Model):
#     visit = models.ForeignKey("Visit")
#     topic = models.ForeignKey(SupportTopic)

#     class Meta:
#         db_table = 'visit_topics'

#     def __unicode__(self):
#         return unicode(self.visit) + u' / ' + unicode(self.topic)


class Visit(models.Model):
    host = models.ForeignKey(Organisation, db_column='host_id')
    title = models.CharField('Name of visit', max_length=255, blank=True, default='')
    description = models.TextField('Description')
    address = models.TextField('Address', null=True, blank=True)
    region = models.ManyToManyField(Region, db_table='visit_region')
    date = models.DateField(null=True, blank=True)
    available = models.NullBooleanField('Currently offered?')
    created = models.DateTimeField('Created', auto_now_add=True)
    topics = models.ManyToManyField(SupportTopic, db_table='visit_topics')

    objects = VisitManager()

    class Meta:
        db_table = 'visit'

    def __unicode__(self):
        return unicode(self.host)

    def visit_date(self):
        if self.date:
            return self.date.strftime(settings.DATE_FORMAT_P)
        else:
            return u'anytime by arrangement'

    def get_type(self):
        return 'VISIT'

    def get_subheading(self):
        return self.title

    def get_absolute_url(self):
        return reverse('visit_detail', args=[self.pk])

    def get_in_touch_url(self):
        return reverse('visit_get_in_touch', args=[self.pk])

    def get_host_sponsorship_amount(self):
        return BursaryTypes.objects.get(name='host').value

    def total_sponsorship_amount_for_host(self):
        # Sponsorship amount for Visit host is a fixed amount GBP 400 per a visit day.
        # If the single visit has several visit instances in different days, each day
        # means sponsorship 400 for a host. See #188
        amount = BursaryTypes.objects.get(name='host').value
        return amount * self.visitinstance_set.count()

    def get_contact_email(self):
        """Returns the host organisation's contact email address
        """
        # ctype = settings.CONTACT_TYPES['email_direct']
        # result = Contact.objects.filter(person=self.host.pk, type__id=ctype['type'],
        #                                 use__id=ctype['use']).order_by('id').all()
        # if result:
        #     return result[0].value
        diag = self.host.get_customer_diagnostic()
        return diag.contact_email

class VisitInstance(models.Model):

    visit = models.ForeignKey(Visit)
    date = models.DateField(null=True, blank=True)
    bursary = models.NullBooleanField('Granted sponsorship?')
    amount = models.IntegerField('Amount', default=0)
    granted = models.DateTimeField('When sponsorsip granted', null=True, blank=True)

    class Meta:
        db_table = 'visit_instance'

    def __unicode__(self):
        if self.date:
            return unicode(self.visit) + u' at ' + self.date.strftime(settings.DATE_FORMAT_P)

    def invoice(self):
        """ Creates payment """
        if self.payment_set.count() > 0:
            # already invoiced
            return

        if not self.bursary or not self.amount:
            # Nothing to invoice
            return

        payment = self.payment_set.create(payee_org=self.visit.host,
                                amount=self.amount)
        return payment


class VisitorManager(models.Manager):
    use_for_related_fields = True

    def potential(self):
        return self.get_queryset().filter(visit_instance__isnull=True)

    def confirmed(self):
        today = datetime.datetime.combine(datetime.datetime.now().date(), datetime.time(23, 59))
        # The same conditions as in def status() in Visitor model
        query = models.Q(visit_instance__isnull=False)
        query &= models.Q(visit_instance__date__gte=today) | models.Q(visit_instance__date__isnull=True, feedback__isnull=True)
        return self.get_queryset().filter(query)

    def completed(self):
        now = datetime.datetime.now().date()
        # The same conditions as in def status() in Visitor model
        query = models.Q(visit_instance__isnull=False)
        query &= models.Q(visit_instance__date__lt=now) | models.Q(visit_instance__date__isnull=True, feedback__isnull=False)
        return self.get_queryset().filter(query)


class Visitor(models.Model):

    STATES = (
        ('pending', u'Pending'),
        ('confirmed', u'Confirmed'),
        ('completed', u'Completed'),
    )

    # visit must be always set, because visitor is asking to be accpeted for particular visit.
    visit = models.ForeignKey(Visit)
    # visit_instance is set as soon as visitor is accepted by visit host for particular (or new) visit_instance.
    visit_instance = models.ForeignKey(VisitInstance, null=True, blank=True)
    visitor_org = models.ForeignKey(Organisation)
    contact_msg = models.TextField('Contact message')
    created = models.DateTimeField('Created', auto_now_add=True)
    benefit = models.TextField('Benefit to visitor', null=True, blank=True)
    purpose = models.TextField('Purpose of the visit', null=True, blank=True)
    bursary = models.NullBooleanField()  # set to True/False as an admin decision. Null until decided.
    amount = models.IntegerField('Sponsorship amount', default=0)  # contains requested amount (if any)
    granted = models.DateTimeField('When sponsorsip granted', null=True, blank=True)
    feedback = models.DateTimeField('When feedback given', null=True, blank=True)
    fb_score = models.IntegerField('Feedback score', null=True, blank=True)
    fb_original_purpose = models.TextField(null=True, blank=True)
    fb_comment = models.TextField('Feedback comment', null=True, blank=True)

    objects = VisitorManager()

    class Meta:
        db_table = 'visitor'

    def __unicode__(self):
        return "%s on %s" % (self.visitor_org, self.visit)

    @property
    def state(self):
        today = datetime.datetime.now().date()

        # Please check VisitorManager as well if changing conditions here
        if self.visit_instance is None:
            return 'pending'
        elif self.visit_instance.date is None:
            if self.feedback:
                return 'completed'
            else:
                return 'confirmed'
        elif self.visit_instance.date >= today:
            return 'confirmed'
        elif self.visit_instance.date < today:
            return 'completed'

    def get_state_display(self):
        return dict(self.STATES).get(self.state, self.state)

    def current_status(self):
        if self.visit_instance is None:
            return u'Message received'
        elif self.feedback is None:
            date = self.visit_instance.date.strftime(settings.DATE_FORMAT_P)
            return u'Visit agreed for %s' % date
        else:
            return u'Completed'

    def invoice(self):
        """ Creates payment """
        if self.payment_set.count() > 0:
            # already invoiced
            return

        if not self.bursary or not self.amount:
            # Nothing to invoice
            return

        payment = self.payment_set.create(payee_org=self.visitor_org,
                                amount=self.amount)
        return payment

    def get_contact_email(self):
        """Returns the host organisation's contact email address
        """
        # ctype = settings.CONTACT_TYPES['email_direct']
        # result = Contact.objects.filter(person=self.visitor_org.pk, type__id=ctype['type'],
        #                                 use__id=ctype['use']).order_by('id').all()
        # if result:
        #     return result[0].value

        diag = self.visitor_org.get_customer_diagnostic()
        return diag.contact_email


class MentorManager(models.Manager):
    use_for_related_fields = True

    def potential(self):
        return self.get_queryset().filter(available__isnull=True)

    def confirmed(self):
        return self.get_queryset().filter(available=True)


class Mentor(models.Model):
    mentor = models.ForeignKey(Person, primary_key=True, db_column='mentor_id')
    mentor_job_title = models.CharField('Job title of mentor', max_length=200)
    mentor_org = models.ForeignKey(Organisation)
    description = models.TextField('Description of mentor role')
    region = models.ManyToManyField(Region, db_table='mentor_region')
    available = models.NullBooleanField('Currently offered?')
    created = models.DateTimeField('Created', auto_now_add=True)
    topics = models.ManyToManyField(SupportTopic, db_table='mentor_topics')

    objects = MentorManager()

    class Meta:
        db_table = 'mentor'

    def __unicode__(self):
        return unicode(self.mentor)

    def get_type(self):
        return 'MENTOR'

    def get_subheading(self):
        # this format has been requested in #188
        return u"%s - %s" % (self.mentor_job_title, self.mentor_org)

    def get_absolute_url(self):
        return reverse('mentor_detail', args=[self.pk])

    def get_in_touch_url(self):
        return reverse('mentor_get_in_touch', args=[self.pk])


# class MentorTopic(models.Model):
#     mentor = models.ForeignKey(Mentor)
#     topic = models.ForeignKey(SupportTopic)

#     class Meta:
#         db_table = 'mentor_topics'

#     def __unicode__(self):
#         return unicode(self.mentor) + u' / ' + unicode(self.topic)


class MenteeManager(models.Manager):
    use_for_related_fields = True

    def active(self):
        return self.get_queryset().filter(state__in=('pending', 'agreed', 'completed'))

    def potential(self):
        return self.get_queryset().filter(state='pending')

    def confirmed(self):
        return self.get_queryset().filter(state='agreed')


class Mentee(models.Model):
    STATES = (
        ('pending', u'Pending'),
        ('agreed', u'Agreed'),
        ('discarded', u'Discarded'),
        ('completed', u'Completed'),
    )

    mentor = models.ForeignKey(Mentor)
    mentee_org = models.ForeignKey(Organisation)
    contact_msg = models.TextField('Contact message')
    created = models.DateTimeField('Created', auto_now_add=True)
    mentoring_cost = models.IntegerField(null=True, blank=True)
    decision = models.DateTimeField('When descision taken', null=True, blank=True)
    is_accepted = models.NullBooleanField()
    date1 = models.DateTimeField('First session date', null=True, blank=True)
    date2 = models.DateTimeField('Second session date', null=True, blank=True)
    date3 = models.DateTimeField('Third session date', null=True, blank=True)
    date4 = models.DateTimeField('Fourth session date', null=True, blank=True)
    completed_early = models.NullBooleanField()
    # the following fields are Sponsorship related but there is no sponsorship for mentee currently
    benefit = models.TextField('Benefit to org being mentored', null=True, blank=True)
    purpose = models.TextField('Purpose of the mentoring', null=True, blank=True)
    bursary = models.NullBooleanField()  # set to True/False as an admin decision. Null until decided.
    amount = models.IntegerField('Sponsorship amount', default=0)  # contains requested amount (if any)
    granted = models.DateTimeField('When sponsorsip granted', null=True, blank=True)
    feedback = models.DateTimeField('When feedback given', null=True, blank=True)
    fb_score = models.IntegerField('Feedback score', null=True, blank=True)
    fb_original_purpose = models.TextField(null=True, blank=True)
    fb_comment = models.TextField('Feedback comment', max_length=255, null=True, blank=True)
    state = models.CharField(max_length=40, choices=STATES, default='pending')

    objects = MenteeManager()

    class Meta:
        db_table = 'mentee'

    def __unicode__(self):
        if len(self.contact_msg) > 100:
            return self.contact_msg[:100] + '...'
        else:
            return self.contact_msg

    def is_completed(self):
        return self.state == 'completed'

    def set_state(self, state, commit=True):
        assert state in [x[0] for x in self.STATES]
        self.state = state
        if state == 'agreed':
            self.is_accepted = True
        elif state == 'discarded':
            self.is_accepted = False

        if commit:
            self.save()

    def invoice(self):
        """ Creates payment """
        if self.payment_set.count() > 0:
            # already invoiced
            return

        if not self.bursary or not self.amount:
            # Nothing to invoice
            return

        payment = self.payment_set.create(payee_org=self.mentor.mentor_org,
                                amount=self.amount)
        return payment


class SupplierDeadlineManager(models.Manager):

    def next_deadlines(self, date=None):
        if date is None:
            date = datetime.date.today()
        return self.get_queryset().filter(deadline__gt=date)

    def next_deadline(self, date=None):
        deadlines = self.next_deadlines(date)
        try:
            deadline = deadlines[0]
        except IndexError:
            deadline = None
        return deadline

    def last_deadline(self):
        date = datetime.date.today()
        try:
            prev_deadline = self.get_queryset().filter(deadline__lte=date).order_by('-deadline')[0]
        except IndexError:
            prev_deadline = None

        return prev_deadline

    def active_deadlines(self):
        return self.get_queryset().exclude(panel_state='completed')


class SupplierDeadline(models.Model):

    STATES = (('not_started', 'Not started'),
              ('in_progress', 'In progress'),
              ('completed', 'Completed'), )

    deadline_id = models.AutoField(primary_key=True)
    deadline = models.DateField()

    panel_state = models.CharField(max_length=20, choices=STATES, default='not_started')

    pdf_file = models.FileField(upload_to='pdf_files', max_length=200, null=True, blank=True)
    pdf_task_id = models.CharField(max_length=100, null=True, blank=True)

    objects = SupplierDeadlineManager()

    class Meta:
        db_table = 'supplier_deadline'
        ordering = ('deadline', )

    def __unicode__(self):
        return self.deadline.strftime(settings.DATE_FORMAT_P)

    def set_state(self, state, commit=True):
        assert state in dict(self.STATES), "Unknown state %s" % state
        self.panel_state = state
        if commit:
            self.save()

    def remaining(self):
        return self.deadline - datetime.date.today()

    def get_potential_approvals(self):
        suppliers = SupplierApproval.objects.active().order_by('appr_id')
        suppliers = suppliers.filter(state='submitted', submitted__lte=self.deadline)
        return suppliers

    def get_newer_approvals(self):
        suppliers = SupplierApproval.objects.active().order_by('appr_id')
        suppliers = suppliers.filter(state='submitted', submitted__gt=self.deadline)
        return suppliers

    def get_approvals(self):
        suppliers = SupplierApproval.objects.active().order_by('appr_id')
        suppliers = suppliers.filter(state='reviewed', submitted__lte=self.deadline)
        return suppliers


class Report(models.Model):

    title = models.CharField(max_length=250)
    description = models.TextField(null=True, blank=True)
    sql = models.TextField()
    enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'report'

    def __unicode__(self):
        return self.title

    def data(self):
        cursor = connections['readonly_db'].cursor()
        cursor.execute(self.sql)
        columns = [x.name for x in cursor.description]
        data = dictfetchall(cursor)
        transaction.commit_unless_managed(using='readonly_db')
        return columns, data

    def as_table(self, columns=None, data=None):
        if columns is None or data is None:
            columns, data = self.data()
        res = GenericReportTable(columns, data, orderable=False)
        return res


class GlobalPermissionManager(models.Manager):
    def get_queryset(self):
        return super(GlobalPermissionManager, self).\
            get_queryset().filter(content_type__name='global_permission')


class GlobalPermission(Permission):
    """A global permission, not attached to a model"""

    objects = GlobalPermissionManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        ct, created = ContentType.objects.get_or_create(
            name="global_permission", app_label=self._meta.app_label
        )
        self.content_type = ct
        super(GlobalPermission, self).save(*args, **kwargs)


""" New models for programmes and multiple user types """


class Programme(models.Model):
    """ One record per programme """

    prog_id = models.AutoField(primary_key=True, db_column='prog_id')
    name = models.CharField(max_length=255, blank=False, null=False)

    class Meta:
        verbose_name = "Programme"
        verbose_name_plural = "Programmes"
        db_table = 'demeter_programme'

    def __unicode__(self):
        return self.name


class OrganisationProgrammeRoleRelationship(models.Model):
    """ Joining table for organisation and prog_role. There
    is a corresponding modelfield on Organisation that establishes
    the through relationship"""

    org = models.ForeignKey('Organisation', db_column='org_id')
    prog_role = models.ForeignKey('ProgrammeRole')

    class Meta:
        db_table = 'demeter_org_prog_role_relationship'


def get_programme_roles_tuple():
    return (('supplier', 'Supplier'),
            ('customer', 'Customer'), )


class ProgrammeRole(models.Model):
    """ There will be one programme role for each of supplier or
    customer, per programme. If there are 2 programmes, there will
    be 4 ProgrammeRole records. """

    ROLES = get_programme_roles_tuple()

    prog_role_id = models.AutoField(primary_key=True, db_column='prog_role_id')

    role = models.CharField(max_length=20, choices=ROLES, default='customer')
    xforms = models.CharField(max_length=255, null=True)

    prog = models.ForeignKey("Programme", db_column='prog_id')

    class Meta:
        verbose_name = "Programme role"
        verbose_name_plural = "Programme roles"
        db_table = 'demeter_programme_role'

    def __unicode__(self):
        pass


class CustomerDiagnosticProgrammeRole(models.Model):
    """ Joining table for customer_diagnostic and prog_role there
    is a corresponding modelfield on CustomerDiagnostic that establishes
    the through relationship"""

    diag = models.ForeignKey('CustomerDiagnostic', db_column='diag_id')
    prog_role = models.ForeignKey('ProgrammeRole')

    class Meta:
        db_table = 'demeter_diag_prog_role_relationship'


class SupplierApprovalProgrammeRole(models.Model):
    """ Joining table for supplier_approval and prog_role there
    is a corresponding modelfield on SupplierApproval that establishes
    the through relationship"""

    appr = models.ForeignKey('SupplierApproval', db_column='appr_id')
    prog_role = models.ForeignKey('ProgrammeRole')

    class Meta:
        db_table = 'demeter_appr_prog_role_relationship'


def get_email_domain_blacklist():
    BLACKLIST = ['', 'aol.com', 'aol.com', 'anonymous.to', 'btconnect.com', 'btinternet.com', 'comcast.net', 'dispostable.com', 'everymail.net', 'everyone.net', 'facebook.com', 'fastmail.fm', 'flashmail.com', 'gmail.com', 'gmx.com', 'gmx.co.uk', 'googlemail.com', 'guerillamail.com', 'hotmail.com', 'hotmail.co.uk', 'hotmail.fr', 'hotmail.it', 'hushmail.com', 'inbox.com', 'knowhownonprofit.org', 'live.com', 'live.co.uk', 'lycos.com', 'mac.com', 'mail.com', 'mail.ru', 'mailinator.com', 'me.com', 'msn.com', 'ncvo-vol.org.uk', 'ncvo.org', 'ntlworld.com', 'o2.co.uk', 'onebox.com', 'outlook.com', 'parliament.org', 'qmail.com', 'rediff.com', 'runbox.com', 'seznam.cz', 'sky.com', 'spamgourmet.com', 'trashmail.net', 'virgin.net', 'virginmedia.com', 'yahoo.com', 'yahoo.co.uk', 'ymail.com', 'yandex.ru', 'zoho.com', ]
    return BLACKLIST


class EmailDomain(models.Model):
    """ Model for existing table email_domain
    used to store and retrieve relationship between a person object
    and an email domain."""
    BLACKLIST = get_email_domain_blacklist()

    object = models.ForeignKey('Person', db_column='object')
    domain = models.CharField(max_length=255, blank=False)

    class Meta:
        db_table = 'email_domain'

    def __unicode__(self):
        return unicode("{object} - {domain}".format(object=self.object, domain=self.domain))


import listeners
listeners  # pyflakes
