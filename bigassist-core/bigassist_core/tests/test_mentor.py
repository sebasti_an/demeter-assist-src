from django.conf import settings
from datetime import datetime
from django.core import mail
from django.core.urlresolvers import reverse
from bigassist_core.views.customer import initialize_customer_diagnostic
from bigassist_core.user import KHNPUser
from bigassist_core import models
from django.contrib.auth.models import User

from .base import TestCase

EMAIL = 'test@khnp.org'
CUST_EMAIL = 'test_cust@khnp.org'


class MentorTest(TestCase):

    fixtures = ['types', 'mails', 'xforms']

    def tearDown(self):
        User.objects.all().delete()
        models.Organisation.objects.all().delete()

    def _setup_users(self):
        self.comm_method = models.CommMethod.objects.create(comm_method='Email')
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.admin = User.objects.create_superuser(username='admin', email=EMAIL, password='secret')
        self.topic = models.SupportTopic.objects.all()[0]
        pri_lev = models.DevPriLev.objects.get(pk=1)
        support_type = models.DevSupportType.objects.get(pk=1)

        user = KHNPUser(self.user)
        self.person = user.get_person()
        self.person.displayname = 'John Doe'
        self.person.save()
        self.company = models.Organisation.objects.register(self.person,
                                                defaults={'is_supplier': False})
        self.company.org.displayname = 'Company 1'
        self.company.org.save()

        initialize_customer_diagnostic(self.company)
        diag = self.company.get_diagnostic()
        diag.set_state('approved')
        diag.customerdevpri_set.create(topic=self.topic,
                                       pri_lev=pri_lev,
                                       support_type=support_type,
                                       value=1000)

    def test_mentor_crud(self):
        self._setup_users()
        mentoring_url = reverse('customer_dashboard', args=['mentoring'])

        self.client.login(username='testuser', password='secret')
        url = reverse('customer_become_a_mentor')
        res = self.client.get(url)
        self.assertTemplateUsed(res, 'customer/become_a_mentor_pre.html')

        res = self.client.post(url, {'disclaimer': '1'}, follow=True)
        self.assertTemplateUsed(res, 'customer/become_a_mentor.html')

        data = dict(
            mentor=self.company.pk,
            description='Mentor description',
            mentor_job_title='Director',
            region=2,  # primary key
            topics=[8, 10]
        )
        url = reverse('customer_become_a_mentor2')
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 302)
        self.assertEqual(models.Mentor.objects.count(), 1)

        # list mentoring hosted by organisation
        res = self.client.get(mentoring_url)
        mentor = models.Mentor.objects.all()[0]
        self.assertContains(res, 'Edit my mentoring details')
        self.assertContains(res, 'You don\'t have any mentees')
        topics = sorted([x.pk for x in mentor.topics.all()])
        regions = sorted([x.pk for x in mentor.region.all()])
        self.assertEqual(regions, [2])
        self.assertEqual(topics, [8, 10])

        # edit form
        url = reverse('mentor_edit', args=[mentor.pk])
        data = dict(
            description='Mentor description2',
            mentor_job_title='Director',
            region=[3, 4],  # primary key
            topics=['11', '21'],
        )
        res = self.client.post(url, data)
        mentor = models.Mentor.objects.all()[0]
        topics = sorted([x.pk for x in mentor.topics.all()])
        regions = sorted([x.pk for x in mentor.region.all()])
        self.assertEqual(mentor.description, 'Mentor description2')
        self.assertEqual(regions, [3, 4])
        self.assertEqual(topics, [11, 21])

    def _setup_mentoring(self):
        self.mentor = models.Mentor.objects.create(
            mentor=self.person,
            mentor_org=self.company,
            description='Test mentor',
            )
        self.mentor.region.add(models.Region.objects.get(pk=2))
        self.mentor.topics.add(models.SupportTopic.objects.get(pk=8))
        self.mentor.topics.add(models.SupportTopic.objects.get(pk=10))

    def test_mentee_rejected(self):
        self._setup_users()
        self._setup_mentoring()

        url = reverse('connectsgroup')
        self.client.login(username='testuser', password='secret')
        # I want to be a mentee!
        res = self.client.get(url)

        profile_url = reverse('mentor_detail', args=(self.mentor.pk, ))
        touch_url = reverse('mentor_get_in_touch', args=(self.mentor.pk, ))
        self.assertContains(res, "<h3>MENTOR: John Doe</h3>", html=True)
        self.assertContains(res, profile_url)
        self.assertContains(res, touch_url)

        res = self.client.post(reverse('mentor_get_in_touch', args=[self.mentor.pk]), {'contact_msg': 'Hello'})
        self.assertEqual(res.status_code, 302)
        # can't get in touch again
        res = self.client.get(url)
        self.assertContains(res, '<h3>MENTOR: John Doe <span class="flag contacted">Contacted</span></h3>', html=True)
        self.assertContains(res, profile_url)
        self.assertNotContains(res, touch_url)

        mentee = self.mentor.mentee_set.all()[0]
        accept_url = reverse('mentee_accept', args=(mentee.pk, ))
        reject_url = reverse('mentee_reject', args=(mentee.pk, ))
        res = self.client.get(reverse('customer_dashboard', args=['mentoring']))
        self.assertContains(res, "Company 1")
        self.assertContains(res, accept_url)
        self.assertContains(res, reject_url)

        res = self.client.post(reverse('mentee_reject', args=[mentee.pk]))
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        self.assertFalse(mentee.is_accepted)

        # I can try to get in touch again
        res = self.client.get(url)
        self.assertContains(res, touch_url)

    def test_mentee_accepted(self):
        self._setup_users()
        self._setup_mentoring()

        self.client.login(username='testuser', password='secret')
        res = self.client.post(reverse('mentor_get_in_touch', args=[self.mentor.pk]), {'contact_msg': 'Hello'})
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        res = self.client.post(reverse('mentee_accept', args=[mentee.pk]))
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        self.assertTrue(mentee.is_accepted)

        profile_url = reverse('mentor_detail', args=(self.mentor.pk, ))
        # I can't get in touch again, because I'm accepted
        res = self.client.get(reverse('connectsgroup'))
        self.assertContains(res, profile_url)

    def test_mentee_discards(self):
        self._setup_users()
        self._setup_mentoring()

        self.client.login(username='testuser', password='secret')
        res = self.client.post(reverse('mentor_get_in_touch', args=[self.mentor.pk]), {'contact_msg': 'Hello'})
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        res = self.client.post(reverse('mentee_discard', args=[mentee.pk]))
        self.assertEqual(res.status_code, 302)
        self.assertEqual(self.mentor.mentee_set.count(), 0)

        profile_url = reverse('mentor_detail', args=(self.mentor.pk, ))
        touch_url = reverse('mentor_get_in_touch', args=(self.mentor.pk, ))

        # I can get in touch again, because mentee does not exist
        res = self.client.get(reverse('connectsgroup'))
        self.assertContains(res, profile_url)
        self.assertContains(res, touch_url)

    def test_payment_no_sponsorship(self):
        self._setup_users()
        self._setup_mentoring()

        self.client.login(username='testuser', password='secret')
        res = self.client.post(reverse('mentor_get_in_touch', args=[self.mentor.pk]), {'contact_msg': 'Hello'})
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        res = self.client.post(reverse('mentee_accept', args=[mentee.pk]))
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        self.assertTrue(mentee.is_accepted)
        mentee.date1 = datetime(2013, 1, 1).date()
        mentee.date2 = datetime(2013, 1, 2).date()
        mentee.date3 = datetime(2013, 1, 3).date()
        mentee.date4 = datetime(2013, 1, 4).date()
        mentee.amount = 100
        mentee.bursary = False
        mentee.save()

        # complete the visit
        res = self.client.get(reverse('customer_dashboard', args=['mentoring']))
        self.assertContains(res, '>Complete</a>')
        self.assertEqual(len(mail.outbox), 4)
        res = self.client.post(reverse('mentee_complete', args=[mentee.pk]))
        # no emails because no sponsorship
        self.assertEqual(len(mail.outbox), 4)

    def test_payment_with_sponsorship(self):
        self._setup_users()
        self._setup_mentoring()

        self.client.login(username='testuser', password='secret')
        res = self.client.post(reverse('mentor_get_in_touch', args=[self.mentor.pk]), {'contact_msg': 'Hello'})
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        res = self.client.post(reverse('mentee_accept', args=[mentee.pk]))
        self.assertEqual(res.status_code, 302)
        mentee = self.mentor.mentee_set.all()[0]
        self.assertTrue(mentee.is_accepted)
        mentee.date1 = datetime(2013, 1, 1).date()
        mentee.date2 = datetime(2013, 1, 2).date()
        mentee.date3 = datetime(2013, 1, 3).date()
        mentee.date4 = datetime(2013, 1, 4).date()
        mentee.amount = 100
        mentee.bursary = True
        mentee.save()

        # complete the visit
        res = self.client.get(reverse('customer_dashboard', args=['mentoring']))
        self.assertContains(res, '>Complete</a>')
        self.assertEqual(len(mail.outbox), 4)
        res = self.client.post(reverse('mentee_complete', args=[mentee.pk]))
        self.assertEqual(len(mail.outbox), 6)
