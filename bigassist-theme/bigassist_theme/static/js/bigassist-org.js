//var charity_url = 'http://opencharities.org/charities/';
$(document).ready(function(){

$('#org_type_detail').hide();

$('input[name="org_type"]').change(function() {
  $('#id_charityno, #id_companyno, #id_detail_3, #id_detail').attr('disabled', true);
  $('#id_charityno, #id_companyno, #id_detail_3, #id_detail').each(function(){
    $(this).closest(".form-field").removeClass('required');
  })
  $('[data-idref]').hide();
  $('#org_type_detail').hide();
  var val = $(this).val();
  switch (val)
  {
    case "1": 
      $('#org_type_detail').hide();
      $('#id_charityno').attr('disabled', null); 
      // $('#id_charityno').closest(".form-field").addClass('required');
      $('#id_companyno').attr('disabled', null); 
      $('[data-idref="id_charityno"]').show();
      $('[data-idref="id_companyno"]').show();
      $('[data-idref="id_detail"]').hide();
      $('#org_type_detail').show();
      break;
    case "2": 
      $('#org_type_detail').hide();
      $('#id_companyno').attr('disabled', null); 
      $('#id_companyno').closest(".form-field").addClass('required');
      $('#id_charityno').attr('disabled', null); 
      $('[data-idref="id_charityno"]').show();
      $('[data-idref="id_companyno"]').show();
      $('[data-idref="id_detail"]').hide();
      $('#org_type_detail').show();
      break;
    case "3": 
      $('#org_type_detail').hide();
      $('#org_type_detail').hide(400, function(){
        $('[data-idref]').hide();
      });
      break;
    case "4": 
      $('#org_type_detail').hide();
      $('#id_detail').attr('disabled', null); 
      $('#id_detail').closest(".form-field").addClass('required');
      $('[data-idref="id_detail"]').show();
      $('#org_type_detail').show();
      break;
  }
});

$('input[name="org_type"]:checked').change();

$('#id_companyno').blur(function() {
  var val = $(this).val();

  //var url = charity_url+val+'.json';
  var url = "/lookup/company/" + val;
  $.getJSON(url, function(data, textStatus) {
    $('#id_registered_address').val(data['results']['company']['registered_address_in_full']);
    $('#id_org_name').val(data['results']['company']['name']);
  });

  var org_type = 2;
  var check_url = "/check_company/";
  var data = {'org_type': org_type, 'companyno': val};
  var response = $.get(check_url, data, function(data) {
    if (data) {
      $('#check_company').html(data);
      $('#check_company .dismiss').click(function(e) {
        e.preventDefault();
        $('#check_company').hide();
      });
      $('#check_company').show();
    }
  });

});
$('#id_charityno').blur(function() {
  var val = $(this).val();

  //var url = charity_url+val+'.json';
  var url = "/lookup/charity/" + val;
  $.getJSON(url, function(data, textStatus) {
    var address = "";
    address += data['charity']['address']['street_address'] + "\n";
    address += data['charity']['address']['locality'];

    $('#id_registered_address').val(address);
    $('#id_registered_postcode').val(data['charity']['address']['postal_code']);


    $('#id_org_name').val(data['charity']['title']);
    $('#id_contact_name').val(data['charity']['contact_name']);
    $('#id_tel_general').val(data['charity']['telephone']);
    $('#id_website').val(data['charity']['website']);
  });
  var org_type = 1;
  var check_url = "/check_company/";
  var data = {'org_type': org_type, 'companyno': val};
  var response = $.get(check_url, data, function(data) {
    if (data) {
      $('#check_company').html(data);
      $('#check_company .dismiss').click(function(e) {
        e.preventDefault();
        $('#check_company').hide();
      });
      $('#check_company').show();
    }
  });


});
$('#id_ncvomem').blur(function() {
  var val = $(this).val();

  var check_url = "/check_company/";
  var data = {'org_type': -1, 'companyno': val};
  var response = $.get(check_url, data, function(data) {
    if (data) {
      $('#check_company').html(data);
      $('#check_company .dismiss').click(function(e) {
        e.preventDefault();
        $('#check_company').hide();
      });
      $('#check_company').show();
    }
  });


});
});
