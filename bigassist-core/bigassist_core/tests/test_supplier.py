from django.core.files.base import ContentFile
from django.core import mail
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from bigassist_core.user import KHNPUser
from bigassist_core.models import Organisation, CustomerProject, CommMethod, \
                                  OrgRegion
from bigassist_core.views.supplier import initialize_supplier_approval
from bigassist_core.views.customer import initialize_customer_diagnostic
from .base import TestCase, XMLTestHelper

EMAIL = 'test@khnp.org'
CUST_EMAIL = 'test_cust@khnp.org'


class SupplierTest(TestCase):

    fixtures = ['types', 'mails', 'xforms']

    def setUp(self):
        super(SupplierTest, self).setUp()
        # create a Django user
        self.user = User.objects.create_user(username='testuser', email=EMAIL, password='secret')
        self.user = KHNPUser(self.user)
        self.person = self.user.get_person()
        # we need to create organization before user can fill eligibility form
        Organisation.objects.register(self.person,
                                      defaults={'is_supplier': True,
                                                'org_type_id': 2})

        self.cust = User.objects.create_user(username='testuser_cust', email=CUST_EMAIL, password='secret')
        self.cust = KHNPUser(self.cust, require_authenticated=False)
        self.cust_person = self.cust.get_person()
        # we need to create organization before user can fill eligibility form
        org = Organisation.objects.register(self.cust_person,
                                      defaults={'is_supplier': False,
                                                'org_type_id': 2})
        org.org.displayname = "Customer company"
        org.org.save()

    def tearDown(self):
        Organisation.objects.all().delete()

    def test_dashboard_assessment(self):
        self.client.login(username='testuser', password='secret')

        url = reverse('home')
        ass_url = reverse('supplier_assessment')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Start your application')
        self.assertContains(response, ass_url)

        company = self.user.get_company()
        initialize_supplier_approval(company)
        diag = company.get_diagnostic()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Continue your application')
        self.assertContains(response, ass_url)

        diag.set_state('submitted')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless('Your application has been submitted and we will email you' in response.content)
        self.assertEqual(diag.state, 'submitted')

        self.assertNotContains(response, "Before we can pay you")

        diag.set_state('approved')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless('Your application has been approved.' in response.content)
        self.assertEqual(diag.state, 'approved')

        self.assertContains(response, "Before we can pay you")

        diag.set_state('rejected')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless('Your application has not been approved. Here is the feedback' in response.content)
        self.assertEqual(diag.state, 'rejected')

    def _setup_project(self):
        company = self.user.get_company()
        initialize_supplier_approval(company)
        diag = company.get_diagnostic()
        diag.set_state('approved')

        cust_company = self.cust.get_company()
        initialize_customer_diagnostic(cust_company)

        project = CustomerProject.objects.create(cust=cust_company,
                                       suppl=company,
                                       comm_method=CommMethod.objects.all()[0],
                                       price=200,
                                       voucher_value=200)
        return project

    def test_dashboard_contacts(self):
        self.client.login(username='testuser', password='secret')

        self._setup_project()

        url = reverse('supplier_dashboard', args=('contacts', ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, "Customer company")

    def test_preproject_detail(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()

        url = reverse('supplier_preproject_detail_view', args=(project.pk, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        make_url = reverse('supplier_project_make_view', args=(project.pk, ))
        self.assertContains(response, make_url)

    def test_capability_report_view(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()

        url = reverse('supplier_customer_capability_report', args=(project.pk, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<h2>Organisational overview</h2>', html=True)

    def test_project_make(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()

        url = reverse('supplier_project_make_view', args=(project.pk, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('price' in response.context['form'].errors)
        self.assertTrue('description' in response.context['form'].errors)

        data = {
            'price': 200,
            'voucher_value': 200,
            'description': "Some description",
            'startdate': "1 Nov 2050",
            'enddate': "1 Dec 2050",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)

        project = CustomerProject.objects.get(pk=project.pk)
        self.assertEqual(project.state, "proposed")

        self.assertEqual(len(mail.outbox), 2)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_dismiss_project(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()

        url = reverse("supplier_project_dismiss_view", args=(project.pk, ))
        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        project = CustomerProject.objects.get(pk=project.pk)
        self.assertEqual(project.state, "dismissed")
        self.assertFalse(project.dismissed_by_cust)

        url = reverse('supplier_dashboard', args=('contacts', ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertNotContains(response, "Customer company")
        self.failUnless(len(mail.outbox), 1)

    def test_dashboard_projects(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()
        project.set_state("accepted")

        url = reverse("supplier_dashboard", args=("projects", ))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Customer company")

        detail_url = reverse("supplier_project_detail_view", args=(project.pk, ))
        self.assertContains(response, detail_url)
        complete_url = reverse("supplier_project_complete_view", args=(project.pk, ))
        self.assertContains(response, complete_url)

    def test_project_detail(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()
        project.set_state("accepted")

        url = reverse("supplier_project_detail_view", args=(project.pk, ))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Customer company")

    def test_project_mark_complete(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()
        project.set_state("accepted")

        url = reverse("supplier_project_complete_view", args=(project.pk, ))
        dash_url = reverse("supplier_dashboard", args=("projects", ))

        response = self.client.get(dash_url)
        self.assertContains(response, "Customer company")
        self.assertContains(response, url)

        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        project = CustomerProject.objects.get(pk=project.pk)
        self.assertEqual(project.state, "mark_completed")

        self.assertEqual(len(mail.outbox), 2)

        self.assertEqual(project.payment_set.count(), 1)
        payment = project.payment_set.get()
        self.assertEqual(payment.payee_org, project.suppl)
        self.assertEqual(payment.amount, project.price)

        dash_url = reverse("supplier_dashboard", args=("projects", ))
        response = self.client.get(dash_url)
        self.assertContains(response, "Customer company")
        self.assertNotContains(response, url)

    def test_dashboard_invoices(self):
        self.client.login(username='testuser', password='secret')
        project = self._setup_project()
        project.set_state("completed")

        url = reverse("supplier_dashboard", args=("invoices", ))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Customer company")

        detail_url = reverse("supplier_project_detail_view", args=(project.pk, ))
        self.assertContains(response, detail_url)
        complete_url = reverse("supplier_project_complete_view", args=(project.pk, ))
        self.assertNotContains(response, complete_url)

    def test_approval_reject_resubmit(self):
        self.client.login(username='testuser', password='secret')

        company = self.user.get_company()
        initialize_supplier_approval(company)
        diag = company.get_diagnostic()

        # add a file to the assessment
        diag.ac_file.save('dummy.txt', ContentFile('file content'))
        diag.ac_file_type = 'text/plain'

        diag.set_state('in_progress')
        url = reverse('supplier_resubmit_assessment')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)

        diag.set_state('rejected')
        xml = XMLTestHelper(diag.supplier_questions)
        # change XML data
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[3]',
                     '4 5 6')
        save_url = reverse('supplier_assessment_save')
        response = self.client.post(save_url, data=xml.tostring(), content_type='text/xml')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        new_diag = company.get_diagnostic()
        self.failIfEqual(diag.pk, new_diag.pk)
        self.assertEqual(new_diag.prev_appr, diag)
        self.assertEqual(diag.state, 'rejected')
        self.assertEqual(new_diag.state, 'in_progress')
        self.assertEqual(new_diag.ac_file_type, diag.ac_file_type)
        self.assertNotEqual(diag.ac_file.path, new_diag.ac_file.path)
        self.failUnless(new_diag.ac_file.path)
        self.assertRedirects(response, reverse('supplier_dashboard', args=['assessment']))
        # cleanup the filesystem
        diag.ac_file.delete()
        new_diag.ac_file.delete()

        url = reverse('home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'Continue reapplication')
        self.assertContains(response, reverse('supplier_assessment'))

    def test_profile_page(self):
        # prepare data
        url = reverse('supplier_assessment')
        save_url = reverse('supplier_assessment_save')

        self.client.login(username='testuser', password='secret')
        self.client.get(url)  # just initialize the diagnostic. Tested in test_approval_init

        company = self.user.get_company()
        company.org.biography = 'Very long <strong>description</strong> of the supplier'
        company.org.save()
        approval = company.get_diagnostic()
        xml = XMLTestHelper(approval.supplier_questions)
        # change XML data
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_people',
                     'Mainly in-house staff and occasionally with associates')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:short_description',
                     'Short description of the supplier')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:long_description',
                     'Very long <strong>description</strong> stored in approval table')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_regions',
                     'NE NW Yorks')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[1]',
                     '1 3')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[3]',
                     '4 5 6')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:finance/tm:vat_number',
                     'GB1234679')
        response = self.client.post(save_url, data=xml.tostring(), content_type='text/xml')
        self.assertEqual(response.status_code, 302)
        # test result - anonymous can view this
        self.client.logout()
        url = reverse('supplier_full_profile', args=[company.pk])
        response = self.client.get(url)
        self.assertContains(response, 'Very long <strong>description</strong> stored in approval table')

        self.assertContains(response, '<h2>Regions</h2>')
        self.assertContains(response, '<li>Yorkshire and The Humber</li>')
        self.assertContains(response, '<li>North East</li>')
        self.assertContains(response, '<li>North West</li>')

        self.assertNotContains(response, '<h2>We can help you with</h2>')

        self.assertContains(response, '<h2>Who delivers support?</h2>')
        self.assertContains(response, '<li>Mainly in-house staff and occasionally with associates</li>')

        company = self.user.get_company()
        self.assertEqual(company.vat_no, 'GB1234679')
        # approve topics
        approval = company.get_diagnostic()
        self.failUnless(approval.long_desc, 'Very long <strong>description</strong> stored in approval table')
        approval.approvaltopic_set.all().update(is_approved=True)
        approval.set_state('approved')
        response = self.client.get(url)
        self.assertContains(response, '<h2>We can help you with</h2>')
        self.assertContains(response, '<li>Organisational strategy</li>')
        self.assertNotContains(response, '<li>Planning, assessing and communicating impact</li>')
        self.assertContains(response, '<li>Business planning</li>')
        self.assertContains(response, '<li>Leading change</li>')
        self.assertContains(response, '<li>Income strategy and new business models</li>')
        self.assertContains(response, '<li>Cost efficiency and savings</li>')


class InviteSupplierUserTest(TestCase):
    """ simplified version of the same test in test_customer.
        Just test it is displayed correctly """

    fixtures = ['types', 'mails']

    def setUp(self):
        super(InviteSupplierUserTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        # we need to create organization before user can fill eligibility form
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': True,
                                                'org_type_id': 2})

    def test_users_page(self):
        self.client.login(username='testuser', password='secret')

        url = reverse('org_users')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.failUnless(('&lt;%s&gt; (you)' % EMAIL) in response.content)
        self.failUnless('Invoices' in response.content)


class SupplierXFormsTest(TestCase):

    fixtures = ['types', 'xforms', 'mails']

    def setUp(self):
        super(SupplierXFormsTest, self).setUp()
        self.user = User.objects.create_user('testuser', EMAIL, password='secret')
        user = KHNPUser(self.user)
        self.person = user.get_person()
        # we need to create organization before user can fill eligibility form
        self.company = Organisation.objects.register(self.person,
                                      defaults={'is_supplier': True,
                                                'org_type_id': 3,
                                                'detail': '007'})

    def test_approval_init(self):
        url = reverse('supplier_assessment')
        self.failIf(self.company.get_diagnostic())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Please register or log in.')

        self.client.login(username='testuser', password='secret')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'].split(';')[0], 'text/xml')
        # check there is an approval
        approval = self.company.get_diagnostic()
        self.failUnless(self.company.get_diagnostic())
        # check there is a file-upload iframe
        uurl = reverse('xforms_fileupload', args=['S', 'ac', approval.pk])
        self.assertContains(response, 'src="%s"' % uurl)

        # check the XML
        xml = XMLTestHelper(approval.supplier_questions)
        self.assertEqual(xml.get_attribute('/tm:assist_supplier', 'appl_no'), str(approval.pk))
        self.assertEqual(xml.get_attribute('/tm:assist_supplier', 'submtd'), '')

        # check readonly form
        response = self.client.get(url + '?readonly=1')
        self.assertNotContains(response, 'src="%s"' % uurl)
        durl = reverse('xforms_filedownload', args=['S', 'ac', approval.pk])
        self.assertContains(response, 'src="%s"' % durl)

    def test_update_company(self):
        url = reverse('supplier_assessment')
        save_url = reverse('supplier_assessment_save')

        self.client.login(username='testuser', password='secret')
        self.client.get(url)  # just initialize the diagnostic. Tested in test_approval_init

        approval = self.company.get_diagnostic()
        xml = XMLTestHelper(approval.supplier_questions)
        # change XML data
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_people',
                     'Mainly in-house staff and occasionally with associates')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:short_description',
                     'Short description of the supplier')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:promotion/tm:long_description',
                     'Very long description of the supplier')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_regions',
                     'NE NW Yorks')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[1]',
                     '1 3')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[3]',
                     '4 5 6')
        response = self.client.post(save_url, data=xml.tostring(), content_type='text/xml')
        self.assertEqual(response.status_code, 302)
        company = KHNPUser(self.user).get_company()
        approval = company.get_diagnostic()
        self.assertEqual(approval.state, 'in_progress')
        self.assertEqual(approval.delivery_people, 'Mainly in-house staff and occasionally with associates')

        self.assertEqual(approval.short_desc, 'Short description of the supplier')
        self.assertEqual(approval.long_desc, 'Very long description of the supplier')
        OrgRegion.objects.get(org=self.company, region='NE')
        OrgRegion.objects.get(org=self.company, region='NW')
        OrgRegion.objects.get(org=self.company, region='Yorks')
        with self.assertRaises(OrgRegion.DoesNotExist):
            OrgRegion.objects.get(org=self.company, region='EW')
        self.failUnless(approval.approvaltopic_set.count(), 5)
        topics = [x[0] for x in approval.approvaltopic_set.values_list('topic')]
        self.failUnless(1 in topics)
        self.failUnless(3 in topics)
        self.failUnless(4 in topics)
        self.failUnless(5 in topics)
        self.failUnless(6 in topics)

        # test another submission but slightly different data
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:delivery_regions',
                     'NE EW NW')
        xml.set_text('/tm:assist_supplier/tm:supplier_application/tm:services/tm:expertise/tm:category[1]',
                     '2')
        response = self.client.post(save_url, data=xml.tostring(), content_type='text/xml')
        self.assertEqual(response.status_code, 302)
        company = KHNPUser(self.user).get_company()
        approval = company.get_diagnostic()
        OrgRegion.objects.get(org=self.company, region='NE')
        OrgRegion.objects.get(org=self.company, region='EW')
        OrgRegion.objects.get(org=self.company, region='NW')
        with self.assertRaises(OrgRegion.DoesNotExist):
            OrgRegion.objects.get(org=self.company, region='Yorks')

        self.failUnless(approval.approvaltopic_set.count(), 4)
        topics = [x[0] for x in approval.approvaltopic_set.values_list('topic')]
        self.failUnless(2 in topics)
        self.failUnless(4 in topics)
        self.failUnless(5 in topics)
        self.failUnless(6 in topics)

    def test_model(self):
        # check if tm:organisation contains correct data in model.xml
        org = self.company.org
        org.displayname = 'John Doe Company'
        org.maincontactname = 'John Doe'
        org.save()
        org.set_contact('website', 'http://john.com')
        org.set_contact('tel_general', '123456798')
        org.set_contact('tel_direct', '003456798')
        org.set_contact('email_general', 'corp@doe.com')
        org.set_contact('email_direct', 'john@doe.com')
        org.set_address(2, **{'address': 'John Avenue', 'postcode': 'SE1 111'})
        org.set_address(5, **{'address': 'John Street', 'postcode': 'SE1 112'})

        self.client.login(username='testuser', password='secret')
        url = reverse('supplier_assessment_xml_model')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'].split(';')[0], 'text/xml')
        xml = XMLTestHelper(response.content)
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:org_name'), 'John Doe Company')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:org_type_id'), '3')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:detail'), '007')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:address'), 'John Avenue')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:postcode'), 'SE1 111')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:reg_address'), 'John Street')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:reg_postcode'), 'SE1 112')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:website'), 'http://john.com')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:tel_general'), '123456798')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:contact_tel'), '003456798')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:email_general'), 'corp@doe.com')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:contact_email'), 'john@doe.com')
        self.assertEqual(xml.get_text('/tm:assist_supplier/tm:organisation/tm:contact_name'), 'John Doe')

    def test_meet(self):
        response = self.client.get(reverse('supplier_dont_meet'))
        self.assertContains(response, "Start the application")
        self.assertContains(response, reverse('supplier_register'))


